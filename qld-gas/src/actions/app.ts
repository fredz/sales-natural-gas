import { Action } from 'redux-actions';
import { IAppState, IPageSettings } from '../state/IAppState';

export const PAGE_LOAD = 'App/PAGE_LOAD';
export type PAGE_LOAD = {
  pageCode: string;
  settings: IPageSettings;
};
export function setPage(pageCode: string, settings: IPageSettings = null) {
  return (dispatch, getState: () => IAppState) => {
    dispatch({
      type: PAGE_LOAD,
      payload: {
        pageCode,
        settings,
      }
    } as Action<PAGE_LOAD>);
  };
}