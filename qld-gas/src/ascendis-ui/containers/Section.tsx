import * as React from 'react';

interface ISectionProps {
  header: string;
}

export default class Section extends React.Component<ISectionProps, any> {
  constructor(props) {
    super(props);
  }

  render() {
    return(
      <fieldset style={{marginTop:'15px'}}>
        <legend>{this.props.header}</legend>
        {this.props.children}
      </fieldset>
    );
  }
}