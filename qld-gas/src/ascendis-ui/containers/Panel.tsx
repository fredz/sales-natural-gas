import * as React from 'react';

interface IPanelProps {
  title: string;
}

export default class Panel extends React.Component<IPanelProps, any> {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="panel panel-info" style={{padding: '10px'}}>
        <div className="panel-heading">
          <div className="panel-title">
            <span>{this.props.title}</span>
          </div>
        </div>
        {this.props.children}
      </div>
    );
  }
}