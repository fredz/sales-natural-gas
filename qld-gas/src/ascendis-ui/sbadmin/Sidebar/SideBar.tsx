import * as React from 'react';

class SideBanner extends React.Component<any, any> {
  constructor(props) {
    super(props);

    this.state = {
      uiElementsCollapsed: false,
    };
  }

  render() {
    
    return (
      <div className="navbar-default sidebar" style={{ marginTop: '0px' }} role="navigation">
        <div className="sidebar-nav navbar-collapse collapse">
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default SideBanner;
