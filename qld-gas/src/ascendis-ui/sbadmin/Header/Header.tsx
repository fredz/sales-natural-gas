import * as React from 'react';

import {
  NavbarBrand,
} from 'react-bootstrap';

export interface IHeaderProps {
  OnHamburgerClick: Function;
  Branding: any;
  RightMenu: any;
}

class Header extends React.Component<IHeaderProps, any> {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div id="wrapper" className="content" style={{height:'80px', background: 'lightgrey', borderBottom: 'solid'}}>
        <div style={ {margin: 0} }>
          <NavbarBrand>
            <span>{this.props.Branding}</span>
            <button 
              type="button" 
              className="navbar-toggle" 
              onClick={() => this.props.OnHamburgerClick} 
              style={{position: 'absolute', right: 0, top: 0, display: 'hidden'}}>
              <span className="sr-only">Toggle navigation</span>
              <span className="icon-bar"/>
              <span className="icon-bar"/>
              <span className="icon-bar"/>
            </button>
          </NavbarBrand>
          <ul className="nav navbar-top-links navbar-right">
            {this.props.RightMenu}
          </ul>
      </div>
    </div>
    );
  }
}

export default Header;