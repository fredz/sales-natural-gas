import * as React from 'react';
import * as Datetime from 'react-datetime';

interface IInputGroupProps {
  label: string;
  placeholder: string;
  containerStyle?: Object;
  inputStyle?: Object;
  inputName: string;
  type?: string;
  onValueChange?: Function;
  disabled?: boolean;
  value?: string;
  maxLength?: number; 
  dataValidation?: string;
  error?: boolean;
  errorMsg?: string;   
}

interface IInputGroupState {

}

export default class InputGroup extends React.Component<IInputGroupProps, IInputGroupState> {
  constructor(props) {
    super(props);
    
    this.handleChange = this.handleChange.bind(this);
    this.clearDatePicker = this.clearDatePicker.bind(this);      
  }

  public focus() {
    
  }

  handleChange(event) {
    
    if (!this.props.onValueChange) {
      return;
    }    

    if (this.props.type === 'datepicker') {
      this.props.onValueChange(event.format('MM/DD/YYYY'));

    } else if ( this.props.type === 'checkbox' ) {
      
      let checkboxValue = (event.target.value === 'true') ? false : true;
      this.props.onValueChange(checkboxValue); 

    } else {

      this.props.onValueChange(event.target.value); 

    }    
  }

  clearDatePicker() {
    this.props.onValueChange('');
  }

  render() {
    const { label, placeholder, inputName, disabled, value, maxLength, dataValidation, error, errorMsg } = this.props;

    let { containerStyle, inputStyle, type } = this.props;

    let hasError = '';
    if (error === true) {
      hasError = 'has-error';
    }     

    if (!containerStyle) {
      containerStyle = {};
    }

    if (!inputStyle) {
      inputStyle = {width: '100%'};
    }

    if (!type) {
      type = 'text';    
    }

    if (type === 'datepicker') { 

      let inputDisabled = disabled;
      let valueDatepicker = null;

      // validate empty date
      if ( value && value.length > 0 ) {
        valueDatepicker = new Date(value);
      }
      if ( !value || value.length === 0) {
        valueDatepicker = '';
      }

      return ( 
        <div className={hasError}>
          <div style={containerStyle} className="input-group">
            <span className="input-group-addon">{label}</span>
            <Datetime            
              value={valueDatepicker}               
              dateFormat={'D MMM YY'} 
              timeFormat={''}  
              closeOnSelect={true} 
              inputProps={{disabled:inputDisabled, readOnly:inputDisabled}}             
              onChange={this.handleChange}
              data-validation={dataValidation}/>
            <span className="input-group-addon btn-info" 
              style={{cursor: 'pointer'}} onClick={this.clearDatePicker}>clear</span>
          </div>
          <span className="help-block">{errorMsg}</span>
        </div>
       );
    }

    if (type === 'checkbox') { 

      return ( 
        <div className={hasError}>
          <div style={containerStyle} className="input-group">
            <span className="input-group-addon">{label}</span>
            <input 
              className="form-control"
              type={type} 
              name={inputName}             
              onChange={this.handleChange}
              disabled={disabled ? true : false} 
              value={value} 
              checked={JSON.parse(value)}
            />
          </div>
          <span className="help-block">{errorMsg}</span>
        </div>
       );
    }    

    if (type === 'textarea') { 

      return (
        <div className={hasError}>
          <div style={containerStyle} className="input-group">
            <span className="input-group-addon">{label}</span>
            <textarea 
              className="form-control" 
              name={inputName} 
              onChange={this.handleChange}
              value={value} 
              rows={3}
              data-validation={dataValidation}/>
          </div>
          <span className="help-block">{errorMsg}</span>
        </div> 
       );
    }
    
    return(
      <div className={hasError}>
        <div style={containerStyle} className="input-group">
          <span className="input-group-addon">{label}</span>
          <input 
            className="form-control"
            type={type} 
            name={inputName} 
            placeholder={placeholder}
            onChange={this.handleChange}
            disabled={disabled ? true : false}
            value={value}
            maxLength={maxLength}
            data-validation={dataValidation}
          />          
        </div>
        <span className="help-block">{errorMsg}</span>        
      </div>
      
    );
  }
}
