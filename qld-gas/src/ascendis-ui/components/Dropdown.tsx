import * as React from 'react';

import * as ReactSelect from 'react-select';

interface IDropdownItem {
  value: string;
  label: string;
}

interface IDropdownProps {
  list: Array<IDropdownItem>;
  label: string;
  inputName: string;
  value?: string;
  onValueChange?: Function;
  onInputChange?: Function;
  error?: boolean;
  errorMsg?: string;
  clearable?: boolean;  
  className?: string;  
}

interface IDropdownState {

}

export default class InputGroup extends React.Component<IDropdownProps, IDropdownState> {
  constructor(props) {
    super(props);
    
    this.handleChange = this.handleChange.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleChange(value) {
    if (!this.props.onValueChange) {
      return;
    }

    this.props.onValueChange(value ? value : {value: null});
  }

  handleInputChange(value) {
    if (!this.props.onInputChange) {
      return;
    }
    
    this.props.onInputChange(value ? value : {value: null});
  }

  render() {
    const { list, value, label, error, errorMsg, className } = this.props;    
    const clearable = this.props.clearable === undefined ? true : this.props.clearable;

    let props = {};
    if (value) {
      props = {
        value,
        className
      };
    }

    let hasError = '';
    if (error === true) {
      hasError = 'has-error';
    }
    
    const hiddenLabel = label == null ? 'hidden' : '';

    return(
      <div className={hasError}>
        <div className="input-group">
          <span className={'input-group-addon ' + hiddenLabel} >{label}</span>
          <ReactSelect options={list} searchable={true} {...props} 
            onChange={this.handleChange} clearable={clearable} onInputChange={this.handleInputChange}/>
        </div>
        <span className="help-block">{errorMsg}</span>        
      </div>      
    );
  }
}
