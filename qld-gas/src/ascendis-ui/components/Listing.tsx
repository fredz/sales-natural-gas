import * as React from 'react';

export interface IListingProps {
  title: string;
}

class Listing extends React.Component<IListingProps, any> {
  constructor(props) {
    super(props);
  }

  render() {
    const { title } = this.props;
    return (
      <nav className="navbar navbar-default">
        <div className="container-fluid">
          <div className="navbar-header">
            <span className="navbar-brand">{title}</span>
          </div>
          
          <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul className="nav navbar-nav navbar-left">
              <li className="active"><a href="#">List <span className="sr-only">(current)</span></a></li>
            </ul>
            
            <div className="navbar-form navbar-left">
              <div className="form-group">
                <input type="text" className="form-control" placeholder="Search (visible in List Mode)"/>
              </div>
              <a className="btn btn-default">Search</a>
            </div>
            
            <ul className="nav navbar-nav navbar-right">
              <li><a href="#">Add Record</a></li>
              <li className="dropdown">
                <a className="dropdown-toggle" 
                  data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                  Addl Page Related Opt <span className="caret"/>
                </a>
                <ul className="dropdown-menu">
                  <li><a href="#">Additional 1</a></li>
                  <li><a href="#">Additional 3</a></li>
                  <li><a href="#">Additional 2</a></li>
                  <li role="separator" className="divider"/>
                  <li><a href="#">Additional 4</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}

export default Listing;
