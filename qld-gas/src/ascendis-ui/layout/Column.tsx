import * as React from 'react';

interface IColumnProps {
  column: string;
}

export default class Column extends React.Component<IColumnProps, any> {
  constructor(props) {
    super(props);
  }

  render() {
    return(
      <section className={`col-md-${this.props.column}`}>
      {this.props.children}
      </section>
    );
  }
}