import * as React from 'react';

export default class Row extends React.Component<any, any> {
  constructor(props) {
    super(props);
  }

  render() {
    return(
      <div className="row">
        {this.props.children}
      </div>
    );
  }
}