export const fetchJson = (url: string, type: string = 'POST', data: FormData = null) => {
  const promise = new Promise<JSON>((resolve, reject) => {
    const request = new XMLHttpRequest();
    request.open(type, url, true);
    
    request.onreadystatechange = () => {
      if (request.readyState === 4 && request.status === 200) {
        try {
          resolve(JSON.parse(request.responseText));
        } catch (e) {
          reject(e);
        }
      }
    };

    request.send(data);
  });

  return promise;
};