import { parsing } from '../parsing';

describe('Parsing Utils', () => {
  describe('cleanHTML method', () => {
    it('should replace <br> tag with \\n', () => {
      const html = 'Test<br class="lineBreak">Test';
      const clean = 'Test\nTest';

      expect(parsing.cleanHTML(html)).toBe(clean);
    });

    it('should replace all html tags', () => {

      const html = '<p>Test<br/><span>Test</span></p>';
      const clean = 'TestTest';

      expect(parsing.cleanHTML(html)).toBe(clean);
    });

    //FIXME Add following test
    xit('should remove new line from start and end', () => {

    });
  });
});