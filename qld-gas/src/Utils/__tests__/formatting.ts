import { formatting } from '../formatting';

describe('Formatting Utils', () => {
  describe('leadNumber method', () => {
    it('should prefix default 0', () => {
      const one = formatting.leading(1, 4);
      const eleven = formatting.leading(11, 4);
      const hundred = formatting.leading(100, 4);
      const thousand = formatting.leading(1000, 4);

      expect(one).toBe('0001');
      expect(eleven).toBe('0011');
      expect(hundred).toBe('0100');
      expect(thousand).toBe('1000');
    });

    it('should prefix with custom', () => {
      const one = formatting.leading(1, 4, '-');
      const eleven = formatting.leading(11, 4, '-');
      const hundred = formatting.leading(100, 4, '-');
      const thousand = formatting.leading(1000, 4, '-');

      expect(one).toBe('---1');
      expect(eleven).toBe('--11');
      expect(hundred).toBe('-100');
      expect(thousand).toBe('1000');
    });
  });
});