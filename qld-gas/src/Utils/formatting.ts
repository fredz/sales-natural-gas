function leading(digits: number, size: number, lead: string = '0'): string {
  let res = digits.toString();

  while (res.length < size) {
    res = lead + res;
  }

  return res;
}

function secondsToHhmmss(totalSeconds: number): string {
  const hours   = Math.floor(totalSeconds / 3600);
  const minutes = Math.floor((totalSeconds - (hours * 3600)) / 60);
  let seconds = totalSeconds - (hours * 3600) - (minutes * 60);

  // round seconds
  seconds = (Math.round(seconds * 100) / 100);

  let result = (hours < 10 ? '0' + hours : hours.toString());
  result += ':' + (minutes < 10 ? '0' + minutes : minutes.toString());
  result += ':' + (seconds  < 10 ? '0' + seconds : seconds.toString());
  return result;
}

export const formatting = {
  leading,
  secondsToHhmmss,
};
