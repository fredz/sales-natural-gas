function cleanHTML(text: string): string {
  // replace <br> tag with \n
  text = text.replace(/<br class="lineBreak">/g, '\n');

  // replace all html tags
  text = text.replace(/(<([^>]+)>)/ig, '');

  // remove new line from start and end
  text = text.replace(/^\s+|\s+$/g, '');

  return text;
}

export const parsing = {
  cleanHTML,
};
