import * as React from 'react';
import { connect } from 'react-redux';
import { TableHeaderColumn } from 'react-bootstrap-table';
import { setPage } from '../../actions/app';
import { IPageSettings } from '../../state';

import * as moment from 'moment';
import { Tabs, Tab } from 'react-bootstrap';

import Listing from '../components/Listing';
import Loadable from '../components/Loadable';
import { Row, Column } from '../../ascendis-ui/layout';
import { InputGroup, Dropdown } from '../../ascendis-ui/components';
import * as utils from '../../App/utils';
import * as fileUtils from '../../App/fileUtils';
import { PageMode, pages } from '../constants';
import Alert from '../components/Alert';
import PreviewInvoiceForm from './forms/PreviewInvoiceForm';


export interface IPreviewInvoicesProps {
  key?: number;
}

interface IConnectedProps {
  pageCode: string;
  editData: any;
}

interface IConnectedDispatch {
  onSetPageCode: (value: string, settings: IPageSettings) => void;
}

export interface IPreviewInvoicesState {
  mode: PageMode;
  key: number;
  keyList: number;
  fromDate: string;
  complexNo: string;
  complexData: any;
  loading: boolean;
  dataCustomer: any;
  customerNo: string;
  listData: Array<any>;
  listProcessed: Array<any>;
  currentType: ExecType;
  rentals: any;
  processed: boolean;
  loadingText: string;
  emailCount?: number;
  emailTotal?: number;
  emailError?: number;
  message?: string; 
  editData?: any; 
  fetchType?: ExecType;
}

export type ExecType = 'customers' | 'complex' | 'date';

class PreviewInvoices extends 
  React.Component<IPreviewInvoicesProps & IConnectedProps & IConnectedDispatch, IPreviewInvoicesState> {
  
  fetching = 'Fetching Data';
  processing = 'Processing Invoices';
  alertRef: any;

  constructor(props) {
    super(props);

    const { editData } = props;

    const key = this.props.key !== undefined ? this.props.key : 1;

    const defaultMessage = 
     'To avoid a late pay fee of #Amount#\nplease pay by due date\nMOVING OR VACATING\nPLEASE CONTACT (07)3166 8685';

    this.state = {
      mode: PageMode.list,
      key,
      keyList: 1,
      fromDate: editData && editData.fromImport ? editData.onTime : null,
      complexNo: null,
      complexData: [],
      loading: false,
      dataCustomer: [],
      customerNo: null,
      listData: [],
      listProcessed: [],
      currentType: null,
      rentals: '',
      processed: false,
      loadingText: this.fetching,      
      message: defaultMessage,
      fetchType: 'date'
    } as IPreviewInvoicesState;

    this.handleSelect = this.handleSelect.bind(this);
    this.handleSelectList = this.handleSelectList.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.getCustomers = this.getCustomers.bind(this);
    this.fetchList = this.fetchList.bind(this);
    this.processList = this.processList.bind(this);
    this.handleProcessedClick = this.handleProcessedClick.bind(this);
    this.email = this.email.bind(this);
    this.print = this.print.bind(this);
    this.getContentFromMode = this.getContentFromMode.bind(this);
    this.handleFormCancel = this.handleFormCancel.bind(this);
    this.handleFormSave = this.handleFormSave.bind(this);
  }

  componentWillMount() {
    this.getDataAndDisplay();
    this.getCustomers('', true);

    if (this.props.editData) {
      this.fetchList('date');
    }
  }

  getCustomers(search, firstLoad = false) {
    if (search.length < 3) {
      this.setState({
        dataCustomer: [{label: 'Type at least 3 characters to fetch', value: null}],
      } as IPreviewInvoicesState);
      return;
    }

    if (search.value === null) {
      return;
    }

    if (search.length > 3 && !firstLoad) {
      return;
    }
    
    const { fetchCustomer } = utils;
    
    Promise.all([fetchCustomer(search)]).then(
      (response: any) => {
        const dataCustomer = response[0].data;

        let nextState = {
          dataCustomer,
          loading: false,
        };

        this.setState( nextState as IPreviewInvoicesState);
      }
    ).catch(
      (ex) => {
        alert(ex);
      }
    );

    this.setState({
      dataCustomer: [{label: 'Fetching, please wait...', value: null}],
    } as IPreviewInvoicesState);
  }

  getDataAndDisplay() {
    const { fetchComplex } = utils;    
    Promise.all([fetchComplex()]).then(
      (response: any) => {
        const complexData = response[0].data;
        this.setState({
          complexData,
          loading: false,
        } as IPreviewInvoicesState);
      }
    ).catch(
      (ex) => {
        alert(ex);
      }
    );
  }

  fetchList(etype: ExecType) {

    this.setState({fetchType: etype});

    const { getTempReadingForComplex, getTempReadingFromDate,
      getTempReadingForCustomer } = utils;
//debugger;
    switch (etype) {
      case 'complex':
        if (!this.state.complexNo) {
          return;
        }

        this.setState({
          loading: true,
        });
        
        getTempReadingForComplex(this.state.complexNo).then(
          (response: any) => {
            const listData = response.result;
            this.setState({
              listData,
              loading: false,
              currentType: 'complex'
            } as IPreviewInvoicesState);
          }
        );
        break;

      case 'date':
        if (!this.state.fromDate) {
          return;
        }

        this.setState({
          loading: true,
        });

        getTempReadingFromDate(this.state.fromDate).then(
          (response: any) => {
            const listData = response.result;
            this.setState({
              listData,
              loading: false,
              currentType: 'date'
            } as IPreviewInvoicesState);
          }
        );
        break;

      case 'customers':
        if (!this.state.customerNo) {
          return;
        }

        this.setState({
          loading: true,
        });

        getTempReadingForCustomer(this.state.customerNo).then(
          (response: any) => {
            const listData = response.result;
            this.setState({
              listData,
              loading: false,
              currentType: 'customers'
            } as IPreviewInvoicesState);
          }
        );
        break;

      default:
        this.setState({
          listData: [],
          loading: false,
          currentType: null
        });
        break;
    }
  }

  handleSelect(key) {
    this.setState({key});
  }

  handleSelectList(keyList) {
    this.setState({keyList});
  }

  handleInputChange(prop, value) {
    let state = {};
    state[prop] = value;

    this.setState(state as IPreviewInvoicesState);
  }

  processList(etype: ExecType) {
    if (this.state.listData.length === 0) {
      return;
    }

    const noDates = this.state.listData.filter(x => x.TempReadingDate.length === 0);
    const noReadings = this.state.listData.filter(
      x => x.TempReading.length === 0 || x.TempReading === '0'
    );

    let message = `Are you sure you want to process ${this.state.listData.length} readings?`;

    if (noDates.length > 0) {
      message += `\n*Please note this list has readings without date and will NOT be processed*`;
    }

    if (noReadings.length > 0) {
      message += `\n*Please note this list has readings without values or 0 and will NOT be processed*`;
    }

    if (!confirm(message)) {
      return;
    }

    const { 
      processInvoices, 
      getTempReadingForComplex, 
      getTempReadingForCustomer, 
      getTempReadingFromDate, 
      fetchProcessedInvoice 
    } = utils;

    switch (etype) {
      case 'date':
        if (!this.state.fromDate) {
          alert('There is no date selected');
          return;
        }

        this.setState({
          loading: true,
          loadingText: this.processing,
        });

        processInvoices(this.state.fromDate, '', '', this.state.message).then(
          (response: any) => {
            const rentals = response.rentals;

            Promise.all([
              getTempReadingFromDate(this.state.fromDate),
              fetchProcessedInvoice(rentals)
            ]).then((processedResult: Array<any>) => {
              const listData: Array<any> = processedResult[0].result;
              const listProcessed: Array<any> = processedResult[1].data;

              this.setState({
                listData,
                listProcessed,
                loading: false
              });
            });
            
            this.setState({
              rentals,
              loading: true,
              processed: true,
              keyList: 2,
              loadingText: this.fetching,
            });
          }
        );
        break;
      
      case 'customers': 
        if (!this.state.customerNo) {
          alert('There is no customer selected');
          return;
        }

        this.setState({
          loading: true,
          loadingText: this.processing,
        });

        processInvoices('', '', this.state.customerNo, this.state.message).then(
          (response: any) => {
            const rentals = response.rentals;
            
            Promise.all([
              getTempReadingForCustomer(this.state.customerNo),
              fetchProcessedInvoice(rentals)
            ]).then((processedResult: Array<any>) => {
              const listData: Array<any> = processedResult[0].result;
              const listProcessed: Array<any> = processedResult[1].data;

              this.setState({
                listData,
                listProcessed,
                loading: false
              });
            });

            this.setState({
              rentals,
              loading: true,
              processed: true,
              keyList: 2,
              loadingText: this.fetching,
            });

          }
        );
        break;

      case 'complex':
        if (!this.state.complexNo) {
          alert('There is no complex selected.');
          return;
        }

        this.setState({
          loading: true,
          loadingText: this.processing,
        });

        processInvoices('', this.state.complexNo, '', this.state.message).then(
          (response: any) => {
            const rentals = response.rentals;
            
            Promise.all([
              getTempReadingForComplex(this.state.complexNo),
              fetchProcessedInvoice(rentals)
            ]).then((processedResult: Array<any>) => {
              const listData: Array<any> = processedResult[0].result;
              const listProcessed: Array<any> = processedResult[1].data;

              this.setState({
                listData,
                listProcessed,
                loading: false
              });
            });

            this.setState({
              rentals,
              loading: true,
              processed: true,
              keyList: 2,
              loadingText: this.fetching,
            });

          }
        );
        break;
      
      default:
        break;
    }
  }

  handleProcessedClick(name: string, row: any) {
    switch (name) {
      case 'email':
        this.email(false, row);
        break;
      
      case 'emailall':
        this.email(true, row);
        break;

      case 'print':
        this.print(false, row);

      case 'printall':
        this.print(true, row);
      
      default:
        break;
    }
  }

  handleEditData(list) {
    this.setState({
      editData: list,
      mode: PageMode.edit,
    } as IPreviewInvoicesState);
  }  

  email(all: boolean, data: any) {
    const { sendInvoiceEmail } = utils;

    if (!all) {

      sendInvoiceEmail(data.InvoiceNo).then((result: any) => {
        if (result.Status !== 'Sent') {
          alert(`Email could not be sent, please communicate invoiceNo: ${data.InvoiceNo}`);
        }
        this.setState({
          loading: false,
          loadingText: this.fetching,
        });
      }).catch(() => {
        alert('An unexpected error happened');
      });
      
      this.setState({
        loading: true,
        loadingText: `Sending Email ${data.InvoiceNo}`,
      });
      return;
    }

    const emailList: Array<any> = data.filter( x => x.Email !== '');

    const total = emailList.length;
    if (total === 0) {
      alert('No customer for the selected invoices has email registered');
      return;
    }

    this.setState({
      loading: true,
      loadingText: `Mailing 0 from ${total}`,
      emailCount: 0,
      emailTotal: total,
      emailError: 0,
    });

    emailList.forEach(element => {
      sendInvoiceEmail(element.InvoiceNo).then((result: any) => {
        let { emailCount, emailError, emailTotal} = this.state;
        if (result.Status !== 'Sent') {
          emailError++;
        }
        emailCount++;

        if (emailCount === emailTotal) {
          this.setState({
            loading: false,
            loadingText: this.fetching,
          });
          return;
        }

        this.setState({
          loading: true,
          loadingText: `Mailing ${emailCount} from ${emailTotal}`,
          emailCount,
          emailTotal: total,
          emailError,
        });
      }).catch(() => {
        let { emailCount, emailError, emailTotal} = this.state;
        emailCount++;
        emailError++;

        if (emailCount === emailTotal) {
          this.setState({
            loading: false,
            loadingText: this.fetching,
          });
          return;
        }

        this.setState({
          loading: true,
          loadingText: `Sending Emails ${emailCount} from ${emailTotal}...`,
          emailCount,
          emailTotal: total,
          emailError,
        });
      });
    });
  }

  print(all: boolean, data: any) {
    const { printInvoice, printInvoiceAll } = fileUtils;

    if (!all) {
      printInvoice(data.InvoiceNo);
      return;
    }

    let invoices = '';
    data.forEach(element => {
      invoices += invoices === '' ? element.InvoiceNo : `,${element.InvoiceNo}`;
    });

    printInvoiceAll(invoices);
  }

  prepareColumns() {
    const columns = [
      {dataField: 'UnitCode', header: 'Unit Code', isKey: true, hidden: true, editable: false},
      {dataField: 'BillingNames', header: 'Billing Names', editable: false},
      {dataField: 'ComplexNo', header: 'Complex', editable: false,
        dataFormat: (cell, row) => {
          const { complexData } = this.state;

          let page = row.ServiceTypeID === '1' ? pages.meterReading : pages.bottleGasDelivery;

          let conf = {
            color: '',
            title: '',
            icon: ''
          };

          conf.title = row.Service;
          if (page === pages.meterReading) {
            conf.color = '#8cc837';
            conf.icon = 'fa fa-dashboard';
          } else {
            conf.color = '#31a1c9';
            conf.icon = 'fa fa-free-code-camp';
          }

          return (
            <a style={{cursor:'pointer', color: conf.color}} title={conf.title}
              onClick={(e) => this.props.onSetPageCode(page, {editData: row})}>
              <i className={conf.icon}/> {complexData.filter(x => x.value === cell)[0].label}
            </a>
          );
        }
      },
      {dataField: 'ComplexUnitNo', header: 'Unit No', editable: false},
      {dataField: 'TempReadingDate', header: 'Reading Date', editable: false,  isDate: true,
        dataFormat: (cell, row) => {
          return <span>{cell ? moment(cell).format('D MMM Y') : ''}</span>;
        }
      },
      {dataField: 'TempReading', header: 'Reading', editable: false},
      {dataField: 'Usage', header: 'Usage', dataFormat: (cell, row) => {
        const { TempReading, CurReading, ServiceTypeID } = row;

        let value;
        if (ServiceTypeID === '1') { // Metered
          if (!isNaN(parseFloat(TempReading))) {
            value = parseFloat(TempReading) - parseFloat(CurReading);

            value = Math.round(value * 100) / 100;
          }
        }
        
        return (
          <span>{value}</span>
        );
      }},

      {dataField: 'FinalReading', header: 'Final?', editable: false,
        dataFormat: (cell, row) => {
          return <span>{cell === 'True' ? 'Yes' : 'No'}</span>;
        }
      },
      {dataField: 'AdjAmountTemp', header: 'Adjustment', editable: false},      
      {dataField: 'Message', header: 'Message', editable: false,
        dataFormat: (cell, row) => {
          return <a  onClick={ ( e ) => this.setState({key: 4})}  >{this.state.message.substr(0,150)}</a>;
        }},      
    ]; 

    return columns;   
  }

  prepareColumnsProcessed() {
    const columnsProcessed = [
      {dataField: 'InvoiceNo', header: 'Invoice', isKey: true, editable: false},
      {dataField: 'ComplexName', header: 'Complex Name', editable: false,
        dataFormat: (cell, row) => {

          let page = row.ServiceTypeID === '1' ? pages.meterReading : pages.bottleGasDelivery;

          let conf = {
            color: '',
            title: '', 
            icon: ''
          };

          conf.title = row.Service;
          if (page === pages.meterReading) {
            conf.color = '#8cc837';
            conf.icon = 'fa fa-dashboard';
          } else {
            conf.color = '#31a1c9';
            conf.icon = 'fa fa-free-code-camp';
          }

          return (
            <a style={{color: conf.color}} title={conf.title}>
              <i className={conf.icon}/> {cell}
            </a>
          );
        }
      },
      {dataField: 'LastReading', header: 'Last Reading', editable: false},
      {dataField: 'CurReading', header: 'This Reading', editable: false},
      {dataField: 'Level1Qty', header: 'Level 1', editable: false},
      {dataField: 'L1UnitPrice', header: 'Price 1', editable: false},
      {dataField: 'Level2Qty', header: 'Level 2', editable: false},
      {dataField: 'L2UnitPrice', header: 'Price 2', editable: false},
      {dataField: 'SubTotal', header: 'Sub Total', dataFormat: (cell, row) => {
        const { CurReading, Level1Qty, L1UnitPrice, Level2Qty, L2UnitPrice } = row;

        let value;
        if (!isNaN(parseFloat(Level1Qty)) && !isNaN(parseFloat(CurReading)) && !isNaN(parseFloat(L1UnitPrice))) {
          value = 
            ( parseFloat(Level1Qty) * parseFloat(L1UnitPrice) ) + ( parseFloat(Level2Qty) * parseFloat(L2UnitPrice) );

          value = Math.round(value * 1000) / 1000;
        }
        
        return (
          <span style={{textAlign: 'right'}}>{value}</span>
        );
      }},
    ];

    return columnsProcessed;
  }

  handleFormSave(data) {
    
    let formData = new FormData();
    formData.append('Action', 'Update');      
    formData.append('Path', 'Adjustment/');
    
    const { unitCode, adjAmountTemp, adjDescriptionTemp } = data;

    formData.append('UnitCode', unitCode);
    formData.append('AdjAmountTemp', adjAmountTemp);
    formData.append('AdjDescriptionTemp', adjDescriptionTemp);

    utils.fetch(formData).then(
      (response: any) => {
        const { status } = response;
        if (status === 'ok') {

          this.fetchList(this.state.fetchType);

          this.setState({
            mode: PageMode.list, 
            listData: [],     
          });          

          this.alertRef.displayAlert('success', 'The Adjustment has been saved', 'Adjustment');
        }else {
          this.alertRef.displayAlert('error', 
            `Error while Saving.\nError Message:\n${response.error}`, 'Error');  
        }
      }
    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );    
  }

  handleFormCancel() {
    this.setState({
      mode: PageMode.list,
    } as IPreviewInvoicesState);    
  }

  getContentFromMode(mode: PageMode) {
    let content;
    switch (mode) {
      case PageMode.list:

        let columns = this.prepareColumns();
        const columnsProcessed = this.prepareColumnsProcessed();
        const actions = (
          <TableHeaderColumn 
            dataField="action" 
            dataFormat={(cell, row) => {
              return (
                <span>
                  <a className="btn btn-xs btn-default" title="Download PDF" 
                    onClick={(e) => {e.preventDefault(); this.handleProcessedClick('print', row);}}>
                    <i className="fa fa-file-pdf-o"/>
                  </a>
                  {
                    row.Email ? ( 
                    <a className="btn btn-xs btn-default" title="Email Invoice" 
                      onClick={(e) => {e.preventDefault(); this.handleProcessedClick('email', row);}}>
                      <i className="fa fa-envelope"/>
                    </a>) : null
                  }
                </span>
              );
          }}>
            Actions
          </TableHeaderColumn>
        );

        const actionsGenerationInvoice = (
          <TableHeaderColumn 
            dataField="action" 
            dataAlign="center"        
            headerAlign="center"
            width="80px"
            dataFormat={(cell, row) => {
              return (
                <span>
                  <a className="btn btn-xs btn-warning" title="Adjustment" 
                    onClick={(e) => {e.preventDefault(); this.handleEditData(row);}}                >
                    <i className="glyphicon glyphicon-cog "/>
                  </a>
                </span>
              );
          }}>
            Actions
          </TableHeaderColumn>
        );   

        content = (
          <div>
            <div className="alert alert-info" role="alert">
              <strong>Review Readings.</strong> Please review readings before processing invoices
              <a className="btn btn-info btn-xs pull-right"
                onClick={(e) => this.props.onSetPageCode(pages.viewInvoices, {})}>
                  View All Invoices
              </a>
            </div>

            <Tabs activeKey={this.state.key} onSelect={this.handleSelect} id="controlled-tab-example">
              <Tab eventKey={1} title="From Date">
                <div className="well">
                  <Row>
                    <Column column="8">
                      <InputGroup 
                        label="From Date" 
                        placeholder="From Date"
                        type="datepicker" 
                        inputName="fromDate"
                        onValueChange={(value) => {this.handleInputChange('fromDate', value);}}
                        value={this.state.fromDate}/>
                    </Column>
                    <Column column="4">
                      <a className="btn btn-success" onClick={(e) => this.fetchList('date')}>Fetch</a>
                      {
                        this.state.currentType === 'date' ? (
                          <a className="btn btn-success" onClick={(e) => this.processList('date')}>
                            <i className="fa fa-retweet" aria-hidden="true" /> Process ({this.state.listData.length})
                          </a>
                        ) : null
                      }
                    </Column>
                  </Row>
                </div>
              </Tab>
              <Tab eventKey={2} title="For Complex">
                <div className="well">
                  <Row>
                    <Column column="8">
                      <Dropdown 
                        list={this.state.complexData}
                        label="Complex" 
                        inputName="complexNo"
                        onValueChange={(value) => {this.handleInputChange('complexNo', value.value);}}
                        value={this.state.complexNo}/>
                    </Column>
                    <Column column="4">
                      <a className="btn btn-success" onClick={(e) => this.fetchList('complex')}>Fetch</a>
                      {
                        this.state.currentType === 'complex' ? (
                          <a className="btn btn-success" onClick={(e) => this.processList('complex')}>
                            <i className="fa fa-retweet" aria-hidden="true" /> Process ({this.state.listData.length})
                          </a>
                        ) : null
                      }
                    </Column>
                  </Row>
                </div>
              </Tab>
              <Tab eventKey={3} title="For Customer">
                <div className="well">
                  <Row>
                    <Column column="8">
                      <Dropdown 
                        list={this.state.dataCustomer}
                        label="Customer" 
                        inputName="customerNo"
                        onValueChange={(value) => {this.handleInputChange('customerNo', value.value);}}
                        value={this.state.customerNo}
                        onInputChange={this.getCustomers}/>
                    </Column>
                    <Column column="4">
                      <a className="btn btn-success" onClick={(e) => this.fetchList('customers')}>Fetch</a>
                      {
                        this.state.currentType === 'customers' ? (
                          <a className="btn btn-success" onClick={(e) => this.processList('customers')}>
                            <i className="fa fa-retweet" aria-hidden="true" /> Process ({this.state.listData.length})
                          </a>
                        ) : null
                      }
                    </Column>
                  </Row>
                </div>
              </Tab>
              <Tab eventKey={4} title="Message">
                <div className="well">
                  <Row>
                    <Column column="12">
                      <InputGroup 
                        label="Message" 
                        placeholder="Message"
                        type="textarea" 
                        inputName="message"
                        onValueChange={(value) => {this.handleInputChange('message', value);}}
                        value={this.state.message}/>
                    </Column>               
                  </Row>
                </div>
              </Tab>              
            </Tabs>
            <Tabs activeKey={this.state.keyList} onSelect={this.handleSelectList} id="controlled-tab-example">
              <Tab eventKey={1} title="For Processing">
                <Listing 
                  data={this.state.listData}
                  columns={columns} 
                  onRefreshData={null}
                  onEditData={null}
                  onDeleteData={null}
                  withActions={true}
                  withNumber={true}
                  actions={actionsGenerationInvoice}/>
              </Tab>
              {
                this.state.processed ? 
                <Tab eventKey={2} title="Invoices Processed">
                  <div className="col-md-12">
                    <div className="col-md-offset-8 col-md-4" style={{textAlign: 'right'}}>
                      <a className="btn btn-info" title="Print All to PDF Bundle"
                        onClick={(e) => {
                          e.preventDefault(); 
                          this.handleProcessedClick('printall', this.state.listProcessed);
                        }}>
                        <i className="fa fa-file-pdf-o"/> All
                      </a>
                      <a className="btn btn-info" title="Print All Without Email"
                        onClick={(e) => {
                          e.preventDefault(); debugger;
                          this.handleProcessedClick('printall', 
                              this.state.listProcessed.filter(x => x.Email === '' || x.Email === undefined));
                        }}>
                        <i className="fa fa-file-pdf-o"/> All Without <i className="fa fa-envelope"/>
                      </a>
                      <a className="btn btn-info" title="Email All"
                        onClick={(e) => {
                          e.preventDefault(); 
                          this.handleProcessedClick('emailall', this.state.listProcessed);
                        }}>
                        <i className="fa fa-envelope"/> All
                      </a>
                    </div>
                  </div>
                  <div>
                    <Listing 
                      data={this.state.listProcessed}
                      columns={columnsProcessed} 
                      onRefreshData={null}
                      onEditData={null}
                      onDeleteData={null}
                      withActions={true}
                      actions={actions}
                      withNumber={false}/>
                  </div>
                </Tab> : null
              }
            </Tabs>               
          </div>
        );
        break;

      case PageMode.edit:      
        content = <PreviewInvoiceForm data={this.state.editData} 
        onSave={this.handleFormSave} onCancel={this.handleFormCancel}/>;        
        break;

      default: 
        content = null;
    }

    return content;
  }  

  render() {

    const content = this.getContentFromMode(this.state.mode);

    return (
      <div style={{margin: '0 10px'}}>

        <Alert ref={x => this.alertRef = x} />
        <Loadable isLoading={this.state.loading} text={this.state.loadingText}>
          {content}
        </Loadable>
      </div>
    );
  }
}

function mapStateToProps(state: any): IConnectedProps {
  return {
    pageCode: state.app.pageCode,
    editData: state.app.editData,
  };
}

function mapDispatchToProps(dispatch): IConnectedDispatch {
  return {
    onSetPageCode: (value: string, settings) => dispatch(setPage(value, settings))
  };
}

export default 
  connect(mapStateToProps, mapDispatchToProps)(PreviewInvoices) as React.ComponentClass<IPreviewInvoicesProps>;
