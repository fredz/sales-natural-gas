import * as React from 'react';
import { connect } from 'react-redux';
import { setPage } from '../../actions/app';
import { IPageSettings } from '../../state';

import * as utils from '../../App/utils';
import Alert from '../components/Alert';
import InvoiceView from './forms/InvoiceView';

export interface IInvoiceViewState { }

class TaxInvoice extends React.Component<any, IInvoiceViewState> {

  listData: Array<any>;
  alertRef: any;

  constructor(props) {
    super(props);

    let complexNo = '';

    if (this.props.editData) {
      complexNo = this.props.editData.ComplexNo;
    } 

    this.state = {      
      loading: true
    } as IInvoiceViewState;

  }
  
  render() {
    return (
      <div>
        <Alert ref={x => this.alertRef = x} />
        <InvoiceView onCancel={null} onSave={null} />
      </div>
    );
  }

}

export default TaxInvoice;
