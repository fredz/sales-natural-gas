import * as React from 'react';
import * as moment from 'moment';
import { connect } from 'react-redux';
import { setPage } from '../../actions/app';
import { IPageSettings } from '../../state';

import Loadable from '../components/Loadable';
import * as utils from '../../App/utils';
import Listing from '../components/Listing';
import { createDatePickerEditor } from '../components/Editors/DatePickerEditor';
import Alert from '../components/Alert';
import SimpleHeader from '../components/SimpleHeader';
import { PageMode, pages } from '../constants';
import { InputGroup, Dropdown } from '../../ascendis-ui/components';
import * as fileUtils from '../../App/fileUtils';

export interface IReadingProps { }

interface IConnectedProps {
  pageCode: string;
  editData: any;
}

interface IConnectedDispatch {
  onSetPageCode: (value: string, settings: IPageSettings) => void;
}

export interface IReadingState {
  mode: PageMode;
  loading: boolean;
  data: Array<any>;
  total: number;
  dataMeteredComplexes: any;
  valueMeteredComplexes: string;
  storing: boolean;
  refreshing: boolean;
  readingDate: string;
}

class MeterReading extends React.Component<IReadingProps & IConnectedProps & IConnectedDispatch, IReadingState> {
  
  alertRef: any; 

  constructor(props) {
    super(props);

    let complexNo = '';

    if (this.props.editData) {
      complexNo = this.props.editData.ComplexNo;
    } 

    this.state = {
      mode: PageMode.list,
      loading: true,
      dataMeteredComplexes: [], 
      valueMeteredComplexes: complexNo,
      storing: false,
      refreshing: false,
      readingDate: new Date().toString()
    } as IReadingState;

    this.handleRightItemClick = this.handleRightItemClick.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.getComboDataAndDisplay = this.getComboDataAndDisplay.bind(this);
    this.downloadFile = this.downloadFile.bind(this);
    this.handleBeforeSaveCell = this.handleBeforeSaveCell.bind(this);
  }

  componentDidMount() {   
    let complexNo = null;

    if (this.props.editData) {
      complexNo = this.props.editData.ComplexNo;
    } 

    this.getDataAndDisplay(this.state.mode, complexNo);
  }

  downloadFile() {
    if (this.state.valueMeteredComplexes === null || !this.state.valueMeteredComplexes) {
      return;
    }

    fileUtils.fetchReading(this.state.valueMeteredComplexes);
  }

  handleInputChange(prop, value) {
    let state = {};

    state[prop] = value;
    this.setState(state as IReadingState);

    if (prop === 'valueMeteredComplexes') {
      this.getDataMeteredAndDisplay(this.state.mode, value);
    }
  }  

  handleRightItemClick(mode: PageMode) {

    if (mode === PageMode.list) {
      this.setState({
        loading: true,
      } as IReadingState);

      this.getDataAndDisplay(mode);
    } else {
      this.setState({
        mode,
      } as IReadingState);
    }
  }

  getComboDataAndDisplay() {
    utils.fetchMeteredComplexes().then(
      (response: any) => {
        const dataMeteredComplexes = response.data;
        this.setState({          
          dataMeteredComplexes,          
        } as IReadingState);
      }
    ).catch(
      (ex) => {
        alert(ex);
      }
    );
  } 

  getDataMeteredAndDisplay(mode, complexNo, refreshing = false) {    
    if (complexNo === null) {
      this.setState({
        data: [],
        loading: false,
        refreshing: false,
        total: 0,
      } as IReadingState);

      return;
    }

    let formData = new FormData();
    formData.append('Action', 'Fetch');
    formData.append('Path', 'Reading/');
    formData.append('ComplexNo', complexNo);
    
    utils.fetch(formData).then(
      (response: any) => {
        
        const { data, total } = response;

        this.setState({
          mode,
          data,          
          loading: false,
          refreshing: false,
          total,           
        } as IReadingState);

      }
    ).catch(
      (ex) => {
        alert(ex);
      }
    );

    if (!refreshing) {
      this.setState({
        loading: true,
      } as IReadingState);
    }
  }  

  getDataAndDisplay(mode, complexNo = null) {
    this.getComboDataAndDisplay();
    this.getDataMeteredAndDisplay(mode, complexNo);
  }

  handleBeforeSaveCell(row, cellName, cellValue) {
    const {UnitCode, TempReadingDate, FinalReading, TempReading } = row; 

    let readingDate = null;

    if (cellName === 'TempReadingDate') {
      readingDate = cellValue.date;
    } else if (TempReadingDate !== '') {
      readingDate = TempReadingDate;
    } else {
      readingDate = this.state.readingDate;
    }

    let date = '';
    if (!readingDate && cellName !== 'TempReadingDate') {
      date = new Date().toString();
    } else {
      date = moment(readingDate).format('M/D/Y');
    }

    switch (cellName) {
      case 'TempReadingDate':
        utils.updateTempReading(UnitCode, date, TempReading, FinalReading === 'True' ? '1' : '0')
        .then(() => {
          this.setState({
            storing: false,
            refreshing: true,
          });
          
          this.getDataMeteredAndDisplay(this.state.mode, this.state.valueMeteredComplexes, true);
        });
        break;

      case 'TempReading':
        utils.updateTempReading(UnitCode, cellValue.length !== 0 ? date : '', 
          cellValue, FinalReading === 'True' ? '1' : '0')
        .then(() => {
          this.setState({
            storing: false,
            refreshing: true,
          });
          
          this.getDataMeteredAndDisplay(this.state.mode, this.state.valueMeteredComplexes, true);
        });
        break;

      case 'FinalReadingValue':
        utils.updateTempReading(UnitCode, date, TempReading, cellValue === 'Yes' ? '1' : '0')
        .then(() => {
          this.setState({
            storing: false,
            refreshing: true,
          });

          this.getDataMeteredAndDisplay(this.state.mode, this.state.valueMeteredComplexes, true);
        });
        break;

      default:
        break;
    }

    this.setState({
      storing: true,
    });
  }

  getUsage(cellValue, CurReading) {

    let value;
    if (!isNaN(parseFloat(cellValue))) {
      value = parseFloat(cellValue) - parseFloat(CurReading);

      value = Math.round(value * 10000) / 10000;
    }

    return (value < 0) ? '' : value;
  }

  getContentFromMode(mode: PageMode) {
    let content;
    switch (mode) {
      case PageMode.list: 
        let { data } = this.state;
        const columns = [
          {dataField: 'UnitCode', header: 'Unit Code', isKey: true, hidden: true, editable: false},
          {dataField: 'ComplexUnitNo', header: 'Unit', editable: false},
          {dataField: 'BillingNames', header: 'Customer Name', editable: false,
            dataFormat: (cell, row) => {
              return (
                <a style={{cursor:'pointer'}}
                  onClick={(e) => this.props.onSetPageCode(pages.customerMaintenance, {editData: row})}>
                  {cell}
                </a>
              );
            }
          },
          {dataField: 'Custno', header: 'Customer Number', editable: false},
          {dataField: 'CurReadingDate', header: 'Last Reading Date', editable: false, isDate: true,
            dataFormat: (cell, row) => {
              return <span>{cell ? moment(cell).format('D MMM Y') : ''}</span>;
            }
          },
          {dataField: 'CurReading', header: 'Last Reading', editable: false},
          {dataField: 'TempReadingDate', header: 'This Reading Date', editable: true, isDate: true,
            thStyle: {background: '#d9edf7'},
            editColumnClassName: 'datePickerCellEdit',
            dataFormat: (cell, row) => {
              return <span>{cell ? moment(cell).format('D MMM Y') : ''}</span>;
            },
            customEditor: { 
              getElement: createDatePickerEditor
            }
          },
          {dataField: 'TempReading', header: 'This Reading', editable: true, thStyle: {background: '#d9edf7'}},
          {dataField: 'Usage', header: 'Usage', dataFormat: (cell, row) => {
            const { TempReading, CurReading } = row;

            let value = this.getUsage(TempReading, CurReading );

            return (
              <span>{value}</span>
            );
          }},
          {dataField: 'FinalReadingValue', header: 'Final?', editable: {
            type: 'select', options: { values: ['Yes', 'No'] }
          }, thStyle: {background: '#d9edf7'}},
        ];

        if (!data) {
          data = [];
        }

        content = (
          <div>
            <div className="alert alert-info" role="alert" style={{textAlign: 'center'}}>
              <strong><i className="glyphicon glyphicon-info-sign" /></strong> 
              Click on row cell to edit values, press enter to save.
            </div>

            <div className="row">
              <div className="col-md-4">
                <InputGroup 
                  type="datepicker" 
                  label="Reading Date" 
                  placeholder="Reading Date" 
                  inputName="readingDate"
                  onValueChange={(value) => {this.handleInputChange('readingDate', value);}}
                  value={this.state.readingDate}/>
              </div>
              <div className="col-md-6">
                <Dropdown 
                  list={this.state.dataMeteredComplexes}
                  label="Metered Complexes" 
                  inputName="valueMeteredComplexes"
                  onValueChange={(value) => {this.handleInputChange('valueMeteredComplexes', value.value);}}
                  value={this.state.valueMeteredComplexes}/>
              </div>
              <div className="col-md-2">
                <div style={{textAlign: 'right', paddingRight: '10px'}}>
                  <a className="btn btn-default" onClick={(e) => {e.preventDefault(); this.downloadFile();}}>
                    <i className="fa fa-file-pdf-o" aria-hidden="true"/>
                  </a>
                  <a className="btn btn-success" title="Go to Generate Invoices" 
                    onClick={(e) => this.props.onSetPageCode(pages.previewInvoices, {})}>
                    <i className="fa fa-retweet" aria-hidden="true" />
                  </a>
                </div>
              </div>
            </div>

            <Listing 
              data={data}
              columns={columns} 
              onRefreshData={null}
              onEditData={null}
              onDeleteData={null}
              withActions={false}
              cellEdit={{
                mode: 'click',
                blurToSave: true,
                beforeSaveCell: this.handleBeforeSaveCell,
              }}
              withNumber={false}/>
          </div>
        );
        break;

      default: 
        content = null;
    }
    
    return content;
  } 

  render() {
    const storing = this.state.storing ? ' (Uploading data...)' : '';
    const refreshing = this.state.refreshing ? ' (Refreshing data...)' : '';

    const brand = (
      <span>
        <i className="glyphicon glyphicon-list-alt"/> Meter Reading 
        <span style={{color:'red'}}>{storing}{refreshing}</span>
      </span>
    );

    const content = this.getContentFromMode(this.state.mode);
    
    return (
      <div>
        <Alert ref={x => this.alertRef = x} />
        <Loadable isLoading={this.state.loading} text={'Fetching Data...'}>
          <SimpleHeader 
            brand={brand}
            listLabel="List"
            newLabel=""
            onRightItemClick={this.handleRightItemClick}
            mode={PageMode.new}
            withOptions={false}
          />
          {content}             
        </Loadable>
      </div>
    );
  }
  
}

function mapStateToProps(state: any): IConnectedProps {
  return {
    pageCode: state.app.pageCode,
    editData: state.app.editData,
  };
}

function mapDispatchToProps(dispatch): IConnectedDispatch {
  return {
    onSetPageCode: (value: string, settings) => dispatch(setPage(value, settings))
  };
}

export default 
  connect(mapStateToProps, mapDispatchToProps)(MeterReading) as React.ComponentClass<IReadingProps>;
