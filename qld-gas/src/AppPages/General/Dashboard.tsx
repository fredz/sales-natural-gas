import * as React from 'react';
import Loadable from '../components/Loadable';
import DashboardPanels from '../components/DashboardPanels';
import * as utils from '../../App/utils';
import { IDashboardPanelProps } from '../components/DashboardPanel';

class Dashboard extends React.Component<any, any> {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      dashboards: [],
    };

    this.getDataAndDisplay = this.getDataAndDisplay.bind(this);
  }

  componentDidMount() {
    this.getDataAndDisplay();
  }

  getDataAndDisplay() {
    let formData = new FormData();
    formData.append('Action', 'Fetch');
    formData.append('Path', 'Dashboard/');

    utils.fetch(formData).then(
      (response: any) => {
        const { data } = response;

        let dashboards = new Array<IDashboardPanelProps>();

        dashboards.push({
          subtext: 'Customers',
          numberText: data[0].Customers.toString(),
          onClick: this.props.onItemClick,
          clickLabel: 'Manage Customers',
          color: 'primary',
          icon: 'address-card',
          pageCode: 'customer-maintenance',
        });

        dashboards.push({
          subtext: 'Complex',
          numberText: data[0].Complex.toString(),
          onClick: this.props.onItemClick,
          clickLabel: 'Manage Complex',
          color: 'green',
          icon: 'building',
          pageCode: 'complex-maintenance',
        });

        dashboards.push({
          subtext: 'Units',
          numberText: data[0].Units.toString(),
          onClick: this.props.onItemClick,
          clickLabel: 'Manage Units',
          color: 'yellow',
          icon: 'building-o',
          pageCode: 'unitcodes-maintenance',
        });

        // Letters
        const { reminder } = response;
        dashboards.push({
          subtext: 'Reminder Letters',
          numberText: `$${reminder.OwingTotal}`,
          onClick: this.props.onItemClick,
          clickLabel: `${reminder.Sent} processed of ${reminder.Total}`,
          color: 'green',
          icon: 'clock-o',
          pageCode: 'debt-collection',
        });
        const { disconnect } = response;
        dashboards.push({
          subtext: 'Disconnect Letters',
          numberText: `$${disconnect.OwingTotal}`,
          onClick: this.props.onItemClick,
          clickLabel: `${disconnect.Sent} processed of ${disconnect.Total}`,
          color: 'yellow',
          icon: 'plug',
          pageCode: 'debt-collection',
        });
        const { legal } = response;
        dashboards.push({
          subtext: 'Legal Letters',
          numberText: `$${legal.OwingTotal}`,
          onClick: this.props.onItemClick,
          clickLabel: `${legal.Sent} processed of ${legal.Total}`,
          color: 'red',
          icon: 'suitcase',
          pageCode: 'debt-collection',
        });
        this.setState({
          loading: false,
          dashboards,
        });
      }
    ).catch(
      (ex) => {
        alert(ex);
      }
    );
  }

  render() {
    return (
      <Loadable isLoading={this.state.loading} text={'Fetching Data...'}>
        <DashboardPanels dashboards={this.state.dashboards}/>
      </Loadable>
    );
  }
}

export default Dashboard;
