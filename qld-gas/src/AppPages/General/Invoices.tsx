import * as React from 'react';
import { connect } from 'react-redux';
import { TableHeaderColumn } from 'react-bootstrap-table';
import { setPage } from '../../actions/app';
import { IPageSettings } from '../../state';

import * as moment from 'moment';
import { Tabs, Tab } from 'react-bootstrap';

import Listing from '../components/Listing';
import Loadable from '../components/Loadable';
import { Row, Column } from '../../ascendis-ui/layout';
import { InputGroup } from '../../ascendis-ui/components';
import * as utils from '../../App/utils';
import * as fileUtils from '../../App/fileUtils';
import { PageMode, pages } from '../constants';
import Alert from '../components/Alert';
import SimpleHeader from '../components/SimpleHeader';
import InvoiceForm from './forms/InvoiceForm';


export interface IInvoicesProps {
  key?: number;
}

interface IConnectedProps {
  pageCode: string;
  editData: any;
}

interface IConnectedDispatch {
  onSetPageCode: (value: string, settings: IPageSettings) => void;
}

export interface IPreviewInvoicesState {  
  key: number;
  keyList: number;
  fromDate: string;
  toDate: string;
  complexName: string;
  complexData: any;
  loading: boolean;
  dataCustomer: any;
  billingNames: string;
  listProcessed: Array<any>;
  listProcessedBackup: Array<any>;
  currentType: ExecType;
  rentals: any;
  processed: boolean;
  loadingText: string;
  emailCount?: number;
  emailTotal?: number;
  emailError?: number;
  fromInvoice: string;
  toInvoice: string;
  classAdvancedSearch: string;
  editData: Array<any>;  
  mode: PageMode;
}

export type ExecType = 'customers' | 'complex' | 'date' | 'byInvoiceNo';

class Invoices extends 
  React.Component<IInvoicesProps & IConnectedProps & IConnectedDispatch, IPreviewInvoicesState> {
  
  fetching = 'Fetching Data';
  processing = 'Processing Invoices';
  alertRef: any; 

  constructor(props) {
    super(props);

    const key = this.props.key !== undefined ? this.props.key : 1;

    this.state = {
      mode: PageMode.list,
      key,
      keyList: 1,
      fromDate: null,
      toDate: null,
      complexName: '',      
      loading: false,      
      billingNames: '',
      listProcessed: [],
      listProcessedBackup: [],
      currentType: null,
      rentals: '',
      processed: false,
      loadingText: this.fetching,
      fromInvoice: '',
      toInvoice: '',
      classAdvancedSearch: 'hidden',
      editData: [],
    } as IPreviewInvoicesState;

    this.handleSelect = this.handleSelect.bind(this);
    this.handleSelectList = this.handleSelectList.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);    
    this.fetchList = this.fetchList.bind(this);
    this.handleProcessedClick = this.handleProcessedClick.bind(this);
    this.email = this.email.bind(this);
    this.print = this.print.bind(this);
    this.filterList = this.filterList.bind(this);    
    this.handleEditData = this.handleEditData.bind(this);
    this.getContentFromMode = this.getContentFromMode.bind(this);
    this.handleRightItemClick = this.handleRightItemClick.bind(this);
    this.handleFormSave = this.handleFormSave.bind(this);
    this.handleFormCancel = this.handleFormCancel.bind(this);    
    
  }

  filterList(prop, value) {
    let state = {};

    let filterByColumn = '';

    if (prop === 'billingNames') {
      filterByColumn = 'BillingNames';
    }else if (prop === 'complexName') {
      filterByColumn = 'ComplexName';
    }

    let listFiltered = this.state.listProcessedBackup.filter((Invoice) => {      
      if (Invoice[filterByColumn].toUpperCase().indexOf(value.toUpperCase()) >= 0 ) {
        return true;
      }else {
        return false;
      }
    });

    state[prop] = value;
    state['listProcessed'] = listFiltered;    
    this.setState(state as IPreviewInvoicesState);
  }

  fetchList(etype: ExecType) {
    const { fetchTaxInvoices } = utils;
    let title = 'Searching';
    let message;

    switch (etype) {
      case 'date':
        if (!this.state.fromDate || !this.state.toDate) {
          message = 'FromDate and ToDate must not be empty or null';
          this.alertRef.displayAlert('error', message, title);
          return;
        }

        if (new Date(this.state.fromDate) > new Date(this.state.toDate)) {
          message = 'FromDate must not be greater than ToDate';
          this.alertRef.displayAlert('error', message, title);
          return;
        }        

        this.setState({
          loading: true,
        });

        fetchTaxInvoices('', 'ByDate', this.state.fromDate, this.state.toDate).then(
          (response: any) => {

            const listProcessed = response.data;
            const classAdvancedSearch = (listProcessed.length > 0) ? 'show' : 'hidden';

            this.setState({
              listProcessed,
              listProcessedBackup: listProcessed,
              loading: false,
              currentType: 'date',
              classAdvancedSearch
            } as IPreviewInvoicesState);
          }
        );
        break;

      case 'byInvoiceNo':
        if (!this.state.fromInvoice || !this.state.toInvoice) {
          message = 'fromInvoice and toInvoice must not be empty or null';
          this.alertRef.displayAlert('error', message, title);          
          return;
        }

        if (this.state.fromInvoice > this.state.toInvoice) {
          message = 'fromInvoice must not be greater than toInvoice';
          this.alertRef.displayAlert('error', message, title);            
          return;
        }        

        this.setState({
          loading: true,
        });

        fetchTaxInvoices(`WHERE InvoiceNo BETWEEN ${this.state.fromInvoice} AND ${this.state.toInvoice}`, 
        'ByInvoiceNo', '', '').then(
          (response: any) => {

            const listProcessed = response.data;
            const classAdvancedSearch = (listProcessed.length > 0) ? 'show' : 'hidden';

            this.setState({
              listProcessed,
              loading: false,
              currentType: 'byInvoiceNo',
              classAdvancedSearch
            } as IPreviewInvoicesState);
          }
        );
        break;
      default:
        this.setState({
          listProcessed: [],
          loading: false,
          currentType: null
        });
        break;
    }
  }

  handleFormSave(data, isEdit, id = null) {
    
    let formData = new FormData();    
    formData.append('Action', 'Update');
    formData.append('Path', 'Invoice/');
    formData.append('InvoiceNo', data.invoiceNo);
    formData.append('L1UnitPrice', data.l1UnitPrice);
    formData.append('LastReading', data.lastReading);
    formData.append('CurReading', data.curReading);

    utils.fetch(formData).then(
      (response: any) => {
        const { status } = response;                
        
        if (status === 'ok') {          
          this.alertRef.displayAlert('success', 'Invoice Updated', 'Invoice');          
        } else {
          this.alertRef.displayAlert('error', `Error while Saving.\nError Message:\n${response.error}`, 'Error');
        }

        this.setState({
          mode: PageMode.list 
        } as IPreviewInvoicesState);

      }
    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );

  }

  handleFormCancel() {
    this.setState({
      mode: PageMode.list,
    } as IPreviewInvoicesState);
  }  

  handleSelect(key) {
    this.setState({key});
  }

  handleSelectList(keyList) {
    this.setState({keyList});
  }

  handleInputChange(prop, value) {
    let state = {};
    state[prop] = value;

    this.setState(state as IPreviewInvoicesState);
  }

  handleEditData(list) {    
    this.setState({
      editData: list,
      mode: PageMode.edit,
    } as IPreviewInvoicesState);
  }  

  handleRightItemClick(mode: PageMode) {    
    this.setState({
      mode,
    } as IPreviewInvoicesState);
  }  

  handleProcessedClick(name: string, row: any) {
    switch (name) {
      case 'email':
        this.email(false, row);
        break;
      
      case 'emailall':
        this.email(true, row);
        break;

      case 'print':
        this.print(false, row);

      case 'printall':
        this.print(true, row);
      
      default:
        break;
    }
  }

  email(all: boolean, data: any) {
    const { sendInvoiceEmail } = utils;

    if (!all) {

      sendInvoiceEmail(data.InvoiceNo).then((result: any) => {
        if (result.Status !== 'Sent') {
          alert(`Email could not be sent, please communicate invoiceNo: ${data.InvoiceNo}`);
        }
        this.setState({
          loading: false,
          loadingText: this.fetching,
        });
      }).catch(() => {
        alert('An unexpected error happened');
      });
      
      this.setState({
        loading: true,
        loadingText: `Sending Email ${data.InvoiceNo}`,
      });
      return;
    }

    const emailList: Array<any> = data.filter( x => x.Email !== '');

    const total = emailList.length;
    if (total === 0) {
      alert('No customer for the selected invoices has email registered');
      return;
    }

    this.setState({
      loading: true,
      loadingText: `Mailing 0 from ${total}`,
      emailCount: 0,
      emailTotal: total,
      emailError: 0,
    });

    emailList.forEach(element => {
      sendInvoiceEmail(element.InvoiceNo).then((result: any) => {
        let { emailCount, emailError, emailTotal} = this.state;
        if (result.Status !== 'Sent') {
          emailError++;
        }
        emailCount++;

        if (emailCount === emailTotal) {
          this.setState({
            loading: false,
            loadingText: this.fetching,
          });
          return;
        }

        this.setState({
          loading: true,
          loadingText: `Mailing ${emailCount} from ${emailTotal}`,
          emailCount,
          emailTotal: total,
          emailError,
        });
      }).catch(() => {
        let { emailCount, emailError, emailTotal} = this.state;
        emailCount++;
        emailError++;

        if (emailCount === emailTotal) {
          this.setState({
            loading: false,
            loadingText: this.fetching,
          });
          return;
        }

        this.setState({
          loading: true,
          loadingText: `Sending Emails ${emailCount} from ${emailTotal}...`,
          emailCount,
          emailTotal: total,
          emailError,
        });
      });
    });
  }

  print(all: boolean, data: any) {
    const { printInvoice, printInvoiceAll } = fileUtils;

    if (!all) {
      printInvoice(data.InvoiceNo);
      return;
    }
    
    let invoices = '';
    data.forEach(element => {
      invoices += invoices === '' ? element.InvoiceNo : `,${element.InvoiceNo}`;
    });

    printInvoiceAll(invoices);
  }

  getContentFromMode(mode: PageMode) {
    let content;
    switch (mode) {
      case PageMode.list: 
        const columnsProcessed = [
          {dataField: 'InvoiceNo', header: 'Invoice', isKey: true, editable: false},
          {dataField: 'BillingNames', header: 'Billing Names', editable: false},
          {dataField: 'ComplexName', header: 'Complex Name', editable: false,
            dataFormat: (cell, row) => {

              let page = row.ServiceTypeID === '1' ? pages.meterReading : pages.bottleGasDelivery;

              let conf = {
                color: '',
                title: '',
                icon: ''
              };

              conf.title = row.Service;
              if (page === pages.meterReading) {
                conf.color = '#8cc837';
                conf.icon = 'fa fa-dashboard';
              } else {
                conf.color = '#31a1c9';
                conf.icon = 'fa fa-free-code-camp';
              }

              return (
                <a style={{color: conf.color}} title={conf.title}>
                  <i className={conf.icon}/> {cell}
                </a>
              );
            }
          },
          {dataField: 'LastReading', header: 'Last Reading', editable: false},
          {dataField: 'CurReading', header: 'This Reading', editable: false},
          // {dataField: 'Level1Qty', header: 'Level 1', editable: false},
          // {dataField: 'L1UnitPrice', header: 'Price 1', editable: false},
          // {dataField: 'Level2Qty', header: 'Level 2', editable: false},
          // {dataField: 'L2UnitPrice', header: 'Price 2', editable: false},
          {dataField: 'AmountDebt', header: 'Sub Total', editable: false},
          {dataField: 'IsPaid', header: 'Is Paid', dataFormat: (cell, row) => {
            let value;
            
            if (!cell || cell === '0' || cell === 'False') {
              value = 'No';
            } else {
              value = 'Yes';
            }

            return (
              <span>{value}</span>
            );
          }},
        ];

        const actions = (
          <TableHeaderColumn 
            dataField="action"            
            width="100px"             
            dataFormat={(cell, row) => {
              return (
                <span>
                  <a className="btn btn-xs btn-warning" title="Edit" 
                    onClick={(e) => {e.preventDefault(); this.handleEditData(row);}}>
                    <i className="glyphicon glyphicon-edit"/>
                  </a>              
                  <a className="btn btn-xs btn-default" title="Download PDF" 
                    onClick={(e) => {e.preventDefault(); this.handleProcessedClick('print', row);}}>
                    <i className="fa fa-file-pdf-o"/>
                  </a>
                  {
                    row.Email ? ( 
                    <a className="btn btn-xs btn-default" title="Email Invoice" 
                      onClick={(e) => {e.preventDefault(); this.handleProcessedClick('email', row);}}>
                      <i className="fa fa-envelope"/>
                    </a>) : null
                  }
                </span>
              );
          }}>
            Actions
          </TableHeaderColumn>
        );   
        content = (      
          <div>
            <Tabs activeKey={this.state.key} onSelect={this.handleSelect} id="controlled-tab-example">
              <Tab eventKey={1} title="By Date">
                <div className="well">
                  <Row>
                    <Column column="3">
                      <InputGroup 
                        label="From Date" 
                        placeholder="From Date"
                        type="datepicker" 
                        inputName="fromDate"
                        onValueChange={(value) => {this.handleInputChange('fromDate', value);}}
                        value={this.state.fromDate}/>
                    </Column>
                    <Column column="3">
                      <InputGroup 
                        label="To Date" 
                        placeholder="To Date"
                        type="datepicker" 
                        inputName="toDate"
                        onValueChange={(value) => {this.handleInputChange('toDate', value);}}
                        value={this.state.toDate}/>
                    </Column>                  
                    <Column column="6">
                      <a className="btn btn-success" onClick={(e) => this.fetchList('date')}>Fetch</a>
                    </Column>
                  </Row>
                </div>
              </Tab>
              <Tab eventKey={4} title="By Invoice Number">
                <div className="well">
                  <Row>
                    <Column column="3">
                      <InputGroup 
                        label="From" 
                        placeholder="From Invoice"
                        type="text" 
                        inputName="fromInvoice"
                        onValueChange={(value) => {this.handleInputChange('fromInvoice', value);}}
                        value={this.state.fromInvoice}/>                  
                    </Column>
                    <Column column="3" >
                      <InputGroup 
                        label="To" 
                        placeholder="To Invoice"
                        type="text" 
                        inputName="toInvoice"
                        onValueChange={(value) => {this.handleInputChange('toInvoice', value);}}
                        value={this.state.toInvoice}/>                      
                    </Column>
                    <Column column="6">
                      <a className="btn btn-success" onClick={(e) => this.fetchList('byInvoiceNo')}>Fetch</a>
                    </Column>
                  </Row>
                </div>
              </Tab>            
            </Tabs>
            <div className={this.state.classAdvancedSearch}>
              <Row>
                <Column column="3">
                  <InputGroup 
                    label="Complex" 
                    placeholder="Complex"
                    type="text" 
                    inputName="complexName"
                    onValueChange={(value) => {this.filterList('complexName', value);}}
                    value={this.state.complexName}/>                    
                </Column>
                <Column column="3">
                  <InputGroup 
                    label="Customer" 
                    placeholder="Customer"
                    type="text" 
                    inputName="billingNames"
                    onValueChange={(value) => {this.filterList('billingNames', value);}}
                    value={this.state.billingNames}/>      
                </Column>                 
              </Row>            
            </div>
          
            <div className="col-md-12">
              <div className="col-md-offset-8 col-md-4" style={{textAlign: 'right'}}>
                <a className="btn btn-info" title="Print All to PDF Bundle"
                  onClick={(e) => {
                    e.preventDefault(); 
                    this.handleProcessedClick('printall', this.state.listProcessed);
                  }}>
                  <i className="fa fa-file-pdf-o"/> All
                </a>
                <a className="btn btn-info" title="Print All Without Email"
                  onClick={(e) => {
                    e.preventDefault(); 
                    this.handleProcessedClick('printall', 
                        this.state.listProcessed.filter(x => x.Email !== ''));
                  }}>
                  <i className="fa fa-file-pdf-o"/> All Without <i className="fa fa-envelope"/>
                </a>
                <a className="btn btn-info" title="Email All"
                  onClick={(e) => {
                    e.preventDefault(); 
                    this.handleProcessedClick('emailall', this.state.listProcessed);
                  }}>
                  <i className="fa fa-envelope"/> All
                </a>
              </div>
            </div>
          <div>
            <Listing 
              data={this.state.listProcessed}
              columns={columnsProcessed} 
              onRefreshData={null}
              onEditData={null}
              onDeleteData={null}
              withActions={true}
              actions={actions}
              withNumber={false}/>
          </div>
        </div> 
        );
      break;
      case PageMode.edit: 
        content = <InvoiceForm data={this.state.editData} 
          onSave={this.handleFormSave} onCancel={this.handleFormCancel}/>;        
      break;

      default: 
        content = null;
    }    

    return content;
  }

  render() {

    const brand = (
      <span>
        <i className="glyphicon glyphicon-list-alt"/> Invoices
      </span>
    );    

    const content = this.getContentFromMode(this.state.mode);
    return (
      <div style={{margin: '10px'}}>
        <Alert ref={x => this.alertRef = x} />
        <Loadable isLoading={this.state.loading} text={this.state.loadingText}>
          <SimpleHeader 
            brand={brand}
            listLabel="List"
            newLabel={null}
            onRightItemClick={this.handleRightItemClick}
            mode={this.state.mode}
          />
          {content}
        </Loadable>
      </div>
    );
  }
}

function mapStateToProps(state: any): IConnectedProps {
  return {
    pageCode: state.app.pageCode,
    editData: state.app.editData,
  };
}

function mapDispatchToProps(dispatch): IConnectedDispatch {
  return {
    onSetPageCode: (value: string, settings) => dispatch(setPage(value, settings))
  };
}

export default 
  connect(mapStateToProps, mapDispatchToProps)(Invoices) as React.ComponentClass<IInvoicesProps>;
