import * as React from 'react';
import Loadable from '../../components/Loadable';
import { Panel } from '../../../ascendis-ui/containers';

import { Row, Column } from '../../../ascendis-ui/layout';
import { InputGroup } from '../../../ascendis-ui/components';

export interface IPreviewInvoiceFormProps {
  data?: any;
  onSave: Function;
  onCancel: Function;
}

export interface IPreviewInvoiceFormState {
  adjAmountTemp: string;
  adjDescriptionTemp: string;
  loading: boolean;
  unitCode: string;
}

class PreviewInvoiceForm extends React.Component<IPreviewInvoiceFormProps, IPreviewInvoiceFormState> {
  constructor(props) {
    super(props);

    this.state = {
      data: {},
      adjAmountTemp: '',
      adjDescriptionTemp: '',
      loading: true,
      unitCode: '',      
    } as IPreviewInvoiceFormState;

    const isEdit = this.props.data ? true : false;

    if (isEdit) {      
      
      const { AdjAmountTemp, AdjDescriptionTemp, UnitCode } = this.props.data;      
      this.state = {
        data: {},
        adjAmountTemp: AdjAmountTemp,
        adjDescriptionTemp: AdjDescriptionTemp,
        loading: false,
        unitCode: UnitCode
      } as IPreviewInvoiceFormState;
    }

    this.handleInputChange = this.handleInputChange.bind(this);    
  }

  componentDidMount() {    
  }
  
  handleInputChange(prop, value) {
    let state = {};
    state[prop] = value;

    this.setState(state as IPreviewInvoiceFormState);
  }

  render() {
    const isEdit = this.props.data && this.props.data[0] ? true : false;

    return (
      <Loadable isLoading={this.state.loading} text={'Loading Form...'}>
        <Panel title="Adjustment Form">
          <div id="PreviewInvoiceForm" >
            <Row>
              <Column column="6">
                <InputGroup 
                  label="Adjustment Amount" 
                  placeholder="Adjustment Amount" 
                  inputName="adjAmountTemp"
                  onValueChange={(value) => {this.handleInputChange('adjAmountTemp', value);}}                  
                  value={this.state.adjAmountTemp}
                  dataValidation="required"/> 
              </Column>
            </Row>  

            <Row>
              <Column column="6">
                <InputGroup 
                  type="textarea"
                  label="Description" 
                  placeholder="Adjustment Description" 
                  inputName="adjDescriptionTemp"
                  onValueChange={(value) => {this.handleInputChange('adjDescriptionTemp', value);}}
                  value={this.state.adjDescriptionTemp}                    
                  dataValidation="required"/> 
              </Column>              
            </Row>

            <div style={{textAlign: 'right', background: '#f2f2f2', padding: '10px'}}>
              <a className="btn btn-success"  onClick={(e) => { this.props.onSave(this.state); }}>Save</a>
              <a className="btn btn-danger"  onClick={(e) => {this.props.onCancel();} }>Cancel</a>
            </div>            
          </div>
        </Panel>
      </Loadable>
    );
  }
}

export default PreviewInvoiceForm;
