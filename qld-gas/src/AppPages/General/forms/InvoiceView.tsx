import * as React from 'react';
import Loadable from '../../components/Loadable';
import { Panel } from '../../../ascendis-ui/containers';

import { Row, Column } from '../../../ascendis-ui/layout';
import { InputGroup, Dropdown } from '../../../ascendis-ui/components';
import * as utils from '../../../App/utils';

export interface IInvoiceViewProps {  
  onSave: Function;
  onCancel: Function;
}

export interface IInvoiceViewState {
  rentalCode: string;
  dataRentalCode: any;
  finalReading: boolean;
  title: string;
  givenNames: string;
  surname: string;
  billAddress1: string;
  billAddress2: string;
  billAddress3: string;
  customerNo: string;
  openingBalance: string;
  weRecieved: string;
  thisInvoice: string;
  lessSecurityDep: string;
  roundingAdj: string;
  amountOwing: string;
  invoiceDate: string;
  invoiceNo: string;
  dueDate: string;
  securityDeposit: string;
  propertyLocation: string;
  complexNo: string;
  complexName: string;
  unitAddress1: string;
  unitAddress2: string;
  unitAddress3: string;
  itemCode: string;
  lastReading: string;
  level1Quantity: string;
  level2Quantity: string;
  description: string;
  currentReading: string;
  level1UnitPrice: string;
  level2UnitPrice: string;
  usageQuantity: string;
  level1Price: string;
  level2Price: string;
  totalPrice: string;
  loading: boolean;

}

class InvoiceViewForm extends React.Component<IInvoiceViewProps, IInvoiceViewState> {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      rentalCode: '',
      dataRentalCode: [],
      finalReading: false,
      title: '',
      givenNames: '',
      surname: '',
      billAddress1: '',
      billAddress2: '',
      billAddress3: '',
      customerNo: '',
      openingBalance: '',
      weRecieved: '',
      thisInvoice: '',
      lessSecurityDep: '',
      roundingAdj: '',
      amountOwing: '',
      invoiceDate: '',
      invoiceNo: '',
      dueDate: '',
      securityDeposit: '',
      propertyLocation: '',
      complexNo: '',
      complexName: '',
      unitAddress1: '',
      unitAddress2: '',
      unitAddress3: '',
      itemCode: '',
      lastReading: '',
      level1Quantity: '',
      level2Quantity: '',
      description: '',
      currentReading: '',
      level1UnitPrice: '',
      level2UnitPrice: '',
      usageQuantity: '',
      level1Price: '',
      level2Price: '',
      totalPrice: '',
    };

    this.handleInputChange = this.handleInputChange.bind(this);    
    this.handleFinalReadingChange = this.handleFinalReadingChange.bind(this);
    this.getRental = this.getRental.bind(this);
    this.fetchInvoice = this.fetchInvoice.bind(this);
  }  

  componentWillMount() {    
    this.getRental('', true);
  }  

  handleInputChange(prop, value) {
    let state = {};
    state[prop] = value;
    this.setState(state as IInvoiceViewState);

    if (prop === 'rentalCode') {
      this.fetchInvoice(value);
    }    
    
  }

   handleFinalReadingChange(value) {
    this.setState({
      finalReading: value,
    });
  }


  getRental(search, firstLoad = false) {
    if (search.length < 1) {
      this.setState({
        dataRentalCode: [{label: 'Type at least 1 characters to fetch', value: null}],
      } as IInvoiceViewState);
      return;
    }

    if (search.value === null) {
      return;
    }

    if (search.length > 3 && !firstLoad) {
      return;
    }
    
    const { fetchTaxInvoice } = utils;
    
    Promise.all([fetchTaxInvoice(search)]).then(
      (response: any) => {
        const dataRentalCode = response[0].data;

        let nextState = {
          dataRentalCode,
          loading: false,
        };

        this.setState( nextState as IInvoiceViewState);
      }
    ).catch(
      (ex) => {
        alert(ex);
      }
    );

    this.setState({
      dataRentalCode: [{label: 'Fetching, please wait...', value: null}],
    } as IInvoiceViewState);
  }

  fetchInvoice(rentalCode) {

    if (!rentalCode) {
      return;
    }    

    this.setState({
      loading: true,
    });

    let formData = new FormData();
    formData.append('Action', 'Fetch');
    formData.append('Path', 'InvoiceView/');
    formData.append('RentalCode', rentalCode);

    utils.fetch(formData).then(
      (response: any) => {

        const data = response.data[0]; 
        const totPrice = (data.Amount * data.FinalUnitPrice).toString();

        this.setState({
          rentalCode: data.RentalCode,          
          finalReading: data.FinalReading === 'False' ? false : true,
          title: data.Title, 
          givenNames: data.GivenNames,
          surname: data.Surname,
          //billAddress1: data.Address1,
          //billAddress2: data.Address2,
          //billAddress3: data.Address3,
          customerNo: data.Custno, 
          //openingBalance: data.OpeningBalance, 
          //weRecieved: data.WeRecieved, 
          //thisInvoice: data.ThisInvoice,
          //lessSecurityDep: data.LessSecurityDep, 
          //roundingAdj: data.RoundingAdj, 
          //amountOwing: data.AmountOwing, 
          invoiceDate: data.Date, 
          invoiceNo: data.InvoiceNo,
          dueDate: data.DueDate, 
          securityDeposit: data.SecDeposit, 
          propertyLocation: data.NoUnits,
          complexNo: data.ComplexNo,
          complexName: data.ComplexName,
          unitAddress1: data.Address1,
          unitAddress2: data.Address2,
          unitAddress3: data.Address3, 
          itemCode: data.ItemCode, 
          lastReading: data.LastReading, 
          level1Quantity: data.Level1Qty, 
          level2Quantity: data.Level2Qty, 
          description: data.Description,
          currentReading: data.CurReading,
          level1UnitPrice: data.L1UnitPrice, 
          level2UnitPrice: data.L2UnitPrice, 

          //usageQuantity: data.UsageQuantity, 
          //level1Price: data.Level1Price,
          //level2Price: data.Level2Price, 
          totalPrice: totPrice,
          loading: false,
        } as IInvoiceViewState);
      }
    );

  }

  render() {
    return (
      <Loadable isLoading={this.state.loading} text={'Loading Form...'}>
        <Panel title="Tax Invoices">
          <Row>
            <Column column="8">
              <Dropdown 
                list={this.state.dataRentalCode}
                label="Rental Code" 
                inputName="Rental Code"
                onValueChange={(value) => {this.handleInputChange('rentalCode', value.value);}}
                value={this.state.rentalCode} 
                onInputChange={this.getRental}
              />
              <InputGroup type="checkbox" 
               inputName="finalReading" 
               placeholder="Title" 
               label="Final Reading(Y/N)?" 
               onValueChange={this.handleFinalReadingChange}
               value={this.state.finalReading.toString()}
              />
            </Column>
            <Column column="4">
                  <InputGroup 
                  label="Customer No" 
                  placeholder="Customer No" 
                  inputName="customerNo"
                  onValueChange={(value) => {this.handleInputChange('customerNo', value);}}
                  value={this.state.customerNo}/>
            </Column>
          </Row>
          <Row>
          <Column column="8">
                 <InputGroup 
                label="Title" 
                placeholder="Title" 
                inputName="title"
                onValueChange={(value) => {this.handleInputChange('title', value);}}
                value={this.state.title}/>
                 <InputGroup 
                label="Given Names" 
                placeholder="Given Names" 
                inputName="givenNames"
                onValueChange={(value) => {this.handleInputChange('givenNames', value);}}
                value={this.state.givenNames}/>
                 <InputGroup 
                label="Surname" 
                placeholder="Surname" 
                inputName="surname"
                onValueChange={(value) => {this.handleInputChange('surname', value);}}
                value={this.state.surname}/>
          </Column>
          <Column column="4">
                 <InputGroup 
                label="Opening Balance" 
                placeholder="Opening Balance" 
                inputName="openingBalance"
                onValueChange={(value) => {this.handleInputChange('openingBalance', value);}}
                value={this.state.openingBalance}/>
          </Column>
        </Row>
          <Row>
          <Column column="8">
                 <InputGroup 
                label="Address 1" 
                placeholder="Address 1" 
                inputName="unitAddress1"
                onValueChange={(value) => {this.handleInputChange('unitAddress1', value);}}
                value={this.state.unitAddress1}/>
          </Column>
          <Column column="4">
                 <InputGroup 
                label="We Recieved" 
                placeholder="We Recieved" 
                inputName="weRecieved"
                onValueChange={(value) => {this.handleInputChange('weRecieved', value);}}
                value={this.state.weRecieved}/>
          </Column>
        </Row>
          <Row>
          <Column column="8">
                 <InputGroup 
                label="Address 2" 
                placeholder="Address 2" 
                inputName="unitAddress2"
                onValueChange={(value) => {this.handleInputChange('unitAddress2', value);}}
                value={this.state.unitAddress2}/>
          </Column>
          <Column column="4">
                 <InputGroup 
                label="This Invoice" 
                placeholder="This Invoice" 
                inputName="thisInvoice"
                onValueChange={(value) => {this.handleInputChange('thisInvoice', value);}}
                value={this.state.thisInvoice}/>
          </Column>
        </Row>
          <Row>
          <Column column="8">
                 <InputGroup 
                label="Address 3" 
                placeholder="Address 3" 
                inputName="unitAddress3"
                onValueChange={(value) => {this.handleInputChange('unitAddress3', value);}}
                value={this.state.unitAddress3}/>
          </Column>
          <Column column="4">
              <InputGroup 
                label="Less Security Dep" 
                placeholder="Less Security Dep" 
                inputName="lessSecurityDep"
                onValueChange={(value) => {this.handleInputChange('lessSecurityDep', value);}}
                value={this.state.lessSecurityDep}/>
          </Column>
        </Row>
          <Row>
          <Column column="8"/>
          <Column column="4">
              <InputGroup 
                label="Rounding Adj" 
                placeholder="Rounding Adj" 
                inputName="roundingAdj"
                onValueChange={(value) => {this.handleInputChange('roundingAdj', value);}}
                value={this.state.roundingAdj}/>
          </Column>
        </Row>
          <Row>
          <Column column="8"/>
          <Column column="4">
              <InputGroup 
                label="Amount Owing" 
                placeholder="Amount Owing" 
                inputName="amountOwing"
                onValueChange={(value) => {this.handleInputChange('amountOwing', value);}}
                value={this.state.amountOwing}/>
          </Column>
        </Row>
          <Row>
            <Column column="4">
              <InputGroup 
                label="Invoice Date" 
                placeholder="Invoice Date" 
                inputName="invoiceDate"
                onValueChange={(value) => {this.handleInputChange('invoiceDate', value);}}
                value={this.state.invoiceDate}/>
            </Column>
            <Column column="4">
              <InputGroup 
                label="Invoice No" 
                placeholder="Invoice No" 
                inputName="invoiceNo"
                onValueChange={(value) => {this.handleInputChange('invoiceNo', value);}}
                value={this.state.invoiceNo}/> 
            </Column>
            <Column column="4">
              <InputGroup 
                label="Due Date" 
                placeholder="Due Date" 
                inputName="dueDate"
                onValueChange={(value) => {this.handleInputChange('dueDate', value);}}
                value={this.state.dueDate}/> 
            </Column>
            <Column column="4">
              <InputGroup 
                label="Security Deposit" 
                placeholder="Security Deposit" 
                inputName="securityDeposit"
                onValueChange={(value) => {this.handleInputChange('securityDeposit', value);}}
                value={this.state.securityDeposit}/> 
            </Column>
          </Row>
          <Row>
            <Column column="8">
              <InputGroup 
                label="Property Location" 
                placeholder="Property Location" 
                inputName="complexNo"
                onValueChange={(value) => {this.handleInputChange('complexNo', value);}}
                value={this.state.complexNo}/> 
            </Column>
          </Row>
          <Row>
            <Column column="8">
              <InputGroup 
                label="" 
                placeholder="Name" 
                inputName="complexName"
                onValueChange={(value) => {this.handleInputChange('complexName', value);}}
                value={this.state.complexName}/> 
            </Column>
          </Row>
          <Row>
            <Column column="8">
              <InputGroup 
                label="" 
                placeholder="Address 1" 
                inputName="unitAddress1"
                onValueChange={(value) => {this.handleInputChange('unitAddress1', value);}}
                value={this.state.unitAddress1}/> 
            </Column>
          </Row>
          <Row>
            <Column column="8">
              <InputGroup 
                label="" 
                placeholder="Address 2" 
                inputName="unitAddress2"
                onValueChange={(value) => {this.handleInputChange('unitAddress2', value);}}
                value={this.state.unitAddress2}/> 
            </Column>
          </Row>
          <Row>
            <Column column="8">
              <InputGroup 
                label="" 
                placeholder="Unit Address 3" 
                inputName="unitAddress3"
                onValueChange={(value) => {this.handleInputChange('unitAddress3', value);}}
                value={this.state.unitAddress3}/> 
            </Column>
          </Row>
          <Row>
            <Column column="4">
              <InputGroup 
                label="Item Code" 
                placeholder="Item Code" 
                inputName="itemCode"
                onValueChange={(value) => {this.handleInputChange('itemCode', value);}}
                value={this.state.itemCode}/>
            </Column>
            <Column column="8">
              <InputGroup 
                label="Description" 
                placeholder="Description" 
                inputName="description"
                onValueChange={(value) => {this.handleInputChange('description', value);}}
                value={this.state.description}/> 
            </Column>
          </Row>
          <Row>
            <Column column="4">
              <InputGroup 
                label="Last Reading" 
                placeholder="Last Reading" 
                inputName="lastReading"
                onValueChange={(value) => {this.handleInputChange('lastReading', value);}}
                value={this.state.lastReading}/>
            </Column>
            <Column column="4">
              <InputGroup 
                label="Current Reading" 
                placeholder="Current Reading" 
                inputName="currentReading"
                onValueChange={(value) => {this.handleInputChange('currentReading', value);}}
                value={this.state.currentReading}/> 
            </Column>
            <Column column="4">
              <InputGroup 
                label="Usage Quantity" 
                placeholder="Usage Quantity" 
                inputName="usageQuantity"
                onValueChange={(value) => {this.handleInputChange('usageQuantity', value);}}
                value={this.state.usageQuantity}/> 
            </Column>
          </Row>
          <Row>
            <Column column="4">
              <InputGroup 
                label="Level 1 Quantity" 
                placeholder="Level 1 Quantity" 
                inputName="level1Quantity"
                onValueChange={(value) => {this.handleInputChange('level1Quantity', value);}}
                value={this.state.level1Quantity}/>
            </Column>
            <Column column="4">
              <InputGroup 
                label="Level 1 Unit Price" 
                placeholder="Level 1 Unit Price" 
                inputName="level1UnitPrice"
                onValueChange={(value) => {this.handleInputChange('level1UnitPrice', value);}}
                value={this.state.level1UnitPrice}/> 
            </Column>
            <Column column="4">
              <InputGroup 
                label="Level 1 Price" 
                placeholder="Level 1 Price" 
                inputName="level1Price"
                onValueChange={(value) => {this.handleInputChange('level1Price', value);}}
                value={this.state.level1Price}/> 
            </Column>
          </Row>
          <Row>
            <Column column="4">
              <InputGroup 
                label="Level 2 Quantity" 
                placeholder="Level 2 Quantity" 
                inputName="level2Quantity"
                onValueChange={(value) => {this.handleInputChange('level2Quantity', value);}}
                value={this.state.level2Quantity}/>
            </Column>
            <Column column="4">
              <InputGroup 
                label="Level 2 Unit Price" 
                placeholder="Level 2 Unit Price" 
                inputName="level2UnitPrice"
                onValueChange={(value) => {this.handleInputChange('level2UnitPrice', value);}}
                value={this.state.level2UnitPrice}/> 
            </Column>
            <Column column="4">
              <InputGroup 
                label="Level 2 Price" 
                placeholder="Level 2 Price" 
                inputName="level2Price"
                onValueChange={(value) => {this.handleInputChange('level2Price', value);}}
                value={this.state.level2Price}/> 
            </Column>
          </Row>
          <Row>
            <Column column="4">
              <InputGroup 
                label="Total Price" 
                placeholder="Total Price" 
                inputName="totalPrice"
                onValueChange={(value) => {this.handleInputChange('totalPrice', value);}}
                value={this.state.totalPrice}/> 
            </Column>
          </Row>            
          <div style={{textAlign: 'right', background: '#f2f2f2', padding: '10px'}}>
            <a className="btn btn-success"  onClick={(e) => {this.props.onSave();}}>Save</a>
            <a className="btn btn-danger"  onClick={(e) => {this.props.onCancel();} }>Cancel</a>
          </div>
        </Panel>
      </Loadable>
    );
  }
}

export default InvoiceViewForm;
