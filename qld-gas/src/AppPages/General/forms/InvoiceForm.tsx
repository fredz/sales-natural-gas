import * as React from 'react';
import Loadable from '../../components/Loadable';
import * as utils from '../../../App/utils';
import { Panel } from '../../../ascendis-ui/containers';

import { Row, Column } from '../../../ascendis-ui/layout';
import { InputGroup } from '../../../ascendis-ui/components';

export interface IInvoiceFormProps {
  data?: any;
  onSave: Function;
  onCancel: Function;
}

export interface IInvoiceFormState {
  invoiceNo: string;
  billingNames: string;
  complexName: string;
  lastReading: string;
  curReading: string;
  level1Qty: string;
  l1UnitPrice: string;
  subTotal: string;
  loading: boolean;
  data: any;
}

class InvoiceForm extends React.Component<IInvoiceFormProps, IInvoiceFormState> {
  constructor(props) {
    super(props);

    this.state = {
      data: {},
      invoiceNo: '',
      billingNames: '',
      complexName: '',
      lastReading: '0',
      curReading: '0',
      subTotal: '0',
      l1UnitPrice: '0',
      loading: true,
      level1Qty: '0',
    } as IInvoiceFormState;

    const isEdit = this.props.data ? true : false;

    if (isEdit) {
      const { InvoiceNo, BillingNames, ComplexName, LastReading, CurReading, 
        L1UnitPrice, Level1Qty } = this.props.data;
      
      this.state = {
        data: {},
        invoiceNo: InvoiceNo,
        billingNames: BillingNames,
        complexName: ComplexName,
        lastReading: LastReading,
        curReading: CurReading,
        l1UnitPrice: L1UnitPrice,        
        level1Qty: Level1Qty,
        loading: false,
      } as IInvoiceFormState;
    }

    this.handleInputChange = this.handleInputChange.bind(this);
    this.verifydata = this.verifydata.bind(this);    
    this.calculateSubTotal = this.calculateSubTotal.bind(this);    
  }

  componentDidMount() {
    this.calculateSubTotal();
  }

  calculateSubTotal() {
    let value;   

    if (!isNaN(parseFloat(this.state.level1Qty)) && !isNaN(parseFloat(this.state.curReading)) 
    && !isNaN(parseFloat(this.state.l1UnitPrice))) {

      let readingValue = ( parseFloat(this.state.curReading) - parseFloat(this.state.lastReading) );
      value = readingValue * parseFloat(this.state.l1UnitPrice);

      let subTotal = (Math.round(value * 1000) / 1000).toString();

      this.setState({
        subTotal
      } as IInvoiceFormState);
    }
  }
  
  handleInputChange(prop, value) {
    let state = {};
    state[prop] = value;

    this.setState(state as IInvoiceFormState, () => {
      this.calculateSubTotal();
    });
  }

  verifydata() {
    const isEdit = this.props.data && this.props.data[0] ? true : false;

    this.props.onSave(this.state, isEdit, isEdit ? this.props.data[0].CustTennentsID : null);
  }

  render() {
    const isEdit = this.props.data && this.props.data[0] ? true : false;

    return (
      <Loadable isLoading={this.state.loading} text={'Loading Form...'}>
        <Panel title="Invoice Form">
          <div id="InvoiceForm" >
            <Row>
              <Column column="6">
                <InputGroup 
                  label="Invoice No." 
                  placeholder="Invoice No." 
                  inputName="invoiceNo"
                  onValueChange={(value) => {this.handleInputChange('invoiceNo', value);}}
                  value={this.state.invoiceNo}
                  disabled={true}                  
                  dataValidation="required"/> 
              </Column>
              <Column column="6">
                <InputGroup 
                  label="Billing Names" 
                  placeholder="Billing Names" 
                  inputName="billingNames"
                  onValueChange={(value) => {this.handleInputChange('billingNames', value);}}
                  value={this.state.billingNames}  
                  disabled={true}                                  
                  dataValidation="required"/> 
              </Column>            
            </Row>
            <Row>
              <Column column="6">
                <InputGroup 
                  label="Complex Name" 
                  placeholder="Complex Name" 
                  inputName="complexName"
                  onValueChange={(value) => {this.handleInputChange('complexName', value);}}                  
                  value={this.state.complexName}
                  disabled={true}
                  />
              </Column>
              <Column column="6">
                <InputGroup 
                  label="Last Reading" 
                  placeholder="Last Reading" 
                  inputName="lastReading"
                  onValueChange={(value) => {this.handleInputChange('lastReading', value);}}
                  value={this.state.lastReading}
                  dataValidation="required"
                  />
              </Column>            
            </Row>
            <Row>
              <Column column="6">
                <InputGroup                   
                  label="This Reading" 
                  placeholder="This Reading" 
                  inputName="curReading"
                  onValueChange={(value) => {this.handleInputChange('curReading', value);}}
                  value={this.state.curReading}
                  dataValidation="required"
                  />
              </Column>
              <Column column="6">
                <InputGroup 
                  label="Reading" 
                  placeholder="Reading" 
                  inputName="level1Qty"                  
                  onValueChange={(value) => {this.handleInputChange('level1Qty', value);}}
                  value={this.state.level1Qty}
                  disabled={true}
                  dataValidation="required"                  
                  />
              </Column>            
            </Row>
            <Row>
              <Column column="6">
                <InputGroup                   
                  label="Price" 
                  placeholder="Price" 
                  inputName="l1UnitPrice"
                  onValueChange={(value) => {this.handleInputChange('l1UnitPrice', value);}}
                  value={this.state.l1UnitPrice}
                  dataValidation="required"
                  />
              </Column>
              <Column column="6">
                <InputGroup 
                  label="Sub Total" 
                  placeholder="Sub Total" 
                  inputName="subTotal"                  
                  value={this.state.subTotal}
                  disabled={true}                  
                  dataValidation="required"                  
                  />
              </Column>            
            </Row>            

            <div style={{textAlign: 'right', background: '#f2f2f2', padding: '10px'}}>
              <a className="btn btn-success"  onClick={(e) => {this.verifydata();}}>Save</a>
              <a className="btn btn-danger"  onClick={(e) => {this.props.onCancel();} }>Cancel</a>
            </div>            
          </div>
        </Panel>
      </Loadable>
    );
  }
}

export default InvoiceForm;
