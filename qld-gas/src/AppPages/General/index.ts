import Dashboard from './Dashboard';
import MeterReading from './MeterReading';
import BottleGasDelivery from './BottleGasDelivery';
import TaxInvoice from './TaxInvoice';
import PreviewInvoices from './PreviewInvoices';
import Invoices from './Invoices';

export default {
  Dashboard,
  MeterReading,
  BottleGasDelivery,
  TaxInvoice,
  PreviewInvoices,
  Invoices
};