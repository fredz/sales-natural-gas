import * as React from 'react';
import { InputGroup } from '../../../ascendis-ui/components';

export interface IDatePickerEditorProps {
  onUpdate: Function;
  defaultValue: string;
}

export interface IDatePickerEditorState {
  date: string;
}

export default class DatePickerEditor extends React.Component<IDatePickerEditorProps, IDatePickerEditorState> {
  refs: {
    inputRef: any;
  };
  
  constructor(props) {
    super(props);

    this.state = {
      date: this.props.defaultValue
    };

    this.updateData = this.updateData.bind(this);
  }

  public focus() {
    
  }

  updateData(name, value) {
    this.props.onUpdate({ 
      date: value,
    });
  }

  render() {
    return (
      <span>
        <InputGroup 
          ref="inputRef"
          type="datepicker" 
          label="" 
          placeholder="" 
          inputName="date"
          onValueChange={(value) => {this.updateData('date', value);}}
          value={this.state.date}/>
      </span>
    );
  }
}

export const createDatePickerEditor = (onUpdate, props) => (<DatePickerEditor onUpdate={ onUpdate } {...props}/>);