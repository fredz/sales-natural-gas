import * as React from 'react';

export enum ActionsHeaderMode {
  Normal,
  Delete,
  Edit,
}

export interface IActionHeaderProps {
  color?: string;
  isDelete: boolean;
  isEdit: boolean;
  onConfirm: Function;
  onCancel: Function; 
  onModeChange: Function;
}

export interface IActionHeaderState {

}

class ActionsHeader extends React.Component<IActionHeaderProps, IActionHeaderState> {
  constructor(props) {
    super(props);

    this.getMode = this.getMode.bind(this);
    this.getText = this.getText.bind(this);

    this.getDeleteComponent = this.getDeleteComponent.bind(this);
    this.getEditComponent = this.getEditComponent.bind(this);
    this.handleActionDelete = this.handleActionDelete.bind(this);
    this.handleActionEdit = this.handleActionEdit.bind(this);
  }

  getMode(isDelete, isEdit) {
    if (!isDelete && !isEdit) {
      return ActionsHeaderMode.Normal;
    }

    if (isDelete && !isEdit) {
      return ActionsHeaderMode.Delete;
    }

    if (!isDelete && isEdit) {
      return ActionsHeaderMode.Edit;
    }

    return ActionsHeaderMode.Normal;
  }

  getText(mode: ActionsHeaderMode) {
    let text;
    switch (mode) {
      case ActionsHeaderMode.Normal:
        text = 'View Mode';
        break;
      case ActionsHeaderMode.Delete:
        text = 'Delete Mode';
        break;
      case ActionsHeaderMode.Edit:
        text = 'Edit Mode';
        break;
      default: 
        text = 'View Mode';
    }

    return text;
  }

  handleActionDelete(isConfirm = false) {
    if (isConfirm) {
      this.props.onConfirm(ActionsHeaderMode.Delete);
    } else {
      this.props.onCancel(ActionsHeaderMode.Delete);
    }
  }

  handleActionEdit(isConfirm = false) {
    if (isConfirm) {
      this.props.onConfirm(ActionsHeaderMode.Edit);
    } else {
      this.props.onCancel(ActionsHeaderMode.Edit);
    }
  }

  modeChange(mode) {
    this.props.onModeChange(mode);
  }

  getDeleteComponent(mode: ActionsHeaderMode) {
    if (mode === ActionsHeaderMode.Delete) {
      return (
        <span>
          <a 
            className="btn btn-xs btn-danger navbar-btn" 
            onClick={(e) => { e.preventDefault(); this.handleActionDelete(true); }}>
            <i className="glyphicon glyphicon-ok"/> Confirm
          </a>
          <a 
            className="btn btn-xs btn-success navbar-btn" 
            onClick={(e) => { e.preventDefault(); this.handleActionDelete(); }}>
            <i className="glyphicon glyphicon-remove"/> Cancel
          </a>
        </span>
      );
    } else if (mode === ActionsHeaderMode.Normal) { 
      return (
        <span>
          <a 
            className="btn btn-xs btn-danger navbar-btn" 
            onClick={(e) => { e.preventDefault(); this.modeChange(ActionsHeaderMode.Delete); }}>
            <i className="glyphicon glyphicon-trash"/> Delete mode
          </a>
        </span>
      );
    } else {
      return null;
    }
  }

  getEditComponent(mode: ActionsHeaderMode) {
    if (mode === ActionsHeaderMode.Edit) {
      return (
        <span>
          <a 
            className="btn btn-xs btn-danger navbar-btn" 
            onClick={(e) => { e.preventDefault(); this.handleActionEdit(true); }}>
            <i className="glyphicon glyphicon-ok"/> Confirm
          </a>
          <a 
            className="btn btn-xs btn-success navbar-btn" 
            onClick={(e) => { e.preventDefault(); this.handleActionEdit(); }}>
            <i className="glyphicon glyphicon-remove"/> Cancel
          </a>
        </span>
      );
    } else if (mode === ActionsHeaderMode.Normal) { 
      return (
        <span>
          <a 
            className="btn btn-xs btn-warning navbar-btn" 
            onClick={(e) => { e.preventDefault(); this.modeChange(ActionsHeaderMode.Edit); }}>
            <i className="glyphicon glyphicon-edit"/> Edit mode
          </a>
        </span>
      );
    } else {
      return null;
    }
  }

  render() {
    let { color } = this.props;
    const { isEdit, isDelete } = this.props;

    const mode = this.getMode(isDelete, isEdit);

    const text = this.getText(mode);

    const deleteButton = this.getDeleteComponent(mode);
    // const editButton = this.getEditComponent(mode);

    if (!color) {
      color = '#444';
    }

    return (
      <nav className="navbar navbar-default" style={{background:color}}>
        <div className="container-fluid">
            <div className="navbar-form navbar-left">
              <span className="navbar-text">{text}</span>
            </div>
            <div className="navbar-form navbar-right">
              {deleteButton}
              {/*editButton*/}
            </div>
          </div>
        </nav>
    );
  }
}

export default ActionsHeader;
