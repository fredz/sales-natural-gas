import * as React from 'react';

import DashboardPanel, { IDashboardPanelProps } from './DashboardPanel';

export interface IDashboardPanels {
  dashboards: Array<IDashboardPanelProps>;
}

class DashboardPanels extends React.Component<IDashboardPanels, any> {
  constructor(props) {
    super(props);
  }

  render() {
    const { dashboards } = this.props;

    if (dashboards.length === 0) {
      return null;
    }

    return (
      <div style={{width: '90%', margin: 'auto',}}>
        <div className="col-md-4">
          <DashboardPanel icon={dashboards[0].icon} color={dashboards[0].color}
            clickLabel={dashboards[0].clickLabel} numberText={dashboards[0].numberText}
            subtext={dashboards[0].subtext} onClick={dashboards[0].onClick} pageCode={dashboards[0].pageCode}/>
        </div>
        <div className="col-md-4">
          <DashboardPanel icon={dashboards[1].icon} color={dashboards[1].color}
            clickLabel={dashboards[1].clickLabel} numberText={dashboards[1].numberText}
            subtext={dashboards[1].subtext} onClick={dashboards[1].onClick} pageCode={dashboards[1].pageCode}/>
        </div>
        <div className="col-md-4">
          <DashboardPanel icon={dashboards[2].icon} color={dashboards[2].color}
            clickLabel={dashboards[2].clickLabel} numberText={dashboards[2].numberText}
            subtext={dashboards[2].subtext} onClick={dashboards[2].onClick} pageCode={dashboards[2].pageCode}/>
        </div>

        <div className="col-md-4">
          <DashboardPanel icon={dashboards[3].icon} color={dashboards[3].color}
            clickLabel={dashboards[3].clickLabel} numberText={dashboards[3].numberText}
            subtext={dashboards[3].subtext} onClick={dashboards[3].onClick} pageCode={dashboards[3].pageCode}/>
        </div>
        <div className="col-md-4">
          <DashboardPanel icon={dashboards[4].icon} color={dashboards[4].color}
            clickLabel={dashboards[4].clickLabel} numberText={dashboards[4].numberText}
            subtext={dashboards[4].subtext} onClick={dashboards[4].onClick} pageCode={dashboards[4].pageCode}/>
        </div>
        <div className="col-md-4">
          <DashboardPanel icon={dashboards[5].icon} color={dashboards[5].color}
            clickLabel={dashboards[5].clickLabel} numberText={dashboards[5].numberText}
            subtext={dashboards[5].subtext} onClick={dashboards[5].onClick} pageCode={dashboards[5].pageCode}/>
        </div>
      </div>
    );
  }
}

export default DashboardPanels;
