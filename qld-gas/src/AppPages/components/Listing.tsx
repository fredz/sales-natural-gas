import * as React from 'react';
import * as _ from 'lodash';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

import ActionsHeader, { ActionsHeaderMode } from './ActionsHeader';

export interface IListingColumn {
  isKey?: boolean;
  dataField: string;
  header: string;
  customEditor?: any;
  dataFormat?: Function;
  hidden?: boolean;
  editable?: any;
  editColumnClassName?: any;
  thStyle?: any;
  width?: string;
  isDate?: boolean;
}

export interface IListingProps {
  data: Array<any>;
  columns: Array<IListingColumn>;
  onRefreshData: Function;
  onDeleteData: Function;
  onEditData: Function;
  withActions: boolean;
  actions?: any;
  withHeader?: boolean;
  cellEdit?: any;
  withNumber?: boolean;
};

export interface IListingState {  
  nextOffset: number;
  isDelete: boolean;
  isEdit: boolean;
  selectedRows?: Array<any>;
  editList: Array<any>;
  deleteList: Array<any>;  
}

class Listing extends React.Component<IListingProps, IListingState> {

  defaultRange = 200;
  maxRange = 450;

  constructor(props) {
    super(props);
    
    this.state = {      
      nextOffset: this.defaultRange,
      isDelete: false,
      isEdit: false,
      editList: new Array(),
      deleteList: new Array()      
    };

    this.refreshData = this.refreshData.bind(this);
    this.handleActionsCancel = this.handleActionsCancel.bind(this);
    this.handleActionsConfirm = this.handleActionsConfirm.bind(this);
    this.onRowSelect = this.onRowSelect.bind(this);
    this.setNormalMode = this.setNormalMode.bind(this);
    this.handleModeChange = this.handleModeChange.bind(this);      
    
  } 

  refreshData(from, to) {
    this.props.onRefreshData(from, to);
  }

  setNormalMode(mode: ActionsHeaderMode) {
    this.setState({
      isEdit: false,
      isDelete: false,
      editList: new Array(),
      deleteList: new Array(),
    } as IListingState);
  }

  handleActionsCancel(mode: ActionsHeaderMode) {
    this.setNormalMode(mode);
  }

  handleActionsConfirm(mode: ActionsHeaderMode) {
    if (mode === ActionsHeaderMode.Delete) {
      this.props.onDeleteData(this.state.deleteList);
    }

    if (mode === ActionsHeaderMode.Edit) {
      this.props.onEditData(this.state.editList);
    }

    this.setNormalMode(mode);
  }

  handleModeChange(newMode: ActionsHeaderMode) {
    switch (newMode) {
      case ActionsHeaderMode.Normal:
        this.setState({
          isEdit: false,
          isDelete: false,
        } as IListingState);
        break;
      case ActionsHeaderMode.Delete:
        this.setState({
          isEdit: false,
          isDelete: true,
        } as IListingState);
        break;
      case ActionsHeaderMode.Edit:
        this.setState({
          isEdit: true,
          isDelete: false,
        } as IListingState);
        break;
      default: 
        this.setState({
          isEdit: false,
          isDelete: false,
        } as IListingState);
    }
  }

  onRowSelect(row, state) {
    const { isEdit, isDelete } = this.state;
    
    if (isEdit) {
      let list = this.state.editList;
      if (state) {
        list.push(row);
      } else {
        const index = list.indexOf(row);
        list.splice(index, 1);
      }

      this.setState({
        editList: list,
      } as IListingState);
    }
    
    if (isDelete) {
      let list = this.state.deleteList;
      if (state) {
        list.push(row);
      } else {
        const index = list.indexOf(row);
        list.splice(index, 1);
      }

      this.setState({
        deleteList: list,
      } as IListingState);
    }
  }

  revertSortFunc(a, b, order, sortField) {   // order is desc or asc
    
    const dateA = new Date(a[sortField]).getTime();
    const dateB = new Date(b[sortField]).getTime();

    if (order === 'desc') {
      return dateB > dateA ? 1 : -1;
    } else {
      return dateA > dateB ? 1 : -1;
    }
  } 

  render() {    
    const { data, columns} = this.props;
    const { isDelete, isEdit } = this.state;
    
    let tableRowProps = {};

    if (isDelete) {
      tableRowProps = {
        selectRow: {
          mode: 'checkbox',
          onSelect: this.onRowSelect,
        }
      };
    }

    if (isEdit) {
      tableRowProps = {
        selectRow: {
          mode: 'radio',
          onSelect: this.onRowSelect,
        }
      };
    }

    let actions = (
      <TableHeaderColumn dataField="action" hidden/>
    );
//debugger;
    if (this.props.withActions && !isDelete) {
      if (this.props.actions) {
        actions = this.props.actions;
      } else {
        actions = (
          <TableHeaderColumn 
            dataField="action" 
            width={'150'}
            dataFormat={(cell, row) => {
              return (
                <a className="btn btn-xs btn-warning" title="Edit" 
                  onClick={(e) => {e.preventDefault(); this.props.onEditData([row]);}}>
                  <i className="glyphicon glyphicon-edit"/>
                </a>
              );
          }}>
            Actions
          </TableHeaderColumn>
        );
      }
    }
    
    const customData = _.map(data, (element, i) => {
      return Object.assign({}, element, {_index: i + 1});
    });

    const options = {
      sizePerPageList: [ 50, 75, 100, 150, 200 ],
      sizePerPage: 50
    };

    let header = null;
    if (this.props.withHeader) {
      header = (
        <ActionsHeader 
          isEdit={isEdit} 
          isDelete={isDelete} 
          onCancel={this.handleActionsCancel}
          onConfirm={this.handleActionsConfirm}
          onModeChange={this.handleModeChange}/>
      );
    }

    const withNumber = this.props.withNumber !== undefined ? this.props.withNumber : true;

    return (
      <div>
        {header}
        
        <BootstrapTable data={customData} pagination={true} {...tableRowProps} options={options} 
          cellEdit={this.props.cellEdit}>
          {
            withNumber ?
            <TableHeaderColumn dataField={'_index'} dataSort={true} width="50">#</TableHeaderColumn> :
            <TableHeaderColumn dataField="_index" hidden/>
          }
          {
            _.map(columns, (element: IListingColumn, i) => {
              const props = element.isKey ? {isKey: true} : {};
              const format = element.dataFormat ? {dataFormat: element.dataFormat} : {};
              const cellEdit = element.customEditor ? {customEditor: element.customEditor} : {};
              const editColumnClassName = element.editColumnClassName ? 
                {editColumnClassName: element.editColumnClassName} : {};
              const hidden = element.hidden ? {hidden: element.hidden} : {};
              const editable = element.editable ? {editable: element.editable} : {editable: false};
              const thStyle = element.thStyle ? {thStyle: element.thStyle} : {};
              const width = element.width ? element.width : '150';
              const sortFunction = element.isDate ? this.revertSortFunc : null;

              return (
                <TableHeaderColumn dataField={element.dataField} {...props} {...format} {...cellEdit} 
                  key={i} width={width}
                  {...hidden} dataSort={true} {...editable} {...{ tdStyle: { whiteSpace: 'normal' } }} {...thStyle}
                  {...editColumnClassName}                  
                  sortFunc={ sortFunction }
                >
                  {element.header}                  
                </TableHeaderColumn>
              );
            })
          }
          {actions}
        </BootstrapTable>
      </div>
    );
  }
}

export default Listing;
