import * as React from 'react';
import * as _ from 'lodash';

export interface IListingSortOptions {
  label: string;
  value: string;
}

export interface IListingProps {
  onRefreshData: Function;
  onSearch?: Function;
  from?: number;
  to?: number;
  total: number;
  withSearch?: boolean;
  searchText?: string;
  withActions: boolean;
  withRange?: boolean;
};

export interface IListingState {
  from: number;
  nextOffset: number;
  selectedRows?: Array<any>;
  searchText: string;
}

class CustomFilter extends React.Component<IListingProps, IListingState> {
  defaultSortOptions: Array<IListingSortOptions> = [
    {label: 'by Newest', value: 'newest'},
    {label: 'by oldest', value: 'oldest'},
    {label: 'No sort', value: 'none'},
  ];

  defaultRange = 200;
  maxRange = 450;

  constructor(props) {
    super(props);
    
    this.state = {
      from: 0,
      to: this.maxRange,
      nextOffset: this.defaultRange,
      searchText: this.props.searchText ? this.props.searchText : '',
      withRange: true,
    } as IListingState;

    if (this.props.from >= 0 && this.props.to > 0) {
      const { from, to } = this.props;
      this.state = {
        from,
        nextOffset: to,
        searchText: this.props.searchText ? this.props.searchText : '',
      } as IListingState;
    }

    this.setFrom = this.setFrom.bind(this);
    this.setTo = this.setTo.bind(this);
    this.refreshData = this.refreshData.bind(this);
  }

  setFrom(value) {
    if (isNaN(value)) {
      return;
    }

    let nextOffset = this.state.nextOffset;
    let from = Number.parseInt(value);

    if ((nextOffset - from) > this.maxRange) {
      alert(`Cannot fetch more than ${this.maxRange} rows at once`);
      return;
    }

    this.setState({
      from,
      nextOffset,
    } as IListingState);
  }

  setTo(value) {
    if (isNaN(value)) {
      return;
    }

    let from = this.state.from;
    let to = Number.parseInt(value);
    
    if (from >= to) {
      from = to - this.defaultRange;
      
      if (from < 0) {
        from = 0;
      }
    }

    if ((to - from) > this.maxRange) {
      alert(`Cannot fetch more than ${this.maxRange} rows at once`);
      return;
    }

    this.setState({
      from,
      nextOffset: to,
    } as IListingState);
  }

  refreshData(from, to) {
    this.props.onRefreshData(from, to);
  }

  render() {
    const { from, to } = this.props;    
    const displaying = to - from === 0 ? to : to - from;

    let searchComponent = null;

    if (this.props.withSearch) {
      searchComponent = (
        <nav className="navbar navbar-default" style={{marginTop: '-20px'}}>
          <div className="container-fluid">
            <div className="navbar-form navbar-left">
              <p className="navbar-text">Search</p>
              <input 
                style={{width: '350px'}}
                className="form-control" type="text" placeholder="Search"
                value={this.state.searchText}
                onKeyPress={(e) => {
                  if (e.charCode === 13) {
                    e.preventDefault();
                    this.props.onSearch(this.state.searchText);
                  }
                }}
                onChange={(e) => { 
                  this.setState({
                    searchText: (e.target as HTMLInputElement).value,
                  } as IListingState);
                }}/>
              <a className="btn btn-default navbar-btn" 
                onClick={(e) => { e.preventDefault(); this.props.onSearch(this.state.searchText); }}>
                <i className="glyphicon glyphicon-search"/>
              </a>
            </div>
          </div>
        </nav>
      );
    }
    
    let withRange = this.props.withRange !== undefined ? this.props.withRange : true;
    let rangeComponent = null;
    if (withRange) {
      rangeComponent = (
        <nav className="navbar navbar-default" style={{marginTop:'-20px'}}>
          <div className="container-fluid">
            <div className="navbar-form navbar-left">
              <p className="navbar-text">From</p>
              <input 
                style={{width: '90px', marginTop: '8px', marginBottom: '8px'}}
                className="form-control" type="number" placeholder="From"
                value={this.state.from}
                onChange={(e) => { this.setFrom((e.target as HTMLInputElement).value);}}/>
            </div>
            <div className="navbar-form navbar-left">
              <p className="navbar-text">Next</p>
              <input 
                style={{width: '90px'}}
                className="form-control" type="number" placeholder="Next"
                value={this.state.nextOffset}
                onChange={(e) => { this.setTo((e.target as HTMLInputElement).value);}}/>
              <a className="btn btn-default navbar-btn" 
                onClick={(e) => { e.preventDefault(); this.refreshData(this.state.from, this.state.nextOffset); }}>
                Fetch <i className="glyphicon glyphicon-circle-arrow-down"/>
              </a>
            </div>
            <div className="navbar-form navbar-left">
              <span className="navbar-text">
                Showing {displaying} rows out of <b>{this.props.total}</b>. {`from ${from} next ${to}.`}
              </span>
            </div>
          </div>
        </nav>
      );
    }
    
    return (
      <div>
        {searchComponent}
        {rangeComponent}
      </div>
    );
  }
}

export default CustomFilter;
