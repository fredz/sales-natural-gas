import * as React from 'react';
import * as ReactToastr from 'react-toastr';
import { ToastContainer } from 'react-toastr';

let ToastMessageFactory = React.createFactory(ReactToastr.ToastMessage.animation);

class Alert extends React.Component<any, any> {

  container: ToastContainer;

  constructor(props) {
    super(props);
    this.displayAlert = this.displayAlert.bind(this);
  }

  displayAlert (alertType, text, title) {

    let msgTimeOut = {
        timeOut: 10000,
        extendedTimeOut: 10000,
        closeButton: true,
    }

    switch (alertType) {  
      case 'success':  
        this.container.success(text, title, msgTimeOut);        
        break;  
      case 'error':  
        this.container.error(text, title, msgTimeOut);        
        break;  
      case 'info':  
        this.container.info(text, title, msgTimeOut);        
        break; 
      case 'warning':  
        this.container.warning(text, title, msgTimeOut);        
        break; 
      case 'confirm':  
        this.container.warning(text, title, msgTimeOut);        
        break;                  
      default:  
        this.container.info(text, title, msgTimeOut);        
    }

  }
  
  render () {
    return (
      <div>
        <ToastContainer ref={(element) => {this.container = element;}}
                        toastMessageFactory={ToastMessageFactory}
                        className="toast-top-right"   />        
      </div>
    );
  }
}

export default Alert;
