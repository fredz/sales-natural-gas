import * as React from 'react';
import { connect } from 'react-redux';
import { setPage } from '../../actions/app';

export interface IDashboardPanelProps {
  color: string;
  icon: string;
  numberText: string;
  subtext: string;
  clickLabel: string;
  onClick: Function;
  pageCode: string;
}

interface IConnectedProps { }

interface IConnectedDispatch {
  onSetPageCode: (value: string) => void;
}

class DashboardPanel extends 
  React.Component<IDashboardPanelProps & IConnectedProps & IConnectedDispatch, any> {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    const { onClick, pageCode } = this.props;

    this.props.onSetPageCode(pageCode);

    onClick(pageCode);
  }

  render() {

    const {color, numberText, subtext, clickLabel, icon} = this.props;

    return (
      <div className={`panel-${color} panel panel-default`}>
        <div className="panel-heading">
          <div className="row panel-title">
            <div className="col-xs-3">
              <i className={`fa fa-${icon} fa-5x`}/>
            </div>
            <div className="col-xs-9 text-right">
              <div className="huge">{numberText}</div>
              <div style={{marginTop: '10px'}}>{subtext}</div>
            </div>
          </div>
         
        </div>
        <div className="panel-footer">
          <a onClick={(e) => {this.handleClick();} } style={{cursor:'pointer'}}>
            <span className="pull-left">{clickLabel}</span>
            <span className="pull-right">
              <i className="fa fa-arrow-circle-right"/>
            </span>
            <div className="clearfix"/>
            </a>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state: any): IConnectedProps {
  return { };
}

function mapDispatchToProps(dispatch): IConnectedDispatch {
  return {
    onSetPageCode: (value: string) => dispatch(setPage(value))
  };
}

export default 
connect(mapStateToProps, mapDispatchToProps)(DashboardPanel) as React.ComponentClass<IDashboardPanelProps>;

