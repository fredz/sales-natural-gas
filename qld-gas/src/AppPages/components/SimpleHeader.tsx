import * as React from 'react';
import { PageMode } from '../constants';

export interface ISimpleHeaderProps {
  brand: JSX.Element;
  newLabel: string;
  listLabel: string;
  mode: PageMode;
  onRightItemClick: Function;
  withOptions?: boolean;
}

class SimpleHeader extends React.Component<ISimpleHeaderProps, any> {
  constructor(props) {
    super(props);

    this.handleRightClick = this.handleRightClick.bind(this);
  }

  handleRightClick(item) {
    this.props.onRightItemClick(item);
  }

  render() {

    const withOptions = this.props.withOptions !== undefined ? this.props.withOptions : true;
    
    return (
      <nav className="navbar navbar-default" style={{background:'lightgray', paddingRight: '15px'}}>
        <div className="container-fluid">
          <div className="navbar-header">
            <a className="navbar-brand">
              {this.props.brand}
            </a>
          </div>
          {
            withOptions ? <p className="navbar-right">
              {
                this.props.mode === PageMode.edit || this.props.mode === PageMode.new ? null :
                (
                  <a className="btn btn-xs btn-default navbar-btn" 
                    onClick={(e) => { e.preventDefault(); this.handleRightClick(PageMode.new); }}>
                    <i className="glyphicon glyphicon-plus"/> {this.props.newLabel}
                  </a>
                )
              }
              {
                this.props.mode === PageMode.list ? null :
                (
                  <a className="btn btn-xs btn-default navbar-btn" 
                    onClick={(e) => { e.preventDefault(); this.handleRightClick(PageMode.list);}}>
                    <i className="glyphicon glyphicon-th"/> {this.props.listLabel}
                  </a>
                )
              }
              
            </p> : null
          }
        </div>
      </nav>
    );
  }
}

export default SimpleHeader;
