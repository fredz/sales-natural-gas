import * as React from 'react';
import './Loadable.css';

export interface ILoadableProps {
  isLoading: boolean;
  text: string;
  marginTop?: number;
}

class Loadable extends React.Component<ILoadableProps, any> {
  constructor(props) {
    super(props);
  }

  render() {
    const { isLoading, text } = this.props;
    if ( isLoading ) {
      const top = this.props.marginTop !== undefined ? this.props.marginTop : 20; 
      return (
        <div style={{paddingTop: `${top}%`}}>
          <div className="loader">{text}</div>
        </div>
      );
    }

    return (
      <div>
        {this.props.children}
      </div>
    );
  }
}

export default Loadable;
