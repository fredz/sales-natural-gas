import * as React from 'react';
import { connect } from 'react-redux';
import { setPage } from '../../actions/app';
import { IPageSettings } from '../../state';

import { Row, Column } from '../../ascendis-ui/layout';
import { InputGroup } from '../../ascendis-ui/components';
import Loadable from '../components/Loadable';
import Alert from '../components/Alert';
import SimpleHeader from '../components/SimpleHeader';

import * as fileUtils from '../../App/fileUtils';

export interface IExportPurchaseState {
  loading: boolean;  
  startingDate: string;
  endingDate: string;
}

export interface IExportPurchaseProps { }

interface IConnectedProps {
  pageCode: string;
  editData: any;
}

interface IConnectedDispatch {
  onSetPageCode: (value: string, settings: IPageSettings) => void;
}

class ExportPurchase extends React.Component<IExportPurchaseProps & IConnectedProps & IConnectedDispatch, 
IExportPurchaseState> {

  alertRef: any;

  constructor(props) {
    super(props);    

    this.state = {
      loading: false,
      startingDate: '',
      endingDate: '',
    } as IExportPurchaseState;

    this.handleInputChange = this.handleInputChange.bind(this);
    this.exportMyobPurchase = this.exportMyobPurchase.bind(this);    
  }  

  handleInputChange(prop, value) {
    let state = {};
    state[prop] = value;

    this.setState(state as IExportPurchaseState);
  }  

  exportMyobPurchase() {

    let startingDate = new Date(this.state.startingDate);
    let endingDate = new Date(this.state.endingDate);

    let message = '';

    if (this.state.startingDate.length > 0 && this.state.endingDate.length && endingDate > startingDate) {
      fileUtils.exportMyobPurchase(this.state.startingDate, this.state.endingDate);
      message = 'File has been exported';
      this.alertRef.displayAlert('success', message, 'Export');      
    }else {
      message = 'The start date must be greater than the end date And Fields must not be empty';
      this.alertRef.displayAlert('error', message, 'Export');
    }
    
  }

  getContentFromMode() {
    let content;

    content = (
      <div>
        <Row>
          <div className="col-md-6 col-md-offset-3">
            <InputGroup 
              type="datepicker"
              label="Starting Date" 
              placeholder="Starting Date" 
              inputName="startingDate"
              onValueChange={(value) => {this.handleInputChange('startingDate', value);}}
              value={this.state.startingDate}
            /> 

            <InputGroup 
              type="datepicker"
              label="Ending Date" 
              placeholder="Ending Date" 
              inputName="endingDate"
              onValueChange={(value) => {this.handleInputChange('endingDate', value);}}
              value={this.state.endingDate}
            />     

            <div style={{width: '100%', marginTop: '1.5em'}}>
              <a onClick={this.exportMyobPurchase} 
                className="btn btn-success"  title="Export" >Export</a>
            </div>                        
          </div>
        </Row>        
      </div>
    );    
    return content;
  } 

  render() {

    const brand = (
      <span>
        <i className="glyphicon glyphicon-list-alt"/> Export Purchase        
      </span>
    );

    const content = this.getContentFromMode();

    return (
      <div>
        <Alert ref={x => this.alertRef = x} />
        <Loadable isLoading={this.state.loading} text={'Fetching Data...'}>
          <SimpleHeader 
            brand={brand}
            listLabel="List"
            newLabel="Reading .csv file"
            onRightItemClick={null}
            mode={null}
            withOptions={false}
          />
          {content}             
        </Loadable>
      </div>
    );
  }
}

function mapStateToProps(state: any): IConnectedProps {
  return {
    pageCode: state.app.pageCode,
    editData: state.app.editData,
  };
}

function mapDispatchToProps(dispatch): IConnectedDispatch {
  return {
    onSetPageCode: (value: string, settings) => dispatch(setPage(value, settings))
  };
}

export default 
  connect(mapStateToProps, mapDispatchToProps)(ExportPurchase) as React.ComponentClass<IExportPurchaseProps>;