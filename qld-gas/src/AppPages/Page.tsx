import * as React from 'react';

import maintenance from './Maintenance';
import importFile from './Import';
import exportFile from './Export';
import general from './General';

export interface IPageProps {
  pageCode: string;
  onItemClick: Function;
}

export interface IPageState {

}

class Page extends React.Component<IPageProps, IPageState> {
  constructor(props) {
    super(props);

    this.getPage = this.getPage.bind(this);
  }

  getPage(code) {
    let activePage = null;    
    switch (code) {
      case undefined : 
        activePage = <general.Dashboard  onItemClick={this.props.onItemClick}/>;
        break;

      /* General Pages */
      case 'dashboard':
        activePage = <general.Dashboard onItemClick={this.props.onItemClick}/>;
        break;

      case 'meter-reading':        
        activePage = <general.MeterReading/>;
        break;

      case 'bottle-gas-delivery':
        activePage = <general.BottleGasDelivery/>;
        break;    

      case 'tax-invoice':
        activePage = <general.TaxInvoice/>;
        break;

      case 'preview-invoices':
        activePage = <general.PreviewInvoices/>;
        break;

      case 'view-invoices' :
        activePage = <general.Invoices/>;
        break;        
      
      case 'debt-collection' :
        activePage = <maintenance.DebtCollection/>;
        break;

      /* Maintenance Pages */
      case 'customer-maintenance' :
        activePage = <maintenance.Customer/>;
        break;
      
      case 'complex-maintenance' :
        activePage = <maintenance.Complex/>;
        break;

      case 'unitcodes-maintenance' :
        activePage = <maintenance.Unit/>;
        break;

      case 'rentalcodes-maintenance' :
        activePage = <maintenance.Rental/>;
        break;  

      case 'readings-maintenance' :
        activePage = <maintenance.Readings/>;
        break;          

      case 'item-maintenance' :
        activePage = <maintenance.Item/>;
        break;

      case 'company-maintenance' :
        activePage = <maintenance.Company/>;
        break;

        case 'receipthistory-maintenance' :
        activePage = <maintenance.ReceiptHistory/>;
        break;        

      case 'city-maintenance' :
        activePage = <maintenance.City/>;
        break;

      case 'transferrental-maintenance' :        
        activePage = <maintenance.TransferRental/>;
        break;          

      /* Import Pages */
      case 'readings-import' :
        activePage = <importFile.Readings/>;
        break;
      case 'payment-import' :
        activePage = <importFile.Payment/>;
        break; 
      case 'bpay-receipts-import' :
        activePage = <importFile.BPayreceipt/>;
        break;
      
      /* Export Pages */
      case 'export-import' :
        activePage = <exportFile.ExportMyob/>;
        break;      
      case 'export-purchase' :
        activePage = <exportFile.ExportPurchase/>;
        break;          
      case 'export-customer' :
        activePage = <exportFile.ExportCustomer/>;
        break;        
      
      /* SMS Pages */
      case 'access-sms' :
        activePage = <maintenance.AccessSms/>;
        break; 

      case 'scheduled-sms' :
        activePage = <maintenance.ScheduledSms/>;
        break; 

      /* No Page Found */
      default: activePage = (
        <span style={{margin:'10px auto'}}>
          <b>{`No Page Found, Attempted to load '${code}', please report this message`}</b>
        </span>
      );
    }

    return activePage;
  }

  render() {
    const style = {
      marginTop: '0px',
      marginLeft: '250px',
      padding: '0 10px',
      minHeight: '840px',
      background: '#fafafa'
    };
    
    const page = this.getPage(this.props.pageCode);

    return (
      <section style={style}>
        { page }
      </section>
    );
  }
}

export default Page;
