import * as React from 'react';
import * as moment from 'moment';
import { connect } from 'react-redux';
import { setPage } from '../../actions/app';
import { IPageSettings } from '../../state';

import { TableHeaderColumn } from 'react-bootstrap-table';
import Loadable from '../components/Loadable';
import Listing from '../components/Listing';
import SimpleHeader from '../components/SimpleHeader';
import RentalForm from './forms/RentalForm';
import { PageMode, pages } from '../constants';
import CustomFilter from '../components/CustomFilter';
import Alert from '../components/Alert';

import * as utils from '../../App/utils';

export interface IRentalState {
  mode: PageMode;
  loading: boolean;
  from: number;
  to: number;
  data: Array<any>;
  total: number;
  editData: Array<any>;
  deleteData: Array<any>;
  searchText: string;
}

export interface IRentalProps {

}

interface IConnectedProps {
  pageCode: string;
  editData: any;
}

interface IConnectedDispatch {
  onSetPageCode: (value: string, settings: IPageSettings) => void;
}

class Rental extends React.Component<IRentalProps & IConnectedProps & IConnectedDispatch, IRentalState> {
  
  alertRef: any; 

  constructor(props) {
    super(props);

    this.state = {
      mode: PageMode.list,
      loading: true,
      from: 0,
      to: 100,
      searchText: '',
    } as IRentalState;

    this.handleRightItemClick = this.handleRightItemClick.bind(this);
    this.getContentFromMode = this.getContentFromMode.bind(this);
    this.getDataAndDisplay = this.getDataAndDisplay.bind(this);
    this.onRangeUpdate = this.onRangeUpdate.bind(this);
    this.handleFormSave = this.handleFormSave.bind(this);
    this.handleFormCancel = this.handleFormCancel.bind(this);
    this.handleEditData = this.handleEditData.bind(this);
    this.handleDeleteData = this.handleDeleteData.bind(this);
    this.handleSearchData = this.handleSearchData.bind(this);
  }

  componentDidMount() {
    if (this.props.editData) {
      this.handleSearchData(this.props.editData.RentalCode);
    } else {
      this.getDataAndDisplay(this.state.mode, this.state.from, this.state.to);
    }
  }

  handleRightItemClick(mode: PageMode) {
    if (mode === PageMode.list) {
      this.setState({
        loading: true,
      } as IRentalState);

      const searchCriteria = this.prepareSearchCriteria(this.state.searchText);
      this.getDataAndDisplay(mode, this.state.from, this.state.to, '', searchCriteria);      
    } else {
      this.setState({
        mode,
      } as IRentalState);
    }
  }

  getDataAndDisplay(mode, from, to, sort = '', searchCriteria = '') {
    let formData = new FormData();
    formData.append('Action', 'Fetch');
    formData.append('Path', 'Rental/');
    formData.append('From', from);
    formData.append('To', to);
    formData.append('Sort', sort);
    formData.append('Search', searchCriteria);

    utils.fetch(formData).then(
      (response: any) => {
        const { data, total } = response;        
        this.setState({
          mode,
          data,
          loading: false,
          from, 
          to,
          total
        } as IRentalState);
      }
    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );
  }

  onRangeUpdate(from, to) {    
    const { mode, searchText } = this.state;
    const searchCriteria = this.prepareSearchCriteria(searchText);
    this.getDataAndDisplay(mode, from, to, '', searchCriteria);

    this.setState({
      loading: true,
    } as IRentalState);
  }

  handleFormSave(data, isEdit, id = null) {
    let formData = new FormData();
    let message;
    let toastrTitle;

    if (utils.IsEmptyNull(data.unitCode) || utils.IsEmptyNull(data.customNo)) {
      
      toastrTitle = 'Field Required';
      message = `The fields Unit and Customer are required`;
      this.alertRef.displayAlert('error', message, toastrTitle);
      return;
    }    
    
    if (isEdit) {
      formData.append('Action', 'Update');
    } else {
      formData.append('Action', 'Insert');
    }

    formData.append('Path', 'Rental/');
    formData.append('RentalCode', data.rentalCode);
    formData.append('Custno', data.customNo);
    formData.append('UnitCode', data.unitCode);
    formData.append('StartDate', data.startDate);    
    formData.append('Terms', data.terms);
    formData.append('SecDeposit', data.secDeposit);
    formData.append('Inactive', data.inactive === true ? '1' : '0');    
    
    utils.fetch(formData).then(
      (response: any) => {

        const { status } = response;        
        let msg;
        let toastrTitle;

        if (status === 'ok') {
          if (!isEdit) {
            msg = `Rental Added`;
            toastrTitle = 'New Rental';
            this.alertRef.displayAlert('success', msg, toastrTitle);
          } else {

            msg = `${data.rentalCode} Updated`;
            toastrTitle = 'Rental';
            this.alertRef.displayAlert('success', msg, toastrTitle);
          }
        } else {          
          this.alertRef.displayAlert('error', 
            `Error while Saving.\nError Message:\n${response.error}`, 'Error');          
        }
        this.getDataAndDisplay(PageMode.list, this.state.from, this.state.to);
      }
    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );

    this.setState({
     loading: true, 
    } as IRentalState);
  }

  handleFormCancel() {
    this.setState({
      mode: PageMode.list,
    } as IRentalState);
  }

  handleEditData(list) {

    this.setState({
      editData: list,
      mode: PageMode.edit,
    } as IRentalState);
  }

  handleDeleteData(list) {
    if (list.length === 0) {
      return;
    }
    
    let ids = '';
    for (let i = 0; i < list.length; i++) {
      ids += ids.length > 0 ? ',' : '';
      ids += list[i].RentalCode;
    }

    utils.canDelete(ids, 'QLD.CanDeleteRental').then(	
        (response: any) => {

          let msg;        
          let toastrTitle;

          // validate if the record does not depend of others records
          if (!response.canDelete) {  

            msg = 'The record(s) cannot be deleted';
            toastrTitle = 'Delete Rental';
            this.alertRef.displayAlert('info', msg, toastrTitle);            

            return;
          } else {  

            if (!confirm('Are you sure you want to delete selected Rentales?')) {
              return;
            }
            
            let formData = new FormData();
            formData.append('Action', 'Delete');
            formData.append('Path', 'Rental/');
            formData.append('RentalCode', ids);

            utils.fetch(formData).then(
              (res: any) => {
                const { status } = res;
                this.getDataAndDisplay(PageMode.list, this.state.from, this.state.to);
                
                if (status === 'ok') {
                  
                  msg = 'Selected Rentals were deleted';
                  toastrTitle = 'Delete Rental';
                  this.alertRef.displayAlert('success', msg, toastrTitle); 
                }
              }
            ).catch(
              (ex) => {
                this.alertRef.displayAlert('error', ex, 'Error');
              }
            );    
          }
      }

    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );

  }

  prepareSearchCriteria(searchText) {
    let searchCriteria = `(RentalCode LIKE '%${searchText}%' `;
    searchCriteria += `OR cu.Custno LIKE '%${searchText}%' `;
    searchCriteria += `OR ComplexName LIKE '%${searchText}%' `;
    searchCriteria += `OR [Billing Names] LIKE '%${searchText}%' `;
    searchCriteria += `OR GivenNames LIKE '%${searchText}%') `;

    return searchCriteria;
  }

  handleSearchData(searchText) {
    const { from, to } = this.state;
    let searchCriteria = this.prepareSearchCriteria(searchText);

    this.getDataAndDisplay(PageMode.list, from, to, '', searchCriteria);
    this.setState({
      loading: true,
      searchText,
    } as IRentalState);
  }

  getContentFromMode(mode: PageMode) {
    let content;
    switch (mode) {
      case PageMode.list: 
        let { data } = this.state;
        
        const columns = [
          {dataField: 'RentalCode', header: 'Rental No', isKey: true, width: '100'},
          {dataField: 'UnitCode', header: 'Unit Code', width: '100'},          
          {dataField: 'NoUnits', header: 'Unit Number', width: '110'},
          {dataField: 'ComplexName', header: 'Complex Name'},
          {dataField: 'Custno', header: 'Customer No.'},
          {dataField: 'Title', header: 'Title', width: '80'},
          {dataField: 'BillingNames', header: 'Billing Names'},
          {dataField: 'GivenNames', header: 'Given Names'},
          {dataField: 'StartDate', header: 'Start Date', width: '100', isDate: true, dataFormat: (cell, row) => {
              return <span>{cell ? moment(cell).format('D MMM Y') : ''}</span>;
            }},
          {dataField: 'EndDate', header: 'End Date', width: '100', isDate: true, dataFormat: (cell, row) => {
              return <span>{cell ? moment(cell).format('D MMM Y') : ''}</span>;
            }},
          {dataField: 'Terms', header: 'Terms', width: '100'},
          {dataField: 'SecDeposit', header: 'Sec Deposit', width: '100'},
          {dataField: 'Inactive', header: 'Inactive', hidden: true}          
        ];

        if (!data) {
          data = [];
        }

        const actions = (
          <TableHeaderColumn 
            dataField="action" 
            dataAlign="center"        
            headerAlign="center"            
            width="100px"
            dataFormat={(cell, row) => {
              return (
                <span>
                  <a className="btn btn-xs btn-warning" title="Edit" 
                    onClick={(e) => {e.preventDefault(); this.handleEditData([row]);}}>
                    <i className="glyphicon glyphicon-edit"/>
                  </a>
                </span>
              );
          }}>
            Actions
          </TableHeaderColumn>
        );

        content = (
          <div>
            <CustomFilter 
              onRefreshData={this.onRangeUpdate} 
              total={this.state.total}  
              from={this.state.from}  
              to={this.state.to} 
              withSearch={true} 
              onSearch={this.handleSearchData} 
              searchText={this.state.searchText} 
              withActions={true} 
              withRange={true}            
            />
            <Listing 
              data={data}
              columns={columns} 
              onRefreshData={this.onRangeUpdate}
              onEditData={this.handleEditData}
              onDeleteData={this.handleDeleteData}
              withActions={true}
              actions={actions}/>
          </div>
        );
        break;

      case PageMode.new: 
        content = <RentalForm onSave={this.handleFormSave} onCancel={this.handleFormCancel}/>;
        break;

      case PageMode.edit: 
        content = <RentalForm data={this.state.editData} 
          onSave={this.handleFormSave} onCancel={this.handleFormCancel}/>;
        break;

      default: 
        content = null;
    }

    return content;
  }

  render() {
    
    const brand = (
      <span>
        <i className="glyphicon glyphicon-list-alt"/> Rental Maintenance
      </span>
    );

    const content = this.getContentFromMode(this.state.mode);

    return (
      <div>
        <Alert ref={x => this.alertRef = x} />
        <Loadable isLoading={this.state.loading} text={'Fetching Data...'}>
          <SimpleHeader 
            brand={brand}
            listLabel="List"
            newLabel="New Rental"
            onRightItemClick={this.handleRightItemClick}
            mode={this.state.mode}
          />
          {content}
        </Loadable>
      </div>
    );
  }
}

function mapStateToProps(state: any): IConnectedProps {
  return {
    pageCode: state.app.pageCode,
    editData: state.app.editData,
  };
}

function mapDispatchToProps(dispatch): IConnectedDispatch {
  return {
    onSetPageCode: (value: string, settings) => dispatch(setPage(value, settings))
  };
}

export default 
  connect(mapStateToProps, mapDispatchToProps)(Rental) as React.ComponentClass<IRentalProps>;
