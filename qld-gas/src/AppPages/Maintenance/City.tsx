import * as React from 'react';

import Loadable from '../components/Loadable';
import CustomFilter from '../components/CustomFilter';
import Listing from '../components/Listing';
import SimpleHeader from '../components/SimpleHeader';
import CityForm from './forms/CityForm';
import { PageMode } from '../constants';

import * as utils from '../../App/utils';
import Alert from '../components/Alert';
import { TableHeaderColumn } from 'react-bootstrap-table';

export interface ICityState {
  mode: PageMode;
  loading: boolean;
  from: number;
  to: number;
  data: Array<any>;
  total: number;
  editData: Array<any>;
  deleteData: Array<any>;
  serviceTypes: Array<any>;
  searchText: string;
}

class City extends React.Component<any, ICityState> {

  alertRef: any;

  constructor(props) {
    super(props);

    this.state = {
      mode: PageMode.list,
      loading: true,
      from: 0,
      to: 100,
      searchText: '',
  } as ICityState;

    this.handleRightItemClick = this.handleRightItemClick.bind(this);
    this.getContentFromMode = this.getContentFromMode.bind(this);
    this.getDataAndDisplay = this.getDataAndDisplay.bind(this);
    this.onRangeUpdate = this.onRangeUpdate.bind(this);
    this.handleFormSave = this.handleFormSave.bind(this);
    this.handleFormCancel = this.handleFormCancel.bind(this);
    this.handleEditData = this.handleEditData.bind(this);
    this.handleDeleteData = this.handleDeleteData.bind(this);
    this.handleSearchData = this.handleSearchData.bind(this);
}

  componentDidMount() {
    this.getDataAndDisplay(this.state.mode, this.state.from, this.state.to);
  }

  handleRightItemClick(mode: PageMode) {
    if (mode === PageMode.list) {
      this.setState({
        loading: true,
      } as ICityState);

      const searchCriteria = this.prepareSearchCriteria(this.state.searchText);
      this.getDataAndDisplay(mode, this.state.from, this.state.to, '', searchCriteria);
    } else {
      this.setState({
        mode,
      } as ICityState);
    }
  }

  getDataAndDisplay(mode, from, to, sort = '', searchCriteria = '') {
    let formData = new FormData();
    formData.append('Action', 'Fetch');
    formData.append('Path', 'City/');
    formData.append('From', from);
    formData.append('To', to);
    formData.append('Sort', sort);
    formData.append('Search', searchCriteria);

    const { fetch, fetchService } = utils;

    Promise.all([fetch(formData), fetchService()]).then(
      (response: any) => {
        const { data, total } = response[0];
        const serviceTypes = response[1].data;
        this.setState({
          mode,
          data,
          loading: false,
          from, 
          to,
          total,
          serviceTypes,
        } as ICityState);
      }
    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );
  }

  onRangeUpdate(from, to) {
    const { mode, searchText } = this.state;
    const searchCriteria = this.prepareSearchCriteria(searchText);
    this.getDataAndDisplay(mode, from, to, '', searchCriteria);

    this.setState({
      loading: true,
    } as ICityState);
  }

  handleFormSave(data, isEdit, id = null) {
    let formData = new FormData();
    let message;
    let toastrTitle;    

    if (utils.IsEmptyNull(data.cityName) || utils.IsEmptyNull(data.cityCode)) {
      toastrTitle = 'Field Required';
      message = `The fields City Code and City/Suburb are required`;
      this.alertRef.displayAlert('error', message, toastrTitle);
      return;
    }       
    
    if (isEdit) {
      formData.append('Action', 'Update');
    } else {
      formData.append('Action', 'Insert');
    }
    formData.append('Path', 'City/');
    
    formData.append('Citycode', data.cityCode);
    formData.append('Cityname', data.cityName);
    formData.append('Statecode', data.stateCode);
    formData.append('Postcode', data.postCode);

    utils.fetch(formData).then(
      (response: any) => {
        const { status } = response;
        let msg;
        let toastrTitle;

        if (status === 'ok') {

          if (!isEdit) {
            msg = `${data.cityName} Added`;
            toastrTitle = 'New City';
          } else {
            msg = `${data.cityName} Updated`;
            toastrTitle = 'City';
          }

          this.alertRef.displayAlert('success', msg, toastrTitle);  
        }else {
          this.alertRef.displayAlert('error', 
            `Error while Saving.\nError Message:\n${response.error}`, 'Error');                       
        }
        this.getDataAndDisplay(PageMode.list, this.state.from, this.state.to);
      }
    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );

    this.setState({
     loading: true, 
    } as ICityState);
  }

  handleFormCancel() {
    this.setState({
      mode: PageMode.list,
    } as ICityState);
  }

  handleEditData(list) {
    this.setState({
      editData: list,
      mode: PageMode.edit,
    } as ICityState);
  }

  handleDeleteData(list) {
    if (list.length === 0) {
      return;
    }
    
    let ids = '';
    for (let i = 0; i < list.length; i++) {
      ids += ids.length > 0 ? ',' : '';
      ids += '\'' + list[i].Citycode + '\'';
    }
    
    utils.canDelete(ids, 'QLD.CanDeleteCity').then(	
      (response: any) => {        
        let msg;
        let toastrTitle;
        // validate if the record does not depend of others records
        if (!response.canDelete) {  
          
            msg = 'The record(s) cannot be deleted';
            toastrTitle = 'Delete City';
            this.alertRef.displayAlert('info', msg, toastrTitle);
            return;
        }else {

          if (!confirm('Are you sure you want to delete selected Cities?')) {
            return;
          }
          
          let formData = new FormData();
          formData.append('Action', 'Delete');
          formData.append('Path', 'City/');
          formData.append('Citycode', ids);

          utils.fetch(formData).then(
            (response: any) => {
              const { status } = response;

              this.getDataAndDisplay(PageMode.list, this.state.from, this.state.to);
              
              if (status === 'ok') {
                
                  msg = 'Selected Cities were deleted';
                  toastrTitle = 'Cities';
                  this.alertRef.displayAlert('success', msg, toastrTitle);                
              }
            }
          ).catch(
            (ex) => {
              this.alertRef.displayAlert('error', ex, 'Error');
            }
          );
        }
      }
    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );
  }

  prepareSearchCriteria(searchText) {
    let searchCriteria = `(c.Citycode LIKE '%${searchText}%' `;
    searchCriteria += `OR Cityname LIKE '%${searchText}%' `;
    searchCriteria += `OR s.Statecode LIKE '%${searchText}%' `;
    searchCriteria += `OR Statename LIKE '%${searchText}%' `;
    searchCriteria += `OR Postcode LIKE '%${searchText}%') `;
    return searchCriteria;
  }  

  handleSearchData(searchText) {
    const { from, to } = this.state;
    let searchCriteria = this.prepareSearchCriteria(searchText);

    this.getDataAndDisplay(PageMode.list, from, to, '', searchCriteria);
    this.setState({
      loading: true,
      searchText,
    } as ICityState);
  }

  getContentFromMode(mode: PageMode) {
    let content;
    switch (mode) {
      case PageMode.list: 
        let { data } = this.state;
        const columns = [
          {dataField: 'Citycode', header: 'City Code', isKey: true, hidden: true},
          {dataField: 'Cityname', header: 'City/Suburb'},
          {dataField: 'Statename', header: 'State'},
          {dataField: 'Postcode', header: 'Postal Code'},
          // {dataField: 'Statecode', header: 'Service Type', dataFormat: (cell, row) => {
          //   const service = this.state.serviceTypes.find(x => x.value === cell);
          //   const label = service && service.label ? service.label : '';
          //   return label;
          // }},
        ];

        if (!data) {
          data = [];
        }

        const actions = (
          <TableHeaderColumn 
            dataField="action" 
            dataAlign="center"        
            headerAlign="center"            
            width="100px"
            dataFormat={(cell, row) => {
              
              return (
                <span>
                  <a className="btn btn-xs btn-warning" title="Edit" 
                    onClick={(e) => {e.preventDefault(); this.handleEditData([row]);}}>
                    <i className="glyphicon glyphicon-edit"/>
                  </a>               
                </span>
              );
          }}>
            Actions
          </TableHeaderColumn>
        );          

        content = (
          <div>
            <CustomFilter 
              onRefreshData={this.onRangeUpdate} 
              total={this.state.total}  
              from={this.state.from}  
              to={this.state.to} 
              withSearch={true} 
              onSearch={this.handleSearchData} 
              searchText={this.state.searchText} 
              withActions={true}                
              />
            <Listing 
              data={data}
              columns={columns} 
              onRefreshData={this.onRangeUpdate}
              onEditData={this.handleEditData}
              onDeleteData={this.handleDeleteData}
              actions={actions}
              withActions={true}/>
          </div>
        );
        break;

      case PageMode.new: 
        content = <CityForm onSave={this.handleFormSave} onCancel={this.handleFormCancel}/>;
        break;

      case PageMode.edit: 
        content = <CityForm data={this.state.editData} 
          onSave={this.handleFormSave} onCancel={this.handleFormCancel}/>;
        break;

      default: 
        content = null;
    }

    return content;
  }

  render() {
    
    const brand = (
      <span>
        <i className="glyphicon glyphicon-list-alt"/> City Maintenance
      </span>
    );

    const content = this.getContentFromMode(this.state.mode);

    return (
      <div>
        <Alert ref={x => this.alertRef = x} />
        <Loadable isLoading={this.state.loading} text={'Fetching Data...'}>
          <SimpleHeader 
            brand={brand}
            listLabel="List"
            newLabel="New City Code"
            onRightItemClick={this.handleRightItemClick}
            mode={this.state.mode}
          />
          {content}
        </Loadable>
      </div>
    );
  }
}

export default City;