import * as React from 'react';
import * as moment from 'moment';
import { connect } from 'react-redux';
import { setPage } from '../../actions/app';
import { IPageSettings } from '../../state';

import Loadable from '../components/Loadable';
import * as utils from '../../App/utils';
import Listing from '../components/Listing';
import Alert from '../components/Alert';
import SimpleHeader from '../components/SimpleHeader';
import { PageMode, pages } from '../constants';
import { InputGroup, Dropdown } from '../../ascendis-ui/components';
import { Row, Column } from '../../ascendis-ui/layout';

import { createDatePickerEditor } from '../components/Editors/DatePickerEditor';

export interface IReadingsProps { }

interface IConnectedProps {
  pageCode: string;
  editData: any;
}

interface IConnectedDispatch {
  onSetPageCode: (value: string, settings: IPageSettings) => void;
}

export interface IReadingsState {
  mode: PageMode;
  loading: boolean;
  data: Array<any>;
  total: number;
  dataBottledComplexes: any;
  valueBottledComplexes: string;
  dataMeteredComplexes: any;
  valueMeteredComplexes: string;
  storing: boolean;
  refreshing: boolean;
  readingDate: string;
  message: string;
}

class Readings extends React.Component<any, IReadingsState> {
  listData: Array<any>;
  alertRef: any;

  constructor(props) {
    super(props);

    let complexNo = '';

    if (this.props.editData) {
      complexNo = this.props.editData.ComplexNo;
    } 

    this.state = {
      mode: PageMode.list,
      loading: true,
      dataBottledComplexes: [], 
      valueBottledComplexes: complexNo,  
      dataMeteredComplexes: [], 
      valueMeteredComplexes: complexNo,      
      storing: false,
      refreshing: false,
      readingDate: new Date().toString(),
      message: 'Click on row cell to edit values, press enter to save.',
    } as IReadingsState;

    this.handleRightItemClick = this.handleRightItemClick.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleBeforeSaveCell = this.handleBeforeSaveCell.bind(this);
    this.fetchReading = this.fetchReading.bind(this);
  }

  componentDidMount() {
    let complexNo = null;

    if (this.props.editData) {
      complexNo = this.props.editData.ComplexNo;
    } 

    this.getDataAndDisplay(this.state.mode, complexNo);
  }  

  handleInputChange(prop, value) {
    let state = {};

    state[prop] = value;
    this.setState(state as IReadingsState);
  }  

  handleRightItemClick(mode: PageMode) {

    if (mode === PageMode.list) {
      this.setState({
        loading: true,
      } as IReadingsState);

      this.getDataAndDisplay(mode);
    } else {
      this.setState({
        mode,
      } as IReadingsState);
    }

  }

  getComboDataAndDisplay() {
    const {fetchBottledComplexes, fetchMeteredComplexes } = utils;
    
    Promise.all([fetchBottledComplexes(), fetchMeteredComplexes()]).then(      
      (response: any) => {
        
        const dataBottledComplexes = response[0].data;
        const dataMeteredComplexes = response[1].data;

        this.setState({          
          dataBottledComplexes,
          dataMeteredComplexes          
        } as IReadingsState);
      }
    ).catch(
      (ex) => {
        alert(ex);
      }
    );
  } 

  getDataReadingsAndDisplay(mode, complexNo, refreshing = false) {

    if (complexNo === null || this.state.readingDate.length === 0 ) {

      let msg = this.state.message;
      if (this.state.readingDate.length === 0 ) {
        msg = 'Please, Select Reading date and complex';
      }

      this.setState({
        data: [],
        loading: false,
        refreshing: false,
        total: 0,  
        message: msg,      
      } as IReadingsState);

      return;
    }

    let formData = new FormData();
    formData.append('Action', 'Fetch');
    formData.append('Path', 'OldReading/');
    formData.append('ComplexNo', complexNo);
    formData.append('CurReadingDate ', this.state.readingDate);
    
    utils.fetch(formData).then(
      (response: any) => {
        
        const { data, total } = response;

        this.setState({
          mode,
          data,          
          loading: false,
          refreshing: false,
          total,           
        } as IReadingsState);

      }
    ).catch(
      (ex) => {
        alert(ex);
      }
    );

    if (!refreshing) {
      this.setState({
        loading: true,        
      } as IReadingsState);
    }
  }  

  getDataAndDisplay(mode, complexNo = null) {
    this.getComboDataAndDisplay();
    this.getDataReadingsAndDisplay(mode, complexNo);
  }

  handleBeforeSaveCell(row, cellName, cellValue) {
    const {UnitCode, TempReadingDate, FinalReading, TempReading} = row; 
    
    let readingDate = null;
    
    if (cellName === 'TempReadingDate') {
      readingDate = cellValue.date;
    } else if (TempReadingDate !== '') {
      readingDate = TempReadingDate;
    } else {
      readingDate = this.state.readingDate;
    }

    let date = '';
    if (!readingDate && cellName !== 'TempReadingDate') {
      date = new Date().toString();
    } else {
      date = moment(readingDate).format('M/D/Y');
    }

    switch (cellName) {
      case 'TempReadingDate':
        utils.updateTempReading(UnitCode, date, TempReading, FinalReading === 'True' ? '1' : '0')
        .then(() => {
          this.setState({
            storing: false,
            refreshing: true,
          });
          
          this.getDataReadingsAndDisplay(this.state.mode, this.state.valueBottledComplexes, true);
        });
        break;

      case 'TempReading':
        utils.updateTempReading(UnitCode, cellValue.length !== 0 ? date : '', 
          cellValue, FinalReading === 'True' ? '1' : '0')
        .then(() => {
          this.setState({
            storing: false,
            refreshing: true,
          });
          
          this.getDataReadingsAndDisplay(this.state.mode, this.state.valueBottledComplexes, true);
        });
        break;

      case 'FinalReadingValue':
        utils.updateTempReading(UnitCode, date, TempReading, cellValue === 'Yes' ? '1' : '0')
        .then(() => {
          this.setState({
            storing: false,
            refreshing: true,
          });

          this.getDataReadingsAndDisplay(this.state.mode, this.state.valueBottledComplexes, true);
        });
        break;

      default:
        break;
    }

    this.setState({
      storing: true,
    });
  }

  getUsage(cellValue, CurReading) {

    let value;
    if (!isNaN(parseFloat(cellValue))) {
      value = parseFloat(cellValue) + parseFloat(CurReading);

      value = Math.round(value * 100) / 100;
    }

    return (value < 0) ? '' : value;
  } 

  fetchReading() {
    let complexNo = null;

    let valueMetered = this.state.valueMeteredComplexes;
    let valueBottled = this.state.valueBottledComplexes;

    if (valueMetered !== null && valueMetered.length > 0) {
      complexNo = valueMetered;
    }

    if (valueBottled !== null && valueBottled.length > 0) {
      complexNo = valueBottled;
    }    

    if (valueMetered !== null && valueMetered.length > 0 && valueBottled !== null && valueBottled.length > 0) {
        
        this.setState ({
          message: 'Metered Complexes And Bottled Complexes are selected Please select one of them'
        });

        complexNo = '';
    }
    
    this.getDataReadingsAndDisplay(this.state.mode, complexNo);
    
  }

  getContentFromMode(mode: PageMode) {
    let content;
    switch (mode) {
      case PageMode.list: 
        let { data } = this.state;
        const columns = [
          {dataField: 'UnitCode', header: 'Unit Code', isKey: true, editable: false},          
          {dataField: 'ComplexName', header: 'Complex Name', editable: false,
            dataFormat: (cell, row) => {              
              let page = row.ServiceType === '1' ? pages.meterReading : pages.bottleGasDelivery;

              let conf = {
                color: '',
                title: '',
                icon: ''
              };

              conf.title = '';
              if (page === pages.meterReading) {
                conf.title = 'Metered Reading';
                conf.color = '#8cc837';
                conf.icon = 'fa fa-dashboard';
              } else {
                conf.title = 'Bottle Gas Delivery';
                conf.color = '#31a1c9';
                conf.icon = 'fa fa-free-code-camp';
              }

              return (
                <a style={{color: conf.color}} title={conf.title}>
                  <i className={conf.icon}/> {cell}
                </a>
              );
            }
          },
          {dataField: 'BillingNames', header: 'Customer Name', editable: false,
            dataFormat: (cell, row) => {
              return (
                <a style={{cursor:'pointer'}}
                  onClick={(e) => this.props.onSetPageCode(pages.customerMaintenance, {editData: row})}>
                  {cell}
                </a>
              );
            }
          },
          {dataField: 'CustNo', header: 'Customer Number', editable: false},
          {dataField: 'CurReadingDate', header: 'Last Reading Date', editable: false, isDate: true,
            dataFormat: (cell, row) => {
              return <span>{cell ? moment(cell).format('D MMM Y') : ''}</span>;
            }
          },
          {dataField: 'CurReading', header: 'Last Reading', editable: false},
          {dataField: 'TempReadingDate', header: 'This Reading Date', editable: false, isDate: true,
            dataFormat: (cell, row) => {
              return <span>{cell ? moment(cell).format('D MMM Y') : ''}</span>;
            }
          },
          {dataField: 'TempReading', header: 'This Reading', editable: false},
          {dataField: 'FinalReadingValue', header: 'Final?', editable: {
            type: 'select', options: { values: ['Yes', 'No'] }
          }, thStyle: {background: '#d9edf7'}},
          {dataField: 'Usage', header: 'Usage', hidden: true, dataFormat: (cell, row) => {
            const { TempReading, CurReading } = row;

            let value = this.getUsage(TempReading, CurReading );

            return (
              <span>{value}</span>
            );
          }}          
        ];

        if (!data) {
          data = [];
        }

        content = (
          <div>
            <div className="alert alert-info" role="alert" style={{textAlign: 'center'}}>
              <strong><i className="glyphicon glyphicon-info-sign" /></strong> 
              {this.state.message}
            </div>
            <Row>
              <Column column="3">
                <InputGroup 
                  type="datepicker" 
                  label="Reading Date" 
                  placeholder="Reading Date" 
                  inputName="readingDate"
                  onValueChange={(value) => {this.handleInputChange('readingDate', value);}}
                  value={this.state.readingDate}/>
              </Column>
              <Column column="4">
                <Dropdown 
                  list={this.state.dataMeteredComplexes}
                  label="Metered Complexes" 
                  inputName="valueMeteredComplexes"
                  onValueChange={(value) => {this.handleInputChange('valueMeteredComplexes', value.value);}}
                  value={this.state.valueMeteredComplexes}/>
              </Column>
              <Column column="4">
                <Dropdown 
                  list={this.state.dataBottledComplexes}
                  label="Bottled Complexes" 
                  inputName="valueBottledComplexes"
                  onValueChange={(value) => {this.handleInputChange('valueBottledComplexes', value.value);}}
                  value={this.state.valueBottledComplexes}/>
              </Column>              
              <Column column="1">
                <div style={{textAlign: 'right', paddingRight: '10px'}}>
                  <a className="btn btn-success" title="Go to Generate Invoices" 
                    onClick={(e) => this.fetchReading()}>Fetch
                  </a>
                </div>
              </Column>
            </Row>

            <Listing 
              data={data}
              columns={columns} 
              onRefreshData={null}
              onEditData={null}
              onDeleteData={null}
              withActions={false}
              cellEdit={{
                mode: 'click',
                blurToSave: true,
                beforeSaveCell: this.handleBeforeSaveCell,
              }}
              withNumber={false}/>
          </div>
        );
        break;

      default: 
        content = null;
    }
    
    return content;
  } 

  render() {

    const storing = this.state.storing ? ' (Uploading data...)' : '';
    const refreshing = this.state.refreshing ? ' (Refreshing data...)' : '';

    const brand = (
      <span>
        <i className="glyphicon glyphicon-list-alt"/> Readings
        <span style={{color:'red'}}>{storing}{refreshing}</span>
      </span>
    );

    const content = this.getContentFromMode(this.state.mode);

    return (
      <div>
        <Alert ref={x => this.alertRef = x} />
        <Loadable isLoading={this.state.loading} text={'Fetching Data...'}>
          <SimpleHeader 
            brand={brand}
            listLabel="List"
            newLabel="Bottle Gas Delivery"
            onRightItemClick={this.handleRightItemClick}
            mode={PageMode.new}
            withOptions={false}
          />
          {content}             
        </Loadable>
      </div>
    );
  }
}

function mapStateToProps(state: any): IConnectedProps {
  return {
    pageCode: state.app.pageCode,
    editData: state.app.editData,
  };
}

function mapDispatchToProps(dispatch): IConnectedDispatch {
  return {
    onSetPageCode: (value: string, settings) => dispatch(setPage(value, settings))
  };
}

export default 
  connect(mapStateToProps, mapDispatchToProps)(Readings) as React.ComponentClass<IReadingsProps>;
