import * as React from 'react';
import { connect } from 'react-redux';
import { setPage } from '../../actions/app';
import { IPageSettings } from '../../state';

import Loadable from '../components/Loadable';
import Listing from '../components/Listing';
import CustomFilter from '../components/CustomFilter';
import SimpleHeader from '../components/SimpleHeader';
import { TableHeaderColumn } from 'react-bootstrap-table';
import UnitCustomer from './UnitCustomer';
import UnitForm from './forms/UnitForm';
import { PageMode, pages, ExcludeTerms } from '../constants';

import * as utils from '../../App/utils';
import * as searchUtils from '../../App/searchUtils';
import Alert from '../components/Alert';

export interface IUnitState {
  mode: PageMode;
  loading: boolean;
  from: number;
  to: number;
  data: Array<any>;
  total: number;
  editData: Array<any>;
  deleteData: Array<any>;
  searchText: string;
}


export interface IUnitProps {

}

interface IConnectedProps {
  pageCode: string;
  editData: any;
}

interface IConnectedDispatch {
  onSetPageCode: (value: string, settings: IPageSettings) => void;
}

class Unit extends React.Component<IUnitProps & IConnectedProps & IConnectedDispatch, IUnitState> {

  alertRef: any; 

  constructor(props) {
    super(props);

    this.state = {
      mode: PageMode.list,
      loading: true,
      from: 0,
      to: 100,
      searchText: '',
    } as IUnitState;

    this.handleRightItemClick = this.handleRightItemClick.bind(this);
    this.getContentFromMode = this.getContentFromMode.bind(this);
    this.getDataAndDisplay = this.getDataAndDisplay.bind(this);
    this.onRangeUpdate = this.onRangeUpdate.bind(this);
    this.handleFormSave = this.handleFormSave.bind(this);
    this.handleFormCancel = this.handleFormCancel.bind(this);
    this.handleEditData = this.handleEditData.bind(this);
    this.handleDeleteData = this.handleDeleteData.bind(this);
    this.handleSearchData = this.handleSearchData.bind(this);
    this.handleCustomerView = this.handleCustomerView.bind(this);
  }

  componentDidMount() {
    if (this.props.editData) {
      this.handleSearchData(this.props.editData);
    } else {
      this.getDataAndDisplay(this.state.mode, this.state.from, this.state.to);
    }
  }

  handleCustomerView(row) {
    this.setState({
      mode: PageMode.customers_unit,
      editData: row,
    } as IUnitState);
  }

  handleRightItemClick(mode: PageMode) {
    if (mode === PageMode.list) {
      this.setState({
        loading: true,
      } as IUnitState);

      const searchCriteria = this.prepareSearchCriteria(this.state.searchText);
      this.getDataAndDisplay(mode, this.state.from, this.state.to, '', searchCriteria);
    } else {
      this.setState({
        mode,
      } as IUnitState);
    }
  }

  getDataAndDisplay(mode, from, to, sort = '', searchCriteria = '') {
    let formData = new FormData();
    formData.append('Action', 'Fetch');
    formData.append('Path', 'Unit/');
    formData.append('From', from);
    formData.append('To', to);
    formData.append('Sort', sort);
    formData.append('Search', searchCriteria);

    utils.fetch(formData).then(
      (response: any) => {
        const { data, total } = response;
        this.setState({
          mode,
          data,
          loading: false,
          from, 
          to,
          total
        } as IUnitState);
      }
    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );
  }

  onRangeUpdate(from, to) {
    
    const { mode, searchText } = this.state;
    const searchCriteria = this.prepareSearchCriteria(searchText);
    this.getDataAndDisplay(mode, from, to, '', searchCriteria);

    this.setState({
      loading: true,
    } as IUnitState);
  }

  handleFormSave(data, isEdit, id = null) {
    let formData = new FormData();
    let toastrTitle;
    let msg;  

    if (utils.IsEmptyNull(data.unitNumber) || utils.IsEmptyNull(data.complexNo)
     || utils.IsEmptyNull(data.meterNumber) || utils.IsEmptyNull(data.itemCode)) {

      toastrTitle = 'Field Required';
      msg = `The fields 'Unit Number', 'Complex Name', 'Meter Number', 'Item Code' are required`;
      this.alertRef.displayAlert('error', msg, toastrTitle);
      return;
    }
    
    if (isEdit) {
      formData.append('Action', 'Update');
    } else {
      formData.append('Action', 'Insert');
    }

    formData.append('Path', 'Unit/');
    formData.append('UnitCode', data.unitCode);
    formData.append('ComplexUnitNo', data.unitNumber);
    formData.append('ComplexNo', data.complexNo);
    formData.append('MeterNo', data.meterNumber);
    formData.append('ItemCode', data.itemCode);    
    formData.append('PrevReadingDate', data.previousReadingDate);
    formData.append('PrevReading', data.previousReading);
    formData.append('LastReadingDate', data.lastReadingDate);
    formData.append('LastReading', data.lastReading);
    formData.append('CurReadingDate', data.currentReadingDate);
    formData.append('CurReading', data.currentReading);
        
    utils.fetch(formData).then(
      (response: any) => {
        const { status } = response;

        if (status === 'ok') {
          if (!isEdit) {
            
            msg = `${data.unitNumber} Added`;
            toastrTitle = 'New Unit';
            this.alertRef.displayAlert('success', msg, toastrTitle); 

          } else {

            msg = `${data.unitNumber} Updated`;
            toastrTitle = 'Unit';
            this.alertRef.displayAlert('success', msg, toastrTitle);             
          }
        }else {
          this.alertRef.displayAlert('error', 
            `Error while Saving.\nError Message:\n${response.error}`, 'Error');          
        }
        this.getDataAndDisplay(PageMode.list, this.state.from, this.state.to);
      }
    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );

    this.setState({
     loading: true, 
    } as IUnitState);
  }

  handleFormCancel() {
    this.setState({
      mode: PageMode.list,
    } as IUnitState);
  }

  handleEditData(list) {
    this.setState({
      editData: list,
      mode: PageMode.edit,
    } as IUnitState);
  }

  handleDeleteData(list) {
    if (list.length === 0) {
      return;
    }
    
    let ids = '';
    for (let i = 0; i < list.length; i++) {
      ids += ids.length > 0 ? ',' : '';
      ids += list[i].UnitCode;
    }

    utils.canDelete(ids, 'QLD.CanDeleteUnit').then(	
        (response: any) => {   

          let msg;
          let toastrTitle;     
          
          // validate if the record does not depend of others records
          if (!response.canDelete) {  

            msg = 'The record(s) cannot be deleted';
            toastrTitle = 'Delete Unit';
            this.alertRef.displayAlert('info', msg, toastrTitle);                        
            return;
          }else {

            if (!confirm('Are you sure you want to delete selected Units?')) {
              return;
            }
            
            let formData = new FormData();
            formData.append('Action', 'Delete');
            formData.append('Path', 'Unit/');
            formData.append('UnitCode', ids);

            utils.fetch(formData).then(
              (res: any) => {
                const { status } = res;

                this.getDataAndDisplay(PageMode.list, this.state.from, this.state.to);
                
                if (status === 'ok') {
                  
                  msg = 'Selected Complexes were deleted';
                  toastrTitle = 'Delete Unit';
                  this.alertRef.displayAlert('success', msg, toastrTitle); 
                }
              }
            ).catch(
              (ex) => {
                this.alertRef.displayAlert('error', ex, 'Error');
              }
            );
          }
      }

    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );
  }

  prepareSearchCriteria(searchText) {
    const columns = [
      'cu.Custno', 'cu.Surname', 'cu.GivenNames', 'ComplexName', 'u.ComplexUnitNo', 'ci.Cityname'
    ];
    
    return searchUtils.tokenizeSearch('u.UnitCode', searchText, columns, ExcludeTerms);
  }

  handleSearchData(searchText) {
    const searchCriteria = this.prepareSearchCriteria(searchText);
    const { from, to } = this.state;

    this.getDataAndDisplay(PageMode.list, from, to, '', searchCriteria);

    this.setState({
      loading: true,
      searchText,
    } as IUnitState);
  }

  getContentFromMode(mode: PageMode) {
    let content;
    switch (mode) {
      case PageMode.list: 
        let { data } = this.state;
        const columns = [
          {dataField: 'UnitCode', header: 'Unit Code', isKey: true, hidden: true},
          {dataField: 'CustomerName', header: 'Current Customer Name', dataFormat: (cell, row) => {
            return (
              <a style={{cursor:'pointer'}}
                onClick={(e) => this.props.onSetPageCode(pages.customerMaintenance, {editData: row})}>
                {cell}
              </a>
            );
          }},          
          {dataField: 'ComplexName', header: 'Complex Name', dataFormat: (cell, row) => {
            return (
              <a style={{cursor:'pointer'}}
                onClick={(e) => this.props.onSetPageCode(pages.complexMaintenance, {editData: row})}>
                {cell}
              </a>
            );
          }},
          {dataField: 'ComplexUnitNo', header: 'Unit No'},
          {dataField: 'ComplexAddress', header: 'Address'},
          {dataField: 'Cityname', header: 'City'},

          {dataField: 'ComplexUnitNo', header: 'Unit Number' , hidden: true},
          {dataField: 'MeterNo', header: 'Meter Number' , hidden: true},
          {dataField: 'ItemCode', header: 'Item Code' , hidden: true},
        ];

        if (!data) {
          data = [];
        }

        const actions = (
          <TableHeaderColumn 
            dataField="action" 
            dataAlign="center"        
            headerAlign="center"            
            width="100px"
            dataFormat={(cell, row) => {
              return (
                <span>
                  <a className="btn btn-xs btn-warning" title="Edit" 
                    onClick={(e) => {e.preventDefault(); this.handleEditData([row]);}}>
                    <i className="glyphicon glyphicon-edit"/>
                  </a>
                  <a className="btn btn-xs btn-success" title="Manage Customers" 
                    onClick={(e) => {e.preventDefault(); this.handleCustomerView([row]);}}>
                    <i className="fa fa-address-card"/>
                  </a>
                </span>
              );
          }}>
            Actions
          </TableHeaderColumn>
        );

        content = (
          <div>
            <CustomFilter 
              onRefreshData={this.onRangeUpdate} 
              total={this.state.total}  
              from={this.state.from}  
              to={this.state.to} 
              withSearch={true} 
              onSearch={this.handleSearchData} 
              searchText={this.state.searchText} 
              withActions={true} 
              withRange={true}            
              />
            <Listing 
              data={data}
              columns={columns} 
              onRefreshData={this.onRangeUpdate}
              onEditData={this.handleEditData}
              onDeleteData={this.handleDeleteData}
              withActions={true}
              actions={actions}/>
          </div>
        );
        break;

      case PageMode.new: 
        content = <UnitForm onSave={this.handleFormSave} onCancel={this.handleFormCancel}/>;
        break;

      case PageMode.edit: 
        content = <UnitForm data={this.state.editData} 
          onSave={this.handleFormSave} onCancel={this.handleFormCancel}/>;
        break;

      case PageMode.customers_unit:      
        content = <UnitCustomer unitCode={this.state.editData[0].UnitCode} 
          complexName={this.state.editData[0].ComplexName} unitNo={this.state.editData[0].ComplexUnitNo}/>;
        break;

      default: 
        content = null;
    }

    return content;
  }

  render() {
    
    const brand = (
      <span>
        <i className="glyphicon glyphicon-list-alt"/> Unit Codes Maintenance
      </span>
    );

    const content = this.getContentFromMode(this.state.mode);

    return (
      <div>
        <Alert ref={x => this.alertRef = x} />
        <Loadable isLoading={this.state.loading} text={'Fetching Data...'}>
          <SimpleHeader 
            brand={brand}
            listLabel="List"
            newLabel="New Unit Code"
            onRightItemClick={this.handleRightItemClick}
            mode={this.state.mode}
          />
          {content}
        </Loadable>
      </div>
    );
  }
}

function mapStateToProps(state: any): IConnectedProps {
  return {
    pageCode: state.app.pageCode,
    editData: state.app.editData,
  };
}

function mapDispatchToProps(dispatch): IConnectedDispatch {
  return {
    onSetPageCode: (value: string, settings) => dispatch(setPage(value, settings))
  };
}

export default 
  connect(mapStateToProps, mapDispatchToProps)(Unit) as React.ComponentClass<IUnitProps>;
