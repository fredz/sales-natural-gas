import * as React from 'react';
import * as moment from 'moment';
import Loadable from '../components/Loadable';
import Listing from '../components/Listing';
import SimpleHeader from '../components/SimpleHeader';
import { PageMode } from '../constants';
import * as utils from '../../App/utils';
import Alert from '../components/Alert';
import { TableHeaderColumn } from 'react-bootstrap-table';
import * as fileUtils from '../../App/fileUtils';
import { Row, Column } from '../../ascendis-ui/layout';
import { Dropdown } from '../../ascendis-ui/components';

export interface IDebtState {
  mode: PageMode;
  loading: boolean;
  data: Array<any>;
  reminderTerms: any;  
  letterFilter: string;
  emailCount?: number;
  emailTotal?: number;
  emailError?: number;  
  loadingText: string;
}

class DebtCollection extends React.Component<any, IDebtState> {

  alertRef: any;
  fetching = 'Fetching Data';

  constructor(props) {
    super(props);

    this.state = {
      mode: PageMode.list,
      loading: true,
      reminderTerms: [], 
      loadingText: this.fetching,     
    } as IDebtState;

    this.getContentFromMode = this.getContentFromMode.bind(this);
    this.getDataAndDisplay = this.getDataAndDisplay.bind(this);
    this.onRangeUpdate = this.onRangeUpdate.bind(this);        
    this.handlePrintLetter = this.handlePrintLetter.bind(this);
    this.handleSendLetterEmail = this.handleSendLetterEmail.bind(this);    
    this.handleInputChange = this.handleInputChange.bind(this);
    this.filterDebCollection = this.filterDebCollection.bind(this);
    this.handleProcessedClick = this.handleProcessedClick.bind(this);
    this.handleBeforeSaveCell = this.handleBeforeSaveCell.bind(this);
  }

  handlePrintLetter(data) {
    
    let self = this;
    const { mode } = this.state;
    let reminderTerm = data[0];

    if (reminderTerm.ReminderTermStatus !== 'None') {
      fileUtils.printLetter(reminderTerm.InvoiceNo, reminderTerm.ReminderTermStatus);

      setTimeout(() => {
        self.getDataAndDisplay(mode);
      }, 1000);
    }else {
        let toastrTitle = 'Print Letter';
        let message = `Select a valid letter type`;
        this.alertRef.displayAlert('error', message, toastrTitle);      
    }

  }  

  handleSendLetterEmail(data) {

    if (!confirm('Are you sure to send Email notification?')) {
      return false;
    }    
    
    let reminderTerm = data[0];
    if (reminderTerm.ReminderTermStatus !== 'None') {      

      utils.sendLetterEmail(reminderTerm.InvoiceNo, reminderTerm.CustNo, 
      reminderTerm.ReminderTermStatus).then((result: any) => {

        if (result.Status !== 'Sent') {
          alert(`Email could not be sent, please communicate Customer No.: ${reminderTerm.CustNo}`);
        }
        this.setState({
          loading: false,
        });
      }).catch(() => {
        alert('An unexpected error happened');
      });
      
      this.setState({
        loading: true,        
      });
    }else {
        let toastrTitle = 'Email Letter';
        let message = `Select a valid letter type`;
        this.alertRef.displayAlert('error', message, toastrTitle);      
    }

  }    

  componentDidMount() {
    this.getDataAndDisplay(this.state.mode);
    this.getReminderDataAndDisplay();
  }

  handleInputChange(prop, value) {
    let state = {};

    state[prop] = value.value;
    this.setState(state as IDebtState);

    if (prop === 'letterFilter') {      
      this.filterDebCollection(value.label);
    }
  }   

  filterDebCollection(letterFilter) {

    const { mode } = this.state;
    let search = '';

    this.setState({
      loading: true,
    } as IDebtState);        
    
    if (letterFilter === 'Reminder') {
      search = 'R=1';
    }else if (letterFilter === 'Disconnect') {
      search = 'D=1';
    }else if (letterFilter === 'Legal') {
      search = 'L=1';
    }else if (letterFilter === 'None') {
      search = 'R=0 AND D=0 AND L=0';
    }

    if (search.length > 0) {
      search = ' WHERE ' + search;
    }

    this.getDataAndDisplay(mode, search);
  }

  handleProcessedClick(name: string, rows: any) {
    switch (name) {
      case 'emailall':
        this.emailAll(rows);
        break;
      case 'printall':
        this.printAll(rows);
      
      default:
        break;
    }
  }

  emailAll(data: any) {
    
    const {sendLetterEmail} = utils;

    if (!confirm('Are you sure to sent Email notification?')) {
      return false;
    }

    const emailList: Array<any> = data.filter( x => x.Email !== '' && x.ReminderTermStatus !== 'None'  );
    const total = emailList.length;
    let message;

    if (total === 0) {
      message = 'No customer for the selected Debt Collection has email registered';
      this.alertRef.displayAlert('Email', message, 'Error');
      return;
    }

    this.setState({
      loading: true,
      loadingText: `Mailing 0 from ${total}`,
      emailCount: 0,
      emailTotal: total,
      emailError: 0,
    });

    emailList.forEach(element => {
      sendLetterEmail(element.InvoiceNo, element.CustNo, element.ReminderTermStatus).then((result: any) => {

        let { emailCount, emailError, emailTotal} = this.state;
        if (result.Status !== 'Sent') {
          emailError++;
        }
        emailCount++;

        if (emailCount === emailTotal) {
          this.setState({
            loading: false,
            loadingText: this.fetching,
          });
          return;
        }

        this.setState({
          loading: true,
          loadingText: `Mailing ${emailCount} from ${emailTotal}`,
          emailCount,
          emailTotal: total,
          emailError,
        });


      }).catch(() => {
        let { emailCount, emailError, emailTotal} = this.state;
        emailCount++;
        emailError++;

        if (emailCount === emailTotal) {
          this.setState({
            loading: false,
            loadingText: this.fetching,
          });
          return;
        }

        this.setState({
          loading: true,
          loadingText: `Sending Emails ${emailCount} from ${emailTotal}...`,
          emailCount,
          emailTotal: total,
          emailError,
        });
      });
    });
  }  

  printAll(data: any) {

    let arrayInvoiceNo: any = [];
    let arrayType: any = [];

    data.forEach( ( item ) => {
      if (item.ReminderTermStatus !== 'None') {
        arrayInvoiceNo.push(item.InvoiceNo);
        arrayType.push(item.ReminderTermStatus);
      }
    }); 

    fileUtils.printLettersAll(arrayInvoiceNo.toString(), arrayType.toString());
  }    

  handleRightItemClick(mode: PageMode) {

    if (mode === PageMode.list) {
      this.setState({
        loading: true,
      } as IDebtState);

      this.getDataAndDisplay(mode);
    } else {
      this.setState({
        mode,
      } as IDebtState);
    }
  }  

  getDataAndDisplay(mode, SearchLetter: string = '') {
    let formData = new FormData();
    formData.append('Action', 'Fetch');
    formData.append('Path', 'DebtCollection/');    
    formData.append('SearchLetter', SearchLetter);    

    utils.fetch(formData).then(
      (response: any) => {
        const { data } = response;
        
        this.setState({
          mode,
          data,
          loading: false,
        } as IDebtState);
      }
    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );
  }

  onRangeUpdate(from, to) {
    const { mode } = this.state;
    this.getDataAndDisplay(mode);

    this.setState({
      loading: true,
    } as IDebtState);
  }

  prepareStatusIcon(status) {
      let iconClass = 'none';

      if (status.length > 0 && status === 'True' ) {
          iconClass = 'fa fa-check';
      }

      return (              
        <div className="text-center" >
          <i className={iconClass}>&nbsp;</i> 
        </div>
      );
  }

  getReminderDataAndDisplay() {
    utils.fetchReminderTerms().then(
      (response: any) => {
        const reminderterms = response.data;        
        this.setState({          
          reminderTerms: reminderterms          
        } as IDebtState);
      }
    ).catch(
      (ex) => {
        alert(ex);
      }
    );
  } 

  handleBeforeSaveCell(row, cellName, cellValue) {
    
    const {InvoiceNo} = row; 

    switch (cellName) {
      case 'ReminderTermStatus':

        this.state.data.forEach((element, index) => {            
          if (element.InvoiceNo === InvoiceNo) {
            element.ReminderTermStatus = cellValue;
          }
        });
        break;

      default:
        break;
    }
  }  

  getContentFromMode(mode: PageMode) {

    const ReminderTerms = ['Reminder', 'Disconnect', 'Legal', 'None'];
    let content;
    
    switch (mode) {
      case PageMode.list: 
        let { data } = this.state;
        const columns = [
          {dataField: 'InvoiceNo', header: 'Invoice No', isKey: true},
          {dataField: 'Name', header: 'Name'},
          {dataField: 'CustNo', header: 'Cust No', width: '90', hidden: true},
          {dataField: 'UnitNo', header: 'Unit No', width: '80'},
          {dataField: 'ComplexName', header: 'Complex Name'},
          {dataField: 'Owing', header: 'Owing', width: '80'},
          {dataField: 'DueDate', header: 'Due Date', hidden: true},
          {dataField: 'LastPay', header: 'Last Pay', hidden: true},
          {dataField: 'LastPayDate', header: 'Last Pay Date', hidden: true},          
          {dataField: 'R', header: 'Reminder Sent' , hidden: false, dataFormat: (cell, row) => {
            const { R } = row;
            return this.prepareStatusIcon(R);
          }, width: '60'},
          {dataField: 'D', header: 'Disconnect Sent', dataFormat: (cell, row) => {
            const { D } = row;
            return this.prepareStatusIcon(D);
          }, width: '60'},
          {dataField: 'L', header: 'Legal Sent', dataFormat: (cell, row) => {
            const { L } = row;
            return this.prepareStatusIcon(L);
          }, width: '60'},
          {dataField: 'RDays', header: 'Days Left for Reminder', width: '60'}, 
          {dataField: 'DDays', header: 'Days Left for Disconnect', width: '60'}, 
          {dataField: 'LDays', header: 'Days Left for Legal', width: '60'},   
          {dataField: 'InvoiceDate', header: 'Date', isDate: true, dataFormat: (cell, row) => {
            return <span>{cell ? moment(cell).format('D MMM Y') : ''}</span>;
          }},                   
          {dataField: 'ReminderTermStatus', header: 'Suggested', thStyle: {background: '#d9edf7'},
           editable: {
            type: 'select', options: { values: ReminderTerms }
          }}
        ];
        if (!data) {
          data = [];
        }

        const actions = (
          <TableHeaderColumn 
            dataField="action"             
            headerAlign="center"             
            width="100px" 
            editable={false}
            dataFormat={(cell, row) => {                 
              return (
                <span>
                  <a className="btn btn-xs " title="Print Letter" 
                    onClick={(e) => {e.preventDefault(); this.handlePrintLetter([row]);}}>
                    <i className="fa fa-print"/>                   
                  </a>
                  {
                    row.Email ? ( 
                    <a className="btn btn-xs btn-default" title="Email Debt Collection" 
                      onClick={(e) => {
                        e.preventDefault(); 
                        this.handleSendLetterEmail([row]);
                        }
                      }>
                      <i className="fa fa-envelope"/>
                    </a>) : null
                  }                  
                </span>
              );
          }}>
            Actions
          </TableHeaderColumn>
        );

        //const printMenu = null;
        const printMenu = (
          <div className="col-md-12">
            <div className="col-md-offset-8 col-md-4" style={{textAlign: 'right'}}>
              <a className="btn btn-info" title="Print All to PDF Bundle"
                onClick={(e) => {
                  e.preventDefault(); 
                  this.handleProcessedClick('printall', this.state.data);
                  //click event 
                }}>
                <i className="fa fa-file-pdf-o"/> All
              </a>

              <a className="btn btn-info" title="Print All Without Email"
                onClick={(e) => {
                  e.preventDefault(); 
                  this.handleProcessedClick('printall', 
                      this.state.data.filter(x => x.Email === ''));
                }}>
                <i className="fa fa-file-pdf-o"/> All Without <i className="fa fa-envelope"/>
              </a>

              <a className="btn btn-info" title="Email All"
                onClick={(e) => {
                  e.preventDefault();
                  this.handleProcessedClick('emailall', this.state.data);
                  //click event 
                }}>
                <i className="fa fa-envelope"/> All
              </a>
            </div>
          </div>          
        );

        const arrayLetters = [{value: '0', label: 'Reminder'},
                              {value: '1', label: 'Disconnect'},
                              {value: '2', label: 'Legal'},
                              {value: '3', label: 'None'}];

        content = (
          <div>
            <Row>
              <Column column="6">
                <Dropdown 
                  list={arrayLetters}
                  label="Filter Deb Collection" 
                  inputName="letterFilter"
                  onValueChange={(value) => {this.handleInputChange('letterFilter', value);}}
                  value={this.state.letterFilter}/>
              </Column>            
            </Row>
            {printMenu}
            <Listing 
              data={data}
              columns={columns} 
              onRefreshData={this.onRangeUpdate}
              onEditData={null}
              onDeleteData={null}
              withActions={true}
              actions={actions}
              cellEdit={{
                mode: 'click',
                blurToSave: true,
                beforeSaveCell: this.handleBeforeSaveCell,
              }}/>
          </div>
        );
        break;

      default: 
        content = null;
    }

    return content;
  }

  render() {
    
    const brand = (
      <span>
        <i className="glyphicon glyphicon-list-alt"/> Debt Collection
      </span>
    );

    const content = this.getContentFromMode(this.state.mode);

    return (
      <div>
        <Alert ref={x => this.alertRef = x} />
        <Loadable isLoading={this.state.loading} text={this.fetching}>
          <SimpleHeader 
            brand={brand}
            listLabel="List"
            newLabel={null}
            onRightItemClick={this.handleRightItemClick}
            mode={PageMode.new}
            withOptions={false}
          />
          {content}
        </Loadable>
      </div>
    );
  }
}

export default DebtCollection;