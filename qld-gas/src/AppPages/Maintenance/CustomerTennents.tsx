import * as React from 'react';
import * as ReactToastr from 'react-toastr';
import { ToastContainer } from 'react-toastr';

import Loadable from '../components/Loadable';
import Listing from '../components/Listing';
import SimpleHeader from '../components/SimpleHeader';
import CustomerTennentForm from './forms/CustomerTennentForm';
import CustomFilter from '../components/CustomFilter';
import { PageMode } from '../constants';

import * as utils from '../../App/utils';
import Alert from '../components/Alert';

export interface ICustomerTennentsProps {
  custno: string;
}

export interface ICustomerTennentsState {
  mode: PageMode;
  loading: boolean;
  from: number;
  to: number;
  data: Array<any>;
  total: number;
  custNo: string;
  editData: Array<any>;
}

class CustomerTennents extends React.Component<ICustomerTennentsProps, ICustomerTennentsState> {
  container: ToastContainer;

  alertRef: any;

  constructor(props) {
    super(props);

    this.state = {
      mode: PageMode.list,
      loading: true,
      from: 0,
      to: 100,
      custNo: '',
      editData: [],
    } as ICustomerTennentsState;

    this.handleRightItemClick = this.handleRightItemClick.bind(this);
    this.getContentFromMode = this.getContentFromMode.bind(this);
    this.getDataAndDisplay = this.getDataAndDisplay.bind(this);
    this.onRangeUpdate = this.onRangeUpdate.bind(this);
    this.onEditDataUpdate = this.onEditDataUpdate.bind(this);
    this.handleFormSave = this.handleFormSave.bind(this);
    this.handleFormCancel = this.handleFormCancel.bind(this);
    this.onDeleteData = this.onDeleteData.bind(this);
  }

  componentDidMount() {
    this.getDataAndDisplay(this.state.mode, this.state.from, this.state.to);
  }

  handleRightItemClick(mode: PageMode) {
    if (mode === PageMode.list) {
      this.setState({
        loading: true,
      } as ICustomerTennentsState);

      this.getDataAndDisplay(mode, this.state.from, this.state.to);
    } else {
      this.setState({
        mode,
      } as ICustomerTennentsState);
    }
  }

  getDataAndDisplay(mode, from, to, sort = '') {
    let formData = new FormData();
    formData.append('Action', 'Fetch');
    formData.append('Path', 'CustomerTennents/');
    formData.append('CustNo', this.props.custno);
    formData.append('From', from);
    formData.append('To', to);
    formData.append('Sort', sort);

    utils.fetch(formData).then(
      (response: any) => {
        const { data, total } = response;
        this.setState({
          mode,
          data,
          loading: false,
          from, 
          to,
          total
        } as ICustomerTennentsState);
      }
    ).catch(
      (ex) => {        
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );
  }

  onRangeUpdate(from, to) {
    const { mode } = this.state;
    this.getDataAndDisplay(mode, from, to);

    this.setState({
      loading: true,
    } as ICustomerTennentsState);
  }

  onEditDataUpdate(list) {
    this.setState({
      editData: list,
      mode: PageMode.edit,
    } as ICustomerTennentsState);
  }

  onDeleteData(list) {
    if (list.length === 0) {
      return;
    }
    
    let ids = '';
    for (let i = 0; i < list.length; i++) {
      ids += ids.length > 0 ? ',' : '';
      ids += list[i].CustTennentsID;
    }

    utils.canDelete(ids, 'QLD.CanDeleteCustomerTennents').then(	
      (response: any) => {        
        
        // validate if the record does not depend of others records
        if(!response.canDelete) {            
          this.alertRef.displayAlert('Delete', 'The record(s) cannot be deleted', 'Error');      
          return;
        }else{  

          if (!confirm('Are you sure you want to delete selected tenants?')) {
            return;
          }
          
          let formData = new FormData();
          formData.append('Action', 'Delete');
          formData.append('Path', 'CustomerTennents/');
          formData.append('CustTennentsID', ids);

          utils.fetch(formData).then(
            (response: any) => {
              const { status } = response;

              this.getDataAndDisplay(PageMode.list, this.state.from, this.state.to);
              
              if (status === 'ok') {                
                this.alertRef.displayAlert('Delete', 'Selected tenants were deleted', 'success');      
              }
            }
          ).catch(
            (ex) => {              
              this.alertRef.displayAlert('error', ex, 'Error');
            }
          );
    }
  }

    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );

    
  }

  getContentFromMode(mode: PageMode) {
    let content;
    switch (mode) {
      case PageMode.list:
        let { data } = this.state;
        const columns = [
          {dataField: 'CustTennentsID', header: 'Tenant No', isKey: true},
          {dataField: 'Title', header: 'Title'},
          {dataField: 'FirstName', header: 'FirstName'},
          {dataField: 'Surname', header: 'SurName'},
          {dataField: 'IncludeOnInvoiceText', header: 'Include On Invoice'},
        ];

        if (!data) {
          data = [];
        }

        content = (
          <div>
            <CustomFilter 
              onRefreshData={this.onRangeUpdate} 
              total={this.state.total}  
              from={this.state.from}  
              to={this.state.to} 
              withSearch={false}  
              withActions={true}             
              />
            <Listing 
              data={data}
              columns={columns} 
              onRefreshData={this.onRangeUpdate}
              onEditData={this.onEditDataUpdate}
              onDeleteData={this.onDeleteData}
              withActions={true}/>
          </div>
        );
        break;

      case PageMode.new: 
        content = <CustomerTennentForm custno={this.props.custno} 
        onCancel={this.handleFormCancel} onSave={this.handleFormSave}/>;
        break;

      case PageMode.edit:
        content = <CustomerTennentForm data={this.state.editData} custno={this.props.custno}
         onSave={this.handleFormSave} onCancel={this.handleFormCancel}/>;
        break;

      default: 
        content = null;
    }

    return content;
  }

  handleFormSave(data, isEdit, id = null) {
    let formData = new FormData();
    
    if (isEdit) {
      formData.append('Action', 'Update');
      formData.append('CustTennentsID', id);
    } else {
      formData.append('Action', 'Insert');
    }
    formData.append('Path', 'CustomerTennents/');
    formData.append('CustNo', this.props.custno);
    formData.append('Title', data.title);
    formData.append('FirstName', data.firstName);
    formData.append('Surname', data.surname);
    formData.append('IncludeOnInvoice', data.includeOnInvoice === true ? '1' : '0');

    utils.fetch(formData).then(
      (response: any) => {
        const { status } = response;

        let message;
        let toastrTitle;
        if (status === 'ok') {
          const { title, firstName, surname } = data;
          if (!isEdit) {
            toastrTitle = 'Record updated';
            message = `New Customer Tenant: ${title} ${firstName} ${surname} for ${this.props.custno} Added`;
          } else {
            toastrTitle = 'Record added';
            message = `Customer Tenant: ${title} ${firstName} ${surname} for ${this.props.custno} Updated`;
          }

          this.alertRef.displayAlert(toastrTitle, message, 'Info');            

        }else {
          this.alertRef.displayAlert('error', 
            `Error while Saving.\nError Message:\n${response.error}`, 'Error');                    
        }

        this.setState({
          loading: false,
          mode: PageMode.list,
        } as ICustomerTennentsState);
      }
    ).catch(
      (ex) => {        
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );

    this.setState({
     loading: true, 
    } as ICustomerTennentsState);

    this.getDataAndDisplay(PageMode.list, this.state.from, this.state.to);
  }

  handleFormCancel() {
    this.setState({
      mode: PageMode.list,
    } as ICustomerTennentsState);
  }

  render() {
    
    const brand = (
      <span>
        <i className="glyphicon glyphicon-list-alt"/> {`Tenants Maintenance`}
      </span>
    );

    const content = this.getContentFromMode(this.state.mode);
    const ToastMessageFactory = React.createFactory(ReactToastr.ToastMessage.animation);
    return (
      <div style={{margin: '5px', border: '1px solid black', marginBottom: '25px'}}>
        <Alert ref={x => this.alertRef = x} />
        <Loadable isLoading={this.state.loading} text={'Fetching Data...'} marginTop={15}>
          <SimpleHeader 
            brand={brand}
            listLabel="List"
            newLabel="New Tenant"
            onRightItemClick={this.handleRightItemClick}
            mode={this.state.mode}
          />
          {content}
        </Loadable>
        <ToastContainer ref={(element) => {this.container = element;}}
          toastMessageFactory={ToastMessageFactory}
          className="toast-top-right" />
      </div>
    );
  }
}

export default CustomerTennents;