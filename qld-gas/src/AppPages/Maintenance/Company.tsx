import * as React from 'react';
import * as ReactToastr from 'react-toastr';
import { ToastContainer } from 'react-toastr';

import Loadable from '../components/Loadable';
import CompanyForm from './forms/CompanyForm';
import { PageMode } from '../constants';

import * as utils from '../../App/utils';
import Alert from '../components/Alert';

export interface ICompanyState {
  mode: PageMode;
  loading: boolean;
  data: Array<any>;
  editData: Array<any>;
}

class Company extends React.Component<any, ICompanyState> {
  container: ToastContainer;
  alertRef: any; 

  constructor(props) {
    super(props);

    this.state = {
      mode: PageMode.edit,
      loading: true,
      editData: [],
    } as ICompanyState;

    this.handleRightItemClick = this.handleRightItemClick.bind(this);
    this.getContentFromMode = this.getContentFromMode.bind(this);
    this.getDataAndDisplay = this.getDataAndDisplay.bind(this);
    this.onEditDataUpdate = this.onEditDataUpdate.bind(this);
    this.handleFormSave = this.handleFormSave.bind(this);
  }

  componentDidMount() {
    this.getDataAndDisplay(this.state.mode, 0, 1);
  }

  handleRightItemClick(mode: PageMode) {
    if (mode === PageMode.list) {
      this.setState({
        loading: true,
      } as ICompanyState);

      this.getDataAndDisplay(mode, 0, 1);
    } else {
      this.setState({
        mode,
      } as ICompanyState);
    }
  }

  getDataAndDisplay(mode, from, to, sort = '') {
    let formData = new FormData();
    formData.append('Action', 'Fetch');
    formData.append('Path', 'CoInfo/');
    formData.append('From', from);
    formData.append('To', to);
    formData.append('Sort', sort);

    utils.fetch(formData).then(
      (response: any) => {
        const { data } = response;
        this.setState({
          data,
          loading: false,
        } as ICompanyState);
      }
    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );
  }

  onRangeUpdate(from, to) {
    const { mode } = this.state;
    this.getDataAndDisplay(mode, from, to);

    this.setState({
      loading: true,
    } as ICompanyState);
  }

  onEditDataUpdate(list) {
    this.setState({
      editData: list,
      mode: PageMode.edit,
    } as ICompanyState);
  }

  getContentFromMode(mode: PageMode) {
    let content;
    switch (mode) {
      case PageMode.edit:
        content = <CompanyForm data={this.state.data}
         onSave={this.handleFormSave}/>;
        break;

      default: 
        content = null;
    }

    return content;
  }

  handleFormSave(data, isEdit, id = null) {
    let formData = new FormData();
    
    if (isEdit) {
      formData.append('Action', 'Update');
    }
    
    formData.append('Path', 'CoInfo/');
    formData.append('CoName', data.companyName);
    formData.append('CoAddr1', data.companyAddress1);
    formData.append('CoAddr2', data.companyAddress2);
    formData.append('CoAddr3', data.companyAddress3);
    formData.append('CoCity', data.city);
    formData.append('CoState', data.state);
    formData.append('CoPcode', data.postcode);
    formData.append('CoCountry', data.country);
    formData.append('CoPhoneNo', data.phoneNumber);
    formData.append('CoFaxNo', data.faxNumber);
    formData.append('CoACN', data.acn);
    formData.append('CoABN', data.abn);
    formData.append('CoDefTerm', data.defaultTerm);
    formData.append('CoAdmFee', data.administrationFee);
    formData.append('CoLateFee', data.lateFee);
    
    utils.fetch(formData).then(
      (response: any) => {
        const { status } = response;

        let message;
        let toastrTitle;
        if (status === 'ok') {
          const { companyName } = data;

          toastrTitle = 'Company Updated';
          message = `Company ${companyName} Updated`;
          
          this.alertRef.displayAlert(toastrTitle, message, 'success');    

        }else {
          this.alertRef.displayAlert('error', 
            `Error while Saving.\nError Message:\n${response.error}`, 'Error');             
        }
        this.getDataAndDisplay(PageMode.list, 0, 1);
      }
    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );

    this.setState({
     loading: true, 
    } as ICompanyState);
  }

  render() {
    const content = this.getContentFromMode(this.state.mode);
    const ToastMessageFactory = React.createFactory(ReactToastr.ToastMessage.animation);
    return (
      <div style={{margin: '0 5px', padding: '15px'}}>
        <Alert ref={x => this.alertRef = x} />
        <Loadable isLoading={this.state.loading} text={'Fetching Data...'}>
          {content}
        </Loadable>
        <ToastContainer ref={(element) => {this.container = element;}}
          toastMessageFactory={ToastMessageFactory}
          className="toast-top-right" />
      </div>
    );
  }
}

export default Company;