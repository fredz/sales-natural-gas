import * as React from 'react';
import { connect } from 'react-redux';
import { setPage } from '../../actions/app';
import { IPageSettings } from '../../state';

import { TableHeaderColumn } from 'react-bootstrap-table';
import Loadable from '../components/Loadable';
import Listing from '../components/Listing';
import SimpleHeader from '../components/SimpleHeader';
import ComplexForm from './forms/ComplexForm';
import { PageMode, pages } from '../constants';
import CustomFilter from '../components/CustomFilter';
import Alert from '../components/Alert';

import * as utils from '../../App/utils';

export interface IComplexState {
  mode: PageMode;
  loading: boolean;
  from: number;
  to: number;
  data: Array<any>;
  total: number;
  editData: Array<any>;
  deleteData: Array<any>;
  searchText: string;
}

export interface IComplexProps {

}

interface IConnectedProps {
  pageCode: string;
  editData: any;
}

interface IConnectedDispatch {
  onSetPageCode: (value: string, settings: IPageSettings) => void;
}

class Complex extends React.Component<IComplexProps & IConnectedProps & IConnectedDispatch, IComplexState> {
  
  alertRef: any; 

  constructor(props) {
    super(props);

    this.state = {
      mode: PageMode.list,
      loading: true,
      from: 0,
      to: 100,
      searchText: '',
    } as IComplexState;

    this.handleRightItemClick = this.handleRightItemClick.bind(this);
    this.getContentFromMode = this.getContentFromMode.bind(this);
    this.getDataAndDisplay = this.getDataAndDisplay.bind(this);
    this.onRangeUpdate = this.onRangeUpdate.bind(this);
    this.handleFormSave = this.handleFormSave.bind(this);
    this.handleFormCancel = this.handleFormCancel.bind(this);
    this.handleEditData = this.handleEditData.bind(this);
    this.handleDeleteData = this.handleDeleteData.bind(this);
    this.handleSearchData = this.handleSearchData.bind(this);
  }

  componentDidMount() {
    if (this.props.editData) {
      this.handleSearchData(this.props.editData.ComplexName);
    } else {
      this.getDataAndDisplay(this.state.mode, this.state.from, this.state.to);
    }
  }

  handleRightItemClick(mode: PageMode) {
    if (mode === PageMode.list) {
      this.setState({
        loading: true,
      } as IComplexState);

      const searchCriteria = this.prepareSearchCriteria(this.state.searchText);
      this.getDataAndDisplay(mode, this.state.from, this.state.to, '', searchCriteria);
    } else {
      this.setState({
        mode,
      } as IComplexState);
    }
  }

  getDataAndDisplay(mode, from, to, sort = '', searchCriteria = '') {
    let formData = new FormData();
    formData.append('Action', 'Fetch');
    formData.append('Path', 'Complex/');
    formData.append('From', from);
    formData.append('To', to);
    formData.append('Sort', sort);
    formData.append('Search', searchCriteria);

    utils.fetch(formData).then(
      (response: any) => {
        const { data, total } = response;
        this.setState({
          mode,
          data,
          loading: false,
          from, 
          to,
          total
        } as IComplexState);
      }
    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );
  }

  onRangeUpdate(from, to) {
    const { mode } = this.state;
    this.getDataAndDisplay(mode, from, to);

    this.setState({
      loading: true,
    } as IComplexState);
  }

  handleFormSave(data, isEdit, id = null) {

    let msg;
    let toastrTitle;

    if (utils.IsEmptyNull(data.complexName) || utils.IsEmptyNull(data.address1)
     || utils.IsEmptyNull(data.cityCode)) {

      toastrTitle = 'Field Required';
      msg = `The fields 'Complex Name', 'address1', 'City Code' are required`;
      this.alertRef.displayAlert('error', msg, toastrTitle);
      return;
    }

    let formData = new FormData();
    
    if (isEdit) {
      formData.append('Action', 'Update');
    } else {
      formData.append('Action', 'Insert');
    }
    formData.append('Path', 'Complex/');
    formData.append('ComplexNo', data.complexNo);
    formData.append('ComplexName', data.complexName);
    formData.append('Address1', data.address1);
    formData.append('Address2', data.address2);
    formData.append('Address3', data.address3);
    formData.append('CompCityCode', data.cityCode);
    formData.append('NoUnits', data.noUnits);
    formData.append('ServiceType', data.serviceType);

    utils.fetch(formData).then(
      (response: any) => {

        const { status } = response;        

        if (status === 'ok') {
          if (!isEdit) {
            msg = `${data.complexNo} ${data.complexName} Added`;
            toastrTitle = 'New Complex';
            this.alertRef.displayAlert('success', msg, toastrTitle);
          } else {

            msg = `${data.complexNo} ${data.complexName} Updated`;
            toastrTitle = 'Complex';
            this.alertRef.displayAlert('success', msg, toastrTitle);
          }
        } else {
          this.alertRef.displayAlert('error', `Error while Saving.\nError Message:\n${response.error}`, 'Error');
        }
        this.getDataAndDisplay(PageMode.list, this.state.from, this.state.to);
      }
    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );

    this.setState({
     loading: true, 
    } as IComplexState);
  }

  handleFormCancel() {
    this.setState({
      mode: PageMode.list,
    } as IComplexState);
  }

  handleEditData(list) {
    this.setState({
      editData: list,
      mode: PageMode.edit,
    } as IComplexState);
  }

  handleDeleteData(list) {
    if (list.length === 0) {
      return;
    }
    
    let ids = '';
    for (let i = 0; i < list.length; i++) {
      ids += ids.length > 0 ? ',' : '';
      ids += list[i].ComplexNo;
    }

    utils.canDelete(ids, 'QLD.CanDeleteComplex').then(	
        (response: any) => {

          let msg;        
          let toastrTitle;

          // validate if the record does not depend of others records
          if (!response.canDelete) {  

            msg = 'The record(s) cannot be deleted';
            toastrTitle = 'Delete Complex';
            this.alertRef.displayAlert('info', msg, toastrTitle);            

            return;
          } else {  

            if (!confirm('Are you sure you want to delete selected Complexes?')) {
              return;
            }
            
            let formData = new FormData();
            formData.append('Action', 'Delete');
            formData.append('Path', 'Complex/');
            formData.append('ComplexNo', ids);

            utils.fetch(formData).then(
              (res: any) => {
                const { status } = res;
                this.getDataAndDisplay(PageMode.list, this.state.from, this.state.to);
                
                if (status === 'ok') {
                  
                  msg = 'Selected Complexes were deleted';
                  toastrTitle = 'Delete Complex';
                  this.alertRef.displayAlert('success', msg, toastrTitle); 
                }
              }
            ).catch(
              (ex) => {
                this.alertRef.displayAlert('error', ex, 'Error');
              }
            );    
          }
      }

    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );

  }

  prepareSearchCriteria(searchText) {

    let searchCriteria = `(ComplexNo LIKE '%${searchText}%' `;
    searchCriteria += `OR ComplexName LIKE '%${searchText}%' `;
    searchCriteria += `OR CompCityCode LIKE '%${searchText}%' `;
    searchCriteria += `OR Address1 LIKE '%${searchText}%') `;

    return searchCriteria;
  }

  handleSearchData(searchText) {
    let searchCriteria = this.prepareSearchCriteria(searchText);
    const { from, to } = this.state;

    this.getDataAndDisplay(PageMode.list, from, to, '', searchCriteria);
    this.setState({
      loading: true,
      searchText,
    } as IComplexState);
  }

  getContentFromMode(mode: PageMode) {
    let content;
    switch (mode) {
      case PageMode.list: 
        let { data } = this.state;
        const columns = [
          {dataField: 'ComplexNo', header: 'Complex No', isKey: true, hidden: true},
          {dataField: 'ComplexName', header: 'Complex Name'},
          {dataField: 'ComplexAddress', header: 'Address'},
          {dataField: 'Cityname', header: 'City'},
          {dataField: 'Statename', header: 'State'},
          {dataField: 'NoUnits', header: 'No. of Units', dataFormat: (cell, row) => {
            return (
              <a style={{cursor:'pointer'}}
                onClick={(e) => this.props.onSetPageCode(pages.unitcodesMaintenance, {editData: row.ComplexName})}>
                {cell}
              </a>
            );
          }},
          {dataField: 'CompCityCode', header: 'Complex City Code', hidden: true},
          
        ];

        if (!data) {
          data = [];
        }

        const actions = (
          <TableHeaderColumn 
            dataField="action" 
            dataAlign="center"        
            headerAlign="center"            
            width="100px"   
            dataFormat={(cell, row) => {
              let page = row.ServiceType === '1' ? pages.meterReading : pages.bottleGasDelivery;

              let conf = {
                color: '',
                title: ''
              };

              if (page === pages.meterReading) {
                conf.color = '#8cc837';
                conf.title = 'Enter Meter Readings';
              } else {
                conf.color = '#31a1c9';
                conf.title = 'Enter Bottle Deliveries';
              }
              
              return (
                <span>
                  <a className="btn btn-xs btn-warning" title="Edit" 
                    onClick={(e) => {e.preventDefault(); this.handleEditData([row]);}}>
                    <i className="glyphicon glyphicon-edit"/>
                  </a>
                  <a className="btn btn-xs btn-default" title={conf.title}
                    style={{color: conf.color}}
                    onClick={(e) => this.props.onSetPageCode(page, {editData: row})}>
                    <i className="glyphicon glyphicon-list-alt"/>
                  </a>
                </span>
              );
          }}>
            Actions
          </TableHeaderColumn>
        );

        content = (
          <div>
            <CustomFilter 
              onRefreshData={this.onRangeUpdate} 
              total={this.state.total}  
              from={this.state.from}  
              to={this.state.to} 
              withSearch={true} 
              onSearch={this.handleSearchData} 
              searchText={this.state.searchText} 
              withActions={true}
              withRange={false}             
              />
            <Listing 
              data={data}
              columns={columns} 
              onRefreshData={this.onRangeUpdate}
              onEditData={this.handleEditData}
              onDeleteData={this.handleDeleteData}
              withActions={true}
              actions={actions}/>
          </div>
        );
        break;

      case PageMode.new: 
        content = <ComplexForm onSave={this.handleFormSave} onCancel={this.handleFormCancel}/>;
        break;

      case PageMode.edit: 
        content = <ComplexForm data={this.state.editData} 
          onSave={this.handleFormSave} onCancel={this.handleFormCancel}/>;
        break;

      default: 
        content = null;
    }

    return content;
  }

  render() {
    
    const brand = (
      <span>
        <i className="glyphicon glyphicon-list-alt"/> Complex Maintenance
      </span>
    );

    const content = this.getContentFromMode(this.state.mode);

    return (
      <div>
        <Alert ref={x => this.alertRef = x} />
        <Loadable isLoading={this.state.loading} text={'Fetching Data...'}>
          <SimpleHeader 
            brand={brand}
            listLabel="List"
            newLabel="New Complex"
            onRightItemClick={this.handleRightItemClick}
            mode={this.state.mode}
          />
          {content}
        </Loadable>
      </div>
    );
  }
}

function mapStateToProps(state: any): IConnectedProps {
  return {
    pageCode: state.app.pageCode,
    editData: state.app.editData,
  };
}

function mapDispatchToProps(dispatch): IConnectedDispatch {
  return {
    onSetPageCode: (value: string, settings) => dispatch(setPage(value, settings))
  };
}

export default 
  connect(mapStateToProps, mapDispatchToProps)(Complex) as React.ComponentClass<IComplexProps>;
