import * as React from 'react';
import Loadable from '../../components/Loadable';
import * as utils from '../../../App/utils';
import { Panel } from '../../../ascendis-ui/containers';

import { Row, Column } from '../../../ascendis-ui/layout';
import { InputGroup, Dropdown } from '../../../ascendis-ui/components';

export interface ICustomerUnitProps {
  data?: any;
  custno: string;
  onSave: Function;
  onCancel: Function;
}

export interface ICustomerUnitState {
  rentalCode?: string;
  unitCode: string;
  terms: string;
  securityDeposit: any;
  isUnitValid: boolean;
  verifying: boolean;
  endDate: string;
  startDate: string;
  dataComplexUnit: any;
  loading: boolean;
}

class CustomerUnitForm extends React.Component<ICustomerUnitProps, ICustomerUnitState> {
  constructor(props) {
    super(props);

    this.state = {
      dataComplexUnit: [],
      verifying: false,
      unitCode: '',
      isUnitValid: false,
      endDate: '',
      startDate: '',
      loading: true,
      terms: '',
      securityDeposit: '',
    };

    const isEdit = this.props.data && this.props.data[0] ? true : false;

    if (isEdit) {
      const { RentalCode, UnitCode, StartDate, EndDate, Terms, SecDeposit} = this.props.data[0];
      
      this.state = {
        dataComplexUnit: [],
        unitCode: UnitCode,
        rentalCode: RentalCode,
        startDate: StartDate,
        endDate: EndDate,
        terms: Terms,
        securityDeposit: SecDeposit,
        isUnitValid: true,
        verifying: false,
        loading: true,
      };
    }

    this.handleInputChange = this.handleInputChange.bind(this);
    this.verifydata = this.verifydata.bind(this);
    this.getDataAndDisplay = this.getDataAndDisplay.bind(this);
    this.verifyUnitCode = this.verifyUnitCode.bind(this);
    this.isSaveDisabled = this.isSaveDisabled.bind(this);
  }

  componentDidMount() {
    this.getDataAndDisplay();
  }

  verifyUnitCode(unitcode) {
    const { verify } = utils;

    const data = [
      {name: 'unitcode', value: unitcode},
    ];
    
    Promise.all([verify('CustomerUnit/', data)]).then(
      (response: any) => {
        const isUnitValid = response[0].result;
        this.setState({
          isUnitValid,
          verifying: false,
        } as ICustomerUnitState);
      }
    ).catch(
      (ex) => {
        alert(ex);
      }
    );
  }

  getDataAndDisplay() {
    const { fetchComplexUnit, nextID } = utils;
    
    Promise.all([fetchComplexUnit(), nextID('RentalCode', 'QLD.tblRental')]).then(
      (response: any) => {
        const dataComplexUnit = response[0].data;

        let nextState = {
          dataComplexUnit,
          loading: false,
        };

        if (!this.props.data) {
          nextState['rentalCode'] = response[1].result;
        }

        this.setState( nextState as ICustomerUnitState);
      }
    ).catch(
      (ex) => {
        alert(ex);
      }
    );
  }
  
  handleInputChange(prop, value) {
    let state = {};
    state[prop] = value;

    if (prop === 'unitCode') {
      if (this.props.data && this.props.data.length > 0) {
        const { UnitCode } = this.props.data[0];
        if (value === UnitCode) {
          state['verifying'] = false;
          state['isUnitValid'] = true;
          this.setState(state as ICustomerUnitState);
          return;
        }
      }
      state['verifying'] = true;
      this.verifyUnitCode(value);
    }
    
    this.setState(state as ICustomerUnitState);
  }

  verifydata() {
    const isEdit = this.props.data && this.props.data[0] ? true : false;

    if (this.isSaveDisabled(isEdit)) {
      return;
    }

    this.props.onSave(this.state, isEdit, isEdit ? this.props.data[0].RentalCode : null);
  }

  isSaveDisabled(isEdit: boolean): boolean {
    if (isEdit) {
      const { UnitCode } = this.props.data[0];
      if (this.state.unitCode === UnitCode) {
        return false;
      }
    }

    if (this.state.verifying) {
      return true;
    }
    
    if (!this.state.isUnitValid) {
      return true;
    }

    return false;
  }

  render() {
    const isEdit = this.props.data && this.props.data[0] ? true : false;

    let canSave = this.isSaveDisabled(isEdit);
    
    return (
      <Loadable isLoading={this.state.loading} text={'Loading Form...'}>
        <Panel title={`Unit`}>
          <Row>
            <Column column="6">
              <Dropdown 
                list={this.state.dataComplexUnit}
                label="Unit Code" 
                inputName="unitCode"
                onValueChange={(value) => {this.handleInputChange('unitCode', value.value);}}
                value={this.state.unitCode}/>
            </Column>
            <Column column="6">
              <InputGroup 
                label="Valid Unit?" 
                placeholder="Choose Unit To Check" 
                inputName="isUnitValid"
                onValueChange={(value) => {this.handleInputChange('isUnitValid', value);}}
                value={
                  this.state.verifying ? 'Verifying...' : 
                    this.state.isUnitValid ? 'Unit can be added' : 'Cannot add selected Unit'}
                disabled={true}/>
            </Column>
          </Row>
          <Row>
            <Column column="6">
              <InputGroup 
                label="Terms" 
                placeholder="Terms" 
                inputName="terms"
                type="number"
                onValueChange={(value) => {this.handleInputChange('terms', value);}}
                value={this.state.terms}/> 
            </Column>
            <Column column="6">
              <InputGroup 
                label="Security Deposit $" 
                placeholder="Security Deposit" 
                inputName="securityDeposit"
                type="number"
                onValueChange={(value) => {this.handleInputChange('securityDeposit', value);}}
                value={this.state.securityDeposit}/>
            </Column>
          </Row>
          <Row>
            <Column column="6">
              <InputGroup 
                type="datepicker" 
                label="Start Date" 
                placeholder="Start Date" 
                inputName="startDate"
                onValueChange={(value) => {this.handleInputChange('startDate', value);}}
                value={this.state.startDate}/>
            </Column>
            <Column column="6">
              <InputGroup 
                type="datepicker" 
                label="Last Date" 
                placeholder="Last Date" 
                inputName="endDate"
                onValueChange={(value) => {this.handleInputChange('endDate', value);}}
                value={this.state.endDate}/>
            </Column>
          </Row>
          
          <div style={{textAlign: 'right', background: '#f2f2f2', padding: '10px'}}>
            <a className="btn btn-success" 
              disabled={canSave} onClick={(e) => {this.verifydata();}}>Save</a>
            <a className="btn btn-danger"  onClick={(e) => {this.props.onCancel();} }>Cancel</a>
          </div>
        </Panel>
      </Loadable>
    );
  }
}

export default CustomerUnitForm;
