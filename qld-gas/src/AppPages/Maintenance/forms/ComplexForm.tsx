import * as React from 'react';
import Loadable from '../../components/Loadable';
import * as utils from '../../../App/utils';
import { Panel } from '../../../ascendis-ui/containers';

import { Row, Column } from '../../../ascendis-ui/layout';
import { InputGroup, Dropdown } from '../../../ascendis-ui/components';

export interface IComplexFormProps {
  data?: any;
  onSave: Function;
  onCancel: Function;
}

export interface IComplexFormState {
  complexNo: string;
  complexName: string;
  address1: string;
  address2: string;
  address3: string;
  cityCode: any;
  noUnits: string;
  loading: boolean;
  data: any;
  dataService: any;
  serviceType: any;
  errors?: any;
}

class ComplexForm extends React.Component<IComplexFormProps, IComplexFormState> {
  constructor(props) {
    super(props);

    this.state = {
      data: {},
      complexNo: '',
      complexName: '',
      address1: '',
      address2: '',
      address3: '',
      noUnits: '0',
      cityCode: '',
      loading: true,
      dataService: {},
      serviceType: '',
      errors: [],
    };

    const isEdit = this.props.data && this.props.data[0] ? true : false;

    if (isEdit) {
      const { ComplexNo, ComplexName, Address1,
         Address2, Address3, CompCityCode, NoUnits, ServiceType} = this.props.data[0];
      
      this.state = {
        data: {},
        complexNo: ComplexNo,
        complexName: ComplexName,
        address1: Address1,
        address2: Address2,
        address3: Address3,        
        cityCode: CompCityCode,
        noUnits: NoUnits,
        loading: true,
        dataService: {},
        serviceType: ServiceType,
        errors: [],
      };
    }

    this.handleInputChange = this.handleInputChange.bind(this);
    this.verifydata = this.verifydata.bind(this);
    this.getDataAndDisplay = this.getDataAndDisplay.bind(this);
  }

  componentDidMount() {
    this.getDataAndDisplay();
  }

  getDataAndDisplay() {

    const { fetchCityCodes, fetchService } = utils;    
    Promise.all([fetchCityCodes(), fetchService()]).then(
      (response: any) => {
        const data = response[0].data;        
        const dataService = response[1].data;        

        this.setState({
          data,
          dataService,
          loading: false,
        } as IComplexFormState);
      }
    ).catch(
      (ex) => {
        alert(ex);
      }
    );
  }
  
  handleInputChange(prop, value) {
    let state = {};
    state[prop] = value;

    this.setState(state as IComplexFormState);
  }

  validateForm() {
    const errors = [];

    if (utils.IsEmptyNull(this.state.complexName)) {
      errors.push({complexName : 'Field required'});
    } 

    if (utils.IsEmptyNull(this.state.address1)) {
      errors.push({address1 : 'Field required'});
    }    

    if (utils.IsEmptyNull(this.state.cityCode)) {
      errors.push({cityCode : 'Field required'});
    }            

    return errors; 
  }

  verifydata() {

    const errors = this.validateForm.bind(this)();
    if (errors.length > 0) {
      this.setState({
        errors
      });
      return;
    }

    const isEdit = this.props.data && this.props.data[0] ? true : false;
    this.props.onSave(this.state, isEdit, isEdit ? this.props.data[0].CustTennentsID : null);
  }

  render() {
    const isEdit = this.props.data && this.props.data[0] ? true : false;

    return (
      <Loadable isLoading={this.state.loading} text={'Loading Form...'}>
        <Panel title="Complex Form">
          <div id="ComplexForm" >
            <Row>
              <Column column="6">
                <InputGroup 
                  label="Complex Name" 
                  placeholder="Complex Name" 
                  inputName="complexName"
                  onValueChange={(value) => {this.handleInputChange('complexName', value);}}
                  value={this.state.complexName}
                  maxLength={50}
                  dataValidation="required"
                  error={this.state.errors.findIndex( (x) => x.complexName !== undefined) > -1}
                  errorMsg={utils.findElementValue(this.state.errors, 'complexName')}
                /> 
                
              </Column>
              <Column column="6">
                <InputGroup 
                  label="Address" 
                  placeholder="Address" 
                  inputName="address"
                  onValueChange={(value) => {this.handleInputChange('address1', value);}}
                  value={this.state.address1}
                  maxLength={50}
                  dataValidation="required"
                  error={this.state.errors.findIndex( (x) => x.address1 !== undefined) > -1}
                  errorMsg={utils.findElementValue(this.state.errors, 'address1')}
                /> 
              </Column>            
            </Row>
            <Row>
              <Column column="6">
                <InputGroup 
                  label="Address Line 2" 
                  placeholder="Address Line 2" 
                  inputName="addressLine2"
                  onValueChange={(value) => {this.handleInputChange('address2', value);}}
                  value={this.state.address2}
                  maxLength={50}/>
              </Column>
              <Column column="6">
                <InputGroup 
                  label="Address Line 3" 
                  placeholder="Address Line 3" 
                  inputName="addressLine3"
                  onValueChange={(value) => {this.handleInputChange('address3', value);}}
                  value={this.state.address3}
                  maxLength={50}/>
              </Column>            
            </Row>
            <Row>
              <Column column="6">
                <Dropdown 
                  list={this.state.data}
                  label="City Code" 
                  inputName="cityCode"
                  onValueChange={(value) => {this.handleInputChange('cityCode', value.value);}}
                  value={this.state.cityCode}
                  error={this.state.errors.findIndex( (x) => x.cityCode !== undefined) > -1}
                  errorMsg={utils.findElementValue(this.state.errors, 'cityCode')}                    
                />
              </Column>
              <Column column="6">
                <InputGroup 
                  label="No Units" 
                  placeholder="Number of Units" 
                  inputName="noUnits"
                  type="number"
                  onValueChange={(value) => {this.handleInputChange('noUnits', value);}}
                  value={this.state.noUnits}
                  maxLength={11}/>
              </Column>            
            </Row>
            <Row>
              <Column column="6">
                <Dropdown 
                  list={this.state.dataService}
                  label="Service Type" 
                  inputName="serviceType"
                  onValueChange={(value) => {this.handleInputChange('serviceType', value.value);}}
                  value={this.state.serviceType}/>
              </Column>              
            </Row>

            <div style={{textAlign: 'right', background: '#f2f2f2', padding: '10px'}}>
              <a className="btn btn-success"  onClick={(e) => {this.verifydata();}}>Save</a>
              <a className="btn btn-danger"  onClick={(e) => {this.props.onCancel();} }>Cancel</a>
            </div>            
          </div>
        </Panel>
      </Loadable>
    );
  }
}

export default ComplexForm;
