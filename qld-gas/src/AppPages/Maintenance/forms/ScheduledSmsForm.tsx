
import * as React from 'react';
import Loadable from '../../components/Loadable';
import * as utils from '../../../App/utils';
import { Panel } from '../../../ascendis-ui/containers';

import { Row, Column } from '../../../ascendis-ui/layout';
import { InputGroup, Dropdown } from '../../../ascendis-ui/components';

export interface IScheduledSmsFormProps {
  data?: any;
  onSave: Function;
  onCancel: Function;
}

export interface IScheduledSmsFormState {
  scheduledID: string;
  complexNo: string;
  deliveryDate: string;
  smsAmount: string;
  notifyWeek: boolean;
  notify1Day: boolean;
  weekSent: string;
  daySent: string;
  complete: boolean;
  data: any;
  dataComplex: any;
  loading: boolean;
  errors?: any;
}

class ScheduledSmsForm extends React.Component<IScheduledSmsFormProps, IScheduledSmsFormState> {
  constructor(props) {
    super(props);

    this.state = {
      data: {},
      dataComplex: {},
      scheduledID: '',
      complexNo: '',
      deliveryDate: '',
      smsAmount: '',
      notifyWeek: false,
      notify1Day: false,
      weekSent: '',
      daySent: '',
      complete: false,
      loading: true,
      errors: [],
    };

    const isEdit = this.props.data && this.props.data[0] ? true : false;

    if (isEdit) {      
      const { ScheduledID, ComplexNo, DeliveryDate, SMSAmount, 
        NotifyWeek, Notify1Day, WeekSent, DaySent, Complete} = this.props.data[0];
      
      this.state = {
        data: {},
        dataComplex: {},
        scheduledID: ScheduledID,
        complexNo: ComplexNo,
        deliveryDate: DeliveryDate,
        smsAmount: SMSAmount,
        notifyWeek: NotifyWeek,
        notify1Day: Notify1Day,
        weekSent: WeekSent,
        daySent: DaySent,
        complete: Complete,
        loading: true,
        errors: [],
      };
    }

    this.handleInputChange = this.handleInputChange.bind(this);
    this.verifydata = this.verifydata.bind(this);
    this.getDataAndDisplay = this.getDataAndDisplay.bind(this);
  }

  componentDidMount() {      
    this.getDataAndDisplay();
  }

  getDataAndDisplay() {
    utils.fetchComplexScheduled(this.state.complexNo).then(
      (response: any) => {
        this.setState({
          dataComplex: response.data,
          loading: false,
        } as IScheduledSmsFormState);
      }
    ).catch(
      (ex) => {
        alert(ex);
      }
    );
  }
  
  handleInputChange(prop, value) {
    let state = {};
    state[prop] = value;

    this.setState(state as IScheduledSmsFormState);
  }

  validateForm() {
    const errors = [];

    if (utils.IsEmptyNull(this.state.complexNo)) {
      errors.push({complexNo : 'Field required'});
    } 

    if (utils.IsEmptyNull(this.state.deliveryDate)) {
      errors.push({deliveryDate : 'Field required'});
    }

    if (this.state.notifyWeek === false && this.state.notify1Day === false) {
      let msg = `At least "Notify Week" or "Notify 1 Day" should be checked  `;
      errors.push({notifyWeek : msg});
      errors.push({notify1Day : msg});      
    }    

    return errors; 
  }

  verifydata() {

    const errors = this.validateForm.bind(this)();
    if (errors.length > 0) {
      this.setState({
        errors
      });
      return;
    }

    const isEdit = this.props.data && this.props.data[0] ? true : false;
    this.props.onSave(this.state, isEdit, isEdit ? this.props.data[0].ScheduledID : null);
  }  

  render() {
    const isEdit = this.props.data && this.props.data[0] ? true : false;

    return (
      <Loadable isLoading={this.state.loading} text={'Loading Form...'}>
        <Panel title="Scheduled SMS Form">
          <div id="ScheduledSmsForm" >
            <Row>
              <Column column="6">
                <Dropdown 
                  list={this.state.dataComplex}
                  label="Complex" 
                  inputName="complexNo"
                  onValueChange={(value) => {this.handleInputChange('complexNo', value.value);}}
                  value={this.state.complexNo}
                  error={this.state.errors.findIndex( (x) => x.complexNo !== undefined) > -1}
                  errorMsg={utils.findElementValue(this.state.errors, 'complexNo')}
                />
              </Column> 
              <Column column="6">
                <InputGroup 
                  type="checkbox"
                  label="Complete" 
                  placeholder="Complete" 
                  inputName="complete"                  
                  onValueChange={(value) => {this.handleInputChange('complete', value);}}
                  value={this.state.complete.toString().toLowerCase()}
                  dataValidation="required"
                  />
              </Column>                         
            </Row>
            <Row>
              <Column column="6">
                <InputGroup 
                  type="datepicker" 
                  label="Delivery Date" 
                  placeholder="Delivery Date" 
                  inputName="deliveryDate"
                  onValueChange={(value) => {this.handleInputChange('deliveryDate', value);}}
                  value={this.state.deliveryDate}                  
                  dataValidation="required"
                  error={this.state.errors.findIndex( (x) => x.deliveryDate !== undefined) > -1}
                  errorMsg={utils.findElementValue(this.state.errors, 'deliveryDate')}
                /> 
              </Column>                
              <Column column="6">
                <InputGroup 
                  type="number"
                  label="SMS Amount" 
                  placeholder="SMS Amount" 
                  inputName="smsAmount"
                  onValueChange={(value) => {this.handleInputChange('smsAmount', value);}}
                  value={this.state.smsAmount}
                  />
              </Column>           
            </Row>
            <Row>
              <Column column="6">
                <InputGroup 
                  type="checkbox"
                  label="Notify Week" 
                  placeholder="Notify Week" 
                  inputName="notifyWeek"
                  onValueChange={(value) => {this.handleInputChange('notifyWeek', value);}}
                  value={this.state.notifyWeek.toString().toLowerCase()}
                  dataValidation="required"
                  error={this.state.errors.findIndex( (x) => x.notifyWeek !== undefined) > -1}
                  errorMsg={utils.findElementValue(this.state.errors, 'notifyWeek')}                  
                />
              </Column>               
              <Column column="6">
                <InputGroup 
                  type="checkbox"
                  label="Notify 1 Day" 
                  placeholder="Notify 1 Day" 
                  inputName="notify1Day"                  
                  onValueChange={(value) => {this.handleInputChange('notify1Day', value);}}
                  value={this.state.notify1Day.toString().toLowerCase()}
                  dataValidation="required"
                  error={this.state.errors.findIndex( (x) => x.notify1Day !== undefined) > -1}
                  errorMsg={utils.findElementValue(this.state.errors, 'notify1Day')}                  
                />
              </Column>          
            </Row>
            <Row>
              <Column column="6">
                <InputGroup 
                  type="datepicker"
                  label="Week Sent" 
                  placeholder="Week Sent" 
                  inputName="weekSent"                  
                  onValueChange={(value) => {this.handleInputChange('weekSent', value);}}
                  value={this.state.weekSent}
                  />
              </Column>                
              <Column column="6">
                <InputGroup 
                  type="datepicker"
                  label="Day Sent" 
                  placeholder="Day Sent" 
                  inputName="daySent"                  
                  onValueChange={(value) => {this.handleInputChange('daySent', value);}}
                  value={this.state.daySent}
                  />
              </Column>
            </Row>           

            <div style={{textAlign: 'right', background: '#f2f2f2', padding: '10px'}}>
              <a className="btn btn-success"  onClick={(e) => {this.verifydata();}}>Save</a>
              <a className="btn btn-danger"  onClick={(e) => {this.props.onCancel();} }>Cancel</a>
            </div>            
          </div>
        </Panel>
      </Loadable>
    );
  }
}

export default ScheduledSmsForm;
