import * as React from 'react';
import Loadable from '../../components/Loadable';
import * as utils from '../../../App/utils';
import { Panel } from '../../../ascendis-ui/containers';

import { Row, Column } from '../../../ascendis-ui/layout';
import { InputGroup, Dropdown } from '../../../ascendis-ui/components';
import SimpleHeader from '../../components/SimpleHeader';
import Alert from '../../components/Alert';
import { PageMode } from '../../constants';
import Listing from '../../components/Listing';
import { TableHeaderColumn } from 'react-bootstrap-table';
import * as moment from 'moment';

export interface IManualCreditCardFormProps {  
  onCancel: Function;
  custno: string;
  data?: any;
}

export interface IManualCreditCardFormState {
  mode: PageMode;
  cardNumber: string;
  custno: string;
  securityCode: string;
  cardHolderName: string;
  expiry: string;
  applyFrom: string;
  applyTo: string;
  loading: boolean;
  errors?: any;
  dataCreditCard: any;
  expiryYear: any;
  expiryMonth: any;
  yearList: any;
  monthList: any;
  processDate: string;
  amount: string;
}

class ManualCreditCardForm extends React.Component<IManualCreditCardFormProps, IManualCreditCardFormState> {
  alertRef: any;

  constructor(props) {
    super(props);

    this.state = {
      cardNumber: '',
      securityCode: '',
      custno: '',      
      cardHolderName: '',
      expiry:  '',
      applyFrom:  '',
      applyTo:  '',      
      loading: true,
      errors: [],
      mode: PageMode.new,
      dataCreditCard: [],
      expiryYear: '',
      expiryMonth: '',
      monthList: [],
      yearList: [],
      processDate: '',
      amount: ''
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.verifydata = this.verifydata.bind(this);
    this.getDataAndDisplay = this.getDataAndDisplay.bind(this);
    this.handleDeleteData = this.handleDeleteData.bind(this);
    this.handleCreditCardSave = this.handleCreditCardSave.bind(this);
    this.handleActiveCreditCard = this.handleActiveCreditCard.bind(this);
    this.useThisCard = this.useThisCard.bind(this);
  }

  componentDidMount() {
    this.getDataAndDisplay();
    this.loadMonthAndYears();
  }

  loadMonthAndYears() {
    const month = utils.generateMonth();    
    const years = utils.generateYears('1980', (new Date().getFullYear()));

    this.setState({
      yearList: years,
      monthList: month,
      expiryYear: years[0].label,
      expiryMonth: month[0].label,
      expiry: (month[0].label + years[0].label)      
    });      
  }

  getDataAndDisplay() {
    const { fetchCreditCard } = utils;
    const CardPayment = 'manual';
    
    fetchCreditCard(this.props.custno, CardPayment).then(
      (response: any) => {
        const data = response.data;

        if (data.length > 0) {

          this.setState({
            dataCreditCard: data,
            loading: false,
          } as IManualCreditCardFormState);

        }else {

          this.setState({
            dataCreditCard: [],
            loading: false,
          } as IManualCreditCardFormState);          
        }
      }
    ).catch(
      (ex) => {
        alert(ex);
      }
    );
  }
  
  handleInputChange(prop, value) {
    let state = {};

    if (prop === 'expiryYear') {
      state['expiry'] = this.state.expiryMonth + value;
    } else if (prop === 'expiryMonth') {
      state['expiry'] = value + this.state.expiryYear;
    }

    state[prop] = value;
    this.setState(state as IManualCreditCardFormState);
  }

  handleActiveCreditCard(cell, row) {

    if (!confirm('Do you really want to disable/enable this credit card')) {
      return;
    }

    const active = !(cell === true || cell === 'True' ? true : false);

    let formData = new FormData();
    let message;
    let toastrTitle;

    const { CreditCardCode } = row;

    formData.append('Action', 'Update');
    formData.append('Path', 'CreditCardProcessed/');
    formData.append('CreditCardCode', CreditCardCode);
    formData.append('Processed', active.toString());

    utils.fetch(formData).then(
      (response: any) => {
        const { status } = response;

        if (status === 'ok') {
          toastrTitle = 'Record updated';
          message = `Credit Card: ${CreditCardCode} has been Processed`;

          this.alertRef.displayAlert('success', message, toastrTitle);
        }else {
          this.alertRef.displayAlert('error', 
            `Error while Saving.\nError Message:\n${response.error}`, 'Error');             
        }
        
        this.getDataAndDisplay();
      }
    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );

  }

  validateForm() {
    const errors = [];

    if (utils.IsEmptyNull(this.state.cardNumber)) {
      errors.push({cardNumber : 'Field required'});
    } 

    if (this.state.cardNumber.length !== 16) {
      errors.push({cardNumber : 'Card Number should be 16 digits'});
    }     

    if (utils.IsEmptyNull(this.state.cardHolderName)) {
      errors.push({cardHolderName : 'Field required'});
    }  

    if (utils.IsEmptyNull(this.state.amount)) {
      errors.push({amount : 'Field required'});
    }  
    
    if (utils.IsEmptyNull(this.state.processDate)) {
      errors.push({processDate : 'Field required'});
    }      
    
    if (utils.IsEmptyNull(this.state.expiry)) {
      errors.push({expiryYear : 'Field required'});
      errors.push({expiryMonth : 'Field required'});
    }      
    
    return errors; 
  }  

  verifydata() {
    const errors = this.validateForm.bind(this)();
    if (errors.length > 0) {
      this.setState({
        errors
      });
      return;
    }    
    
    this.handleCreditCardSave(this.state, this.props.custno);
  }

  handleCreditCardSave(data,  custno) {
    let formData = new FormData();
    let message;
    let toastrTitle;

    formData.append('Action', 'Insert');
    formData.append('Path', 'CreditCardManual/');
    
    const { cardNumber, securityCode, expiry, applyFrom, applyTo, cardHolderName, amount, processDate } = data;
    const CardPayment = 'manual';

    formData.append('Custno', custno);
    formData.append('CardNumber', cardNumber);
    formData.append('SecurityCode', securityCode);
    formData.append('Expiry', expiry);
    formData.append('ApplyFrom', applyFrom);
    formData.append('ApplyTo', applyTo);
    formData.append('CardHolderName', cardHolderName);    
    formData.append('Processed', 'false'); /* equals to processed = false */
    formData.append('Amount', amount);
    formData.append('ProcessDate', processDate);
    formData.append('CardPayment', CardPayment);

    utils.fetch(formData).then(
      (response: any) => {
        const { status } = response;

        if (status === 'ok') {
          toastrTitle = 'Record updated';
          message = `Credit Card: ${cardNumber} and Security Code ${securityCode} Updated`;

          this.alertRef.displayAlert('success', message, toastrTitle);
        }else {
          this.alertRef.displayAlert('error', 
            `Error while Saving.\nError Message:\n${response.error}`, 'Error');             
        }
        
        this.getDataAndDisplay();
      }
    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );

    this.setState({
      loading: true,
      cardNumber: '',
      securityCode: '',
      custno: '',      
      cardHolderName: '',
      expiry:  this.state.monthList[0].label + this.state.yearList[0].label,
      applyFrom:  '',
      applyTo:  '',
      amount: '',
      processDate: '',
      expiryMonth: this.state.monthList[0].label,
      expiryYear: this.state.yearList[0].label,
      errors: [],      
    });
  }
  
  useThisCard(row) {

    const { Expiry } = row[0];
    const month =  Expiry.substring(0, 2);
    const year =  Expiry.substring(2, Expiry.length);

    this.setState({      
      cardNumber: row[0].CardNumber,
      securityCode: row[0].SecurityCode,      
      cardHolderName: row[0].CardHolderName,
      expiry: row[0].Expiry,
      errors: [],
      expiryMonth: month,
      expiryYear: year
    });    
  }

  handleDeleteData(row) {
    if (!confirm('Are you sure you want to delete selected Credit Card?')) {
      return;
    }
          
    let formData = new FormData();
    formData.append('Action', 'Delete');
    formData.append('Path', 'CreditCard/');
    formData.append('CreditCardCode', row[0].CreditCardCode);

    utils.fetch(formData).then(
      (res: any) => {
        const { status } = res;        
        let message;
        let toastrTitle;
        if (status === 'ok') {

          this.getDataAndDisplay();          

          toastrTitle = 'Records deleted';
          message = `Selected Credit Cards deleted`;
          this.alertRef.displayAlert('info', message, toastrTitle);
        }
      }
    ).catch(
      (ex) => {              
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );    
  }

  columns() {
    const columns = [      
      {dataField: 'CardHolderName', header: 'Name'},
      {dataField: 'CardNumber', header: 'Card', isKey: true},
      {dataField: 'Expiry', header: 'Expiry', hidden: false},
      {dataField: 'SecurityCode', header: 'CVV'},
      {dataField: 'Amount', header: 'Amount', hidden: false,
        dataFormat: (cell, row) => {
          let value = parseFloat(cell);
          value = Math.round(value * 10000) / 10000;
          return value;
        }      
      },
      {dataField: 'ProcessDate', header: 'Process Date', hidden: false,
        dataFormat: (cell, row) => {
          return <span>{cell ? moment(cell).format('D MMM Y') : ''}</span>;
        }
      },
      {dataField: 'Processed', header: 'Processed', hidden: false, dataFormat: (cell, row) => {
        
        const active = (cell.toLowerCase() === 'true' ? 'true' : 'false');
        return (
          <input
            className="form-control"             
            type={'checkbox'} 
            name={'active'} 
            onChange={() => this.handleActiveCreditCard(cell, row)}
            checked={JSON.parse(active)}     
          />  
        );
      }}
    ];

    return columns;
  }

  actions() {
    return  (
      <TableHeaderColumn 
        dataField="action" 
        dataAlign="center"        
        headerAlign="center"            
        width="100px"                            
        dataFormat={(cell, row) => {
          return (
            <div>
              <span>
                <a className="btn btn-xs btn-warning" title="Use this Credit Card" 
                  onClick={(e) => {e.preventDefault(); this.useThisCard([row]);}}>
                  <i className="glyphicon glyphicon-copyright-mark"/>
                </a>
              </span>

              {/*<span>
                <a className="btn btn-xs btn-danger" title="Delete" 
                  onClick={(e) => {e.preventDefault(); this.handleDeleteData([row]);}}>
                  <i className="glyphicon glyphicon-trash"/>
                </a>
              </span>*/}
            </div>
          );
      }}>
        Actions
      </TableHeaderColumn>
  );    
  }

  render() {
    const {FullName, RentalAddress1, RentalAddress2, RentalAddress3, OSBalance} = this.props.data[0];

    return (
      <div >
      <Alert ref={x => this.alertRef = x} />      
      <Loadable isLoading={this.state.loading} text={'Loading Form...'}>
        <Panel title={'Manual Card Payments'}>
          <Row >
            <Column column="12">
              <p className="alert alert-info">
                <b>Customer:</b> {FullName} <br/>
                <b>Property:</b> {RentalAddress1} {RentalAddress2} {RentalAddress3} <br/>
                <b>Balance Due:</b> ${OSBalance}
              </p>
            </Column>
          </Row>          
          <Row>
            <Column column="6">
              <InputGroup 
                label="Card Number" 
                placeholder="xxxx xxxx xxxx xxxx" 
                inputName="cardNumber"
                onValueChange={(value) => {this.handleInputChange('cardNumber', value);}}
                value={this.state.cardNumber}                
                error={this.state.errors.findIndex( (x) => x.cardNumber !== undefined) > -1}
                errorMsg={utils.findElementValue(this.state.errors, 'cardNumber')}                
              />
            </Column> 
            <Column column="6">
              <InputGroup 
                label="Cardholder Name" 
                placeholder="Cardholder Name" 
                inputName="cardHolderName"
                onValueChange={(value) => {this.handleInputChange('cardHolderName', value);}}
                value={this.state.cardHolderName}                
                error={this.state.errors.findIndex( (x) => x.cardHolderName !== undefined) > -1}
                errorMsg={utils.findElementValue(this.state.errors, 'cardHolderName')}                
              /> 
            </Column>
          </Row>
          <Row> 
            <Column column="6">
              <InputGroup 
                label="CVV" 
                placeholder="CVV" 
                inputName="securityCode"
                onValueChange={(value) => {this.handleInputChange('securityCode', value);}}
                value={this.state.securityCode}
                error={this.state.errors.findIndex( (x) => x.securityCode !== undefined) > -1}
                errorMsg={utils.findElementValue(this.state.errors, 'securityCode')}
              />              
            </Column>
            <Column column="1">
              <Dropdown 
                list={this.state.monthList}
                label={'Expriry'}
                inputName="expiryMonth"
                onValueChange={(value) => {this.handleInputChange('expiryMonth', value.value);}}
                value={this.state.expiryMonth}
                error={this.state.errors.findIndex( (x) => x.expiryMonth !== undefined) > -1}
                errorMsg={utils.findElementValue(this.state.errors, 'expiryMonth')}
                clearable= {false}
                className={'dropdown-expiry-month'}
              />
            </Column>
            <Column column="2">
              <Dropdown 
                list={this.state.yearList}
                label={null} 
                inputName="expiryYear"
                onValueChange={(value) => {this.handleInputChange('expiryYear', value.value);}}
                value={this.state.expiryYear}
                error={this.state.errors.findIndex( (x) => x.expiryYear !== undefined) > -1}
                errorMsg={utils.findElementValue(this.state.errors, 'expiryYear')}
                clearable= {false}
                className={'dropdown-expiry-year'}
              />
            </Column>            
          </Row>
          <Row>
            <Column column="6">
              <InputGroup 
                label="Amount" 
                placeholder="Amount" 
                inputName="amount"
                onValueChange={(value) => {this.handleInputChange('amount', value);}}
                value={this.state.amount}
                error={this.state.errors.findIndex( (x) => x.amount !== undefined) > -1}
                errorMsg={utils.findElementValue(this.state.errors, 'amount')}
              />  
            </Column>
            <Column column="6">
              <InputGroup 
                label="Process Date" 
                type="datepicker"                
                placeholder="Process Date" 
                inputName="processDate"
                onValueChange={(value) => {this.handleInputChange('processDate', value);}}
                value={this.state.processDate}
                error={this.state.errors.findIndex( (x) => x.processDate !== undefined) > -1}
                errorMsg={utils.findElementValue(this.state.errors, 'processDate')}
              />
            </Column>            
          </Row>          
          <Row>
            <Column column="12">
              <div style={{textAlign: 'right', background: '#f2f2f2', padding: '10px'}}>
                <a className="btn btn-success"  onClick={(e) => {this.verifydata();}}>Add Payment</a>
              </div>
            </Column>
          </Row>

          <Row>
            <br/><br/>
            <Listing 
              data={this.state.dataCreditCard} 
              columns={this.columns()} 
              onRefreshData={null} 
              onEditData={null} 
              onDeleteData={null} 
              withActions={true} 
              actions={this.actions()} 
            />
          </Row>

          <Row>
            <Column column="12">
              <div style={{textAlign: 'right', background: '#f2f2f2', padding: '10px'}}>
                <a className="btn btn-danger"  onClick={(e) => {this.props.onCancel();}}>Cancel</a>
              </div>
            </Column>
          </Row>          

        </Panel>
      </Loadable>
      </div>
    );
  }
}

export default ManualCreditCardForm;
