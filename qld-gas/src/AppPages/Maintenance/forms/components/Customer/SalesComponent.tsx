import * as React from 'react';
import * as _ from 'lodash';

import { Row, Column } from '../../../../../ascendis-ui/layout';
import { InputGroup, Dropdown } from '../../../../../ascendis-ui/components';
import { Section } from '../../../../../ascendis-ui/containers';

export interface ISalesComponentState {
  salesAccount: string;
  postBarcodeCheckSum: string;
  lastStatementDate: string;
  outstandingBalance: string;
  cardPayment: string;
}

export default class SalesComponent extends React.Component<any, ISalesComponentState> {
  constructor(props) {
    super(props);
  
    this.state = {
      salesAccount: '40101',
      postBarcodeCheckSum: '',
      lastStatementDate: '',
      outstandingBalance: '0',
      cardPayment: '',
    };

    if (this.props.isEdit) {

      const { data } = this.props;
      
      this.state = {
        salesAccount: data.GLSalesAc,
        postBarcodeCheckSum: data.APostBCodeChkSum,        
        lastStatementDate: data.LastStmtDate,
        outstandingBalance: data.OSBalance,
        cardPayment: data.CardPayment
      };
    }

    this.handleInputChange = this.handleInputChange.bind(this);
  }

  componentDidMount(){

    // init parent state
    this.props.onInfoChange(this.state);   
  }

  handleInputChange(prop, value) {
    let state = {};
    state[prop] = value;

    const newState = _.merge({}, this.state, state);
    
    this.props.onInfoChange(newState);

    this.setState(state as ISalesComponentState);
  }

  render() {

    const cardPaymnets = [{label: 'Automatic Direct Debit', value: 'automatic'}, 
    {label: 'Manual Card', value: 'manual'}];

    return (
      <Section header="">
        <Row>
          <Column column="4">
            <InputGroup 
              label="Sales Account" 
              placeholder="Sales Account" 
              inputName="salesAccount"
              onValueChange={(value) => {this.handleInputChange('salesAccount', value);}}
              disabled={true}
              value={this.state.salesAccount}
              maxLength={10}/>
          </Column>

          <Column column="4">
            <InputGroup 
              label="Outstanding Balance" 
              placeholder="Outstanding Balance" 
              inputName="outstandingBalance"
              onValueChange={(value) => {this.handleInputChange('outstandingBalance', value);}}
              value={this.state.outstandingBalance}/>
          </Column>        
          <Column column="4">
            <Dropdown 
              list={cardPaymnets}
              label={'Card Payment'}
              inputName="cardPayment"
              onValueChange={(value) => {this.handleInputChange('cardPayment', value.value);}}
              value={this.state.cardPayment}              
            />
          </Column>                    
        </Row>

        <div className="hidden" >
          <Row>
            <Column column="6">
              <InputGroup
                type="datepicker"               
                label="Last Statement Date" 
                placeholder="Last Statement Date" 
                inputName="lastStatementDate"
                onValueChange={(value) => {this.handleInputChange('lastStatementDate', value);}}
                value={this.state.lastStatementDate}/>
            </Column>

            <Column column="6">
              <InputGroup 
                label="Post Barcode Check Sum" 
                placeholder="Post Barcode Check Sum" 
                inputName="postBarcodeCheckSum"
                onValueChange={(value) => {this.handleInputChange('postBarcodeCheckSum', value);}}
                value={this.state.postBarcodeCheckSum}
                maxLength={4}
                disabled={true}/>
            </Column>
          </Row>
        </div>
       
      </Section>
    );
  }
}