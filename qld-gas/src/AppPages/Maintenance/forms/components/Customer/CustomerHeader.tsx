import * as React from 'react';
import * as _ from 'lodash';
import * as utils from '../../../../../App/utils';

import { Row, Column } from '../../../../../ascendis-ui/layout';
import InputGroup from '../../../../../ascendis-ui/components/InputGroup';
import { Section } from '../../../../../ascendis-ui/containers';

export interface ICustomerHeaderProps {
  isEdit: boolean;
  data?: any;
  onInfoChange: Function;
}

export interface ICustomerHeaderState {
  customerNumber: string;
  bPay: string;
  inactive: boolean;
  accessSMS: boolean;
}

class CustomerHeader extends React.Component<ICustomerHeaderProps, ICustomerHeaderState> {
  constructor(props) {
    super(props);
 
    if (!this.props.isEdit) {
      this.state = {
        customerNumber: 'Fetching ID...',
        bPay: '',
        inactive: false,
        accessSMS: false
      };
    } else {
      
      this.state = {
        customerNumber: this.props.data.CustomerNo,
        bPay: this.props.data.BPayNo,        
        inactive: (this.props.data.Inactive === '' || this.props.data.Inactive === 'False') ? false : true,
        accessSMS: (this.props.data.AccessSMS === '' || this.props.data.AccessSMS === 'False') ? false : true
      };
    }
    
    this.handleInputChange = this.handleInputChange.bind(this);
    this.getDataAndDisplay = this.getDataAndDisplay.bind(this);
  }

  componentDidMount() {
    this.getDataAndDisplay();
  }

  getDataAndDisplay() {
    if (this.props.isEdit) {
      return;
    }
    const { nextID } = utils;
    
    Promise.all([nextID('Custno', 'QLD.tblCustomer')]).then(
      (response: any) => {

        const CustNo = response[0].result;

        let nextState = {
          customerNumber: CustNo,
        };

        this.setState( nextState as ICustomerHeaderState);
      }
    ).catch(
      (ex) => {
        alert(ex);
      }
    );
  }

  handleInputChange(prop, value) {

    let state = {};
    state[prop] = value;

    const newState = _.merge({}, this.state, state);
    
    this.props.onInfoChange(newState);

    this.setState(state as ICustomerHeaderState);

  }
  
  render() {
    const { isEdit } = this.props;
    let BPayClass = (isEdit === false) ? 'hidden' : '';

    return (
      <Section header="">
        <Row>
          <Column column={'6 ' + BPayClass} >
            <InputGroup 
              label="BPay" 
              placeholder="To be defined" 
              inputName="bPay"
              onValueChange={(value) => {this.handleInputChange('bPay', value);}}
              value={this.state.bPay}
              disabled={true}/>
          </Column>
          <Column column="2">
            <InputGroup 
              label="Inactive"
              placeholder="Inactive"
              type="checkbox"
              inputName="inactive" 
              value={this.state.inactive.toString()}               
              onValueChange={(value) => {this.handleInputChange('inactive', value);}}               
              />
          </Column>
          <Column column="2">
            <InputGroup 
              label="Access SMS"
              placeholder="Access SMS"
              type="checkbox"
              inputName="accessSMS" 
              value={this.state.accessSMS.toString()}               
              onValueChange={(value) => {this.handleInputChange('accessSMS', value);}}               
              />
          </Column>          
        </Row>
      </Section>
    );
  }
}

export default CustomerHeader;
