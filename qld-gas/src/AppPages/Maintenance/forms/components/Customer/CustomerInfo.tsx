import * as React from 'react';
import * as _ from 'lodash';

import { Row, Column } from '../../../../../ascendis-ui/layout';
import { InputGroup, Dropdown } from '../../../../../ascendis-ui/components';
import { Section } from '../../../../../ascendis-ui/containers';
import * as utils from '../../../../../App/utils';

export interface ICustomerInfoProps {
  onInfoChange: Function;
  isEdit: boolean;
  data?: any;
  list: any;
  titleList: any;
  errors: any;
}

export interface ICustomerInfoState {
  title: string;
  surname: string;
  givenName: string;
  postalAddress: string;
  postalAddress2: string;
  postalAddress3: string;
  homePhone: string;
  workPhone: string;
  mobileNumber: string;
  billingName: string;
  email: string;
  comments: string;
  cityCode: any;
  MYOBJobCode: string;
}

export default class CustomerInfo extends React.Component<ICustomerInfoProps, ICustomerInfoState> {
  constructor(props) {
    super(props);

    this.state = {
      title: '',
      surname: '',
      givenName: '', 
      postalAddress: '',
      postalAddress2: '',
      postalAddress3: '',
      homePhone: '',
      workPhone: '',
      mobileNumber: '',
      billingName: '',
      comments: '',
      cityCode: '',
      email: '',
      MYOBJobCode: '',
    };

    if (this.props.isEdit) {
      const { data } = this.props;
      
      this.state = {
        title: data.Title,
        surname: data.Surname,
        givenName: data.GivenNames, 
        postalAddress: data.POAddress1,
        postalAddress2: data.POAddress2,
        postalAddress3: data.POAddress3,
        homePhone: data.HomePhone,
        workPhone: data.WorkPhone,
        mobileNumber: data.MobileNo,
        billingName: data.BillingNames,
        comments: data.Comments,
        cityCode: data.POCityCode,
        email: data.Email,
        MYOBJobCode: data.MYOBJobCode,
      };
    }

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleBillingName = this.handleBillingName.bind(this);
    
  }

  handleInputChange(prop, value) {
    let state = {};
    state[prop] = value;

    const newState = _.merge({}, this.state, state);
    
    this.props.onInfoChange(newState);

    this.setState(state as ICustomerInfoState, () => {      
      this.handleBillingName(prop);
    });
  }

  handleBillingName(prop) {
    
    if (prop === 'title' || prop === 'surname' || prop === 'givenName' ) {
      let billingName = this.state.title + ' ' + this.state.givenName + ' ' + this.state.surname;      
      this.setState({billingName});
      this.props.onInfoChange(this.state);
    }    

  }

  render() {
    let rentalAddress = null;
    
    if (this.props.isEdit) {
      rentalAddress = (
        <Row>
          <Column column="4">
            Rental Address:
          </Column>
          <Column column="8">
            <span>{this.props.data.RentalAddress1}</span><br/>
            <span>{this.props.data.RentalAddress2}</span><br/>
            <span>{this.props.data.RentalAddress3}</span><br/>
          </Column>
        </Row>
      );
    }

    return (
      <Section header="Personal Information">
        <Row>
          <Column column="4">
            <Dropdown 
              list={this.props.titleList}
              label="Title" 
              inputName="title"
              onValueChange={(value) => {this.handleInputChange('title', value.value);}}
              value={this.state.title}
              error={this.props.errors.findIndex( (x) => x.title !== undefined) > -1}
              errorMsg={utils.findElementValue(this.props.errors, 'title')}              
            />
          </Column>
          <Column column="4">
            <InputGroup 
              label="Surname" 
              placeholder="Surname" 
              inputName="surname"
              onValueChange={(value) => {this.handleInputChange('surname', value);}}              
              value={this.state.surname}
              maxLength={25}
              error={this.props.errors.findIndex( (x) => x.surname !== undefined) > -1}
              errorMsg={utils.findElementValue(this.props.errors, 'surname')}
            />
          </Column>
          <Column column="4">
            <InputGroup 
              label="Given Name" 
              placeholder="Given Name" 
              inputName="givenName"
              onValueChange={(value) => {this.handleInputChange('givenName', value);}}
              value={this.state.givenName}
              maxLength={40}
              error={this.props.errors.findIndex( (x) => x.givenName !== undefined) > -1}
              errorMsg={utils.findElementValue(this.props.errors, 'givenName')}
            />
          </Column>
        </Row>
        <Row>
          <Column column="6">
            <Dropdown 
              list={this.props.list}
              label="City Code" 
              inputName="cityCode"
              onValueChange={(value) => {this.handleInputChange('cityCode', value.value);}}
              value={this.state.cityCode}/>
          </Column>
          <Column column="6" >
            <InputGroup 
              label="MYOB Job Code" 
              placeholder="MYOB Job Code" 
              inputName="MYOBJobCode"
              onValueChange={(value) => {this.handleInputChange('MYOBJobCode', value);}}
              value={this.state.MYOBJobCode}
              maxLength={40}/>
          </Column>
        </Row>
        <Row>
          <Column column="6">
            <InputGroup 
              label="Postal Address" 
              placeholder="Postal Address" 
              inputName="postalAddress"
              onValueChange={(value) => {this.handleInputChange('postalAddress', value);}}
              value={this.state.postalAddress}
              maxLength={50}/>
          </Column>
          <Column column="6">
            <InputGroup 
              label="Postal Address 2" 
              placeholder="Postal Address 2" 
              inputName="postalAddress2"
              onValueChange={(value) => {this.handleInputChange('postalAddress2', value);}}
              value={this.state.postalAddress2}
              maxLength={50}/>
          </Column>
        </Row>
        <Row>
          <Column column="6">
            <InputGroup 
              label="Postal Address 3" 
              placeholder="Postal Address 3" 
              inputName="postalAddress3"
              onValueChange={(value) => {this.handleInputChange('postalAddress3', value);}}
              value={this.state.postalAddress3}
              maxLength={50}/>
          </Column>
        </Row>
        <Row>
          <Column column="4">
            <InputGroup 
              label="Home Phone" 
              placeholder="Home Phone" 
              inputName="homePhone"
              onValueChange={(value) => {this.handleInputChange('homePhone', value);}}
              value={this.state.homePhone}
              maxLength={15}/>
          </Column>
          <Column column="4">
            <InputGroup 
              label="Work Phone" 
              placeholder="Work Phone" 
              inputName="workPhone"
              onValueChange={(value) => {this.handleInputChange('workPhone', value);}}
              value={this.state.workPhone}
              maxLength={15}/>
          </Column>
          <Column column="4">
            <InputGroup 
              label="Mobile Number" 
              placeholder="Mobile Number" 
              inputName="mobileNumber"
              onValueChange={(value) => {this.handleInputChange('mobileNumber', value);}}
              value={this.state.mobileNumber}
              maxLength={20}/>
          </Column>
        </Row>
        <Row>
          <Column column="6">
            <InputGroup 
              label="Billing Name" 
              placeholder="Billing Name" 
              inputName="billingName"
              onValueChange={(value) => {this.handleInputChange('billingName', value);}}
              value={this.state.billingName}
              disabled={true}
              maxLength={255}/>
          </Column>

          <Column column="6">
            <InputGroup 
              label="Email" 
              placeholder="email" 
              inputName="email"
              onValueChange={(value) => {this.handleInputChange('email', value);}}
              value={this.state.email}
              maxLength={150}
              error={this.props.errors.findIndex( (x) => x.email !== undefined) > -1}
              errorMsg={utils.findElementValue(this.props.errors, 'email')}              
            />
          </Column>
        </Row>

        <Row>
          <Column column="12">
            <InputGroup 
              label="Comments" 
              type="textarea"
              placeholder="Comments" 
              inputName="comments"
              onValueChange={(value) => {this.handleInputChange('comments', value);}}
              value={this.state.comments}/>
          </Column>
        </Row>
        <hr/>
        {rentalAddress}
        <hr/>
      </Section>
    );
  }
}