import * as React from 'react';
import Loadable from '../../components/Loadable';
import * as utils from '../../../App/utils';
import { Panel } from '../../../ascendis-ui/containers';

import { Row, Column } from '../../../ascendis-ui/layout';
import { InputGroup, Dropdown } from '../../../ascendis-ui/components';
import SimpleHeader from '../../components/SimpleHeader';
import Alert from '../../components/Alert';
import { PageMode } from '../../constants';
import Listing from '../../components/Listing';
import { TableHeaderColumn } from 'react-bootstrap-table';
import * as moment from 'moment';

export interface IAutomaticCreditCardFormProps {  
  onCancel: Function;
  custno: string;
  data?: any;
}

export interface IAutomaticCreditCardFormState {
  mode: PageMode;
  cardNumber: string;
  custno: string;
  securityCode: string;
  cardHolderName: string;
  expiry: string;
  applyFrom: string;
  applyTo: string;
  loading: boolean;
  errors?: any;
  dataCreditCard: any;
  expiryYear: any;
  expiryMonth: any;
  yearList: any;
  monthList: any;
}

class AutomaticCreditCardForm extends React.Component<IAutomaticCreditCardFormProps, IAutomaticCreditCardFormState> {
  alertRef: any;

  constructor(props) {
    super(props);

    this.state = {
      cardNumber: '',
      securityCode: '',
      custno: '',      
      cardHolderName: '',
      expiry:  '',
      applyFrom:  '',
      applyTo:  '',      
      loading: true,
      errors: [],
      mode: PageMode.new,
      dataCreditCard: [],
      expiryYear: '',
      expiryMonth: '',
      monthList: [],
      yearList: [],
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.verifydata = this.verifydata.bind(this);
    this.getDataAndDisplay = this.getDataAndDisplay.bind(this);
    this.handleDeleteData = this.handleDeleteData.bind(this);
    this.handleCreditCardSave = this.handleCreditCardSave.bind(this);
    this.handleActiveCreditCard = this.handleActiveCreditCard.bind(this);
  }

  componentDidMount() {
    this.getDataAndDisplay();
    this.loadMonthAndYears();
  }

  loadMonthAndYears() {
    const month = utils.generateMonth();    
    const years = utils.generateYears('1980', (new Date().getFullYear()));

    this.setState({
      yearList: years,
      monthList: month,
      expiryYear: years[0].label,
      expiryMonth: month[0].label,
      expiry: (month[0].label + years[0].label)      
    });      
  }

  getDataAndDisplay() {
    const { fetchCreditCard } = utils;
    const CardPayment = 'automatic';
    
    fetchCreditCard(this.props.custno, CardPayment).then(
      (response: any) => {
        const data = response.data;

        if (data.length > 0) {

          this.setState({
            dataCreditCard: data
          } as IAutomaticCreditCardFormState);

        }else {

          this.setState({
            dataCreditCard: []
          } as IAutomaticCreditCardFormState);          
        }

        this.setState({            
          loading: false,
        } as IAutomaticCreditCardFormState);        

      }
    ).catch(
      (ex) => {
        alert(ex);
      }
    );
  }
  
  handleInputChange(prop, value) {
    let state = {};

    if (prop === 'expiryYear') {
      state['expiry'] = this.state.expiryMonth + value;
    } else if (prop === 'expiryMonth') {
      state['expiry'] = value + this.state.expiryYear;
    }

    state[prop] = value;
    this.setState(state as IAutomaticCreditCardFormState);
  }

  handleActiveCreditCard(cell, row) {
    const active = !(cell === true ? true : false);

    let formData = new FormData();
    let message;
    let toastrTitle;

    formData.append('Action', 'Update');
    formData.append('Path', 'CreditCardActive/');
    const { CreditCardCode, Custno, cardNumber } = row;

    formData.append('CreditCardCode', CreditCardCode);
    formData.append('Custno', Custno);
    formData.append('Active', active.toString());

    utils.fetch(formData).then(
      (response: any) => {
        const { status } = response;

        if (status === 'ok') {
          toastrTitle = 'Record updated';
          message = `Credit Card: ${cardNumber} has been actived`;

          this.alertRef.displayAlert('success', message, toastrTitle);
        }else {
          this.alertRef.displayAlert('error', 
            `Error while Saving.\nError Message:\n${response.error}`, 'Error');             
        }
        
        this.getDataAndDisplay();
      }
    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );

  }

  validateForm() {
    const errors = [];

    if (utils.IsEmptyNull(this.state.cardNumber)) {
      errors.push({cardNumber : 'Field required'});
    } 

    if (this.state.cardNumber.length !== 16) {
      errors.push({cardNumber : 'Card Number should be 16 digits'});
    }     

    if (utils.IsEmptyNull(this.state.cardHolderName)) {
      errors.push({cardHolderName : 'Field required'});
    }  
    
    if (utils.IsEmptyNull(this.state.expiry)) {
      errors.push({expiryYear : 'Field required'});
      errors.push({expiryMonth : 'Field required'});
    }      
    
    return errors; 
  }  

  verifydata() {
    const errors = this.validateForm.bind(this)();
    if (errors.length > 0) {
      this.setState({
        errors
      });
      return;
    }    
    
    this.handleCreditCardSave(this.state, this.props.custno);
  }

  handleCreditCardSave(data,  custno) {
    let formData = new FormData();
    let message;
    let toastrTitle;

    formData.append('Action', 'Insert');
    formData.append('Path', 'CreditCard/');
    
    const { cardNumber, securityCode, expiry, applyFrom, applyTo, cardHolderName } = data;
    const CardPayment = 'automatic';

    formData.append('Custno', custno);
    formData.append('CardNumber', cardNumber);
    formData.append('SecurityCode', securityCode);
    formData.append('Expiry', expiry);
    formData.append('ApplyFrom', applyFrom);
    formData.append('ApplyTo', applyTo);
    formData.append('CardHolderName', cardHolderName);    
    formData.append('Active', 'true');  
    formData.append('Amount', this.props.data[0].OSBalance); 
    formData.append('CardPayment', CardPayment);

    utils.fetch(formData).then(
      (response: any) => {
        const { status } = response;

        if (status === 'ok') {
          toastrTitle = 'Record updated';
          message = `Credit Card: ${cardNumber} and Security Code ${securityCode} Updated`;

          this.alertRef.displayAlert('success', message, toastrTitle);
        }else {
          this.alertRef.displayAlert('error', 
            `Error while Saving.\nError Message:\n${response.error}`, 'Error');             
        }
        
        this.getDataAndDisplay();
      }
    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );

    this.setState({
      loading: true,
      cardNumber: '',
      securityCode: '',
      custno: '',      
      cardHolderName: '',
      expiry:  this.state.monthList[0].label + this.state.yearList[0].label,
      applyFrom:  '',
      applyTo:  '',  
      errors: [],    
    });
  }    

  handleDeleteData(row) {
    if (!confirm('Are you sure you want to delete selected Credit Card?')) {
      return;
    }
          
    let formData = new FormData();
    formData.append('Action', 'Delete');
    formData.append('Path', 'CreditCard/');
    formData.append('CreditCardCode', row[0].CreditCardCode);

    utils.fetch(formData).then(
      (res: any) => {
        const { status } = res;        
        let message;
        let toastrTitle;
        if (status === 'ok') {

          this.getDataAndDisplay();          

          toastrTitle = 'Records deleted';
          message = `Selected Credit Cards deleted`;
          this.alertRef.displayAlert('info', message, toastrTitle);
        }
      }
    ).catch(
      (ex) => {              
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );    
  }

  columns() {
    const columns = [
      {dataField: 'CardHolderName', header: 'Name'},
      {dataField: 'CardNumber', header: 'Card', isKey: true},
      {dataField: 'Expiry', header: 'Expiry', hidden: false},
      {dataField: 'SecurityCode', header: 'CVV'},
      {dataField: 'ApplyFrom', header: 'Apply From', hidden: false,
        dataFormat: (cell, row) => {
          return <span>{cell ? moment(cell).format('D MMM Y') : ''}</span>;
        }},
      {dataField: 'ApplyTo', header: 'Apply To', hidden: true, 
        dataFormat: (cell, row) => {
          return <span>{cell ? moment(cell).format('D MMM Y') : ''}</span>;
        }},
      {dataField: 'Active', header: 'Active', hidden: false, dataFormat: (cell, row) => {
        
        const active = (cell.toLowerCase() === 'true' ? 'true' : 'false');
        return (
          <input
            className="form-control"             
            type={'checkbox'} 
            name={'active'} 
            onChange={() => this.handleActiveCreditCard(cell, row)}
            checked={JSON.parse(active)}     
          />  
        );
      }}
    ];

    return columns;
  }

  actions() {
    return  (
      <TableHeaderColumn 
        dataField="action" 
        dataAlign="center"        
        headerAlign="center"            
        width="100px"                            
        dataFormat={(cell, row) => {
          return (
            <span>
              <a className="btn btn-xs btn-danger" title="Delete" 
                onClick={(e) => {e.preventDefault(); this.handleDeleteData([row]);}}>
                <i className="glyphicon glyphicon-trash"/>
              </a>
            </span>
          );
      }}>
        Actions
      </TableHeaderColumn>
  );    
  }

  render() {
    const {FullName, RentalAddress1, RentalAddress2, RentalAddress3, OSBalance} = this.props.data[0];

    return (
      <div >
      <Alert ref={x => this.alertRef = x} />      
      <Loadable isLoading={this.state.loading} text={'Loading Form...'}>
        <Panel title={'Automatic Direct Debit'}>
          <Row >
            <Column column="12">
              <p className="alert alert-info">
                <b>Customer:</b> {FullName} <br/>
                <b>Property:</b> {RentalAddress1} {RentalAddress2} {RentalAddress3} <br/>
                <b>Balance Due:</b> ${OSBalance}
              </p>
            </Column>
          </Row>          
          <Row>
            <Column column="6">
              <InputGroup 
                label="Card Number" 
                placeholder="xxxx xxxx xxxx xxxx" 
                inputName="cardNumber"
                onValueChange={(value) => {this.handleInputChange('cardNumber', value);}}
                value={this.state.cardNumber}                
                error={this.state.errors.findIndex( (x) => x.cardNumber !== undefined) > -1}
                errorMsg={utils.findElementValue(this.state.errors, 'cardNumber')}                
              />
            </Column> 
            <Column column="6">
              <InputGroup 
                label="Cardholder Name" 
                placeholder="Cardholder Name" 
                inputName="cardHolderName"
                onValueChange={(value) => {this.handleInputChange('cardHolderName', value);}}
                value={this.state.cardHolderName}                
                error={this.state.errors.findIndex( (x) => x.cardHolderName !== undefined) > -1}
                errorMsg={utils.findElementValue(this.state.errors, 'cardHolderName')}                
              /> 
            </Column>
          </Row>
          <Row> 
            <Column column="6">
              <InputGroup 
                label="CVV" 
                placeholder="CVV" 
                inputName="securityCode"
                onValueChange={(value) => {this.handleInputChange('securityCode', value);}}
                value={this.state.securityCode}
                error={this.state.errors.findIndex( (x) => x.securityCode !== undefined) > -1}
                errorMsg={utils.findElementValue(this.state.errors, 'securityCode')}
              />              
            </Column>
            <Column column="1">
              <Dropdown 
                list={this.state.monthList}
                label={'Expriry'}
                inputName="expiryMonth"
                onValueChange={(value) => {this.handleInputChange('expiryMonth', value.value);}}
                value={this.state.expiryMonth}
                error={this.state.errors.findIndex( (x) => x.expiryMonth !== undefined) > -1}
                errorMsg={utils.findElementValue(this.state.errors, 'expiryMonth')}
                clearable= {false}
                className={'dropdown-expiry-month'}
              />
            </Column>
            <Column column="2">
              <Dropdown 
                list={this.state.yearList}
                label={null} 
                inputName="expiryYear"
                onValueChange={(value) => {this.handleInputChange('expiryYear', value.value);}}
                value={this.state.expiryYear}
                error={this.state.errors.findIndex( (x) => x.expiryYear !== undefined) > -1}
                errorMsg={utils.findElementValue(this.state.errors, 'expiryYear')}
                clearable= {false}
                className={'dropdown-expiry-year'}
              />
            </Column>            
          </Row>
          <Row>
            <Column column="6">
              <InputGroup 
                label="Apply From" 
                type="datepicker"                
                placeholder="Apply From" 
                inputName="applyFrom"
                onValueChange={(value) => {this.handleInputChange('applyFrom', value);}}
                value={this.state.applyFrom}
                error={this.state.errors.findIndex( (x) => x.applyFrom !== undefined) > -1}
                errorMsg={utils.findElementValue(this.state.errors, 'applyFrom')}
              />
            </Column>
          </Row>
          <Row>
            <Column column="12">
              <div style={{textAlign: 'right', background: '#f2f2f2', padding: '10px'}}>
                <a className="btn btn-success"  onClick={(e) => {this.verifydata();}}>Add Card</a>
              </div>
            </Column>
          </Row>

          <Row>
            <br/><br/>
            <Listing 
              data={this.state.dataCreditCard} 
              columns={this.columns()} 
              onRefreshData={null} 
              onEditData={null} 
              onDeleteData={null} 
              withActions={false} 
              actions={this.actions()} 
            />
          </Row>
          <Row>
            <Column column="12">
              <div style={{textAlign: 'right', background: '#f2f2f2', padding: '10px'}}>
                <a className="btn btn-danger"  onClick={(e) => {this.props.onCancel();}}>Cancel</a>
              </div>
            </Column>
          </Row>          

        </Panel>
      </Loadable>
      </div>
    );
  }
}

export default AutomaticCreditCardForm;
