import * as React from 'react';
import { Tab, Nav, NavItem } from 'react-bootstrap';
import Loadable from '../../components/Loadable';
import * as utils from '../../../App/utils';
import { Panel } from '../../../ascendis-ui/containers';

import { Row, Column } from '../../../ascendis-ui/layout';
import { InputGroup, Dropdown } from '../../../ascendis-ui/components';

export interface IUnitFormProps {
  data?: any;
  onSave: Function;
  onCancel: Function;
}

export interface IUnitFormState {
  itemCode: string;
  itemDescription: string;
  inactive: any;
  itemClass: string;
  unitOfMeasure: string;
  itemType: string;
  taxType: string;
  costMethod: string;
  glSalesAccount: string;
  glInventoryAccount: string;
  glSalaryAccount: string;
  dataItemClass: any;
  dataCostMethod: any;
  dataGLChart: any;
  dataServiceType: any;
  dataTaxType: any;
  loading: boolean;
  pricebreakFrom1: string;
  pricebreakTo1: string;
  pricebreakL1Price: string;
  pricebreakFrom2: string;
  pricebreakTo2: string;
  pricebreakL2Price: string;
  pricebreakFrom3: string;
  pricebreakTo3: string;
  pricebreakL3Price: string;
  serviceType: string;
  errors?: any;
}

class ItemForm extends React.Component<IUnitFormProps, IUnitFormState> {
  constructor(props) {
    super(props);

    const itemClass = [
      { value: '1', label: 'Stock Item'},
      { value: '2', label: 'Description'},
      { value: '4', label: 'Service'},
    ];
    const methods = [
      { value: '0', label: 'Average Cost'},
      { value: '1', label: 'FIFO - First In First Out'},
    ];

    this.state = {
      dataItemClass: itemClass,
      dataCostMethod: methods,
      dataTaxType: [],
      dataGLChart: {},
      dataServiceType: {},
      itemCode: '',
      itemClass: '',
      itemDescription: '',
      inactive: false,
      unitOfMeasure: '',
      itemType: '',
      taxType: '',
      costMethod: '',
      glSalesAccount: '',
      glSalaryAccount: '',
      glInventoryAccount: '',
      loading: true,
      pricebreakFrom1: '',
      pricebreakTo1: '',
      pricebreakL1Price: '',
      pricebreakFrom2: '',
      pricebreakTo2: '',
      pricebreakL2Price: '',
      pricebreakFrom3: '',
      pricebreakTo3: '',
      pricebreakL3Price: '',
      serviceType: '',
      errors: [],
    };

    const isEdit = this.props.data && this.props.data[0] ? true : false;

    if (isEdit) {
      const { ItemCode, ItemDesc, ItemClass, Inactive,
        UnitofMeasure, ItemType, TaxType, CostMethod,
        GLSalesAc, GLStockAc, GLCOGSAc,
        PriceBreakFrom1, PriceBreakTo1, PriceBreakL1Price,
        PriceBreakFrom2, PriceBreakTo2, PriceBreakL2Price,
        PriceBreakFrom3, PriceBreakTo3, PriceBreakL3Price,
        ServiceType,
      } = this.props.data[0];
      
      this.state = {
        dataItemClass: itemClass,
        dataCostMethod: methods,
        dataGLChart: {},
        dataTaxType: [],
        dataServiceType: [],
        itemCode: ItemCode,
        itemClass: ItemClass,
        itemDescription: ItemDesc,
        inactive: Inactive === 'True' ? true : false,
        unitOfMeasure: UnitofMeasure,
        itemType: ItemType,
        taxType: TaxType,
        costMethod: CostMethod,
        glSalesAccount: GLSalesAc,
        glSalaryAccount: GLCOGSAc,
        glInventoryAccount: GLStockAc,
        loading: true,
        pricebreakFrom1: PriceBreakFrom1,
        pricebreakTo1: PriceBreakTo1,
        pricebreakL1Price: PriceBreakL1Price,
        pricebreakFrom2: PriceBreakFrom2,
        pricebreakTo2: PriceBreakTo2,
        pricebreakL2Price: PriceBreakL2Price,
        pricebreakFrom3: PriceBreakFrom3,
        pricebreakTo3: PriceBreakTo3,
        pricebreakL3Price: PriceBreakL3Price,
        serviceType: ServiceType,
        errors: [],
      };
    }

    this.handleInputChange = this.handleInputChange.bind(this);
    this.verifydata = this.verifydata.bind(this);
    this.getDataAndDisplay = this.getDataAndDisplay.bind(this);
  }

  componentDidMount() {
    this.getDataAndDisplay();
  }

  getDataAndDisplay() {
    const { fetchGLChart, fetchTaxType, fetchService } = utils;
    
    Promise.all([fetchGLChart(), fetchTaxType(), fetchService()]).then(
      (response: any) => {
        const dataGLChart = response[0].data;
        const dataTaxType = response[1].data;
        const dataServiceType = response[2].data;
        this.setState({
          dataGLChart,
          dataTaxType,
          dataServiceType,
          loading: false,
        } as IUnitFormState);
      }
    ).catch(
      (ex) => {
        alert(ex);
      }
    );
  }
  
  handleInputChange(prop, value) {
    let state = {};
    state[prop] = value;

    this.setState(state as IUnitFormState);
  }

  validateForm() {
    const errors = [];

    if (utils.IsEmptyNull(this.state.serviceType)) {
      errors.push({serviceType : 'Field required'});
    } 

    if (utils.IsEmptyNull(this.state.itemDescription)) {
      errors.push({itemDescription : 'Field required'});
    }    

    if (utils.IsEmptyNull(this.state.glSalesAccount)) {
      errors.push({glSalesAccount : 'Field required'});
    }

    if (utils.IsEmptyNull(this.state.glInventoryAccount)) {
      errors.push({glInventoryAccount : 'Field required'});
    } 

    if (utils.IsEmptyNull(this.state.glSalaryAccount)) {
      errors.push({glSalaryAccount : 'Field required'});
    }    

    if (utils.IsEmptyNull(this.state.itemClass)) {
      errors.push({itemClass : 'Field required'});
    }  

    if (utils.IsEmptyNull(this.state.costMethod)) {
      errors.push({costMethod : 'Field required'});
    }  

    if (utils.IsEmptyNull(this.state.pricebreakFrom1)) {
      errors.push({pricebreakFrom1 : 'Field required'});
    }  

    if (utils.IsEmptyNull(this.state.pricebreakFrom2)) {
      errors.push({pricebreakFrom2 : 'Field required'});
    }                              

    return errors; 
  }
  
  verifydata() {

    const errors = this.validateForm.bind(this)();
    if (errors.length > 0) {
      this.setState({
        errors
      });
      return;
    }

    const isEdit = this.props.data && this.props.data[0] ? true : false;
    this.props.onSave(this.state, isEdit, isEdit ? this.props.data[0].CustTennentsID : null);
  }

  render() {
    const isEdit = this.props.data && this.props.data[0] ? true : false;
    
    return (
      <Loadable isLoading={this.state.loading} text={'Loading Form...'}>
        <Panel title="Tariffs Form">
          <Row>
            <Column column="4">
              <InputGroup 
                label="Tariff Code" 
                placeholder="Tariff Code" 
                inputName="itemCode"
                onValueChange={(value) => {this.handleInputChange('itemCode', value);}}
                value={this.state.itemCode}
                disabled={isEdit}/>
            </Column>
            <Column column="4">
              <InputGroup 
                label="Description" 
                placeholder="Description" 
                inputName="itemDescription"
                onValueChange={(value) => {this.handleInputChange('itemDescription', value);}}
                value={this.state.itemDescription}
                error={this.state.errors.findIndex( (x) => x.itemDescription !== undefined) > -1}
                errorMsg={utils.findElementValue(this.state.errors, 'itemDescription')}                 
              /> 
            </Column>
            <Column column="4">
              <InputGroup 
                label="Inactive" 
                placeholder="Inactive" 
                inputName="inactive"
                type="checkbox"
                onValueChange={(value) => {this.handleInputChange('inactive', value);}}
                value={this.state.inactive}/>
            </Column>
          </Row>
          <Row>
            <Column column="4">
              <Dropdown 
                list={this.state.dataItemClass}
                label="Tariff Class" 
                inputName="itemClass"
                onValueChange={(value) => {this.handleInputChange('itemClass', value.value);}}
                value={this.state.itemClass}
                error={this.state.errors.findIndex( (x) => x.itemClass !== undefined) > -1}
                errorMsg={utils.findElementValue(this.state.errors, 'itemClass')}
              />
            </Column>
             <Column column="4">
              <Dropdown 
                list={this.state.dataServiceType}
                label="Service Type" 
                inputName="serviceType"
                onValueChange={(value) => {this.handleInputChange('serviceType', value.value);}}
                value={this.state.serviceType}
                error={this.state.errors.findIndex( (x) => x.serviceType !== undefined) > -1}
                errorMsg={utils.findElementValue(this.state.errors, 'serviceType')}                
              />
            </Column>
          </Row>
          
          <div style={{padding: '5px', marginTop: '10px'}}>
            <Tab.Container id="tabs-with-dropdown" defaultActiveKey="first">
              <Row className="clearfix">
                <Column column="12">
                  <Nav bsStyle="tabs">
                    <NavItem eventKey="first">
                      <i className="fa fa-info-circle"/> General Details
                    </NavItem>
                    <NavItem eventKey="second">
                      <i className="fa fa-money"/> Pricing
                    </NavItem>
                  </Nav>
                </Column>
                <Column column="12">
                  <Tab.Content animation>
                    <Tab.Pane eventKey="first">
                      <div style={{padding: '10px'}}>
                        <Row>
                          <Column column="6">
                            <InputGroup 
                              label="Unit of Measure" 
                              placeholder="Unit Of Measure" 
                              inputName="unitOfMeasure"
                              onValueChange={(value) => {this.handleInputChange('unitOfMeasure', value);}}
                              value={this.state.unitOfMeasure}/>
                          </Column>
                          <Column column="6">
                            <Dropdown 
                              list={this.state.dataGLChart}
                              label="G/L Sales Account" 
                              inputName="glSalesAccount"
                              onValueChange={(value) => {this.handleInputChange('glSalesAccount', value.value);}}
                              value={this.state.glSalesAccount}
                              error={this.state.errors.findIndex( (x) => x.glSalesAccount !== undefined) > -1}
                              errorMsg={utils.findElementValue(this.state.errors, 'glSalesAccount')}
                            />
                          </Column>
                        </Row>
                        <Row>
                          <Column column="6">
                            <InputGroup 
                              label="Tariff Type" 
                              placeholder="Tariff Type" 
                              inputName="itemType"
                              onValueChange={(value) => {this.handleInputChange('itemType', value);}}
                              value={this.state.itemType}/>
                          </Column>
                          <Column column="6">
                            <Dropdown 
                              list={this.state.dataGLChart}
                              label="G/L Inventory Account" 
                              inputName="glInventoryAccount"
                              onValueChange={(value) => {this.handleInputChange('glInventoryAccount', value.value);}}
                              value={this.state.glInventoryAccount}
                              error={this.state.errors.findIndex( (x) => x.glInventoryAccount !== undefined) > -1}
                              errorMsg={utils.findElementValue(this.state.errors, 'glInventoryAccount')}
                            />
                          </Column>
                        </Row>
                        <Row>
                          <Column column="6">
                            <Dropdown 
                              list={this.state.dataTaxType}
                              label="Tax Type" 
                              inputName="taxType"
                              onValueChange={(value) => {this.handleInputChange('taxType', value.value);}}
                              value={this.state.taxType}/>
                          </Column>
                          <Column column="6">
                            <Dropdown 
                              list={this.state.dataGLChart}
                              label="G/L COGS/Salary Account" 
                              inputName="glSalaryAccount"
                              onValueChange={(value) => {this.handleInputChange('glSalaryAccount', value.value);}}
                              value={this.state.glSalaryAccount}
                              error={this.state.errors.findIndex( (x) => x.glSalaryAccount !== undefined) > -1}
                              errorMsg={utils.findElementValue(this.state.errors, 'glSalaryAccount')}
                            />
                          </Column>
                        </Row>
                        <Row>
                          <Column column="6">
                            <Dropdown 
                              list={this.state.dataCostMethod}
                              label="Cost Method" 
                              inputName="costMethod"
                              onValueChange={(value) => {this.handleInputChange('costMethod', value.value);}}
                              value={this.state.costMethod}
                              error={this.state.errors.findIndex( (x) => x.costMethod !== undefined) > -1}
                              errorMsg={utils.findElementValue(this.state.errors, 'costMethod')}
                            />
                          </Column>
                        </Row>
                      </div>
                    </Tab.Pane>
                    <Tab.Pane eventKey="second">
                      <div style={{padding: '10px'}}>
                        <Row>
                          <Column column="4">
                            <InputGroup 
                              label="Level 1 From" 
                              placeholder="From" 
                              inputName="pricebreakFrom1"
                              onValueChange={(value) => {this.handleInputChange('pricebreakFrom1', value);}}
                              value={this.state.pricebreakFrom1}
                              dataValidation="required"
                              error={this.state.errors.findIndex( (x) => x.pricebreakFrom1 !== undefined) > -1}
                              errorMsg={utils.findElementValue(this.state.errors, 'pricebreakFrom1')}
                            />
                          </Column>
                          <Column column="4">
                            <InputGroup 
                              label="To" 
                              placeholder="To" 
                              inputName="pricebreakTo1"
                              onValueChange={(value) => {this.handleInputChange('pricebreakTo1', value);}}
                              value={this.state.pricebreakTo1}/>
                          </Column>
                          <Column column="4">
                            <InputGroup 
                              label="Price" 
                              placeholder="Price" 
                              inputName="pricebreakL1Price"
                              onValueChange={(value) => {this.handleInputChange('pricebreakL1Price', value);}}
                              value={this.state.pricebreakL1Price}/>
                          </Column>
                        </Row>
                        <Row>
                          <Column column="4">
                            <InputGroup 
                              label="Level 2 From" 
                              placeholder="From" 
                              inputName="pricebreakFrom2"
                              onValueChange={(value) => {this.handleInputChange('pricebreakFrom2', value);}}
                              value={this.state.pricebreakFrom2}
                              dataValidation="required"
                              error={this.state.errors.findIndex( (x) => x.pricebreakFrom2 !== undefined) > -1}
                              errorMsg={utils.findElementValue(this.state.errors, 'pricebreakFrom2')}
                            />
                          </Column>
                          <Column column="4">
                            <InputGroup 
                              label="To" 
                              placeholder="To" 
                              inputName="pricebreakTo2"
                              onValueChange={(value) => {this.handleInputChange('pricebreakTo2', value);}}
                              value={this.state.pricebreakTo2}/>
                          </Column>
                          <Column column="4">
                            <InputGroup 
                              label="Price" 
                              placeholder="Price" 
                              inputName="pricebreakL2Price"
                              onValueChange={(value) => {this.handleInputChange('pricebreakL2Price', value);}}
                              value={this.state.pricebreakL2Price}/>
                          </Column>
                        </Row>
                        <Row>
                          <Column column="4">
                            <InputGroup 
                              label="Level 3 From" 
                              placeholder="From" 
                              inputName="pricebreakFrom3"
                              onValueChange={(value) => {this.handleInputChange('pricebreakFrom3', value);}}
                              value={this.state.pricebreakFrom3}/>
                          </Column>
                          <Column column="4">
                            <InputGroup 
                              label="To" 
                              placeholder="To" 
                              inputName="pricebreakTo3"
                              onValueChange={(value) => {this.handleInputChange('pricebreakTo3', value);}}
                              value={this.state.pricebreakTo3}/>
                          </Column>
                          <Column column="4">
                            <InputGroup 
                              label="Price" 
                              placeholder="Price" 
                              inputName="pricebreakL3Price"
                              onValueChange={(value) => {this.handleInputChange('pricebreakL3Price', value);}}
                              value={this.state.pricebreakL3Price}/>
                          </Column>
                        </Row>
                      </div>
                    </Tab.Pane>
                  </Tab.Content>
                </Column>
              </Row>
            </Tab.Container>
          </div>
          
          <div style={{textAlign: 'right', background: '#f2f2f2', padding: '10px'}}>
            <a className="btn btn-success"  onClick={(e) => {this.verifydata();}}>Save</a>
            <a className="btn btn-danger"  onClick={(e) => {this.props.onCancel();} }>Cancel</a>
          </div>
        </Panel>
      </Loadable>
    );
  }
}

export default ItemForm;
