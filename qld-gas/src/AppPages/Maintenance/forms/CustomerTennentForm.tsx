import * as React from 'react';
import { Panel } from '../../../ascendis-ui/containers';

import { Row, Column } from '../../../ascendis-ui/layout';
import {InputGroup, Dropdown} from '../../../ascendis-ui/components';
import * as utils from '../../../App/utils';

export interface ICustomerTennentsFormProps {
  data?: any;
  custno: string;
  onSave: Function;
  onCancel: Function;  
}

class CustomerTennentsForm extends React.Component<ICustomerTennentsFormProps, any> {
  constructor(props) {
    super(props);

    this.state = {
      title: '',
      firstName: '',
      surname: '',
      includeOnInvoice: false,
      titleList: []
    };
    
    const isEdit = this.props.data && this.props.data[0] ? true : false;

    if (isEdit) {
      const { Title, FirstName, Surname, IncludeOnInvoice} = this.props.data[0];
      
      this.state = {
        title: Title,
        firstName: FirstName,
        surname: Surname,
        includeOnInvoice: IncludeOnInvoice === 'False' ? false : true,
      };
    }

    this.handleTitleChange = this.handleTitleChange.bind(this);
    this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
    this.handleSurnameChange = this.handleSurnameChange.bind(this);
    this.handleIncludeOnInvoiceChange = this.handleIncludeOnInvoiceChange.bind(this);
    this.verifydata = this.verifydata.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleInputChange(prop, value) {
    let state = {};
    state[prop] = value;
    this.setState(state);
  }  
  
  handleTitleChange(value) {
    this.setState({
      title: value,
    });
  }
  
  handleFirstNameChange(value) {
    this.setState({
      firstName: value,
    });
  }
  
  handleSurnameChange(value) {
    this.setState({
      surname: value,
    });
  }
  
  handleIncludeOnInvoiceChange(value) {
    this.setState({
      includeOnInvoice: value,
    });
  }

  verifydata() {
    const isEdit = this.props.data && this.props.data[0] ? true : false;

    this.props.onSave(this.state, isEdit, isEdit ? this.props.data[0].CustTennentsID : null);
  }

  componentDidMount() {
    this.getDataAndDisplay();
  }

  getDataAndDisplay() {
    
    let list = (utils.fetchTitles()).data;
    this.setState({          
      titleList: list,
      loading: false,
    });


  }  

  render() {
    return (
      <Panel title="Customer Tenants Form">
        <Row>
          <Column column="4">
            <Dropdown 
              list={this.state.titleList}
              label="Title" 
              inputName="title"
              onValueChange={(value) => {this.handleInputChange('title', value.value);}}
              value={this.state.title}/>
          </Column>
          <Column column="4">
            <InputGroup type="text" inputName="firstName" placeholder="First Name" 
              label="First Name" onValueChange={this.handleFirstNameChange}
              value={this.state.firstName}/>
          </Column>
          <Column column="4">
            <InputGroup type="text" inputName="surname" placeholder="Surname" 
              label="Surname" onValueChange={this.handleSurnameChange}
              value={this.state.surname}/>
          </Column>
          <Column column="4">
            <InputGroup type="checkbox" inputName="IncludeOnInvoice" placeholder="Include On Invoice?" 
              label="Include On Invoice?" onValueChange={this.handleIncludeOnInvoiceChange}
              value={this.state.includeOnInvoice.toString()}/>
          </Column>
        </Row>

        <div style={{textAlign: 'right', background: '#f2f2f2', padding: '10px'}}>
          <a className="btn btn-success"  onClick={(e) => {this.verifydata();}}>Save</a>
          <a className="btn btn-danger"  onClick={(e) => {this.props.onCancel();} }>Cancel</a>
        </div>
      </Panel>
    );
  }
}

export default CustomerTennentsForm;