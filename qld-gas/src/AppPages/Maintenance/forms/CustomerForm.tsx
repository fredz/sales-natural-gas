import * as React from 'react';
import { Panel } from '../../../ascendis-ui/containers';

import CustomerHeader from './components/Customer/CustomerHeader';
import CustomerInfo from './components/Customer/CustomerInfo';
import SalesComponent from './components/Customer/SalesComponent';
import CustomerTennents from '../CustomerTennents';

import Loadable from '../../components/Loadable';
import * as utils from '../../../App/utils';

export interface ICustomerFormProps {
  data?: any;
  onSave: Function;
  onCancel: Function;
  goToCardDetails?: Function;
}

export interface ICustomerFormState {
  headerCustomer: {
    customerNumber: string;
    bPay: string;
    inactive: boolean;
    accessSMS: boolean;
  };
  customerInfo: {};
  salesComponent: {};
  list: Array<any>;
  titleList: Array<any>;
  loading: boolean;
  errors?: any;
}

class CustomerForm extends React.Component<ICustomerFormProps, ICustomerFormState> {
  constructor(props) {
    super(props);
    this.state = {
      headerCustomer: {
        customerNumber: '',
        bPay: '',
        inactive: false,
        accessSMS: false
      },
      customerInfo: {},
      salesComponent: {},
      loading: true,
      list: [],
      titleList: [],
      errors: [],
    };

    const isEdit = props.data && props.data[0] ? true : false;

    if (isEdit) {
      const data = props.data[0];
      
      this.state = {
        headerCustomer: {
          customerNumber: data.CustomerNo,
          bPay: data.BPayNo,
          inactive: (data.Inactive === '' || data.Inactive === 'False') ? false : true,
          accessSMS: (data.AccessSMS === '' || data.AccessSMS === 'False') ? false : true
        },
        customerInfo: {
          title: data.Title,
          surname: data.Surname,
          givenName: data.GivenNames, 
          postalAddress: data.POAddress1,
          postalAddress2: data.POAddress2,
          postalAddress3: data.POAddress3,
          homePhone: data.HomePhone,
          workPhone: data.WorkPhone,
          mobileNumber: data.MobileNo,
          billingName: data.BillingNames,
          comments: data.Comments,
          cityCode: data.POCityCode,
          email: data.Email,
          MYOBJobCode: data.MYOBJobCode
        },
        salesComponent: {
          salesAccount: data.GLSalesAc,
          postBarcodeCheckSum: data.APostBCodeChkSum,
          lastStatementDate: data.LastStmtDate,
          outstandingBalance: data.OSBalance,
          cardPayment: data.CardPayment
        },
        loading: true,
        list: [],
        titleList: [],
        errors: [],
      };
    }

    this.handleHeaderCostumerChange = this.handleHeaderCostumerChange.bind(this);
    this.handleCostumerInfoChange = this.handleCostumerInfoChange.bind(this);
    this.handleSalesComponentChange = this.handleSalesComponentChange.bind(this);
    this.getDataAndDisplay = this.getDataAndDisplay.bind(this);
    this.verifydata = this.verifydata.bind(this);
  }

  componentDidMount() {
    this.getDataAndDisplay();
  }

  getDataAndDisplay() {
    const {fetchCityCodes, fetchTitles} = utils;
    
    Promise.all([fetchCityCodes(), fetchTitles()]).then(
      (response: any) => {                
        this.setState({
          list: response[0].data,
          titleList: response[1].data,
          loading: false,
        } as ICustomerFormState);
      }
    ).catch(
      (ex) => {
        alert(ex);
      }
    );
  }
  
  handleHeaderCostumerChange(value) {
    this.setState({
      headerCustomer: value,
    } as ICustomerFormState);
  }
  
  handleCostumerInfoChange(value) {
    this.setState({
      customerInfo: value,
    } as ICustomerFormState);
  }
  
  handleSalesComponentChange(value) {
    this.setState({
      salesComponent: value,
    } as ICustomerFormState);
  }

  validateForm(data: any) {
    const errors = [];

    const { title, givenName, surname, email} = data.customerInfo;    

    if (utils.IsEmptyNull(title)) {
      errors.push({title : 'Field required'});
    } 

    if (utils.IsEmptyNull(givenName)) {
      errors.push({givenName : 'Field required'});
    }    

    if (utils.IsEmptyNull(surname)) {
      errors.push({surname : 'Field required'});
    }   
    
    if (!utils.validateEmail(email)) {
      errors.push({email : 'Field Email is not valid'});
    }             

    return errors; 
  }  

  verifydata() {

    const errors = this.validateForm.bind(this)(this.state);
    if (errors.length > 0) {
      this.setState({
        errors
      });
      return;
    }

    const isEdit = this.props.data && this.props.data[0] ? true : false;    
    this.props.onSave(this.state, isEdit, isEdit ? this.state.headerCustomer.customerNumber : null);
  }

  render() {
    const isEdit = this.props.data && this.props.data[0] ? true : false;
    
    const data = isEdit ? this.props.data[0] : null; 
    const withTennents = isEdit ? (
      <CustomerTennents custno={data.CustomerNo}/>
    ) : null;
    
    return (
      <Loadable isLoading={this.state.loading} text={'Loading Form...'}>
        <Panel title="Customer Form">
          <div id="CustomerForm" >
            <CustomerHeader isEdit={isEdit} data={data} onInfoChange={this.handleHeaderCostumerChange}/>
            <CustomerInfo isEdit={isEdit} data={data} errors={this.state.errors}
              onInfoChange={this.handleCostumerInfoChange} list={this.state.list} titleList={this.state.titleList} />
            <SalesComponent isEdit={isEdit} data={data} onInfoChange={this.handleSalesComponentChange}/>
            <div style={{textAlign: 'right', background: '#f2f2f2', padding: '10px'}}>
              <a className={'btn btn-success ' + (isEdit === false ? 'hidden' : '')} 
                onClick={(e) => {this.props.goToCardDetails(this.props.data);} }>Go to Card Details</a>
              <a className="btn btn-success" onClick={(e) => {this.verifydata();} }>Save</a>
              <a className="btn btn-danger" onClick={(e) => {this.props.onCancel(); }}>Cancel</a>
            </div>
            {withTennents}
          </div>
        </Panel>
      </Loadable>
    );
  }
}

export default CustomerForm;