import * as React from 'react';
import Loadable from '../../components/Loadable';
import * as utils from '../../../App/utils';
import { Panel } from '../../../ascendis-ui/containers';

import { Row, Column } from '../../../ascendis-ui/layout';
import { InputGroup, Dropdown } from '../../../ascendis-ui/components';

export interface IComplexFormProps {
  data?: any;
  onSave: Function;
  onCancel: Function;
}

export interface IRentalFormState {
  rentalCode: string;
  inactive: boolean;
  unitCode: string;
  unitNumber: string;
  complexName: string;
  customNo: string;
  title: string;
  billingName: string;
  givenNames: string;
  startDate: string;
  endDate: string;
  terms: string;
  secDeposit: string;
  loading: boolean;
  data: any;
  dataUnit: any;
  dataCustomer: any;
  errors?: any;
}

class ComplexForm extends React.Component<IComplexFormProps, IRentalFormState> {
  constructor(props) {
    super(props);

    this.state = {
      data: {},
      rentalCode: '',
      inactive: false,
      unitCode: '',
      unitNumber: '',
      complexName: '',
      customNo: '',
      title: '',
      billingName: '',
      givenNames: '',
      startDate: '',
      endDate: '',
      terms: '',
      secDeposit: '',      
      loading: false,
      dataUnit: [],
      dataCustomer: [],
      errors: [],
    };

    const isEdit = this.props.data && this.props.data[0] ? true : false;    

    if (isEdit) {
      
      const { RentalCode, Inactive, UnitCode, NoUnits, ComplexName, Custno, 
        Title, BillingNames, GivenNames, StartDate, EndDate, Terms, SecDeposit} = this.props.data[0];

      this.state = {
        data: {},        
        rentalCode: RentalCode,
        inactive: Inactive,
        unitCode: UnitCode,
        unitNumber: NoUnits,
        complexName: ComplexName,
        customNo: Custno,
        title: Title,        
        givenNames: GivenNames,
        billingName: BillingNames,
        startDate: StartDate,
        endDate: EndDate,
        terms: Terms,
        secDeposit: SecDeposit,        
        loading: true,
        dataUnit: [],
        dataCustomer: [],
        errors: [],
      };
    }

    this.handleInputChange = this.handleInputChange.bind(this);
    this.verifydata = this.verifydata.bind(this);
    this.getDataAndDisplay = this.getDataAndDisplay.bind(this);    
    this.handleInputUnitChange = this.handleInputUnitChange.bind(this);
    this.handleInputCustomerChange = this.handleInputCustomerChange.bind(this);
    
  }

  componentDidMount() {
    this.getDataAndDisplay();        
  }

  getDataAndDisplay() {        
    const {fetchUnitForRental, fetchCustomerRental} = utils;    

    Promise.all([fetchUnitForRental(this.state.unitCode), fetchCustomerRental()]).then(      
      (response: any) => {        
        
        const dataUnit = response[0].data;
        const dataCustomer = response[1].data;        
        this.setState({
          dataUnit,
          dataCustomer,
          loading: false,
        } as IRentalFormState);
      }
    ).catch(
      (ex) => {
        alert(ex);
      }
    );
  }
  
  handleInputChange(prop, value) {
    let state = {};
    state[prop] = value;

    this.setState(state as IRentalFormState);
  }

  handleInputUnitChange(prop, value) {    
    let state = {};

    state['unitNumber'] = value.NoUnits;
    state['complexName'] = value.ComplexName;
    state[prop] = value.value;

    this.setState(state as IRentalFormState);
  }  

  handleInputCustomerChange(prop, value) {    

    let state = {};

    state['givenNames'] = value.GivenNames;
    state['title'] = value.Title;
    state['billingName'] = value.label;
    state[prop] = value.value;

    this.setState(state as IRentalFormState);
  }  

  validateForm() {
    const errors = [];

    if (utils.IsEmptyNull(this.state.unitCode)) {
      errors.push({unitCode : 'Field required'});
    } 

    if (utils.IsEmptyNull(this.state.customNo)) {
      errors.push({customNo : 'Field required'});
    } 

    if (utils.IsEmptyNull(this.state.startDate)) {
      errors.push({startDate : 'Field required'});      
    }    

    return errors; 
  }    

  verifydata() {
    const errors = this.validateForm.bind(this)();
    if (errors.length > 0) {
      this.setState({
        errors
      });
      return;
    }

    const isEdit = this.props.data && this.props.data[0] ? true : false;    
    this.props.onSave(this.state, isEdit, isEdit ? this.props.data[0].CustTennentsID : null);
  }

  render() {
    const isEdit = this.props.data && this.props.data[0] ? true : false;

    return (
      <Loadable isLoading={this.state.loading} text={'Loading Form...'}>
        <Panel title="Rental Form">
          <div id="RentalForm" >
            <Row>
              <Column column="6">
                <Dropdown 
                  list={this.state.dataUnit}                  
                  label="Unit Code" 
                  inputName="unitCode"
                  onValueChange={(value) => {this.handleInputUnitChange('unitCode', value);}}
                  value={this.state.unitCode}
                  error={this.state.errors.findIndex( (x) => x.unitCode !== undefined) > -1}
                  errorMsg={utils.findElementValue(this.state.errors, 'unitCode')}                  
                />
              </Column>
              <Column column="2">
                <InputGroup 
                  label="Inactive"
                  placeholder="Inactive"
                  type="checkbox"
                  inputName="inactive" 
                  value={this.state.inactive.toString().toLowerCase()}               
                  onValueChange={(value) => {this.handleInputChange('inactive', value);}}               
                  />
              </Column>          
            </Row>
            <Row>
              <Column column="6">
                <InputGroup 
                  label="Complex Name" 
                  placeholder="Complex Name" 
                  inputName="complexName"
                  onValueChange={(value) => {this.handleInputChange('complexName', value);}}
                  value={this.state.complexName}
                  disabled={true}/>
              </Column>
              <Column column="6">
                <InputGroup 
                  label="Total Unit of Complex" 
                  placeholder="Total Unit of Complex" 
                  inputName="unitNumber"
                  type="number"
                  onValueChange={(value) => {this.handleInputChange('unitNumber', value);}}
                  value={this.state.unitNumber}
                  disabled={true}/>
              </Column>            
            </Row>
            <hr/>
            <Row>
              <Column column="6">
                <Dropdown 
                  list={this.state.dataCustomer}
                  label="Customer" 
                  inputName="customNo"
                  onValueChange={(value) => {this.handleInputCustomerChange('customNo', value);}}
                  value={this.state.customNo}
                  error={this.state.errors.findIndex( (x) => x.customNo !== undefined) > -1}
                  errorMsg={utils.findElementValue(this.state.errors, 'customNo')}                  
                />
              </Column>
            </Row>
            <Row>
              <Column column="6">
                <InputGroup 
                  label="Title" 
                  placeholder="Title" 
                  inputName="title"                  
                  onValueChange={(value) => {this.handleInputChange('title', value);}}
                  value={this.state.title}
                  disabled={true}/>
              </Column>                  
            </Row>
            <Row>
              <Column column="6">
                <InputGroup 
                  label="Billing Name" 
                  placeholder="Billing Name" 
                  inputName="billingname"                  
                  onValueChange={(value) => {this.handleInputChange('billingname', value);}}
                  value={this.state.billingName}
                  disabled={true}/>
              </Column> 
              <Column column="6">
                <InputGroup 
                  label="Given Names" 
                  placeholder="Given Names" 
                  inputName="givenNames"                  
                  onValueChange={(value) => {this.handleInputChange('givenNames', value);}}
                  value={this.state.givenNames}
                  disabled={true}/>
              </Column>                                  
            </Row> 
            <hr/>
            <Row>
              <Column column="6">
                <InputGroup 
                  type="datepicker" 
                  label="Start Date" 
                  placeholder="Start Date" 
                  inputName="startDate"                  
                  onValueChange={(value) => {this.handleInputChange('startDate', value);}}
                  value={this.state.startDate}
                  error={this.state.errors.findIndex( (x) => x.startDate !== undefined) > -1}
                  errorMsg={utils.findElementValue(this.state.errors, 'startDate')}
                />
              </Column>                                  
            </Row>
            <Row>
              <Column column="6">
                <InputGroup 
                  label="Payment Terms" 
                  placeholder="Payment Terms" 
                  inputName="terms"                  
                  onValueChange={(value) => {this.handleInputChange('terms', value);}}
                  value={this.state.terms}/>
              </Column> 
              <Column column="6">
                <InputGroup 
                  label="Security Deposit" 
                  placeholder="Security Deposit" 
                  inputName="secDeposit"                  
                  onValueChange={(value) => {this.handleInputChange('secDeposit', value);}}
                  value={this.state.secDeposit}/>
              </Column>                                  
            </Row>                                   

            <div style={{textAlign: 'right', background: '#f2f2f2', padding: '10px'}}>
              <a className="btn btn-success"  onClick={(e) => {this.verifydata();}}>Save</a>
              <a className="btn btn-danger"  onClick={(e) => {this.props.onCancel();} }>Cancel</a>
            </div>            
          </div>
        </Panel>
      </Loadable>
    );
  }
}

export default ComplexForm;
