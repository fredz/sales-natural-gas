import * as React from 'react';
import Loadable from '../../components/Loadable';
import * as utils from '../../../App/utils';
import { Panel } from '../../../ascendis-ui/containers';

import { Row, Column } from '../../../ascendis-ui/layout';
import { InputGroup, Dropdown } from '../../../ascendis-ui/components';

export interface ICityFormProps {
  data?: any;
  onSave: Function;
  onCancel: Function;
}

export interface ICityFormState {
  cityCode: string;
  cityName: string;
  stateCode: string;
  postCode: string;
  data: any;
  loading: boolean;
  errors?: any;
}

class CityForm extends React.Component<ICityFormProps, ICityFormState> {
  constructor(props) {
    super(props);

    this.state = {
      data: {},
      cityCode: '',
      cityName: '',
      stateCode: '',
      postCode: '',
      loading: true,
      errors: [],
    };

    const isEdit = this.props.data && this.props.data[0] ? true : false;

    if (isEdit) {
      const { Citycode, Cityname, Statecode, Postcode,
        
      } = this.props.data[0];
      
      this.state = {
        data: {},
        cityCode: Citycode,
        cityName: Cityname,
        postCode: Postcode,
        stateCode: Statecode,
        loading: true,
        errors: [],
      };
    }

    this.handleInputChange = this.handleInputChange.bind(this);
    this.verifydata = this.verifydata.bind(this);
    this.getDataAndDisplay = this.getDataAndDisplay.bind(this);
  }

  componentDidMount() {
    this.getDataAndDisplay();
  }

  getDataAndDisplay() {
    const { fetchState } = utils;
    
    Promise.all([fetchState()]).then(
      (response: any) => {
        const data = response[0].data;
        this.setState({
          data,
          loading: false,
        } as ICityFormState);
      }
    ).catch(
      (ex) => {
        alert(ex);
      }
    );
  }
  
  handleInputChange(prop, value) {
    let state = {};
    state[prop] = value;

    this.setState(state as ICityFormState);
  }

  validateForm() {
    const errors = [];

    if (utils.IsEmptyNull(this.state.cityCode)) {
      errors.push({cityCode : 'Field required'});
    } 

    if (utils.IsEmptyNull(this.state.cityName)) {
      errors.push({cityName : 'Field required'});
    }
    
    return errors; 
  }  

  verifydata() {
    const errors = this.validateForm.bind(this)();
    if (errors.length > 0) {
      this.setState({
        errors
      });
      return;
    }    

    const isEdit = this.props.data && this.props.data[0] ? true : false;
    this.props.onSave(this.state, isEdit, isEdit ? this.props.data[0].CityCode : null);
  }

  render() {
    const isEdit = this.props.data && this.props.data[0] ? true : false;
    
    return (
      <Loadable isLoading={this.state.loading} text={'Loading Form...'}>
        <Panel title="City Form">
          <Row>
            <Column column="4">
              <InputGroup 
                label="City Code" 
                placeholder="City Code" 
                inputName="cityCode"
                onValueChange={(value) => {this.handleInputChange('cityCode', value);}}
                value={this.state.cityCode}
                disabled={isEdit}
                error={this.state.errors.findIndex( (x) => x.cityCode !== undefined) > -1}
                errorMsg={utils.findElementValue(this.state.errors, 'cityCode')}                
              />
            </Column>
            <Column column="4">
              <InputGroup 
                label="City/Suburb" 
                placeholder="City/Suburb" 
                inputName="cityName"
                onValueChange={(value) => {this.handleInputChange('cityName', value);}}
                value={this.state.cityName}
                error={this.state.errors.findIndex( (x) => x.cityName !== undefined) > -1}
                errorMsg={utils.findElementValue(this.state.errors, 'cityName')}
              /> 
            </Column>
          </Row>
          <Row>
        <Column column="4">
          <Dropdown
            list={this.state.data}
            label="State Name" 
            inputName="stateCode"
            onValueChange={(value) => {this.handleInputChange('stateCode', value.value);}}
            value={this.state.stateCode}/> 
        </Column>
         <Column column="4">
              <InputGroup 
                label="Postal Code" 
                placeholder="Postal Code" 
                inputName="postCode"
                onValueChange={(value) => {this.handleInputChange('postCode', value);}}
                value={this.state.postCode}/> 
            </Column>
          </Row>
          
          <div style={{textAlign: 'right', background: '#f2f2f2', padding: '10px'}}>
            <a className="btn btn-success"  onClick={(e) => {this.verifydata();}}>Save</a>
            <a className="btn btn-danger"  onClick={(e) => {this.props.onCancel();} }>Cancel</a>
          </div>
        </Panel>
      </Loadable>
    );
  }
}

export default CityForm;
