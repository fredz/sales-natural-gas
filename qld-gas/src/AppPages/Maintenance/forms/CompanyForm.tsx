import * as React from 'react';
import { Panel } from '../../../ascendis-ui/containers';

import { Row, Column } from '../../../ascendis-ui/layout';
import InputGroup from '../../../ascendis-ui/components/InputGroup';

export interface ICompanyFormProps {
  data?: any;
  onSave: Function;
}

class CompanyForm extends React.Component<ICompanyFormProps, any> {
  constructor(props) {
    super(props);

    const isEdit = this.props.data && this.props.data[0] ? true : false;
    
    if (isEdit) {
      const { CoName, CoAddr1, CoAddr2, CoAddr3, CoCity, CoState, CoPcode, CoCountry,
         CoPhoneNo, CoFaxNo, CoACN, CoABN, CoDefTerm, CoAdmFee, CoLateFee} = this.props.data[0];
      
      this.state = {
        companyName: CoName,
        companyAddress1: CoAddr1,
        companyAddress2: CoAddr2,
        companyAddress3: CoAddr3,
        city: CoCity,
        state: CoState,
        postcode: CoPcode,
        country: CoCountry,
        phoneNumber: CoPhoneNo,
        faxNumber: CoFaxNo,
        acn: CoACN,
        abn: CoABN,
        defaultTerm: CoDefTerm,
        administrationFee: CoAdmFee,
        lateFee: CoLateFee,

      };
    }

    this.handleInputChange = this.handleInputChange.bind(this);
    this.verifydata = this.verifydata.bind(this);
  }
  
  handleInputChange(prop, value) {
    let state = {};
    state[prop] = value;

    this.setState(state);
  }

  verifydata() {
    const isEdit = this.props.data && this.props.data[0] ? true : false;
    
    this.props.onSave(this.state, isEdit, null);
  }

  render() {
    const isEdit = this.props.data && this.props.data[0] ? true : false;

    return (
      <Panel title="Company Information">
        <Row>
          <Column column="4">
              <InputGroup 
                label="Company Name" 
                placeholder="Company Name" 
                inputName="companyName"
                onValueChange={(value) => {this.handleInputChange('companyName', value);}}
                value={this.state.companyName}
                disabled={isEdit}/>
            </Column>
        </Row>
          <Row>
          <Column column="4">
              <InputGroup 
                label="Company Address 1" 
                placeholder="Company Address 1" 
                inputName="companyAddress1"
                onValueChange={(value) => {this.handleInputChange('companyAddress1', value);}}
                value={this.state.companyAddress1}/>
            </Column>
          <Column column="4">
              <InputGroup 
                 label="Company Address 2" 
                placeholder="Company Address 2" 
                inputName="companyAddress2"
                onValueChange={(value) => {this.handleInputChange('companyAddress2', value);}}
                value={this.state.companyAddress2}/>
            </Column>
          <Column column="4">
              <InputGroup 
                label="Company Address 3" 
                placeholder="Company Address 3" 
                inputName="companyAddress3"
                onValueChange={(value) => {this.handleInputChange('companyAddress3', value);}}
                value={this.state.companyAddress3}/>
            </Column>
        </Row>
          <Row>
          <Column column="4">
              <InputGroup 
                label="City/Suburb" 
                placeholder="City/Suburb" 
                inputName="city"
                onValueChange={(value) => {this.handleInputChange('city', value);}}
                value={this.state.city}/>
            </Column>
              <Column column="4">
              <InputGroup 
                label="State" 
                placeholder="State" 
                inputName="state"
                onValueChange={(value) => {this.handleInputChange('state', value);}}
                value={this.state.state}/>
            </Column>
        </Row>
          <Row>
            <Column column="4">
              <InputGroup 
                label="Postcode" 
                placeholder="Postcode" 
                inputName="postcode"
                onValueChange={(value) => {this.handleInputChange('postcode', value);}}
                value={this.state.postcode}/>
            </Column>
             <Column column="4">
              <InputGroup 
                label="Country" 
                placeholder="Country" 
                inputName="country"
                onValueChange={(value) => {this.handleInputChange('country', value);}}
                value={this.state.country}/>
            </Column>
        </Row>
          <Row>
          <Column column="4">
              <InputGroup 
                label="Phone Number" 
                placeholder="Phone Number" 
                inputName="phoneNumber"
                onValueChange={(value) => {this.handleInputChange('phoneNumber', value);}}
                value={this.state.phoneNumber}/>
            </Column>
                 <Column column="4">
              <InputGroup 
                label="Fax Number" 
                placeholder="Fax Number" 
                inputName="faxNumber"
                onValueChange={(value) => {this.handleInputChange('faxNumber', value);}}
                value={this.state.faxNumber}/>
            </Column>
        </Row>
          <Row>
          <Column column="4">
              <InputGroup 
                label="A.C.N" 
                placeholder="A.C.N" 
                inputName="acn"
                onValueChange={(value) => {this.handleInputChange('acn', value);}}
                value={this.state.acn}/>
            </Column>
                 <Column column="4">
              <InputGroup 
               label="A.B.N" 
                placeholder="A.B.N" 
                inputName="abn"
                onValueChange={(value) => {this.handleInputChange('abn', value);}}
                value={this.state.abn}/>
            </Column>
        </Row>
          <Row>
          <Column column="4">
              <InputGroup 
                label="Default Term(Days)" 
                placeholder="Default Term(Days)" 
                inputName="defaultTerm"
                onValueChange={(value) => {this.handleInputChange('defaultTerm', value);}}
                value={this.state.defaultTerm}/>
            </Column>
              <Column column="4">
              <InputGroup 
                label="Administration Fee" 
                placeholder="Administration Fee" 
                inputName="administrationFee"
                onValueChange={(value) => {this.handleInputChange('administrationFee', value);}}
                value={this.state.administrationFee}/>
            </Column>
              <Column column="4">
              <InputGroup 
                label="Late Fee" 
                placeholder="Late Fee" 
                inputName="lateFee"
                onValueChange={(value) => {this.handleInputChange('lateFee', value);}}
                value={this.state.lateFee}/>
            </Column>
        </Row>
        <div style={{textAlign: 'right', background: '#f2f2f2', padding: '10px'}}>
          <a className="btn btn-success"  onClick={(e) => {this.verifydata();}}>Save</a>
        </div>
      </Panel>
    );
  }
}

export default CompanyForm;