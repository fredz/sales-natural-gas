import * as React from 'react';
import Loadable from '../../components/Loadable';
import * as utils from '../../../App/utils';
import { Panel } from '../../../ascendis-ui/containers';

import { Row, Column } from '../../../ascendis-ui/layout';
import { InputGroup, Dropdown } from '../../../ascendis-ui/components';

export interface IUnitFormProps {
  data?: any;
  onSave: Function;
  onCancel: Function;
}

export interface IUnitFormState {
  unitCode: string;
  unitNumber: string;
  complexNo: any;
  meterNumber: string;
  itemCode: any;  
  previousReadingDate: string;
  previousReading: string;
  lastReadingDate: string;
  lastReading: string;
  currentReadingDate: string;
  currentReading: string;
  dataTariff: any;
  dataComplex: any;  
  loading: boolean;
  serviceType: string;
  errors?: any;
}

class UnitForm extends React.Component<IUnitFormProps, IUnitFormState> {
  constructor(props) {
    super(props);

    this.state = {
      dataTariff: [],
      dataComplex: [],      
      unitCode: '',
      unitNumber: '',
      complexNo: '',
      meterNumber: '',
      itemCode: '',      
      previousReadingDate: '',
      previousReading: '',
      lastReadingDate: '',
      lastReading: '',
      currentReadingDate: '',
      currentReading: '',
      loading: true,
      serviceType: '',
      errors: [],
    };

    const isEdit = this.props.data && this.props.data[0] ? true : false;

    if (isEdit) {
      const { UnitCode, ComplexUnitNo, ComplexNo, MeterNo, ItemCode,
        PrevReadingDate, PrevReading, LastReadingDate, LastReading,
        CurReadingDate, CurReading, ServiceType} = this.props.data[0];
      
      this.state = {
        dataTariff: [],
        dataComplex: [],        
        unitCode: UnitCode,
        unitNumber: ComplexUnitNo,
        complexNo: ComplexNo,
        meterNumber: MeterNo,
        itemCode: ItemCode,        
        previousReadingDate: PrevReadingDate, //new Date(PrevReadingDate).toISOString(),
        previousReading: PrevReading,
        lastReadingDate: LastReadingDate, //new Date(LastReadingDate).toISOString(),
        lastReading: LastReading,
        currentReadingDate: CurReadingDate, //new Date(CurReadingDate).toISOString(),
        currentReading: CurReading,
        loading: true,
        serviceType: ServiceType,
        errors: [],
      };
    }

    this.handleInputChange = this.handleInputChange.bind(this);
    this.verifydata = this.verifydata.bind(this);
    this.getDataAndDisplay = this.getDataAndDisplay.bind(this);
    this.getTariffByServiceType = this.getTariffByServiceType.bind(this);
  }

  componentDidMount() {
    this.getDataAndDisplay();

    const isEdit = this.props.data && this.props.data[0] ? true : false;
    if (isEdit) {
      this.getTariffByServiceType(this.state.serviceType);
    }    
  }

  getDataAndDisplay() {    
    const { fetchComplex } = utils;    
    fetchComplex().then(
      (response: any) => {
        const dataComplex = response.data;        
        this.setState({          
          dataComplex,          
          loading: false,
        } as IUnitFormState);
      }
    ).catch(
      (ex) => {
        alert(ex);
      }
    );
  }
  
  handleInputChange(prop, value) {
    let state = {};
    

    if (prop === 'complexNo') {
      state[prop] = value.value;
      this.getTariffByServiceType(value.serviceType);
    }else {
      state[prop] = value;
    }

    this.setState(state as IUnitFormState);
  }

  getTariffByServiceType(serviceType: string) {

    const { fetchTariffServiceType } = utils;    
    fetchTariffServiceType(serviceType).then(
      (response: any) => {        
        const dataTariff = response.data;        
        this.setState({
          dataTariff,          
          loading: false,
        } as IUnitFormState);
      }
    ).catch(
      (ex) => {
        alert(ex);
      }
    );    

  }

  validateForm() {
    const errors = [];

    if (utils.IsEmptyNull(this.state.unitNumber)) {
      errors.push({unitNumber : 'Field required'});
    } 

    if (utils.IsEmptyNull(this.state.complexNo)) {
      errors.push({complexNo : 'Field required'});
    }    

    if (utils.IsEmptyNull(this.state.meterNumber)) {
      errors.push({meterNumber : 'Field required'});
    }

    if (utils.IsEmptyNull(this.state.itemCode)) {
      errors.push({itemCode : 'Field required'});
    }                   

    return errors; 
  }  

  verifydata() {

    const errors = this.validateForm.bind(this)();
    if (errors.length > 0) {
      this.setState({
        errors
      });
      return;
    }

    const isEdit = this.props.data && this.props.data[0] ? true : false;
    this.props.onSave(this.state, isEdit, isEdit ? this.props.data[0].CustTennentsID : null);
  }

  render() {
    const isEdit = this.props.data && this.props.data[0] ? true : false;
    
    return (
      <Loadable isLoading={this.state.loading} text={'Loading Form...'}>
        <Panel title="Unit Form">
          <div id="UnitForm" >
            <Row>
              <Column column="4">
                <InputGroup 
                  label="Unit Number" 
                  placeholder="Unit Number" 
                  inputName="unitNumber"
                  onValueChange={(value) => {this.handleInputChange('unitNumber', value);}}
                  value={this.state.unitNumber}
                  maxLength={5}
                  dataValidation="required"
                  error={this.state.errors.findIndex( (x) => x.unitNumber !== undefined) > -1}
                  errorMsg={utils.findElementValue(this.state.errors, 'unitNumber')}                  
                /> 
              </Column>
              <Column column="4">
                <InputGroup 
                  label="Meter Number" 
                  placeholder="Meter Number" 
                  inputName="meterNumber"
                  onValueChange={(value) => {this.handleInputChange('meterNumber', value);}}
                  value={this.state.meterNumber}
                  maxLength={15}
                  dataValidation="required"
                  error={this.state.errors.findIndex( (x) => x.meterNumber !== undefined) > -1}
                  errorMsg={utils.findElementValue(this.state.errors, 'meterNumber')}
                />
              </Column>
            </Row>
            <Row>
              <Column column="4">
                <Dropdown 
                  list={this.state.dataComplex}
                  label="Complex Name" 
                  inputName="complexNo"
                  onValueChange={(value) => {this.handleInputChange('complexNo', value);}}
                  value={this.state.complexNo} 
                  error={this.state.errors.findIndex( (x) => x.complexNo !== undefined) > -1}
                  errorMsg={utils.findElementValue(this.state.errors, 'complexNo')}                  
                />
              </Column>
              <Column column="4">
                <Dropdown 
                  list={this.state.dataTariff}
                  label="Tariff" 
                  inputName="itemCode"
                  onValueChange={(value) => {this.handleInputChange('itemCode', value.value);}}
                  value={this.state.itemCode}
                  error={this.state.errors.findIndex( (x) => x.itemCode !== undefined) > -1}
                  errorMsg={utils.findElementValue(this.state.errors, 'itemCode')}                  
                />
              </Column>
            </Row>
            <Row>
              <Column column="6">
                <InputGroup 
                  type="datepicker" 
                  label="Previous Reading Date" 
                  placeholder="Previous Reading Date" 
                  inputName="previousReadingDate"
                  onValueChange={(value) => {this.handleInputChange('previousReadingDate', value);}}                  
                  value={this.state.previousReadingDate}/>
              </Column>
              <Column column="6">
                <InputGroup 
                  label="Previous Reading" 
                  placeholder="Previous Reading" 
                  inputName="previousReading"
                  onValueChange={(value) => {this.handleInputChange('previousReading', value);}}
                  value={this.state.previousReading}/> 
              </Column>
            </Row>
            <Row>
              <Column column="6">
                <InputGroup 
                  type="datepicker" 
                  label="Last Reading Date" 
                  placeholder="Last Reading Date" 
                  inputName="lastReadingDate"
                  onValueChange={(value) => {this.handleInputChange('lastReadingDate', value);}}
                  value={this.state.lastReadingDate}/>
              </Column>
              <Column column="6">
                <InputGroup 
                  label="Last Reading" 
                  placeholder="Last Reading" 
                  inputName="lastReading"
                  onValueChange={(value) => {this.handleInputChange('lastReading', value);}}
                  value={this.state.lastReading}/> 
              </Column>
            </Row>
            <Row>
              <Column column="6">
                <InputGroup 
                  type="datepicker" 
                  label="Current Reading Date" 
                  placeholder="Current Reading Date" 
                  inputName="currentReadingDate"
                  onValueChange={(value) => {this.handleInputChange('currentReadingDate', value);}}
                  value={this.state.currentReadingDate}/>
              </Column>
              <Column column="6">
                <InputGroup 
                  label="Current Reading" 
                  placeholder="Current Reading" 
                  inputName="currentReading"
                  onValueChange={(value) => {this.handleInputChange('currentReading', value);}}
                  value={this.state.currentReading}/> 
              </Column>
            </Row>
            
            <div style={{textAlign: 'right', background: '#f2f2f2', padding: '10px'}}>
              <a className="btn btn-success"  onClick={(e) => {this.verifydata();}}>Save</a>
              <a className="btn btn-danger"  onClick={(e) => {this.props.onCancel();} }>Cancel</a>
            </div>            
          </div>
        </Panel>
      </Loadable>
    );
  }
}

export default UnitForm;
