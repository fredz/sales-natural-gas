import * as React from 'react';
import { connect } from 'react-redux';
import { setPage } from '../../actions/app';
import { IPageSettings } from '../../state';
import SimpleHeader from '../components/SimpleHeader';

import { TableHeaderColumn } from 'react-bootstrap-table';

import Loadable from '../components/Loadable';
import CustomFilter from '../components/CustomFilter';
import Listing from '../components/Listing';
import CustomerUnitForm from './forms/CustomerUnitForm';
import { PageMode, pages } from '../constants';

import * as utils from '../../App/utils';
import Alert from '../components/Alert';

export interface ICustomerUnitProps {
  custno: string;
  fullName: string;
}

interface IConnectedProps {
  pageCode: string;
  pageSettings: IPageSettings;
}

interface IConnectedDispatch {
  onSetPageCode: (value: string, settings: IPageSettings) => void;  
}

export interface ICustomerUnitState {
  mode: PageMode;
  loading: boolean;
  pageLoading: boolean;
  data: Array<any>;
  total: number;
  editData: Array<any>;
  surname?: string;
  givennames?: string;
  dataComplex?: Array<any>;
  complexNo?: string;
}

class CustomerUnit 
  extends React.Component<ICustomerUnitProps & IConnectedProps & IConnectedDispatch, ICustomerUnitState> {

  alertRef: any;

  constructor(props) {
    super(props);

    this.state = {
      total: 0,
      data: [],
      mode: PageMode.list,
      loading: true,
      editData: [],
      pageLoading: true,
      dataComplex: [],
    } as ICustomerUnitState;

    this.getContentFromMode = this.getContentFromMode.bind(this);
    this.getDataAndDisplay = this.getDataAndDisplay.bind(this);
    this.onEditDataUpdate = this.onEditDataUpdate.bind(this);
    this.handleFormSave = this.handleFormSave.bind(this);
    this.handleFormCancel = this.handleFormCancel.bind(this);
    this.onDeleteData = this.onDeleteData.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleRightItemClick = this.handleRightItemClick.bind(this);
  }

  componentDidMount() {
    this.getDataAndDisplay(PageMode.list);
  }

  handleRightItemClick(mode: PageMode) {
    if (mode === PageMode.list) {
      this.setState({
        loading: true,
      } as ICustomerUnitState);

      this.getDataAndDisplay(mode);
    } else {
      this.setState({
        mode,
      } as ICustomerUnitState);
    }
  }

  getDataAndDisplay(mode, from = '', to = '', sort = '') {
    let formData = new FormData();
    formData.append('Action', 'Fetch');
    formData.append('Path', 'CustomerUnits/');
    formData.append('CustNo', this.props.custno);
    
    utils.fetch(formData).then(
      (response: any) => {
        const { data, total } = response;
        this.setState({
          mode,
          data,
          loading: false,
          pageLoading: false,
          total,
          editData: [],
        } as ICustomerUnitState);
      }
    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );
  }

  onEditDataUpdate(list) {
    this.setState({
      editData: list,
      mode: PageMode.edit,
    } as ICustomerUnitState);
  }

  onDeleteData(list) {
    if (list.length === 0) {
      return;
    }
    
    let ids = '';
    for (let i = 0; i < list.length; i++) {
      ids += ids.length > 0 ? ',' : '';
      ids += list[i].RentalCode;
    }

    utils.canDelete(ids, 'QLD.CanDeleteRental').then(	
      (response: any) => {        
        
        // validate if the record does not depend of others records
        if (!response.canDelete) {  
          
            let msg = 'The record(s) cannot be deleted';
            let toastrTitle = 'Delete Rental';
            this.alertRef.displayAlert('info', msg, toastrTitle);              

            return;
        }else {

          if (!confirm('Are you sure you want to delete selected rentals?')) {
            return;
          }
          
          let formData = new FormData();
          formData.append('Action', 'Delete');
          formData.append('Path', 'Rental/');
          formData.append('RentalCode', ids);

          utils.fetch(formData).then(
            (response: any) => {
              const { status } = response;

              this.getDataAndDisplay(PageMode.list);
              
              if (status === 'ok') {          
                let msg = 'Selected rentals were deleted';
                let toastrTitle = 'Delete Rental';
                this.alertRef.displayAlert('info', msg, toastrTitle);                                  
              }
            }
          ).catch(
            (ex) => {
              this.alertRef.displayAlert('error', ex, 'Error');
            }
          );
        }
      }
    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );

  }

  getContentFromMode(mode: PageMode) {
    let content;
    switch (mode) {
      case PageMode.list:
        let { data } = this.state;
        const columns = [
          {dataField: 'RentalCode', header: 'Rental Code', isKey: true, hidden: true},
          {dataField: 'UnitCode', header: 'Unit No.', dataFormat: (cell, row) => {
            return (
              <a style={{cursor:'pointer'}}
                onClick={(e) => this.props.onSetPageCode(pages.unitcodesMaintenance, {editData: row.UnitCode})}>
                {cell}
              </a>
            );
          }},
          {dataField: 'ComplexName', header: 'Complex Name', dataFormat: (cell, row) => {
            return (
              <a style={{cursor:'pointer'}}
                onClick={(e) => this.props.onSetPageCode(pages.complexMaintenance, {editData: row})}>
                {cell}
              </a>
            );
          }},
          {dataField: 'ComplexAddress', header: 'Address'},          
          {dataField: 'StartDateF', header: 'Start Date'},
          {dataField: 'EndDateF', header: 'Last Date'},

          {dataField: 'Terms', header: 'Terms', hidden: true},
          {dataField: 'SecDeposit', header: 'Security Deposit', hidden: false},
        ];

        if (!data) {
          data = [];
        }

        const actions = (
          <TableHeaderColumn 
            dataField="action"             
            dataAlign="center"        
            headerAlign="center"             
            width="100px" 
            dataFormat={(cell, row) => {

              row.FullName = this.props.fullName;

              let transferButton: any =  null;
              if(utils.IsEmptyNull(row.EndDateF)){
                transferButton = (
                  <a className="btn btn-xs btn-success" title="Transfer Rental" 
                    onClick={(e) => {
                      e.preventDefault();  
                      this.props.onSetPageCode(pages.transferrentalMaintenance, {editData: row});                       
                    }}>
                    <i className="fa fa-exchange"/>
                  </a>                
                );
              }

              return (
                <span>
                  <a className="btn btn-xs btn-warning" title="Edit" 
                    onClick={(e) => {e.preventDefault(); this.onEditDataUpdate([row]);}}>
                    <i className="glyphicon glyphicon-edit"/>
                  </a>                  
                  {transferButton}
                </span>
              );
          }}>
            Actions
          </TableHeaderColumn>
        );        

        content = (
          <div>
            <CustomFilter 
              onRefreshData={null} 
              total={this.state.total} 
              from={this.state.total}
              to={this.state.total}
              withSearch={false} 
              withActions={true}             
              />
            <Listing 
              data={data}
              columns={columns} 
              onRefreshData={null}
              onEditData={this.onEditDataUpdate}
              onDeleteData={this.onDeleteData}
              withActions={true}
              actions={actions}
              />
          </div>
        );
        break;

      case PageMode.new: 
        content = (
          <CustomerUnitForm custno={this.props.custno} onCancel={this.handleFormCancel} onSave={this.handleFormSave}/>
        );
        break;

      case PageMode.edit:
        content = (
          <CustomerUnitForm data={this.state.editData} custno={this.props.custno} 
            onSave={this.handleFormSave} onCancel={this.handleFormCancel}/>
        );
        break;

      default: 
        content = null;
    }

    return content;
  }

  handleFormSave(data, isEdit, id = null) {
    let formData = new FormData();
    
    if (isEdit) {
      formData.append('Action', 'Update');
    } else {
      formData.append('Action', 'Insert');
    }
    formData.append('Path', 'Rental/');
    formData.append('RentalCode', data.rentalCode);
    formData.append('CustNo', this.props.custno);
    formData.append('UnitCode', data.unitCode);
    formData.append('StartDate', data.startDate);
    formData.append('EndDate', data.endDate);
    formData.append('Terms', data.terms);
    formData.append('SecDeposit', data.securityDeposit);

    utils.fetch(formData).then(
      (response: any) => {
        const { status } = response;

        let message;
        let toastrTitle;
        if (status === 'ok') {
          if (!isEdit) {
            toastrTitle = 'Record updated';
            message = `New Rental: ${id} for ${this.props.custno} Added`;
          } else {
            toastrTitle = 'Record added';
            message = `Rental: ${id} for ${this.props.custno} Updated`;
          }         
          
          this.alertRef.displayAlert('success', message, toastrTitle); 
        }else{
          this.alertRef.displayAlert('error', 
            `Error while Saving.\nError Message:\n${response.error}`, 'Error');                    
        }

        this.getDataAndDisplay(PageMode.list);
      }
    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );

    this.setState({
     loading: true, 
    } as ICustomerUnitState);
  }

  handleInputChange(prop, value) {
    let state = {};
    state[prop] = value;

    this.setState(state as ICustomerUnitState);
  }

  handleFormCancel() {
    this.setState({
      mode: PageMode.list,
    } as ICustomerUnitState);
  }

  render() {
    const content = this.getContentFromMode(this.state.mode);
    const brand = (
      <span>
        <i className="fa fa-building-o"/> {`Units list for customer "${this.props.fullName}"`}
      </span>
    );

    return (
      <div style={{margin: '5px', border: '1px solid black'}}>
        <SimpleHeader 
          brand={brand}
          listLabel={`List properties for customer `}
          newLabel="Add Unit"
          onRightItemClick={this.handleRightItemClick}
          mode={this.state.mode}
        />
        <Alert ref={x => this.alertRef = x} />
        <Loadable isLoading={this.state.loading} text={'Fetching units...'}>
          {content}
        </Loadable>
      </div>
    );
  }
}

function mapStateToProps(state: any): IConnectedProps {
  return {
    pageCode: state.app.pageCode,
    pageSettings: state.app.editData,
  };
}

function mapDispatchToProps(dispatch): IConnectedDispatch {
  return {
    onSetPageCode: (value: string, settings) => dispatch(setPage(value, settings))
  };
}

export default 
  connect(mapStateToProps, mapDispatchToProps)(CustomerUnit) as React.ComponentClass<ICustomerUnitProps>;
