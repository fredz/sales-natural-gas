import * as React from 'react';
import * as moment from 'moment';
import { connect } from 'react-redux';
import { setPage } from '../../actions/app';
import { IPageSettings } from '../../state';

import Loadable from '../components/Loadable';
import Listing from '../components/Listing';
import CustomFilter from '../components/CustomFilter';
import SimpleHeader from '../components/SimpleHeader';
import { TableHeaderColumn } from 'react-bootstrap-table';
import ReceiptHistoryInvoice from './ReceiptHistoryInvoice';
//import ReceiptHistoryForm from './forms/ReceiptHistoryForm';
import { PageMode, pages, ExcludeTerms } from '../constants';

import * as utils from '../../App/utils';
import * as searchUtils from '../../App/searchUtils';
import Alert from '../components/Alert';

export interface IReceiptHistoryState {
  mode: PageMode;
  loading: boolean;
  from: number;
  to: number;
  data: Array<any>;
  total: number;
  historyInvoiceData: Array<any>;
  deleteData: Array<any>;
  searchText: string;
}


export interface IReceiptHistoryProps {

}

interface IConnectedProps {
  pageCode: string;
  editData: any;
}

interface IConnectedDispatch {
  onSetPageCode: (value: string, settings: IPageSettings) => void;
}

class ReceiptHistory extends React.Component<IReceiptHistoryProps & IConnectedProps & IConnectedDispatch, 
IReceiptHistoryState> {

  alertRef: any; 

  constructor(props) {
    super(props);

    this.state = {
      mode: PageMode.list,
      loading: true,
      from: 0,
      to: 100,
      searchText: '',
    } as IReceiptHistoryState;

    this.handleRightItemClick = this.handleRightItemClick.bind(this);
    this.getContentFromMode = this.getContentFromMode.bind(this);
    this.getDataAndDisplay = this.getDataAndDisplay.bind(this);
    this.onRangeUpdate = this.onRangeUpdate.bind(this);
    //this.handleFormSave = this.handleFormSave.bind(this);
    this.handleFormCancel = this.handleFormCancel.bind(this);
    //this.handleEditData = this.handleEditData.bind(this);
    //this.handleDeleteData = this.handleDeleteData.bind(this);
    this.handleSearchData = this.handleSearchData.bind(this);
    //this.handleCustomerView = this.handleCustomerView.bind(this);
  }

  componentDidMount() {
    if (this.props.editData) {
      this.handleSearchData(this.props.editData);
    } else {
      this.getDataAndDisplay(this.state.mode, this.state.from, this.state.to);
    }
  }

  handleRightItemClick(mode: PageMode) {
    if (mode === PageMode.list) {
      this.setState({
        loading: true,
      } as IReceiptHistoryState);

      const searchCriteria = this.prepareSearchCriteria(this.state.searchText);
      this.getDataAndDisplay(mode, this.state.from, this.state.to, '', searchCriteria);
    } else {
      this.setState({
        mode,
      } as IReceiptHistoryState);
    }
  }

  getDataAndDisplay(mode, from, to, sort = '', searchCriteria = '') {
    let formData = new FormData();
    formData.append('Action', 'Fetch');
    formData.append('Path', 'ReceiptHistory/');
    formData.append('From', from);
    formData.append('To', to);
    formData.append('Sort', sort);
    formData.append('Search', searchCriteria);

    utils.fetch(formData).then(
      (response: any) => {
        const { data, total } = response;
        this.setState({
          mode,
          data,
          loading: false,
          from, 
          to,
          total
        } as IReceiptHistoryState);
      }
    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );
  }

  onRangeUpdate(from, to) {
    
    const { mode, searchText } = this.state;
    const searchCriteria = this.prepareSearchCriteria(searchText);
    this.getDataAndDisplay(mode, from, to, '', searchCriteria);

    this.setState({
      loading: true,
    } as IReceiptHistoryState);
  }

  handleFormCancel() {
    this.setState({
      mode: PageMode.list,
    } as IReceiptHistoryState);
  }

  handleViewDetails(list) {
    this.setState({
      historyInvoiceData: list,
      mode: PageMode.receipt_history_invoice,
    } as IReceiptHistoryState);
  }

  prepareSearchCriteria(searchText) {
    const columns = [
      'Custno', 'BillingNames', 'BPayno'
    ];
    
    return searchUtils.tokenizeSearch('BPayno', searchText, columns, ExcludeTerms);
  }

  handleSearchData(searchText) {
    const searchCriteria = this.prepareSearchCriteria(searchText);
    const { from, to } = this.state;

    this.getDataAndDisplay(PageMode.list, from, to, '', searchCriteria);

    this.setState({
      loading: true,
      searchText,
    } as IReceiptHistoryState);
  }

  getContentFromMode(mode: PageMode) {
    let content;
    switch (mode) {
      case PageMode.list: 
        let { data } = this.state;
        const columns = [
          {dataField: 'PaymentID', header: 'PaymentID', isKey: true, hidden: true},
          {dataField: 'BillingNames', header: 'Billing Names', 
            dataFormat: (cell, row) => {
              return <p>{cell}</p>;
            }           
          },          
          {dataField: 'Custno', header: 'Custno'},
          {dataField: 'BPayno', header: 'BPayno'},
          {dataField: 'Property', header: 'Property', hidden: true},
          {dataField: 'OSBalance', header: 'Outstanding Balance', hidden: true},          
          {dataField: 'PaymentAmount', header: 'Payment Amount'},
          {dataField: 'PaymentDate', header: 'Payment Date',
            dataFormat: (cell, row) => {
              return <span>{cell ? moment(cell).format('D MMM Y') : ''}</span>;
            }          
          },
          {dataField: 'CardNumber', header: 'Card Number',
            dataFormat: (cell, row) => {
              if (cell.length > 0) {
                return 'xxxx xxxx xxxx ' + cell.substring((cell.length - 4), cell.length);
              }else {
                return '';
              }              
            }          
          }, 
          {dataField: 'CardPayment', header: 'Card Payment'},                   
          {dataField: 'Consolidated', header: 'Consolidated' , hidden: false,
            dataFormat: (cell, row) => {
              
              let iconClass = 'none';
              if (cell.length > 0 && cell === '1' ) {
                  iconClass = 'fa fa-check';
              }
              return (              
                <div className="text-center" >
                  <i className={iconClass}>&nbsp;</i> 
                </div>
              );
            }                              
          },
        ];

        if (!data) {
          data = [];
        }

        const actions = (
          <TableHeaderColumn 
            dataField="action" 
            dataAlign="center"        
            headerAlign="center"            
            width="100px"
            dataFormat={(cell, row) => {
              return (
                <span>
                  <a className="btn btn-xs btn-info" title="Details" 
                    onClick={(e) => {e.preventDefault(); this.handleViewDetails([row]);}}>
                    <i className="glyphicon glyphicon-eye-open"/>
                  </a>
                </span>
              );
          }}>
            Actions
          </TableHeaderColumn>
        );

        content = (
          <div>
            <CustomFilter 
              onRefreshData={this.onRangeUpdate} 
              total={this.state.total}  
              from={this.state.from}  
              to={this.state.to} 
              withSearch={true} 
              onSearch={this.handleSearchData} 
              searchText={this.state.searchText} 
              withActions={true} 
              withRange={true}            
              />
            <Listing 
              data={data}
              columns={columns} 
              onRefreshData={this.onRangeUpdate}
              onEditData={null}
              onDeleteData={null}
              withActions={true}
              actions={actions}/>
          </div>
        );
        break;

      case PageMode.receipt_history_invoice:
        content = <ReceiptHistoryInvoice historyInvoiceData={this.state.historyInvoiceData} 
          onCancel={this.handleFormCancel} />;
        break;

      default: 
        content = null;
    }

    return content;
  }

  render() {
    
    const brand = (
      <span>
        <i className="glyphicon glyphicon-list-alt"/> Invoice & Receipt History Maintenance
      </span>
    );

    const content = this.getContentFromMode(this.state.mode);

    return (
      <div>
        <Alert ref={x => this.alertRef = x} />
        <Loadable isLoading={this.state.loading} text={'Fetching Data...'}>
          <SimpleHeader 
            brand={brand}
            listLabel="List"
            newLabel="New ReceiptHistory Code"
            onRightItemClick={null}
            mode={null}
            withOptions={false}
          />
          {content}          
        </Loadable>
      </div>
    );
  }
}

function mapStateToProps(state: any): IConnectedProps {
  return {
    pageCode: state.app.pageCode,
    editData: state.app.editData,
  };
}

function mapDispatchToProps(dispatch): IConnectedDispatch {
  return {
    onSetPageCode: (value: string, settings) => dispatch(setPage(value, settings))
  };
}

export default 
  connect(mapStateToProps, mapDispatchToProps)(ReceiptHistory) as React.ComponentClass<IReceiptHistoryProps>;
