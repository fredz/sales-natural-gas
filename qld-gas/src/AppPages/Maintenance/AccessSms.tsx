import * as React from 'react';
import { connect } from 'react-redux';
import { TableHeaderColumn } from 'react-bootstrap-table';
import { setPage } from '../../actions/app';
import { IPageSettings } from '../../state';

import { Tabs, Tab } from 'react-bootstrap';
import Listing from '../components/Listing';
import Loadable from '../components/Loadable';
import { Row, Column } from '../../ascendis-ui/layout';
import { InputGroup, Dropdown } from '../../ascendis-ui/components';
import * as utils from '../../App/utils';
import * as smsUtils from '../../App/smsutils';
import Alert from '../components/Alert';

export interface IAccessSmsProps {
  key?: number;
}

interface IConnectedProps {
  pageCode: string;
  editData: any;
}

interface IConnectedDispatch {
  onSetPageCode: (value: string, settings: IPageSettings) => void;
}

export interface IAccessSmsState {
  key: number;    
  complexNo: string;
  complexData: any;
  loading: boolean; 
  listData: Array<any>;
  currentType: ExecType;  
  loadingText: string;
  message?: string;  
}

export type ExecType = 'complex';

class AccessSms extends 
  React.Component<IAccessSmsProps & IConnectedProps & IConnectedDispatch, IAccessSmsState> {
  
  fetching = 'Fetching Data';  
  alertRef: any;

  constructor(props) {
    super(props);    

    const key = this.props.key !== undefined ? this.props.key : 1;

    const defaultMessage = 
     'QLD Gas shall need access to your property tomorrow. Please ensure it is left unlocked for us so we can provide your gas refill.';

    this.state = {
      key,            
      complexNo: null,
      complexData: [],
      loading: false,
      listData: [],      
      currentType: null,      
      loadingText: this.fetching,      
      message: defaultMessage
    };

    this.handleSelect = this.handleSelect.bind(this);    
    this.handleInputChange = this.handleInputChange.bind(this);    
    this.fetchList = this.fetchList.bind(this);   

  }

  componentWillMount() {
    this.getDataAndDisplay();

    if (this.props.editData) {
      this.fetchList('complex');
    }
  }

  getDataAndDisplay() {
    const { fetchComplex } = utils;
    
    fetchComplex().then(
      (response: any) => {
        const complexData = response.data;        
        this.setState({
          complexData,
          loading: false,
        } as IAccessSmsState);
      }
    ).catch(
      (ex) => {
        alert(ex);
      }
    );
  }

  fetchList(etype: ExecType) {
    

    switch (etype) {
      case 'complex':
        if (!this.state.complexNo) {
          return;
        }

        this.setState({
          loading: true,
        });

        let formData = new FormData();
        formData.append('Action', 'Fetch');
        formData.append('Path', 'AccessSms/');
        formData.append('ComplexNo', this.state.complexNo);           
        
        utils.fetch(formData).then(
          (response: any) => {
            const listData = response.data;            
            this.setState({
              listData,
              loading: false,
              currentType: 'complex'
            } as IAccessSmsState);
          }
        );
        break;
      default:
        this.setState({
          listData: [],
          loading: false,
          currentType: null
        });
        break;
    }
  }

  handleSelect(key) {
    this.setState({key});
  }

  handleInputChange(prop, value) {
    let state = {};
    state[prop] = value;

    this.setState(state as IAccessSmsState);
  }

  handleSendSMS(phonelist: Array<string>) {
    if (phonelist.length === 0) {
      alert('We need at least one number to send sms');
    }

    smsUtils.sendSMS(this.state.message, phonelist)
    .then( (res: any) => {
      if (res.resultCode === 3) {
        alert(res.message);
        return;
      }
      alert('SMS was sent succesfully');
    })
    .catch( () => {
      alert('An Unknown error occured while trying to send SMS, please try again later');
    });
  }

  render() {

    const columns = [
      {dataField: 'Custno', header: 'Custno', isKey: true, hidden: true, editable: false},
      {dataField: 'BillingNames', header: 'Billing Names', editable: false},
      {dataField: 'ComplexName', header: 'Complex Name', editable: false},      
      {dataField: 'ComplexUnitNo', header: 'Unit No', editable: false}, 
      {dataField: 'FormattedMobileNo', header: 'Mobile No', editable: false},            
      {dataField: 'Message', header: 'Message', editable: false, 
        dataFormat: (cell, row) => {
          return <a  onClick={ ( e ) => this.setState({key: 2})}  >{this.state.message.substr(0,150)}</a>;
        }},      
    ];

    const actions = (
      <TableHeaderColumn 
        dataField="action" 
        dataAlign="center"        
        headerAlign="center"         
        width="50"
        dataFormat={(cell, row) => {
          return (
            <span>
              <a className="btn btn-xs btn-default" title="Send SMS" 
                onClick={(e) => {this.handleSendSMS([row.FormattedMobileNo]);}}>
                <i className="fa fa-mobile"/>
              </a>
            </span>
          );
      }}>
        Actions
      </TableHeaderColumn>
    );

    return (
      <div style={{margin: '0 10px'}}>
        <Alert ref={x => this.alertRef = x} />
        <Loadable isLoading={this.state.loading} text={this.state.loadingText}>
          <Tabs activeKey={this.state.key} onSelect={this.handleSelect} id="controlled-tab-example">
            <Tab eventKey={1} title="For Complex">
              <div className="well">
                <Row>
                  <Column column="8">
                    <Dropdown 
                      list={this.state.complexData}
                      label="Complex" 
                      inputName="complexNo"
                      onValueChange={(value) => {this.handleInputChange('complexNo', value.value);}}
                      value={this.state.complexNo}/>
                  </Column>
                  <Column column="4">
                    <a className="btn btn-success" onClick={(e) => this.fetchList('complex')}>Fetch</a>
                    {
                      this.state.currentType === 'complex' ? (
                        <a className="btn btn-success" onClick={(e) => {
                            this.handleSendSMS(this.state.listData.map(x => x.FormattedMobileNo));
                          }}>
                          <i className="fa fa-reply-all" aria-hidden="true" /> Send SMS ({this.state.listData.length})
                        </a>
                      ) : null
                    }
                  </Column>
                </Row>
              </div>
            </Tab>
            <Tab eventKey={2} title="Message">
              <div className="well">
                <Row>
                  <Column column="12">
                    <InputGroup 
                      label="Message" 
                      placeholder="Message"
                      type="textarea" 
                      inputName="message"
                      onValueChange={(value) => {this.handleInputChange('message', value);}}
                      value={this.state.message}/>
                  </Column>               
                </Row>
              </div>
            </Tab>              
          </Tabs>
          
          <Row>
            <Column column="12">
              <Listing 
                data={this.state.listData}
                columns={columns} 
                onRefreshData={null}
                onEditData={null}
                onDeleteData={null}
                withActions={true}
                actions={actions}
                withNumber={true}/>
              </Column> 
          </Row>
        </Loadable>
      </div>
    );
  }
}

function mapStateToProps(state: any): IConnectedProps {
  return {
    pageCode: state.app.pageCode,
    editData: state.app.editData,
  };
}

function mapDispatchToProps(dispatch): IConnectedDispatch {
  return {
    onSetPageCode: (value: string, settings) => dispatch(setPage(value, settings))
  };
}

export default 
  connect(mapStateToProps, mapDispatchToProps)(AccessSms) as React.ComponentClass<IAccessSmsProps>;
