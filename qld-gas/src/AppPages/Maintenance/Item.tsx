import * as React from 'react';

import Loadable from '../components/Loadable';
import Listing from '../components/Listing';
import SimpleHeader from '../components/SimpleHeader';
import ItemForm from './forms/ItemForm';
import { PageMode } from '../constants';
import CustomFilter from '../components/CustomFilter';

import * as utils from '../../App/utils';
import Alert from '../components/Alert';
import { TableHeaderColumn } from 'react-bootstrap-table';

export interface IItemState {
  mode: PageMode;
  loading: boolean;
  from: number;
  to: number;
  data: Array<any>;
  total: number;
  editData: Array<any>;
  deleteData: Array<any>;
  serviceTypes: Array<any>;
  searchText: string;
}

class Item extends React.Component<any, IItemState> {

  alertRef: any;

  constructor(props) {
    super(props);

    this.state = {
      mode: PageMode.list,
      loading: true,
      from: 0,
      to: 100,
      searchText: '',
    } as IItemState;

    this.handleRightItemClick = this.handleRightItemClick.bind(this);
    this.getContentFromMode = this.getContentFromMode.bind(this);
    this.getDataAndDisplay = this.getDataAndDisplay.bind(this);
    this.onRangeUpdate = this.onRangeUpdate.bind(this);
    this.handleFormSave = this.handleFormSave.bind(this);
    this.handleFormCancel = this.handleFormCancel.bind(this);
    this.handleEditData = this.handleEditData.bind(this);
    this.handleDeleteData = this.handleDeleteData.bind(this);
    this.handleSearchData = this.handleSearchData.bind(this);
}

  componentDidMount() {
    this.getDataAndDisplay(this.state.mode, this.state.from, this.state.to);
  }

  handleRightItemClick(mode: PageMode) {
    if (mode === PageMode.list) {
      this.setState({
        loading: true,
      } as IItemState);
      
      const searchCriteria = this.prepareSearchCriteria(this.state.searchText);
      this.getDataAndDisplay(mode, this.state.from, this.state.to, '', searchCriteria);
    } else {
      this.setState({
        mode,
      } as IItemState);
    }
  }

  getDataAndDisplay(mode, from, to, sort = '', searchCriteria = '') {
    let formData = new FormData();
    formData.append('Action', 'Fetch');
    formData.append('Path', 'Item/');
    formData.append('From', from);
    formData.append('To', to);
    formData.append('Sort', sort);
    formData.append('Search', searchCriteria);

    const { fetch, fetchService } = utils;

    Promise.all([fetch(formData), fetchService()]).then(
      (response: any) => {
        const { data, total } = response[0];
        const serviceTypes = response[1].data;
        this.setState({
          mode,
          data,
          loading: false,
          from, 
          to,
          total,
          serviceTypes,
        } as IItemState);
      }
    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );
  }

  onRangeUpdate(from, to) {
    const { mode, searchText } = this.state;
    const searchCriteria = this.prepareSearchCriteria(searchText);
    this.getDataAndDisplay(mode, from, to, '', searchCriteria);

    this.setState({
      loading: true,
    } as IItemState);
  }

  handleFormSave(data, isEdit, id = null) {
    
    let formData = new FormData();
    let message;
    let toastrTitle;    
    
    if (utils.IsEmptyNull(data.serviceType) || utils.IsEmptyNull(data.itemDescription) || 
        utils.IsEmptyNull(data.glSalesAccount) || utils.IsEmptyNull(data.glInventoryAccount) ||
        utils.IsEmptyNull(data.glSalaryAccount) || utils.IsEmptyNull(data.itemClass) || 
        utils.IsEmptyNull(data.costMethod) || utils.IsEmptyNull(data.pricebreakFrom1) ||
        utils.IsEmptyNull(data.pricebreakFrom2) ) {

      toastrTitle = 'Tariff';
      message = `"Sales Account" , "Inventory Account", "Sales Account", "Inventory Account",
       "Salary Account" , "Service Type" are required`;
      this.alertRef.displayAlert('error', message, toastrTitle);  

      return;
    }

    
    if (isEdit) {
      formData.append('Action', 'Update');
    } else {
      formData.append('Action', 'Insert');
    }
    formData.append('Path', 'Item/');
    
    formData.append('ItemCode', data.itemCode);
    formData.append('ItemDesc', data.itemDescription);
    formData.append('ItemClass', data.itemClass);
    formData.append('Inactive', data.inactive ? '1' : '0');
    formData.append('UnitofMeasure', data.unitOfMeasure);
    formData.append('ItemType', data.itemType);
    formData.append('TaxType', data.taxType);
    formData.append('CostMethod', data.costMethod);
    formData.append('ServiceType', data.serviceType);

    formData.append('GLSalesAc', data.glSalesAccount);
    formData.append('GLStockAc', data.glInventoryAccount);
    formData.append('GLCOGSAc', data.glSalaryAccount);
    
    formData.append('PriceBreakFrom1', data.pricebreakFrom1);
    formData.append('PriceBreakTo1', data.pricebreakTo1);
    formData.append('PriceBreakL1Price', data.pricebreakL1Price);

    formData.append('PriceBreakFrom2', data.pricebreakFrom2);
    formData.append('PriceBreakTo2', data.pricebreakTo2);
    formData.append('PriceBreakL2Price', data.pricebreakL2Price);

    formData.append('PriceBreakFrom3', data.pricebreakFrom3);
    formData.append('PriceBreakTo3', data.pricebreakTo3);
    formData.append('PriceBreakL3Price', data.pricebreakL3Price);
    
    utils.fetch(formData).then(
      (response: any) => {
        const { status } = response;

        if (status === 'ok') {
          if (!isEdit) {            
            message = `${data.itemDescription} Added`;
            toastrTitle = 'New Tariff';
          } else {
            message = `${data.itemDescription} Updated`;
            toastrTitle = 'Tariff';
          }

          this.alertRef.displayAlert('success', message, toastrTitle);  
        }else {
          this.alertRef.displayAlert('error', 
            `Error while Saving.\nError Message:\n${response.error}`, 'Error');  
        }
        this.getDataAndDisplay(PageMode.list, this.state.from, this.state.to);
      }
    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );

    this.setState({
     loading: true, 
    } as IItemState);
  }

  handleFormCancel() {
    this.setState({
      mode: PageMode.list,
    } as IItemState);
  }

  handleEditData(list) {
    this.setState({
      editData: list,
      mode: PageMode.edit,
    } as IItemState);
  }

  handleDeleteData(list) {
    if (list.length === 0) {
      return;
    }
    
    let ids = '';
    for (let i = 0; i < list.length; i++) {
      ids += ids.length > 0 ? ',' : '';
      ids += '\'' + list[i].ItemCode + '\'';
    }

    utils.canDelete(ids, 'QLD.CanDeleteItem').then(	
        (response: any) => {  

          let msg;
          let toastrTitle;                
          
          // validate if the record does not depend of others records
          if (!response.canDelete) {  
            
            msg = 'The record(s) cannot be deleted';
            toastrTitle = 'Delete Tariffs';
            this.alertRef.displayAlert('info', msg, toastrTitle);                     
            return;
          } else {  

            if (!confirm('Are you sure you want to delete selected Tariffs?')) {
              return;
            }
            
            let formData = new FormData();
            formData.append('Action', 'Delete');
            formData.append('Path', 'Item/');
            formData.append('ItemCode', ids);

            utils.fetch(formData).then(
              (res: any) => {
                const { status } = res;

                this.getDataAndDisplay(PageMode.list, this.state.from, this.state.to);
                
                if (status === 'ok') {
                  
                  msg = 'Selected Tariffs were deleted';
                  toastrTitle = 'Tariffs';
                  this.alertRef.displayAlert('success', msg, toastrTitle);
                }
              }
            ).catch(
              (ex) => {
                this.alertRef.displayAlert('error', ex, 'Error');
              }
            );    
          }
      }

    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );
  }

  prepareSearchCriteria(searchText) {
    let searchCriteria = `(ItemCode LIKE '%${searchText}%' `;
    searchCriteria += `OR ItemDesc LIKE '%${searchText}%' `;
    searchCriteria += `OR ItemClass LIKE '%${searchText}%') `;
    return searchCriteria;
  }

  handleSearchData(searchText) {
    const searchCriteria = this.prepareSearchCriteria(searchText); 
    const { from, to } = this.state;

    this.getDataAndDisplay(PageMode.list, from, to, '', searchCriteria);
    this.setState({
      loading: true,
      searchText,
    } as IItemState);
  }

  getContentFromMode(mode: PageMode) {
    let content;
    switch (mode) {
      case PageMode.list: 
        let { data } = this.state;
        const columns = [
          {dataField: 'ItemCode', header: 'Tariff Code', isKey: true},
          {dataField: 'ItemDesc', header: 'Description'},
          {dataField: 'PriceBreakL1Price', header: 'Price Break L1'},
          {dataField: 'PriceBreakL2Price', header: 'Price Break L2'},
          {dataField: 'PriceBreakL3Price', header: 'Price Break L3'},
          {dataField: 'ServiceType', header: 'Service Type', dataFormat: (cell, row) => {
            const service = this.state.serviceTypes.find(x => x.value === cell);
            const label = service && service.label ? service.label : '';
            return label;
          }},
        ];

        if (!data) {
          data = [];
        }

        const actions = (
          <TableHeaderColumn 
            dataField="action" 
            dataAlign="center"        
            headerAlign="center"            
            width="100px"
            dataFormat={(cell, row) => {
              
              return (
                <span>
                  <a className="btn btn-xs btn-warning" title="Edit" 
                    onClick={(e) => {e.preventDefault(); this.handleEditData([row]);}}>
                    <i className="glyphicon glyphicon-edit"/>
                  </a>               
                </span>
              );
          }}>
            Actions
          </TableHeaderColumn>
        );     

        content = (
          <div>
            <CustomFilter 
              onRefreshData={this.onRangeUpdate} 
              total={this.state.total}  
              from={this.state.from}  
              to={this.state.to} 
              withSearch={true} 
              onSearch={this.handleSearchData} 
              searchText={this.state.searchText} 
              withActions={true}             
              />
            <Listing 
              data={data}
              columns={columns} 
              onRefreshData={this.onRangeUpdate}
              onEditData={this.handleEditData}
              onDeleteData={this.handleDeleteData}
              withActions={true}
              actions={actions}
            />
          </div>
        );
        break;

      case PageMode.new: 
        content = <ItemForm onSave={this.handleFormSave} onCancel={this.handleFormCancel}/>;
        break;

      case PageMode.edit: 
        content = <ItemForm data={this.state.editData} 
          onSave={this.handleFormSave} onCancel={this.handleFormCancel}/>;
        break;

      default: 
        content = null;
    }

    return content;
  }

  render() {
    
    const brand = (
      <span>
        <i className="glyphicon glyphicon-list-alt"/> Tariffs Maintenance
      </span>
    );

    const content = this.getContentFromMode(this.state.mode);

    return (
      <div>
        <Alert ref={x => this.alertRef = x} />
        <Loadable isLoading={this.state.loading} text={'Fetching Data...'}>
          <SimpleHeader 
            brand={brand}
            listLabel="List"
            newLabel="New Tariff Code"
            onRightItemClick={this.handleRightItemClick}
            mode={this.state.mode}
          />
          {content}
        </Loadable>
      </div>
    );
  }
}

export default Item;