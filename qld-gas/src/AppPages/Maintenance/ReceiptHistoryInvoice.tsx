import * as React from 'react';
import { connect } from 'react-redux';
import { setPage } from '../../actions/app';
import { IPageSettings } from '../../state';
import SimpleHeader from '../components/SimpleHeader';

import { TableHeaderColumn } from 'react-bootstrap-table';

import Loadable from '../components/Loadable';
import CustomFilter from '../components/CustomFilter';
import Listing from '../components/Listing';
import { PageMode, pages } from '../constants';

import * as utils from '../../App/utils';
import Alert from '../components/Alert';

export interface IReceiptHistoryInvoiceProps {
  historyInvoiceData: any;
  onCancel: Function;
}

interface IConnectedProps {
  pageCode: string;
  pageSettings: IPageSettings;
}

interface IConnectedDispatch {
  onSetPageCode: (value: string, settings: IPageSettings) => void;  
}

export interface IReceiptHistoryInvoiceState {
  mode: PageMode;
  loading: boolean;
  pageLoading: boolean;
  data: Array<any>;
  total: number;
  editData: Array<any>;
  surname?: string;
  givennames?: string;
  dataComplex?: Array<any>;
  complexNo?: string;
}

class ReceiptHistoryInvoice extends React.Component<IReceiptHistoryInvoiceProps & IConnectedProps & IConnectedDispatch, 
IReceiptHistoryInvoiceState> {

  alertRef: any;

  constructor(props) {
    super(props);

    this.state = {
      total: 0,
      data: [],
      mode: PageMode.list,
      loading: true,
      editData: [],
      pageLoading: true,
      dataComplex: [],
    } as IReceiptHistoryInvoiceState;

    this.getContentFromMode = this.getContentFromMode.bind(this);
    this.getDataAndDisplay = this.getDataAndDisplay.bind(this);
    this.onEditDataUpdate = this.onEditDataUpdate.bind(this);    
    this.handleFormCancel = this.handleFormCancel.bind(this);    
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleRightItemClick = this.handleRightItemClick.bind(this);
  }

  componentDidMount() {
    this.getDataAndDisplay(PageMode.list);
  }

  handleRightItemClick(mode: PageMode) {
    if (mode === PageMode.list) {
      this.setState({
        loading: true,
      } as IReceiptHistoryInvoiceState);

      this.getDataAndDisplay(mode);
    } else {
      this.setState({
        mode,
      } as IReceiptHistoryInvoiceState);
    }
  }

  getDataAndDisplay(mode, from = '', to = '', sort = '') {
    
    const { PaymentID } = this.props.historyInvoiceData[0];

    let formData = new FormData();
    formData.append('Action', 'Fetch');
    formData.append('Path', 'ReceiptHistoryInvoices/');
    formData.append('PaymentId', PaymentID);
    
    utils.fetch(formData).then(
      (response: any) => {        
        const { data, total } = response;
        this.setState({
          mode,
          data,
          loading: false,
          pageLoading: false,
          total,
          editData: [],
        } as IReceiptHistoryInvoiceState);
      }
    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );
  }

  onEditDataUpdate(list) {
    this.setState({
      editData: list,
      mode: PageMode.edit,
    } as IReceiptHistoryInvoiceState);
  }

  getContentFromMode(mode: PageMode) {
    let content;
    switch (mode) {
      case PageMode.list:
        let { data } = this.state;
        const columns = [
          {dataField: 'PaymentID', header: 'PaymentID', isKey: true, hidden: true},
          {dataField: 'PaymentAmount', header: 'Payment Amount',  hidden: true, 
            dataFormat: (cell, row) => {
              return Math.round(cell * 100) / 100;
            }
          },
          {dataField: 'PartialAmount', header: 'PartialAmount',  hidden: true, 
            dataFormat: (cell, row) => {
              return Math.round(cell * 100) / 100;
            }
          },          
          {dataField: 'Custno', header: 'Custno', hidden: true},
          {dataField: 'BillingNames', header: 'BillingNames', hidden: true},

          {dataField: 'InvoiceNo', header: 'InvoiceNo'},
          {dataField: 'AmountDebt', header: 'Amount',
            dataFormat: (cell, row) => {
              return Math.round(cell * 100) / 100;
            }          
          },
          {dataField: 'IsPaid', header: 'IsPaid',
            dataFormat: (cell, row) => {
              
              let iconClass = 'none';
              if (cell.length > 0 && cell === 'True' ) {
                  iconClass = 'fa fa-check';
              }
              return (              
                <div className="text-center" >
                  <i className={iconClass}>&nbsp;</i> 
                </div>
              );
            }            
          },
        ];

        if (!data) {
          data = [];
        }

        content = (
          <div>
            
            <Listing 
              data={data}
              columns={columns} 
              onRefreshData={null}
              onEditData={this.onEditDataUpdate}
              onDeleteData={null}
              withActions={false}
              actions={null}
              />

            <div style={{textAlign: 'right', background: '#f2f2f2', padding: '10px'}}>
              <a className="btn btn-danger"  onClick={(e) => {this.props.onCancel();}}>Cancel</a>
            </div>

          </div>
        );
        break;

      default: 
        content = null;
    }

    return content;
  }

  handleInputChange(prop, value) {
    let state = {};
    state[prop] = value;

    this.setState(state as IReceiptHistoryInvoiceState);
  }

  handleFormCancel() {
    this.setState({
      mode: PageMode.list,
    } as IReceiptHistoryInvoiceState);
  }

  render() {
    const content = this.getContentFromMode(this.state.mode);
    const { BillingNames, Custno, CardPayment } = this.props.historyInvoiceData[0];
    const brand = (
      <div>
        <span>
          <i className="fa fa-building-o"/> {`Receipt History Details`}
        </span>
      </div>
    );

    return (
      <div style={{margin: '5px', border: '1px solid black'}}>
        <SimpleHeader 
          brand={brand}
          listLabel={`List properties for customer `}
          newLabel="Add Unit"
          onRightItemClick={this.handleRightItemClick}
          mode={null}
          withOptions={false}
        />
        <Alert ref={x => this.alertRef = x} />
        <Loadable isLoading={this.state.loading} text={'Fetching units...'}>

          <p className="alert alert-info">
            <b>Customer:</b> {BillingNames} <br/>
            <b>Custno:</b> {Custno} <br/>            
          </p>

          {content}
        </Loadable>
      </div>
    );
  }
}

function mapStateToProps(state: any): IConnectedProps {
  return {
    pageCode: state.app.pageCode,
    pageSettings: state.app.editData,
  };
}

function mapDispatchToProps(dispatch): IConnectedDispatch {
  return {
    onSetPageCode: (value: string, settings) => dispatch(setPage(value, settings))
  };
}

export default 
  connect(mapStateToProps, mapDispatchToProps)
    (ReceiptHistoryInvoice) as React.ComponentClass<IReceiptHistoryInvoiceProps>;
