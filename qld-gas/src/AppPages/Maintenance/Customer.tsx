import * as React from 'react';
import { connect } from 'react-redux';
import { setPage } from '../../actions/app';
import { IPageSettings } from '../../state';

import { TableHeaderColumn } from 'react-bootstrap-table';

import Loadable from '../components/Loadable';
import Alert from '../components/Alert';
import Listing from '../components/Listing';
import CustomFilter from '../components/CustomFilter';
import SimpleHeader from '../components/SimpleHeader';
import CustomerForm from './forms/CustomerForm';
import { PageMode, pages } from '../constants';
import CustomerUnit from './CustomerUnit';
import AutomaticCreditCardForm from './forms/AutomaticCreditCardForm';
import ManualCreditCardForm from './forms/ManualCreditCardForm';

import * as utils from '../../App/utils';
import * as fileUtils from '../../App/fileUtils';

export interface ICustomerState {
  mode: PageMode;
  loading: boolean;
  from: number;
  to: number;
  data: Array<any>;
  total: number;
  editData: Array<any>;
  deleteData: Array<any>;
  searchText: string;
  viewSetting: string;
}

export interface ICustomerProps {

}

interface IConnectedProps {
  pageCode: string;
  editData: any;
}

interface IConnectedDispatch {
  onSetPageCode: (value: string, settings: IPageSettings) => void;
}

class Customer extends React.Component<ICustomerProps & IConnectedProps & IConnectedDispatch, ICustomerState> {
  alertRef: any; 

  constructor(props) {
    super(props);

    this.state = {
      mode: PageMode.list,
      loading: true,
      from: 0,
      to: 100,
      searchText: '',
      viewSetting: 'current'
    } as ICustomerState;

    this.handleRightItemClick = this.handleRightItemClick.bind(this);
    this.getContentFromMode = this.getContentFromMode.bind(this);
    this.getDataAndDisplay = this.getDataAndDisplay.bind(this);
    this.onRangeUpdate = this.onRangeUpdate.bind(this);
    this.handleEditData = this.handleEditData.bind(this);
    this.handleDeleteData = this.handleDeleteData.bind(this);
    this.handleFormCancel = this.handleFormCancel.bind(this);
    this.handleFormSave = this.handleFormSave.bind(this);
    this.handleSearchData = this.handleSearchData.bind(this);
    this.handleUnitView = this.handleUnitView.bind(this);
    this.handleExtraSettings = this.handleExtraSettings.bind(this);
    this.handleCreditCard = this.handleCreditCard.bind(this);    
    this.exportCreditCard = this.exportCreditCard.bind(this);    
    
  }

  componentDidMount() {
    if (this.props.editData) {
      this.handleSearchData(this.props.editData.CustNo);
    } else {
      this.getDataAndDisplay(this.state.mode, this.state.from, this.state.to
      , '', this.state.searchText, this.state.viewSetting);
    }
  }

  exportCreditCard() {

    const { processCreditCardPayment } = utils;

    processCreditCardPayment().then(
      (response: any) => {
        
        const { status } = response;
        if (status === 'ok') {
          fileUtils.exportCreditCard();
          this.alertRef.displayAlert('success', 'File has been exported', 'Export');
        }
      }
    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );
  }  

  handleRightItemClick(mode: PageMode) {
    if (mode === PageMode.list) {
      this.setState({
        loading: true,
      } as ICustomerState);

      this.getDataAndDisplay(mode, this.state.from, this.state.to, '', 
      this.getSearchCriteria(this.state.searchText), this.state.viewSetting);
    } else {
      this.setState({
        mode,
      } as ICustomerState);
    }
  }

  getDataAndDisplay(mode, from, to, sort = '', searchCriteria = '', view) {
    let formData = new FormData();
    formData.append('Action', 'Fetch');
    formData.append('Path', 'Customer/');
    formData.append('From', from);
    formData.append('To', to);
    formData.append('Sort', sort);
    formData.append('Search', searchCriteria);
    formData.append('View', view);

    utils.fetch(formData).then(
      (response: any) => {
        const { data, total } = response;
        
        this.setState({
          mode,
          data,
          loading: false,
          from, 
          to,
          total
        } as ICustomerState);
      }
    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );
  }

  onRangeUpdate(from, to) {
    const { mode } = this.state;
    this.getDataAndDisplay(mode, from, to, '', this.getSearchCriteria(this.state.searchText), this.state.viewSetting);

    this.setState({
      loading: true,
    } as ICustomerState);
  }

  handleEditData(list) {
    this.setState({
      editData: list,
      mode: PageMode.edit,
    } as ICustomerState);
  }

  handleDeleteData(list) {


    if (list.length === 0) {
      return;
    }
    
    let ids = '';
    for (let i = 0; i < list.length; i++) {
      ids += ids.length > 0 ? ',' : '';
      ids += list[i].CustomerNo;
    }

    utils.canDelete(ids, 'QLD.CanDeleteCustomer').then(
      (response: any) => {        
        
        // validate if the record does not depend of others records
        if (!response.canDelete) {

          this.alertRef.displayAlert('info',
            'The record(s) cannot be deleted',
            'Delete');

          return;
        }else{          
          
          if (!confirm('Are you sure you want to delete selected Customers?')) {
            return;
          }
          
          let formData = new FormData();
          formData.append('Action', 'Delete');
          formData.append('Path', 'Customer/');
          formData.append('CustNo', ids);

          utils.fetch(formData).then(
            (res: any) => {
              const { status } = res;

              this.getDataAndDisplay(PageMode.list, this.state.from, this.state.to
              , '', this.state.searchText, this.state.viewSetting);
              
              let message;
              let toastrTitle;
              if (status === 'ok') {

                toastrTitle = 'Records deleted';
                message = `Selected customers deleted`;
                this.alertRef.displayAlert('info', message, toastrTitle);
              }
            }
          ).catch(
            (ex) => {              
              this.alertRef.displayAlert('error', ex, 'Error');
            }
          );

        }        
      }
    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );
    
  }

  getSearchCriteria(searchText) {    
    let searchCriteria = `(Custno LIKE '%${searchText}%' `;
    searchCriteria += `OR Title LIKE '%${searchText}%' `;
    searchCriteria += `OR Surname LIKE '%${searchText}%' `;
    searchCriteria += `OR ComplexName LIKE '%${searchText}%' `;
    searchCriteria += `OR [Billing Names] LIKE '%${searchText}%') `;

    return searchCriteria;
  }

  handleSearchData(searchText, view = '') {
    let searchCriteria = this.getSearchCriteria(searchText);

    const { from, to } = this.state;

    this.getDataAndDisplay(PageMode.list, from, to, '', searchCriteria, 
      view.length > 0 ? view : this.state.viewSetting);

    this.setState({
      loading: true,
      searchText,
    } as ICustomerState);
  }

  handleUnitView(row) {
    this.setState({
      mode: PageMode.customers_unit,
      editData: row,
    } as ICustomerState);
  }

  handleCreditCard(row) {
    
    const {CardPayment} = row[0];
    let kindCardPayment: any;

    if (CardPayment === 'automatic') {
      kindCardPayment = PageMode.automatic_credit_card;
    }else if (CardPayment === 'manual') {
      kindCardPayment = PageMode.manual_credit_card;
    }      

    this.setState({
      mode: kindCardPayment,
      editData: row,
    } as ICustomerState);

  }  

  handleExtraSettings(value) {
    this.setState({
      viewSetting: value,
      loading: true,
    } as ICustomerState);

    this.handleSearchData(this.state.searchText, value);
  }

  getContentFromMode(mode: PageMode) {
    let content;
    switch (mode) {
      case PageMode.list: 
        let { data } = this.state;
        const columns = [
          {dataField: 'CustomerNo', header: 'Customer No', isKey: true},
          {dataField: 'FullName', header: 'Customer Name'},
          {dataField: 'ComplexUnitNo', header: 'Unit No', dataFormat: (cell, row) => {
            return (
              <a style={{cursor:'pointer'}}
                onClick={(e) => this.props.onSetPageCode(pages.unitcodesMaintenance, {editData: row.UnitCode})}>
                {cell}
              </a>
            );
          }},
          {dataField: 'ComplexName', header: 'Complex Name.', dataFormat: (cell, row) => {
            return (
              <a style={{cursor:'pointer'}}
                onClick={(e) => this.props.onSetPageCode(pages.complexMaintenance, {editData: row})}>
                {cell}
              </a>
            );
          }},
          {dataField: 'Cityname', header: 'City'},
          {dataField: 'MobileNo', header: 'Mobile Phone'},

          {dataField: 'BPayNo', header: 'BPay No', hidden: true},
          {dataField: 'HomePhone', header: 'Home Phone', hidden: true},
          {dataField: 'WorkPhone', header: 'Work Phone', hidden: true}
        ];

        if (!data) {
          data = [];
        }

        const actions = (
          <TableHeaderColumn 
            dataField="action" 
            dataAlign="center"        
            headerAlign="center"            
            width="100px"                            
            dataFormat={(cell, row) => {

              /*let hiddenClass = 'hidden';
              if(row.OSBalance.length > 0 && row.CardNumber.length > 0) {
                hiddenClass = '';
              } */

              return (
                <span>
                  <a className="btn btn-xs btn-warning" title="Edit" 
                    onClick={(e) => {e.preventDefault(); this.handleEditData([row]);}}>
                    <i className="glyphicon glyphicon-edit"/>
                  </a>
                  <a className="btn btn-xs btn-success" title="Manage Units" 
                    onClick={(e) => {e.preventDefault(); this.handleUnitView([row]);}}>
                    <i className="fa fa-building-o"/>
                  </a>
                  <a className="btn btn-xs btn-primary" title="Credit Card" 
                    onClick={(e) => {e.preventDefault(); this.handleCreditCard([row]);}}>
                    <i className="fa fa-usd"/>
                  </a>
                  {/*<a className={'btn btn-xs ' + hiddenClass} title="Direct Debit" style={{cursor:'default'}}>
                    <i className="fa fa-credit-card-alt " style={{color: '#5cb85c'}}/>
                  </a>*/}

                </span>
              );
          }}>
            Actions
          </TableHeaderColumn>
        );

        content = (
          <div>
            <nav className="navbar navbar-default" style={{marginTop:'-20px'}}>
              <div className="container-fluid">
                <div className="navbar-form navbar-right">
                  <select className="form-control" style={{marginTop: '8px'}} value={this.state.viewSetting}
                    onChange={(e) => this.handleExtraSettings((e.target as HTMLSelectElement).value)}>
                    <option value="current">View Current Customers</option>
                    <option value="all">Include Past Customers</option>
                  </select>
                </div>
              </div>
            </nav>
            
            <CustomFilter 
              onRefreshData={this.onRangeUpdate} 
              total={this.state.total}  
              from={this.state.from}  
              to={this.state.to} 
              withSearch={true} 
              onSearch={this.handleSearchData} 
              searchText={this.state.searchText} 
              withActions={true}             
            />

            <div className="col-md-12">
              <div className="col-md-offset-8 col-md-4" style={{textAlign: 'right'}}>

                <a className="btn btn-info" title="Export Credit Card"
                  onClick={(e) => {
                    e.preventDefault();
                    this.exportCreditCard();  
                  }}>
                  <i className="fa fa-arrow-down"/> Export Credit Card
                </a>                
              </div>
            </div>            
              
            <Listing 
              data={data} 
              columns={columns} 
              onRefreshData={this.onRangeUpdate} 
              onEditData={this.handleEditData} 
              onDeleteData={this.handleDeleteData} 
              withActions={true} 
              actions={actions} 
              />

          </div>
        );
        break;

      case PageMode.new: 
        content = <CustomerForm onSave={this.handleFormSave} onCancel={this.handleFormCancel}/>;
        break;

      case PageMode.edit:      
        content = <CustomerForm data={this.state.editData} 
        onSave={this.handleFormSave} onCancel={this.handleFormCancel} goToCardDetails={this.handleCreditCard} />;
        break;

      case PageMode.customers_unit:        
        content = <CustomerUnit custno={this.state.editData[0].CustomerNo} fullName={this.state.editData[0].FullName}/>;
        break;

      case PageMode.automatic_credit_card:        
        content = <AutomaticCreditCardForm 
                    custno={this.state.editData[0].CustomerNo}                     
                    onCancel={this.handleFormCancel}
                    data={this.state.editData}
                  />;
        break;

      case PageMode.manual_credit_card:        
        content = <ManualCreditCardForm 
                    custno={this.state.editData[0].CustomerNo}                     
                    onCancel={this.handleFormCancel}
                    data={this.state.editData}
                  />;
        break;                  

      default: 
        content = null;
    }

    return content;
  }

  handleFormSave(data, isEdit, id = null) {
    let formData = new FormData();
    let message;
    let toastrTitle;    
    
    if (isEdit) {
      formData.append('Action', 'Update');      
    } else {
      formData.append('Action', 'Insert');
    }
    formData.append('Path', 'Customer/');
    
    const { customerNumber, bPay, inactive, accessSMS } = data.headerCustomer;
    const { billingName, comments, givenName, homePhone,
      mobileNumber, postalAddress, postalAddress2, postalAddress3,
    surname, title, workPhone, cityCode, email, MYOBJobCode} = data.customerInfo;

    const { lastStatementDate, outstandingBalance, 
      postBarcodeCheckSum, salesAccount, cardPayment } = data.salesComponent;

    if (utils.IsEmptyNull(title) || utils.IsEmptyNull(givenName) || 
    utils.IsEmptyNull(surname) || !utils.validateEmail(email)) {
      
      toastrTitle = 'Field Required';
      message = `The fields title, givenName, surname are required or Email is not valid`;
      this.alertRef.displayAlert('error', message, toastrTitle);
      return;
    }

    formData.append('Custno', customerNumber);
    formData.append('BPayno', bPay);
    formData.append('Title', title);
    formData.append('Surname', surname);
    formData.append('GivenNames', givenName);
    formData.append('POAddress1', postalAddress);
    formData.append('POAddress2', postalAddress2);
    formData.append('POAddress3', postalAddress3);
    formData.append('POCityCode', cityCode);
    formData.append('MYOBJobCode', MYOBJobCode);
    formData.append('Inactive', inactive === 'on' ? '1' : '0');
    formData.append('HomePhone', homePhone);
    formData.append('WorkPhone', workPhone);
    formData.append('MobileNo', mobileNumber);
    formData.append('Comments', comments);
    formData.append('GLSalesAc', salesAccount);
    formData.append('APostBCodeChkSum', postBarcodeCheckSum);
    formData.append('LastStmtDate', lastStatementDate);
    formData.append('OSBalance', outstandingBalance);
    formData.append('BillingNames', billingName);
    formData.append('Email', email);    
    formData.append('AccessSMS', accessSMS);
    formData.append('CardPayment', cardPayment);

    utils.fetch(formData).then(
      (response: any) => {
        const { status } = response;

        if (status === 'ok') {
          if (!isEdit) {
            toastrTitle = 'Record added';
            message = `New Customer: ${title} ${givenName} ${surname} Added`;
          } else {
            toastrTitle = 'Record updated';
            message = `Customer: ${title} ${givenName} ${surname} Updated`;
          }

          this.alertRef.displayAlert('success', message, toastrTitle);

        }else {
          this.alertRef.displayAlert('error', 
            `Error while Saving.\nError Message:\n${response.error}`, 'Error');             
        }
        
        this.getDataAndDisplay(PageMode.list, this.state.from, this.state.to
          , '', this.getSearchCriteria(this.state.searchText), this.state.viewSetting);
      }
    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );

    this.setState({
     loading: true, 
    } as ICustomerState);
  }

  handleFormCancel() {
    this.setState({
      mode: PageMode.list,
    } as ICustomerState);
  }

  render() {
    
    const brand = (
      <span>
        <i className="glyphicon glyphicon-list-alt"/> Customer Maintenance
      </span>
    );

    const content = this.getContentFromMode(this.state.mode);    
    return (
      <div>
        
        <Alert ref={x => this.alertRef = x} />

        <Loadable isLoading={this.state.loading} text={'Fetching Data...'}>
          <SimpleHeader 
            brand={brand}
            listLabel="List"
            newLabel="New Customer"
            onRightItemClick={this.handleRightItemClick}
            mode={this.state.mode}
          />
          {content}
        </Loadable>

      </div>
    );
  }
}

function mapStateToProps(state: any): IConnectedProps {
  return {
    pageCode: state.app.pageCode,
    editData: state.app.editData,
  };
}

function mapDispatchToProps(dispatch): IConnectedDispatch {
  return {
    onSetPageCode: (value: string, settings) => dispatch(setPage(value, settings))
  };
}

export default 
  connect(mapStateToProps, mapDispatchToProps)(Customer) as React.ComponentClass<ICustomerProps>;
