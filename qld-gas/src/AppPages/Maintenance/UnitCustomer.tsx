import * as React from 'react';
import { connect } from 'react-redux';
import { setPage } from '../../actions/app';
import { IPageSettings } from '../../state';

import SimpleHeader from '../components/SimpleHeader';

import Loadable from '../components/Loadable';
import CustomFilter from '../components/CustomFilter';
import Listing from '../components/Listing';
import UnitCustomerForm from './forms/UnitCustomerForm';
import { PageMode, pages } from '../constants';

import * as utils from '../../App/utils';
import Alert from '../components/Alert';
import { TableHeaderColumn } from 'react-bootstrap-table';

export interface IUnitCustomerProps {
  unitCode: string;
  complexName: string;
  unitNo: string;
}

interface IConnectedProps {
  pageCode: string;
  pageSettings: IPageSettings;  
}

interface IConnectedDispatch {
  onSetPageCode: (value: string, settings: IPageSettings) => void;
}

export interface IUnitCustomerState {
  mode: PageMode;
  loading: boolean;
  pageLoading: boolean;
  data: Array<any>;
  total: number;
  editData: Array<any>;
  surname?: string;
  givennames?: string;
  dataComplex?: Array<any>;
  complexNo?: string;
}

class UnitCustomer 
  extends React.Component<IUnitCustomerProps & IConnectedProps & IConnectedDispatch, IUnitCustomerState> {
  alertRef: any;

  constructor(props) {
    super(props);

    this.state = {
      total: 0,
      data: [],
      mode: PageMode.list,
      loading: true,
      editData: [],
      pageLoading: true,
      dataComplex: [],
    } as IUnitCustomerState;

    this.getContentFromMode = this.getContentFromMode.bind(this);
    this.getDataAndDisplay = this.getDataAndDisplay.bind(this);
    this.onEditDataUpdate = this.onEditDataUpdate.bind(this);
    this.handleFormSave = this.handleFormSave.bind(this);
    this.handleFormCancel = this.handleFormCancel.bind(this);
    this.onDeleteData = this.onDeleteData.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleRightItemClick = this.handleRightItemClick.bind(this);
  }

  componentDidMount() {
    this.getDataAndDisplay(PageMode.list);
  }

  handleRightItemClick(mode: PageMode) {
    if (mode === PageMode.list) {
      this.setState({
        loading: true,
      } as IUnitCustomerState);

      this.getDataAndDisplay(mode);
    } else {
      this.setState({
        mode,
      } as IUnitCustomerState);
    }
  }

  getDataAndDisplay(mode, from = '', to = '', sort = '') {
    let formData = new FormData();
    formData.append('Action', 'Fetch');
    formData.append('Path', 'UnitCustomer/');
    formData.append('UnitCode', this.props.unitCode);
    
    utils.fetch(formData).then(
      (response: any) => {
        const { data, total } = response;
        this.setState({
          mode,
          data,
          loading: false,
          pageLoading: false,
          total,
          editData: [],
        } as IUnitCustomerState);
      }
    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );
  }

  onEditDataUpdate(list) {
    this.setState({
      editData: list,
      mode: PageMode.edit,
    } as IUnitCustomerState);
  }

  onDeleteData(list) {
    if (list.length === 0) {
      return;
    }
    
    let ids = '';
    for (let i = 0; i < list.length; i++) {
      ids += ids.length > 0 ? ',' : '';
      ids += list[i].RentalCode;
    }


    utils.canDelete(ids, 'QLD.CanDeleteRental').then(	
      (response: any) => {        
        
        let msg;
        let toastrTitle;

        // validate if the record does not depend of others records
        if (!response.canDelete) {  
          
            msg = 'The record(s) cannot be deleted';
            toastrTitle = 'Delete Rental';
            this.alertRef.displayAlert('info', msg, toastrTitle);  
            return;
        }else {

          if (!confirm('Are you sure you want to delete selected rentals?')) {
            return;
          }
          
          let formData = new FormData();
          formData.append('Action', 'Delete');
          formData.append('Path', 'Rental/');
          formData.append('RentalCode', ids);

          utils.fetch(formData).then(
            (response: any) => {
              const { status } = response;

              this.getDataAndDisplay(PageMode.list);
              
              if (status === 'ok') {
                
                msg = 'Selected rentals were deleted';
                toastrTitle = 'Delete Rental';
                this.alertRef.displayAlert('success', msg, toastrTitle);
              }
            }
          ).catch(
            (ex) => {
              this.alertRef.displayAlert('error', ex, 'Error');
            }
          );	
        }
      }

    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );
  }

  getContentFromMode(mode: PageMode) {
    let content;
    switch (mode) {
      case PageMode.list:
        let { data } = this.state;        
        const columns = [
          {dataField: 'RentalCode', header: 'Rental Code', isKey: true, hidden: true},
          {dataField: 'CustNo', header: 'Customer No.', dataFormat: (cell, row) => {
            return (
              <a style={{cursor:'pointer'}}
                onClick={(e) => this.props.onSetPageCode(pages.customerMaintenance, {editData: row})}>
                {cell}
              </a>
            );
          }},
          {dataField: 'FullName', header: 'Customer Name', dataFormat: (cell, row) => {
            return (
              <a style={{cursor:'pointer'}}
                onClick={(e) => this.props.onSetPageCode(pages.customerMaintenance, {editData: row})}>
                {cell}
              </a>
            );
          }},
          {dataField: 'StartDateF', header: 'Start Date'},
          {dataField: 'EndDateF', header: 'End Date'},
          {dataField: 'SecDeposit', header: 'Security Deposit', hidden: false},
        ];

        if (!data) {
          data = [];
        }

        const actions = (
          <TableHeaderColumn 
            dataField="action" 
            dataAlign="center"        
            headerAlign="center"            
            width="100px"
            dataFormat={(cell, row) => {
              
              return (
                <span>
                  <a className="btn btn-xs btn-warning" title="Edit" 
                    onClick={(e) => {e.preventDefault(); this.onEditDataUpdate([row]);}}>
                    <i className="glyphicon glyphicon-edit"/>
                  </a>               
                </span>
              );
          }}>
            Actions
          </TableHeaderColumn>
        );         

        content = (
          <div>
            <CustomFilter 
              onRefreshData={null} 
              total={this.state.total}  
              from={this.state.total}  
              to={this.state.total} 
              withSearch={false} 
              withActions={true}             
              />
            <Listing 
              data={data}
              columns={columns} 
              onRefreshData={null}
              onEditData={this.onEditDataUpdate}
              onDeleteData={this.onDeleteData}
              actions={actions}
              withActions={true}/>
          </div>
        );
        break;

      case PageMode.new: 
        content = (
          <UnitCustomerForm unitCode={this.props.unitCode}
           onCancel={this.handleFormCancel} onSave={this.handleFormSave}/>
        );
        break;

      case PageMode.edit:
        content = (
          <UnitCustomerForm data={this.state.editData} unitCode={this.props.unitCode} 
            onSave={this.handleFormSave} onCancel={this.handleFormCancel}/>
        );
        break;

      default: 
        content = null;
    }

    return content;
  }

  handleFormSave(data, isEdit, id = null) {
    let formData = new FormData();
    
    if (isEdit) {
      formData.append('Action', 'Update');
    } else {
      formData.append('Action', 'Insert');
    }
    formData.append('Path', 'Rental/');
    formData.append('RentalCode', data.rentalCode);
    formData.append('CustNo', data.customer);
    formData.append('UnitCode', this.props.unitCode);
    formData.append('StartDate', data.startDate);
    formData.append('EndDate', data.endDate);
    formData.append('Terms', data.terms);
    formData.append('SecDeposit', data.securityDeposit);

    utils.fetch(formData).then(
      (response: any) => {
        const { status } = response;

        let message;
        let toastrTitle;
        if (status === 'ok') {
          if (!isEdit) {
            toastrTitle = 'Record updated';
            message = `New Rental: ${id} for ${this.props.unitCode} Added`;
          } else {
            toastrTitle = 'Record added';
            message = `Rental: ${id} for ${this.props.unitCode} Updated`;
          }

          this.alertRef.displayAlert('success', message, toastrTitle); 
        }else {
          this.alertRef.displayAlert('error', 
            `Error while Saving.\nError Message:\n${response.error}`, 'Error');                    
        }

        this.getDataAndDisplay(PageMode.list);
      }
    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );

    this.setState({
     loading: true, 
    } as IUnitCustomerState);
  }

  handleInputChange(prop, value) {
    let state = {};
    state[prop] = value;

    this.setState(state as IUnitCustomerState);
  }

  handleFormCancel() {
    this.setState({
      mode: PageMode.list,
    } as IUnitCustomerState);
  }

  render() {
    const content = this.getContentFromMode(this.state.mode);   

    const brand = (
      <span>
        <i className="fa fa-building-o"/> 
        {`Customers list for "${this.props.complexName} ${this.props.unitNo}" `}
      </span>
    );

    return (
      <div style={{margin: '5px', border: '1px solid black'}}>
        <SimpleHeader 
            brand={brand}
            listLabel={`List Customers of Unit`}
            newLabel="Add Customers"
            onRightItemClick={this.handleRightItemClick}
            mode={this.state.mode}
          />
        <Alert ref={x => this.alertRef = x} />
        <Loadable isLoading={this.state.loading} text={'Fetching units...'}>
          {content}
        </Loadable>
      </div>
    );
  }
}


function mapStateToProps(state: any): IConnectedProps {
  return {
    pageCode: state.app.pageCode,
    pageSettings: state.app.editData,
  };
}

function mapDispatchToProps(dispatch): IConnectedDispatch {
  return {
    onSetPageCode: (value: string, settings) => dispatch(setPage(value, settings))
  };
}

export default 
  connect(mapStateToProps, mapDispatchToProps)(UnitCustomer) as React.ComponentClass<IUnitCustomerProps>;
