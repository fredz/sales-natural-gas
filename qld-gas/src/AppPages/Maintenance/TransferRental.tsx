import * as React from 'react';
import { connect } from 'react-redux';
import { setPage } from '../../actions/app';
import { IPageSettings } from '../../state';
import Loadable from '../components/Loadable';
import Alert from '../components/Alert';

import { Panel } from '../../ascendis-ui/containers';
import { Row, Column } from '../../ascendis-ui/layout';
import { InputGroup, Dropdown } from '../../ascendis-ui/components';
import { pages } from '../constants';

import * as utils from '../../App/utils';

export interface ITransferRentalState {  
  rentalCode: string;  
  customNo: string;  
  startDate: string;
  endDate: string;
  terms: string;  
  loading: boolean;  
  dataUnit: any;

  unitCodeOrigin: string;
  unitNumberOrigin: string;
  complexNameOrigin: string;

  unitCodeDestiny: string;
  unitNumberDestiny: string;
  complexNameDestiny: string;  

  fullName: string;
  errors?: any;
}

export interface ITransferRentalProps {

}

interface IConnectedProps {
  pageCode: string;
  editData: any;
}

interface IConnectedDispatch {
  onSetPageCode: (value: string, settings: IPageSettings) => void;
}

class TransferRental extends React.Component<ITransferRentalProps & IConnectedProps & IConnectedDispatch, 
ITransferRentalState> {
  
  alertRef: any; 

  constructor(props) {
    super(props);

    this.state = {      
      rentalCode: '',
      inactive: false,
      customNo: '',
      startDate: '',
      endDate: '',
      terms: '',
      loading: true,
      dataUnit: [],

      unitCodeOrigin: '',
      unitNumberOrigin: '',
      complexNameOrigin: '',

      unitCodeDestiny: '',
      unitNumberDestiny: '',
      complexNameDestiny: '',
      fullName: '',
      errors: [],

    } as ITransferRentalState;
    
    this.handleFormSave = this.handleFormSave.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);    
    this.getDataAndDisplay = this.getDataAndDisplay.bind(this);    
    this.handleInputUnitChange = this.handleInputUnitChange.bind(this);    
    this.fillDatas = this.fillDatas.bind(this);
  }

  componentDidMount() {    
    this.fillDatas();
    this.getDataAndDisplay();        
  }  

  fillDatas() {
    const {UnitCode, ComplexName, ComplexUnitNo, FullName, Custno, RentalCode} = this.props.editData;

    this.setState({
      unitCodeOrigin: UnitCode,
      complexNameOrigin: ComplexName,
      unitNumberOrigin: ComplexUnitNo,
      fullName: FullName,
      customNo: Custno,
      rentalCode: RentalCode,
      loading: false,
    } as ITransferRentalState);

  }

  getDataAndDisplay() {    

    utils.fetchUnitForRental().then( 
      (response: any) => {        
        const { data } = response;        
        this.setState({
          dataUnit: data,          
          loading: false,
        } as ITransferRentalState);
      }
    ).catch(
      (ex) => {
        alert(ex);
      }
    );
  }
  
  handleInputChange(prop, value) {
    let state = {};
    state[prop] = value;

    this.setState(state as ITransferRentalState);
  }

  handleInputUnitChange(prop, value) {    
    let state = {};

    state['unitNumberDestiny'] = value.NoUnits;
    state['complexNameDestiny'] = value.ComplexName;
    state[prop] = value.value;

    this.setState(state as ITransferRentalState);
  } 

  validateForm() {
    const errors = [];

    if (utils.IsEmptyNull(this.state.unitCodeDestiny)) {
      errors.push({unitCodeDestiny : 'Field required'});
    } 

    if (utils.IsEmptyNull(this.state.startDate)) {
      errors.push({startDate : 'Field required'});
    }    

    if (utils.IsEmptyNull(this.state.terms)) {
      errors.push({terms : 'Field required'});
    }            

    return errors; 
  }   

  handleFormSave() {
    
    let message;
    let toastrTitle;

    const errors = this.validateForm.bind(this)();
    if (errors.length > 0) {
      this.setState({
        errors
      });
      return;
    }    

    if (utils.IsEmptyNull(this.state.unitCodeDestiny) || utils.IsEmptyNull(this.state.startDate)
        || utils.IsEmptyNull(this.state.terms)) {
      
      toastrTitle = 'Field Required';
      message = `The fields Unit Destiny , Start Date and Terms are required`;
      this.alertRef.displayAlert('error', message, toastrTitle);
      return;
    }  

    this.setState({
     loading: true, 
    } as ITransferRentalState);
    
    utils.transferRental(
      this.state.rentalCode, 
      this.state.customNo, 
      this.state.unitCodeOrigin, 
      this.state.unitCodeDestiny, 
      this.state.startDate, 
      this.state.terms).then(
      (response: any) => {

        const { status } = response;        
        let msg;
        let toastrTitle: any = '';

        if (status === 'ok') {
          
          msg = `The Transfer Rental has been successful`;
          toastrTitle = 'Transfer Rental';
          this.alertRef.displayAlert('success', msg, toastrTitle);

          this.props.onSetPageCode(pages.customerMaintenance, null);
           
        } else {          
          this.alertRef.displayAlert('error', 
            `Error while Saving.\nError Message:\n${response.error}`, 'Error');          
        }

        this.setState({
        loading: false, 
        } as ITransferRentalState);         
        
      }
    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );

    this.setState({
     loading: false, 
    } as ITransferRentalState);
  }

  render() {
    
    return (
      <div>
        <Alert ref={x => this.alertRef = x} />
        <Loadable isLoading={this.state.loading} text={'Loading Form...'}>
          <Panel title="Transfer Rental Form">
            <div id="RentalForm" >
                <hr/>
                <h5>Origin Unit</h5>
                <Row>
                  <Column column="6">
                    <InputGroup 
                      label="Unit Code Origin" 
                      placeholder="Unit Code Origin" 
                      inputName="unitCodeOrigin"
                      onValueChange={(value) => {this.handleInputChange('unitCodeOrigin', value);}}
                      value={this.state.unitCodeOrigin}
                      disabled={true}/>

                  </Column>         
                </Row>
                <Row>
                  <Column column="6">
                  <InputGroup 
                    label="Complex Name Origin" 
                    placeholder="Complex Name Origin" 
                    inputName="complexNameOrigin"
                    onValueChange={(value) => {this.handleInputChange('complexNameOrigin', value);}}
                    value={this.state.complexNameOrigin}
                    disabled={true}/>
                </Column>
                <Column column="6">
                  <InputGroup 
                    label="Total Unit of Complex" 
                    placeholder="Total Unit of Complex" 
                    inputName="unitNumberOrigin"
                    type="number"
                    onValueChange={(value) => {this.handleInputChange('unitNumberOrigin', value);}}
                    value={this.state.unitNumberOrigin}
                    disabled={true}/>
                </Column>            
              </Row>
                <hr/>
                <h5>Destiny Unit</h5>
                <Row>
                  <Column column="6">
                    <Dropdown 
                      list={this.state.dataUnit}                  
                      label="Unit Code Destiny" 
                      inputName="unitCodeDestiny"
                      onValueChange={(value) => {this.handleInputUnitChange('unitCodeDestiny', value);}}
                      value={this.state.unitCodeDestiny}
                      error={this.state.errors.findIndex( (x) => x.unitCodeDestiny !== undefined) > -1}
                      errorMsg={utils.findElementValue(this.state.errors, 'unitCodeDestiny')}
                    />
                  </Column>         
                </Row>
                <Row>
                  <Column column="6">
                  <InputGroup 
                    label="Complex Name" 
                    placeholder="Complex Name" 
                    inputName="complexNameDestiny"
                    onValueChange={(value) => {this.handleInputChange('complexNameDestiny', value);}}
                    value={this.state.complexNameDestiny}
                    disabled={true}                    
                  />
                </Column>
                <Column column="6">
                  <InputGroup 
                    label="Total Unit of Complex" 
                    placeholder="Total Unit of Complex" 
                    inputName="unitNumberDestiny"
                    type="number"
                    onValueChange={(value) => {this.handleInputChange('unitNumberDestiny', value);}}
                    value={this.state.unitNumberDestiny}
                    disabled={true}/>
                </Column>            
              </Row>            
              
              <hr/>
              <h5>Customer</h5>
              <Row>
                <Column column="6">
                  <InputGroup 
                    label="Customer Name" 
                    placeholder="Customer Name" 
                    inputName="fullName"                  
                    onValueChange={(value) => {this.handleInputChange('fullName', value);}}
                    value={this.state.fullName}
                    disabled={true}/>
                </Column>        
              </Row> 
              <hr/>
              <Row>
                <Column column="6">
                  <InputGroup 
                    type="datepicker" 
                    label="Start Date" 
                    placeholder="Start Date" 
                    inputName="startDate"                  
                    onValueChange={(value) => {this.handleInputChange('startDate', value);}}
                    value={this.state.startDate}
                    error={this.state.errors.findIndex( (x) => x.startDate !== undefined) > -1}
                    errorMsg={utils.findElementValue(this.state.errors, 'startDate')}
                  />
                </Column>                                 

                <Column column="6">
                  <InputGroup 
                    label="Payment Terms" 
                    placeholder="Payment Terms" 
                    inputName="terms"                  
                    onValueChange={(value) => {this.handleInputChange('terms', value);}}
                    value={this.state.terms}
                    error={this.state.errors.findIndex( (x) => x.terms !== undefined) > -1}
                    errorMsg={utils.findElementValue(this.state.errors, 'terms')}
                  />
                </Column>  

              </Row>               

              <div style={{textAlign: 'right', background: '#f2f2f2', padding: '10px'}}>
                <a className="btn btn-success"  onClick={(e) => {this.handleFormSave();} }>Save</a>  
                <a className="btn btn-danger"  
                onClick={(e) => {this.props.onSetPageCode(pages.customerMaintenance, null);}} >Cancel</a>            
              </div>            
            </div>
          </Panel>
        </Loadable>
      </div>

    );
  }
}

function mapStateToProps(state: any): IConnectedProps {
  return {
    pageCode: state.app.pageCode,
    editData: state.app.editData,
  };
}

function mapDispatchToProps(dispatch): IConnectedDispatch {
  return {
    onSetPageCode: (value: string, settings) => dispatch(setPage(value, settings))
  };
}

export default 
  connect(mapStateToProps, mapDispatchToProps)(TransferRental) as React.ComponentClass<ITransferRentalProps>;
