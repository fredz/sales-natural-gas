import Customer from './Customer';
import Complex from './Complex';
import Unit from './Unit';
import Rental from './Rental';
import Readings from './Readings';
import Item from './Item';
import Company from './Company';
import City from './City';
import DebtCollection from './DebtCollection';
import AccessSms from './AccessSms';
import ScheduledSms from './ScheduledSms';
import TransferRental from './TransferRental';
import ReceiptHistory from './ReceiptHistory';

export default {
  Customer,
  Complex,
  Unit,
  Rental,
  Readings,
  Item,
  Company,
  City,  
  DebtCollection,
  AccessSms,
  ScheduledSms,
  TransferRental,
  ReceiptHistory
};