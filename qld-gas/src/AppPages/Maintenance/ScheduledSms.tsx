import * as React from 'react';
import { connect } from 'react-redux';
import { setPage } from '../../actions/app';
import { IPageSettings } from '../../state';

import * as moment from 'moment';
import { TableHeaderColumn } from 'react-bootstrap-table';
import Loadable from '../components/Loadable';
import Listing from '../components/Listing';
import { Row, Column } from '../../ascendis-ui/layout';
import { InputGroup } from '../../ascendis-ui/components';
import SimpleHeader from '../components/SimpleHeader';
import ScheduledSmsForm from './forms/ScheduledSmsForm';
import { PageMode, pages } from '../constants';
import Alert from '../components/Alert';

import * as utils from '../../App/utils';

export interface IScheduledSmsState {
  mode: PageMode;
  loading: boolean;
  data: Array<any>;
  total: number;
  editData: Array<any>;    
  backupList: Array<any>;
  filterComplexName: string;
}

export interface IScheduledSmsProps {

}

interface IConnectedProps {
  pageCode: string;
  editData: any;
}

interface IConnectedDispatch {
  onSetPageCode: (value: string, settings: IPageSettings) => void;
}

class ScheduledSms extends React.Component<IScheduledSmsProps & IConnectedProps & IConnectedDispatch, 
IScheduledSmsState> {
  
  alertRef: any; 

  constructor(props) {
    super(props);

    this.state = {
      mode: PageMode.list,
      loading: true,
      backupList: [],      
      filterComplexName: ''
    } as IScheduledSmsState;

    this.handleRightItemClick = this.handleRightItemClick.bind(this);
    this.getContentFromMode = this.getContentFromMode.bind(this);
    this.getDataAndDisplay = this.getDataAndDisplay.bind(this);
    this.onRangeUpdate = this.onRangeUpdate.bind(this);
    this.handleFormSave = this.handleFormSave.bind(this);
    this.handleFormCancel = this.handleFormCancel.bind(this);
    this.handleEditData = this.handleEditData.bind(this);        
    this.prepareStatusIcon = this.prepareStatusIcon.bind(this);  
    this.filterList = this.filterList.bind(this);      
  }

  componentDidMount() {
    this.getDataAndDisplay(this.state.mode);    
  }

  handleRightItemClick(mode: PageMode) {
    if (mode === PageMode.list) {
      this.setState({
        loading: true,
      } as IScheduledSmsState);

      this.getDataAndDisplay(mode);
    } else {
      this.setState({
        mode,
      } as IScheduledSmsState);
    }
  }

  getDataAndDisplay(mode) {
    let formData = new FormData();
    formData.append('Action', 'Fetch');
    formData.append('Path', 'ScheduledSms/'); 

    utils.fetch(formData).then(
      (response: any) => {
        const { data, total } = response;
        this.setState({
          mode,
          data,
          loading: false,
          total,
          backupList: data
        } as IScheduledSmsState);
        
        this.filterList('filterComplexName', this.state.filterComplexName);
      }
    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );
  }

  onRangeUpdate(from, to) {
    const { mode } = this.state;
    this.getDataAndDisplay(mode);

    this.setState({
      loading: true,
    } as IScheduledSmsState);
  }

  handleFormSave(data, isEdit, id = null) {
    let formData = new FormData();

    if ((data.notifyWeek === 'False' && data.notify1Day === 'False') || 
    data.notifyWeek === false && data.notify1Day === false || 
    utils.IsEmptyNull(data.complexNo) || utils.IsEmptyNull(data.deliveryDate)) {
      let msg = `At least one of them should be checked Notify Week or Notify 1 Day, 
      Complex and Delivery Date are required`;
      this.alertRef.displayAlert('error', msg , 'Error');
      return;
    }
    
    if (isEdit) {
      formData.append('Action', 'Update');
    } else {
      formData.append('Action', 'Insert');
    }
    formData.append('Path', 'ScheduledSms/');
    formData.append('ScheduledID', data.scheduledID);
    formData.append('ComplexNo', data.complexNo);
    formData.append('DeliveryDate', data.deliveryDate);
    formData.append('SMSAmount', data.smsAmount);
    formData.append('NotifyWeek', data.notifyWeek);
    formData.append('Notify1Day', data.notify1Day);
    formData.append('WeekSent', data.weekSent);
    formData.append('DaySent', data.daySent);
    formData.append('Complete', data.complete);

    utils.fetch(formData).then(
      (response: any) => {

        const { status } = response;        
        let msg;
        let toastrTitle;
        if (status === 'ok') {
          if (!isEdit) {
            msg = `${data.scheduledID} Added`;
            toastrTitle = 'New Scheduled SMS';
            this.alertRef.displayAlert('success', msg, toastrTitle);
          } else {

            msg = `${data.scheduledID} Updated`;
            toastrTitle = 'Scheduled SMS';
            this.alertRef.displayAlert('success', msg, toastrTitle);
          }
        } else {
          this.alertRef.displayAlert('error', `Error while Saving.\nError Message:\n${response.error}`, 'Error');
        }
        this.getDataAndDisplay(PageMode.list);
      }
    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );

    this.setState({
     loading: true, 
    } as IScheduledSmsState);
  }

  handleFormCancel() {
    this.setState({
      mode: PageMode.list,
    } as IScheduledSmsState);
  }

  handleEditData(list) {
    this.setState({
      editData: list,
      mode: PageMode.edit,
    } as IScheduledSmsState);
  }
  
  prepareStatusIcon(status) {
      let iconClass = 'none';

      if (status.length > 0 && status === 'True' ) {
          iconClass = 'fa fa-check';
      }

      return (              
        <div className="text-center" >
          <i className={iconClass}>&nbsp;</i> 
        </div>
      );
  }  

  filterList(prop, value) {
    let state = {};

    let filterByColumn = '';

    if (prop === 'filterComplexName') {
      filterByColumn = 'ComplexName';
    }

    let listFiltered = this.state.backupList.filter((Scheduled) => {      
      if (Scheduled[filterByColumn].toUpperCase().indexOf(value.toUpperCase()) >= 0 ) {
        return true;
      }else {
        return false;
      }
    });

    state[prop] = value;
    state['data'] = listFiltered;    
    this.setState(state as IScheduledSmsState);
  }

  getContentFromMode(mode: PageMode) {
    let content;
    switch (mode) {
      case PageMode.list: 
        let { data } = this.state;
        const columns = [
          {dataField: 'ScheduledID', header: 'Scheduled ID', isKey: true, hidden: true},
          {dataField: 'ComplexName', header: 'Complex Name', },          
          {dataField: 'DeliveryDate', header: 'Delivery Date', editable: false, isDate: true,
            dataFormat: (cell, row) => {
              return <span>{cell ? moment(cell).format('D MMM Y') : ''}</span>;
            }
          },          
          {dataField: 'SMSAmount', header: 'SMS Amount'},
          {dataField: 'NotifyWeek', header: 'Notify Week', dataFormat: (cell, row) => {
            const { NotifyWeek } = row;
            return this.prepareStatusIcon(NotifyWeek);
          }},
          {dataField: 'Notify1Day', header: 'Notify 1 Day', dataFormat: (cell, row) => {
            const { Notify1Day } = row;
            return this.prepareStatusIcon(Notify1Day);
          }},
          {dataField: 'WeekSent', header: 'Week Sent', editable: false, isDate: true,
            dataFormat: (cell, row) => {
              return <span>{cell ? moment(cell).format('D MMM Y') : ''}</span>;
            }
          },          
          {dataField: 'DaySent', header: 'Day Sent', editable: false, isDate: true,
            dataFormat: (cell, row) => {
              return <span>{cell ? moment(cell).format('D MMM Y') : ''}</span>;
            }
          },                                        
          {dataField: 'Complete', header: 'Complete', dataFormat: (cell, row) => {
            const { Complete } = row;
            return this.prepareStatusIcon(Complete);
          }},
          
        ];

        if (!data) {
          data = [];
        }

        const actions = (
          <TableHeaderColumn 
            dataField="action" 
            dataAlign="center"        
            headerAlign="center"            
            width="100px"
            dataFormat={(cell, row) => {              
              return (
                <span>
                  <a className="btn btn-xs btn-warning" title="Edit" 
                    onClick={(e) => {e.preventDefault(); this.handleEditData([row]);}}>
                    <i className="glyphicon glyphicon-edit"/>
                  </a>
                </span>
              );
          }}>
            Actions
          </TableHeaderColumn>
        );

        content = (
          <div>
            <Row>
              <Column column="3">
                <InputGroup 
                  label="Complex" 
                  placeholder="Complex"
                  type="text" 
                  inputName="filterComplexName"
                  onValueChange={(value) => {this.filterList('filterComplexName', value);}}
                  value={this.state.filterComplexName}/>                    
              </Column>
            </Row>                
            <Listing 
              data={data}
              columns={columns} 
              onRefreshData={this.onRangeUpdate}
              onEditData={this.handleEditData}
              onDeleteData={null}
              withActions={true}
              actions={actions}/>
          </div>
        );
        break;

      case PageMode.new: 
        content = <ScheduledSmsForm onSave={this.handleFormSave} onCancel={this.handleFormCancel}/>;
        break;

      case PageMode.edit:       
        content = <ScheduledSmsForm data={this.state.editData} 
          onSave={this.handleFormSave} onCancel={this.handleFormCancel}/>;
        break;

      default: 
        content = null;
    }

    return content;
  }

  render() {
    
    const brand = (
      <span>
        <i className="glyphicon glyphicon-list-alt"/> Scheduled SMS
      </span>
    );

    const content = this.getContentFromMode(this.state.mode);

    return (
      <div>
        <Alert ref={x => this.alertRef = x} />
        <Loadable isLoading={this.state.loading} text={'Fetching Data...'}>
          <SimpleHeader 
            brand={brand}
            listLabel="List"
            newLabel="New Scheduled SMS"
            onRightItemClick={this.handleRightItemClick}
            mode={this.state.mode}
          />
          {content}
        </Loadable>
      </div>
    );
  }
}

function mapStateToProps(state: any): IConnectedProps {
  return {
    pageCode: state.app.pageCode,
    editData: state.app.editData,
  };
}

function mapDispatchToProps(dispatch): IConnectedDispatch {
  return {
    onSetPageCode: (value: string, settings) => dispatch(setPage(value, settings))
  };
}

export default 
  connect(mapStateToProps, mapDispatchToProps)(ScheduledSms) as React.ComponentClass<IScheduledSmsProps>;
