export const pages = {
  dashboard: 'dashboard',
  meterReading: 'meter-reading',
  bottleGasDelivery: 'bottle-gas-delivery',
  taxInvoice: 'tax-invoice',
  customerMaintenance: 'customer-maintenance',
  complexMaintenance: 'complex-maintenance',
  unitcodesMaintenance: 'unitcodes-maintenance',
  itemMaintenance: 'item-maintenance',
  companyMaintenance: 'company-maintenance',
  cityMaintenance: 'city-maintenance',
  previewInvoices: 'preview-invoices',
  viewInvoices: 'invoices-maintenance',
  transferrentalMaintenance: 'transferrental-maintenance'
};
