export * from './pages';

export enum PageMode {
  list,
  new,
  edit,
  delete,
  customers_unit,
  unit_customers,
  automatic_credit_card,
  manual_credit_card,
  receipt_history_invoice
}

export const ExcludeTerms = [
  'street',
  'views',
  'place',
  'mr',
  'mrs',
  'miss',
  'ms',
];