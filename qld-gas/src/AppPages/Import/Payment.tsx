import * as React from 'react';
import * as moment from 'moment';
import * as Dropzone  from 'react-dropzone';
import { connect } from 'react-redux';
import { setPage } from '../../actions/app';
import { IPageSettings } from '../../state';
import { pages } from '../constants';

import Loadable from '../components/Loadable';
import Listing from '../components/Listing';
import Alert from '../components/Alert';
import SimpleHeader from '../components/SimpleHeader';

import * as utils from '../../App/utils';
import * as fileUtils from '../../App/fileUtils';

export interface IPaymentState {
  loading: boolean;
  data: Array<any>;
  files: Array<any>;
  headerList: Array<any>;
}

export interface IPaymentProps { }

interface IConnectedProps {
  pageCode: string;
  editData: any;
}

interface IConnectedDispatch {
  onSetPageCode: (value: string, settings: IPageSettings) => void;
}

class Payments extends React.Component<IPaymentProps & IConnectedProps & IConnectedDispatch, IPaymentState> {

  alertRef: any;
  dropzone: any;
  uid: any;

  constructor(props) {
    super(props);

    this.uid = null;

    this.state = {
      loading: false,
      files: []
    } as IPaymentState;

    this.onDrop = this.onDrop.bind(this);
    this.onOpenClick = this.onOpenClick.bind(this);
    this.cleanListing = this.cleanListing.bind(this);
    this.savePayment = this.savePayment.bind(this);
    
  }

  onDrop(acceptedFiles, rejectedFiles) {

    let  isListingEmpty = true;
    if (this.state.data !== undefined && this.state.data.length > 0) {
      isListingEmpty = confirm (`The current list data will be deleted. Do you want to continue?`);
    }

    if (acceptedFiles.length > 0 && isListingEmpty === true) {

      this.setState({
        files: acceptedFiles,
        loading: true
      } as IPaymentState);

      let self = this;
      this.uid = null;

      acceptedFiles.forEach((file) => {

        let fr = new FileReader();
        fr.onload = (e: any) => {

          let csv = e.target.result;
          self.processData(csv); 

          self.setState({
            loading: false
          } as IPaymentState);

        };
        fr.readAsText(file);

      });

    }
  }

  processData(csv) {
    
    let allTextLines = csv.split(/\r\n|\n/);    
    let lines = [];

    let headerTextLines = (allTextLines[0]).split(',');

    while (allTextLines.length) {            
      let rowSplit = allTextLines.shift().split(',');
      if(rowSplit.length > 1){
        lines.push(rowSplit);
      }
    }    

    this.prepareHeaderList(headerTextLines);
    this.prepareListingData(lines);
  }

  prepareHeaderList(list) {

    for (let i = 0; i < list.length; i++) {

      let column = (list[i]).replace(/"/g, '').trim();      
      column = column.replace(/ /g, '');

      list[i] = column;
    }

    this.setState({
      headerList: list
    } as IPaymentState);

  }

  prepareListingData(lines) {
    
    const filterCode = '12000';
    const filterItem = 'AccountNumber';

    lines.splice(0, 1); // remove first row

    let dataPayments = [];
    let rowdata = {};

    for (let i = 0; i < lines.length; i++) {

      rowdata = {};
      for (let j = 0; j < lines[i].length; j++) {

        let val = (lines[i][j]).replace(/"/g, '');
        val = val.replace('$', '');

        let headColumn = this.state.headerList[j];        
        
        if (headColumn !== '' && headColumn !== ',') {
          if (headColumn === 'Date') {
            // Set proper format for date
            let datearray = val.trim().split('/');
            rowdata[headColumn] = datearray[1] + '/' + datearray[0] + '/' + datearray[2];            
          }else {
            rowdata[headColumn] = val.trim();
          }
          
        }
        
      }

      dataPayments.push(rowdata);
    }

    // filter to Consider only those who have 12000
    let listPaymentsFiltered = dataPayments.filter((payment) => {      
      if (payment[filterItem].toUpperCase().indexOf(filterCode) >= 0 ) {
        return true;
      }else {
        return false;
      }
    });

    this.setState({
      data: listPaymentsFiltered
    } as IPaymentState);    

  }

  onOpenClick() {    
    this.dropzone.open();    
  }  

  cleanListing() {
    this.setState({
      data: []
    } as IPaymentState);      
  }

  savePayment() {

    this.setState({
      loading: true,
    } as IPaymentState);
    
    let formData = new FormData();
    formData.append('Action', 'Invoices');
    formData.append('Path', 'ImportPayment/');
    formData.append('FrmPaymentJSON', JSON.stringify(this.state.data));
    
    utils.fetch(formData).then(
      (response: any) => {

        this.setState({
          loading: false,
          data: []
        } as IPaymentState);

      }
    ).catch(
      (ex) => {
        this.alertRef.displayAlert('error', ex, 'Error');
      }
    );
  }

  getContentFromMode() {
    let content;

    const dropzone = (
      <div className="row">
          <div className="col-md-12 text-center">
            <Dropzone
              onDrop={this.onDrop}
              style={{width: '80%', margin: '0 auto', 
                      height: '78px', 
                      display: 'inline-block', 
                      borderColor: 'rgb(102, 102, 102)', 
                      borderStyle: 'dashed'}}              
              ref={(node) => { this.dropzone = node; }}
              className={'alert alert-info'}
              activeClassName={''}
              disableClick={false}
              accept={'.csv, .txt'}
              >
              <p> Drag & Drop your csv file here or click to select file to upload.</p>
            </Dropzone>
          </div>
        </div>
    );

    let { data, headerList } = this.state;

    if (!headerList) {
      return dropzone;
    }

    const editableColumns = [
      'Amt(inclGST)',
      'QtyDelivered'
    ];

    let columns = headerList.map( (x, i) => {
      if (x === '' || x === ',') {
        return;
      }

      return {
        dataField: x,
        header: x,
        editable: editableColumns.includes(x) ? true : false,
        thStyle: editableColumns.includes(x) ? {background: '#d9edf7'} : {},
      };
    });

    columns = columns.filter(x => x !== undefined);

    columns[0]['isKey'] = true;

    if (!data) {
      data = [];
    }

    content = (
      <div>
        <div className="alert alert-info" role="alert" style={{textAlign: 'center'}}>
          <strong><i className="glyphicon glyphicon-info-sign" /></strong> 
          &nbsp; Click on row cell to edit values, press enter to save.
        </div>
        
        { dropzone }

        {
          data !== [] ? (
            <Listing 
              data={data}
              columns={columns} 
              onRefreshData={null}
              onEditData={null}
              onDeleteData={null}
              withActions={false}
              cellEdit={{
                mode: 'click',
                blurToSave: true,
                //beforeSaveCell: this.handleBeforeSaveCell,
              }}
              withNumber={false}/>
          ) : <span style={{textAlign: 'center', fontWeight: 'bold'}}>Please choose file.</span>
        }

        <div className="row">
          <div className="col-md-6">
            <div style={{textAlign: 'left', paddingRight: '10px'}}>
              <a onClick={this.cleanListing} className="btn btn-danger" title="Clear .csv data" >Clear</a>
            </div>
          </div>
          <div className="col-md-6">
            <div style={{textAlign: 'right', paddingRight: '10px'}}>
              <a onClick={this.savePayment} className="btn btn-success"  title="Save data" >Save</a>
            </div>
          </div>
                  
        </div>
      </div>
    );    
    return content;
  } 

  render() {

    const brand = (
      <span>
        <i className="glyphicon glyphicon-list-alt"/> File Import        
      </span>
    );

    const content = this.getContentFromMode();

    return (
      <div>
        <Alert ref={x => this.alertRef = x} />
        <Loadable isLoading={this.state.loading} text={'Fetching Data...'}>
          <SimpleHeader 
            brand={brand}
            listLabel="List"
            newLabel="Payment .csv file"
            onRightItemClick={null}
            mode={null}
            withOptions={false}
          />
          {content}             
        </Loadable>
      </div>
    );
  }
}

function mapStateToProps(state: any): IConnectedProps {
  return {
    pageCode: state.app.pageCode,
    editData: state.app.editData,
  };
}

function mapDispatchToProps(dispatch): IConnectedDispatch {
  return {
    onSetPageCode: (value: string, settings) => dispatch(setPage(value, settings))
  };
}

export default 
  connect(mapStateToProps, mapDispatchToProps)(Payments) as React.ComponentClass<IPaymentProps>;

