import Readings from './Readings';
import Payment from './Payment';
import BPayreceipt from './BPayreceipt';

export default {
  Readings,  
  Payment,
  BPayreceipt
};