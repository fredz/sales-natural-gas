import * as searchTextTokenizer from 'search-text-tokenizer';
import * as _ from 'lodash';

export const tokenizeSearch = (keyColumn: string, text: string, columns: Array<string>, exclude: Array<string>) => {
  if (text.length === 0) {
    return `${keyColumn} LIKE '%%'`;
  }
  let terms: Array<{term: string; }> = searchTextTokenizer(text);
  terms = _.uniqBy(terms, x => x.term);

  let searchSQL = isNaN(parseInt(text)) ? '(' : `${keyColumn} = '${parseInt(text)}'`;

  for (let i = 0; i < terms.length; i++) {
    if (exclude.indexOf(terms[i].term.toLowerCase()) > -1) {
      if (terms.length > 1) {
        continue;
      }
    }

    searchSQL += searchSQL === '(' ?  '' : ' OR (';
    let subquery = '';
    for (let j = 0; j < columns.length; j++) {
      subquery += subquery !== '' ? ' OR ' : '';
      subquery += `${columns[j]} LIKE '%${terms[i].term}%'`;
    }
    searchSQL += `${subquery} )`;
  }
  
  return `(${searchSQL})`;
};