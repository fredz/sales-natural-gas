const SMSurl = 'http://sms.ascendis.com.au/api/sendsms/sendsmsmessage';

const postJson = (url: string, type: string = 'POST', data: any) => {
  const promise = new Promise<JSON>((resolve, reject) => {

    let xhr = new XMLHttpRequest();
    
    xhr.open(type, url, true);

    xhr.setRequestHeader('Content-type', 'application/json');

    xhr.onreadystatechange = () => { 
        if (xhr.readyState === 4 && xhr.status === 200) {
          try {
            resolve(JSON.parse(xhr.responseText));
          } catch (e) {
            reject(e);
          }
        } else {
          reject(xhr);
        }
    };

    xhr.send(JSON.stringify(data));
  });

  return promise;
};

const postSMS = (TextMessage: string, FromReseller: string, PhoneListNumbers: Array<string>) => {
  const data = {
    TextMessage,
    FromReseller,
    PhoneListNumbers,
    Validator: '4777F1D3A9F747BFAB5EA4FA56320542',
  };

  return postJson(SMSurl, 'POST', data);
};

export const sendSMS = (TextMessage: string, PhoneListNumbers: Array<string>) => {
  return postSMS(TextMessage, 'QLD Gas', PhoneListNumbers);
};
