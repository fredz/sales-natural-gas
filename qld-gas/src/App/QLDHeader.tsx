import * as React from 'react';
import Header from '../ascendis-ui/sbadmin/Header/Header';

class QLDHeader extends React.Component<any, any> {
  constructor(props) {
    super(props);
  }

  render() {
    const rightMenu = null;
    return (
      <Header OnHamburgerClick={() => alert('test')} 
        Branding={
          <img
            style={ {height: '60px', width: '240px'} } 
            src="http://qldgas.ascendis.com.au/Portals/0/QldGas2_03.png?ver=2016-12-01-035955-397"/>
        }
        RightMenu={rightMenu}/>
    );
  }
}

export default QLDHeader;
