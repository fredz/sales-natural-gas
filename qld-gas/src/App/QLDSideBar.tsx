import * as React from 'react';
import { connect } from 'react-redux';
import SideBar from '../ascendis-ui/sbadmin/SideBar/SideBar';
import * as classNames from 'classnames';
import QLDSideBarItem from './QLDSideBarItem';
import { setPage } from '../actions/app';

export interface IQLDSideBarProps {
  onItemClick: Function;
}

interface IConnectedProps {
  pageCode: string;
}

interface IConnectedDispatch {
  onSetPageCode: (value: string) => void;
}

interface IQLDSideBarState {
  maintenanceCollapsed: boolean;
  importCollapsed: boolean;
  exportCollapsed: boolean;
  smsCollapsed: boolean;
}

class QLDSideBar extends React.Component<IQLDSideBarProps & IConnectedProps & IConnectedDispatch, IQLDSideBarState> {
  constructor(props) {
    super(props);

    this.state = {
      maintenanceCollapsed: true,
      importCollapsed: true,
      exportCollapsed: true,
      smsCollapsed: true,
    };

    this.setActiveGroup = this.setActiveGroup.bind(this);
    this.handleItemClick = this.handleItemClick.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  handleItemClick(item: string) {
    this.props.onSetPageCode(item);

    this.props.onItemClick(item);
  }

  handleClick(item: string) {
    let { maintenanceCollapsed, importCollapsed, smsCollapsed, exportCollapsed } = this.state;

    if (item.indexOf('maintenance') > -1) {
      maintenanceCollapsed = !maintenanceCollapsed;
    }

    if (item.indexOf('import') > -1) {
      importCollapsed = !importCollapsed;
    }

    if (item.indexOf('export') > -1) {
      exportCollapsed = !exportCollapsed;
    }    

    if (item.indexOf('sms') > -1) {
      smsCollapsed = !smsCollapsed;
    }

    this.setState({
      maintenanceCollapsed,
      importCollapsed,
      exportCollapsed,
      smsCollapsed
    });
  }

  setActiveGroup(group) {
    let groups: IQLDSideBarState = {
      maintenanceCollapsed: true,
      importCollapsed: true,
      smsCollapsed: true,
      exportCollapsed: true
    };

    if (group.indexOf('maintenance') > -1) {
      groups.maintenanceCollapsed = false;
    }

    if (group.indexOf('import') > -1) {
      groups.importCollapsed = false;
    }

    if (group.indexOf('export') > -1) {
      groups.exportCollapsed = false;
    }    

    if (group.indexOf('sms') > -1) {
      groups.smsCollapsed = false;
    }

    return groups;
  }

  render() {
    const groups = this.setActiveGroup(this.props.pageCode);

    let maintenanceCollapsed = true;
    let importCollapsed = true;
    let exportCollapsed = true;
    let smsCollapsed = true;

    if (!this.state.maintenanceCollapsed) {
      maintenanceCollapsed = false;
    }

    if (!this.state.importCollapsed) {
      importCollapsed = false;
    }

    if (!this.state.exportCollapsed) {
      exportCollapsed = false;
    }    

    if (!this.state.smsCollapsed) {
      smsCollapsed = false;
    }

    if (maintenanceCollapsed) {
      maintenanceCollapsed = groups.maintenanceCollapsed;
    }

    if (importCollapsed) {
      importCollapsed = groups.importCollapsed;
    }

    if (exportCollapsed) {
      exportCollapsed = groups.exportCollapsed;
    }    

    if (smsCollapsed) {
      smsCollapsed = groups.smsCollapsed;
    }

    return (
      <SideBar>
        <ul className="nav in" id="side-menu">
          <QLDSideBarItem 
            label="Dashboard" 
            name="dashboard" 
            onItemClick={this.handleItemClick} 
            icon="fa fa-area-chart"/>

          <QLDSideBarItem 
            label="Meter Readings" 
            name="meter-reading" 
            onItemClick={this.handleItemClick} 
            icon="glyphicon glyphicon-dashboard"/>

          <QLDSideBarItem 
            label="Bottle Gas Deliveries" 
            name="bottle-gas-delivery" 
            onItemClick={this.handleItemClick} 
            icon="fa fa-free-code-camp"/>

          {/*<QLDSideBarItem 
            label="Tax Invoice" 
            name="tax-invoice" 
            onItemClick={this.handleItemClick} 
            icon="glyphicon glyphicon-list-alt"/> }*/}

          <QLDSideBarItem 
            label="Generate Invoices" 
            name="preview-invoices" 
            onItemClick={this.handleItemClick} 
            icon="fa fa-file-text-o"/>

          <QLDSideBarItem
            label="View Invoices" 
            name="view-invoices" 
            onItemClick={this.handleItemClick}
            icon="fa fa-eye" 
          />            

          <QLDSideBarItem
            label="Debt Collection" 
            name="debt-collection" 
            onItemClick={this.handleItemClick} 
            icon="fa fa-money"
          />

          <li className={classNames({ active: importCollapsed })}>
            <a onClick={() => this.handleClick('import')}>
              <i className="fa fa-arrow-up" /> Import
              {
                importCollapsed ? 
                <span className="glyphicon glyphicon-menu-down pull-right" /> :
                <span className="glyphicon glyphicon-menu-up pull-right" />
              } 
            </a>

            <ul
              className={classNames({
                'nav nav-second-level': true,
                collapse: importCollapsed,
              })}>
              <QLDSideBarItem
                label="Readings" 
                name="readings-import"
                icon="fa fa-upload" 
                onItemClick={this.handleItemClick} 
              />
              {/*<QLDSideBarItem
                label="Payments" 
                name="payment-import" 
                onItemClick={this.handleItemClick} 
                icon="fa fa-upload"
              />*/}
              <QLDSideBarItem
                label="Bank Receipts" 
                name="bpay-receipts-import" 
                onItemClick={this.handleItemClick} 
                icon="fa fa-upload"
              />
            </ul>
          </li>

          <li className={classNames({ active: exportCollapsed })}>
            <a onClick={() => this.handleClick('export')}>
              <i className="fa fa-arrow-down" /> Export
              {
                exportCollapsed ? 
                <span className="glyphicon glyphicon-menu-down pull-right" /> :
                <span className="glyphicon glyphicon-menu-up pull-right" />
              } 
            </a>

            <ul
              className={classNames({
                'nav nav-second-level': true,
                collapse: exportCollapsed,
              })}>
              <QLDSideBarItem
                label="MYOB" 
                name="export-import" 
                onItemClick={this.handleItemClick} 
                icon="fa fa-download"
              />
              <QLDSideBarItem
                label="Customer" 
                name="export-customer" 
                onItemClick={this.handleItemClick} 
                icon="fa fa-download"
              />
              <QLDSideBarItem
                label="Purchase" 
                name="export-purchase" 
                onItemClick={this.handleItemClick} 
                icon="fa fa-download"
              /> 
            </ul>
          </li>          

          <li className={classNames({ active: maintenanceCollapsed })}>
            <a onClick={() => this.handleClick('maintenance')}>
              <i className="fa fa-cogs" /> Maintenance
              {
                maintenanceCollapsed ? 
                <span className="glyphicon glyphicon-menu-down pull-right" /> :
                <span className="glyphicon glyphicon-menu-up pull-right" />
              } 
            </a>

            <ul
              className={classNames({
                'nav nav-second-level': true,
                collapse: maintenanceCollapsed,
              })}>
              <QLDSideBarItem
                label="Customer" 
                name="customer-maintenance" 
                onItemClick={this.handleItemClick} 
              />
              <QLDSideBarItem
                label="Complex" 
                name="complex-maintenance" 
                onItemClick={this.handleItemClick} 
              />
              <QLDSideBarItem
                label="Unit Codes" 
                name="unitcodes-maintenance" 
                onItemClick={this.handleItemClick} 
              />
              <QLDSideBarItem
                label="Rental Codes" 
                name="rentalcodes-maintenance" 
                onItemClick={this.handleItemClick} 
              />
              <QLDSideBarItem
                label="Readings" 
                name="readings-maintenance" 
                onItemClick={this.handleItemClick} 
              />  
              <QLDSideBarItem
                label="Invoice & Receipt History" 
                name="receipthistory-maintenance" 
                onItemClick={this.handleItemClick}
              />                                                                                        
              <QLDSideBarItem
                label="Tariffs"
                name="item-maintenance"
                onItemClick={this.handleItemClick}
                icon="fa fa-wrench"
              />
              <QLDSideBarItem
                label="City Codes" 
                name="city-maintenance" 
                onItemClick={this.handleItemClick} 
                icon="fa fa-wrench"
              />
              <QLDSideBarItem
                label="Company Information" 
                name="company-maintenance" 
                onItemClick={this.handleItemClick} 
                icon="fa fa-wrench"
              />
            </ul>
          </li>
          <li className={classNames({ active: smsCollapsed })}>
            <a onClick={() => this.handleClick('sms')}>
              <i className="fa fa-mobile" /> SMS
              {
                smsCollapsed ? 
                <span className="glyphicon glyphicon-menu-down pull-right" /> :
                <span className="glyphicon glyphicon-menu-up pull-right" />
              } 
            </a>

            <ul
              className={classNames({
                'nav nav-second-level': true,
                collapse: smsCollapsed,
              })}>
              <QLDSideBarItem
                label="Send Access SMS" 
                name="access-sms" 
                onItemClick={this.handleItemClick} 
              />
              <QLDSideBarItem
                label="Scheduled SMS" 
                name="scheduled-sms" 
                onItemClick={this.handleItemClick} 
              /> 
            </ul>
          </li>
        </ul>
      </SideBar>
    );
  }
}

function mapStateToProps(state: any): IConnectedProps {
  return {
    pageCode: state.app.pageCode,
  };
}

function mapDispatchToProps(dispatch): IConnectedDispatch {
  return {
    onSetPageCode: (value: string) => dispatch(setPage(value))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(QLDSideBar) as React.ComponentClass<IQLDSideBarProps>;
