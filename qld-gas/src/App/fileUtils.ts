import * as fetchUtils from '../Utils/fetchUtils';

// const url = window.location.toString();
let url = 'http://qldgas.ascendis.com.au/intranet/FileServer';

if (window.location.origin.indexOf('localhost') > 0) {
  url = `http://qldgas.ascendis.com.au/intranet/FileServer`;
} else {
  url = `${window.location.origin}/FileServer`;
}

export const fetch = (formData: FormData, method: string = 'POST') => {
  return fetchUtils.fetchJson(url, method, formData);
};

export const fetchReading = (ComplexNo: string) => {
  let params = `Action=PDFOutput/`;
  params += `&Path=MeterSheet`;
  params += `&ComplexNo=${ComplexNo}`;

  window.location.assign(`${url}?${params}`);
};

export const printInvoice = (InvoiceNo: string) => {
  let params = `Action=PDFOutput/`;
  params += `&Path=Invoice`;
  params += `&InvoiceNo=${InvoiceNo}`;

  window.location.assign(`${url}?${params}`);
};

export const printInvoiceAll = (Invoices: string) => {
  let params = `Action=PDFOutput/`;
  params += `&Path=InvoiceAll`;
  params += `&Invoices=${Invoices}`;

  window.location.assign(`${url}?${params}`);
};

export const printLetter = (InvoiceNo: string, Type: string) => {
  let params = `Action=PDFOutput/`;
  params += `&Path=Letter`;
  params += `&InvoiceNo=${InvoiceNo}`;
  params += `&Type=${Type}`;

  window.location.assign(`${url}?${params}`);
};

export const exportBadImportReadings = (ImportID: string) => {
  let params = `Action=ImportedBadReadings/`;
  params += `&ImportID=${ImportID}`;

  window.location.assign(`${url}?${params}`);
};

export const exportMyob = (StartingDate: string, EndingDate: string) => {
  let params = `Action=ExportMyob/`;
  params += `&StartingDate=${StartingDate}`;
  params += `&EndingDate=${EndingDate}`;

  window.location.assign(`${url}?${params}`);
};

export const exportMyobPurchase = (StartingDate: string, EndingDate: string) => {
  let params = `Action=ExportMyobPurchase/`;
  params += `&StartingDate=${StartingDate}`;
  params += `&EndingDate=${EndingDate}`;

  window.location.assign(`${url}?${params}`);
};

export const exportMyobCustomer = (StartingDate: string, EndingDate: string) => {
  let params = `Action=ExportMyobCustomer/`;
  params += `&StartingDate=${StartingDate}`;
  params += `&EndingDate=${EndingDate}`;

  window.location.assign(`${url}?${params}`);
};

export const printLettersAll = (InvoiceNos: string, Types: string) => {
  let params = `Action=PrintLetterAll/`;
  params += `&InvoiceNos=${InvoiceNos}`;
  params += `&Types=${Types}`;  

  window.location.assign(`${url}?${params}`);
};

export const exportCreditCard = () => {
  let params = `Action=ExportCreditCard/`;
  window.location.assign(`${url}?${params}`);
};