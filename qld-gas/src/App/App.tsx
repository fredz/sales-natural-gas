import * as React from 'react';
import { connect } from 'react-redux';
import { setPage } from '../actions/app';

import QLDHeader from './QLDHeader';
import QLDSideBar from './QLDSideBar';
import Page from '../AppPages/Page';

export interface IAppProps {

}

interface IConnectedProps {
  pageCode: string;
}

interface IConnectedDispatch {
  onSetPageCode: (value: string) => void;
}

class App extends React.Component<IAppProps & IConnectedProps & IConnectedDispatch, any> {
  constructor(props) {
    super(props);

    this.state = {
      pageChild: (
        <div>
          Welcome to QLD Gas Intranet System.
        </div>
      ),
    };

    this.handleSideBarClick = this.handleSideBarClick.bind(this);
  }

  componentWillReceiveProps(nextProps: IAppProps & IConnectedProps) {
    if (this.props.pageCode !== nextProps.pageCode) {
      this.setState({
        activePage: nextProps.pageCode,
      });
    }
  }

  handleSideBarClick(item: string) {
    this.setState({
      activePage: item,
    });
  }

  render() {
    const { activePage } = this.state;

    return (
      <div style={{margin:'-10px'}}>
        <QLDHeader style={{background: '#ddd',marginBottom: '-10px;'}}/>
        <div>
          <QLDSideBar onItemClick={this.handleSideBarClick}/>
          <Page pageCode={activePage} onItemClick={this.handleSideBarClick}/>
        </div>
      </div>
    );
  }
}


function mapStateToProps(state: any): IConnectedProps {
  return {
    pageCode: state.app.pageCode,
  };
}

function mapDispatchToProps(dispatch): IConnectedDispatch {
  return {
    onSetPageCode: (value: string) => dispatch(setPage(value))
  };
}

export default 
  connect(mapStateToProps, mapDispatchToProps)(App) as React.ComponentClass<IAppProps>;
