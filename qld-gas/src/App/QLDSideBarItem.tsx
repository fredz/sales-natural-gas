import * as React from 'react';
import { connect } from 'react-redux';
import { setPage } from '../actions/app';

export interface IQLDSideBarItemProps {
  onItemClick: Function;
  label: string;
  name: string;
  icon?: string;
};

interface IConnectedProps {
  pageCode: string;
}

interface IConnectedDispatch {
  onSetPageCode: (value: string) => void;
}

class QLDSideBarItem extends 
  React.Component<IQLDSideBarItemProps & IConnectedProps & IConnectedDispatch, any> {
  constructor(props) {
    super(props);
  }

  render() {
    const {label, name, onItemClick, pageCode} = this.props;

    const withIcon = this.props.icon ?
      <i className={this.props.icon} /> :
      null;

    let active = name === pageCode ? {
      style: {
        background: 'lightgrey',
      }
    } : null;

    return (
      <li>
        <a
          {...active}
          href=""
          onClick={ (e) => { e.preventDefault();  onItemClick(name); }}
        >
          {withIcon} {label}
        </a>
      </li>
    );
  }
}

function mapStateToProps(state: any): IConnectedProps {
  return {
    pageCode: state.app.pageCode,
  };
}

function mapDispatchToProps(dispatch): IConnectedDispatch {
  return {
    onSetPageCode: (value: string) => dispatch(setPage(value))
  };
}

export default 
  connect(mapStateToProps, mapDispatchToProps)(QLDSideBarItem) as React.ComponentClass<IQLDSideBarItemProps>;
