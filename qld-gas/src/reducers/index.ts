import { combineReducers } from 'redux';
import { handleActions, Action } from 'redux-actions';
import { IAppState, IPageSettings } from '../state';
import { pages } from '../AppPages/constants';

import { PAGE_LOAD } from '../actions/app';

const DEFAULT_STATE: IAppState = {
  pageCode: pages.dashboard,
  editData: null,
};

const app = handleActions<IAppState, any>({
  [PAGE_LOAD]: (state: IAppState, action: Action<PAGE_LOAD>): IAppState => {
    let settings = action.payload.settings;

    return Object.assign({}, state, {
      pageCode: action.payload.pageCode,
      editData: settings !== null ? action.payload.settings.editData : null,
    }) as IAppState;
  }
}, DEFAULT_STATE);

export default combineReducers<IAppState>({
  app,
});