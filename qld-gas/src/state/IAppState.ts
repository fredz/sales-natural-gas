export interface IPageSettings {
  subPage?: string;
  editData?: any;
}

export interface IAppState {
  /**
   * The current page of project
   */
  pageCode: string;

  editData: any;
}