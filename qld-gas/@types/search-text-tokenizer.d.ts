declare var searchTextTokenizer: (searchText: any) => Array<{
    term: string;
    phrase?: boolean;
    tag?: string;
    exclude?: boolean;
  }>;

// declare module 'search-text-tokenizer' {
//   export default const tokenizer: (searchText: any) => Array<{
//     term: string;
//     phrase?: boolean;
//     tag?: string;
//     exclude?: boolean;
//   }>
// }