/// <reference types="react" />
/// <reference types="node" />
/// <reference types="react-bootstrap-table" />

import { ComponentClass, Props, ReactElement } from 'react';
import { TableHeaderColumn } from 'react-bootstrap-table';

export interface TableHeaderColumnProps extends Props<TableHeaderColumn> {
    thStyle?: any;
    tdStyle?: any;
}
