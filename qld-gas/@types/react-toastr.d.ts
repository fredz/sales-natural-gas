declare var reactToastr: any;
declare module 'react-toastr' {
  import * as React from 'react';
  export const ToastMessage: any;
  export class ToastContainer extends React.Component<any, any> {
    success: any;
    info: any;
    warning: any;
    error: any;
  }
}