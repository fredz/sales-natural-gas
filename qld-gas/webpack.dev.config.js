const webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: {
    'index': './src/index.tsx'
  },

  output: {
    filename: "./src/public/qldgas-bundle.js"
  },

  plugins: [
    new HtmlWebpackPlugin({
      filename: 'index.html',
      chunks: 'tt-comp',
      template: 'index.ejs'
    }),

    new webpack.HotModuleReplacementPlugin({ multiStep: true }),
  ],

  resolve: {
    extensions: ["", ".webpack.js", ".web.js", ".ts", ".tsx", ".js"]
  },

  module: {
    loaders: [
      { test: /\.tsx?$/, loader: "ts-loader" },
      { test: /\.css$/, loader: "style-loader!css-loader" },
    ]
  },

  devServer: {
    contentBase: 'src/public/',
    noInfo: true,
    historyApiFallback: true
  }
};