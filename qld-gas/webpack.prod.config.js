var HtmlWebpackPlugin = require('html-webpack-plugin');
var JavaScriptObfuscator = require('webpack-obfuscator');

module.exports = {
  plugins: [
    new JavaScriptObfuscator ({
      rotateUnicodeArray: true,
      compact: true,
    }, [])
  ],

  devtool: 'cheap-module-source-map',

  entry: {
    'index': './src/index.tsx'
  },
  output: {
    filename: "./src/public/qldgas-bundle.js"
  },
  resolve: {
    extensions: ["", ".webpack.js", ".web.js", ".ts", ".tsx", ".js"]
  },

  module: {
    loaders: [
      { test: /\.tsx?$/, loader: "ts-loader" },
      { test: /\.css$/, loader: "style-loader!css-loader" },
    ]
  },
};