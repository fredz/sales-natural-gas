# QLD Gas

# NPM Scripts

`test:unit` - Runs unit tests for application
> Needs to enhance coverage

`dev:watch` - generate file in dev mode and watch for automatic update of bundle, *For production please uso `prod:build`
`prod:watch` - generate file in dev mode and watch for automatic update of bundle, *For production please uso `prod:build`
`dev:build` - Once compile in dev mode
`prod:build` - Once compile in prod mode
`lint` - review linting errors, useful before commiting