-- =============================================
-- Author:		Ascendis Business Solutions
-- Create date: 02/15/2017
-- Description:	Restore Unit for next processing * User after generating invoices*
-- =============================================
CREATE PROCEDURE QLD.RestoreUnit 
	@UnitCode INT, 
	@isFinal TINYINT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE QLD.tblUnit
		SET
		LastReadingDate = CurReadingDate, 
		LastReading = CurReading,  
      
		CurReadingDate = TempReadingDate, 
		CurReading = TempReading
    WHERE UnitCode = @UnitCode; 
	
	IF (@isFinal = 1) BEGIN
		UPDATE QLD.tblUnit SET
			FinalReadingDate = TempReadingDate,
			FinalReading = 1,
			TempReadingDate = NULL, 
			TempReading = NULL 
		WHERE UnitCode = @UnitCode;
	END ELSE BEGIN
		UPDATE QLD.tblUnit SET
			FinalReadingDate = NULL,
			FinalReading = 0,
			TempReadingDate = NULL, 
			TempReading = NULL 
		WHERE UnitCode = @UnitCode;
    END
  
END
GO
