-- =============================================
-- Author:		Ascendis Business Solutions
-- Create date: 2/15/2017
-- Description:	Returns Price and information from a temporary reading in unit
-- =============================================
CREATE PROCEDURE QLD.GetPriceLeveFromTempReading 
	@UnitCode INT,
	@TariffCode NVARCHAR(20) OUTPUT,
	@TempReading FLOAT OUTPUT,
	@Price FLOAT OUTPUT,
	@Level INT OUTPUT,
	@TaxRate FLOAT OUTPUT,
	@TaxType SMALLINT OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT @TariffCode = ItemCode, @TempReading = TempReading
	FROM QLD.tblUnit
	WHERE UnitCode = @UnitCode 


	SELECT @Price =  
		  CASE
			WHEN @TempReading BETWEEN [PriceBreakFrom1] AND [PriceBreakTo1] THEN [PriceBreakL1Price]
			WHEN @TempReading BETWEEN [PriceBreakFrom2] AND [PriceBreakTo2] THEN [PriceBreakL2Price]
			WHEN @TempReading BETWEEN [PriceBreakFrom3] AND [PriceBreakTo3] THEN [PriceBreakL3Price] 
			ELSE -1 
		  END,
		  @Level =  
		  CASE
			WHEN @TempReading BETWEEN [PriceBreakFrom1] AND [PriceBreakTo1] THEN 1
			WHEN @TempReading BETWEEN [PriceBreakFrom2] AND [PriceBreakTo2] THEN 2
			WHEN @TempReading BETWEEN [PriceBreakFrom3] AND [PriceBreakTo3] THEN 3 
			ELSE -1 
		  END,
		  @TaxRate = TaxRate,
		  @TaxType = t.TaxType
	FROM [QLD].[tblItem] i, [QLD].[tblTaxType] t
	WHERE i.TaxType = t.TaxType
	AND ItemCode = @TariffCode

	-- SELECT @Price AS Price, @TariffCode AS TariffCode, @TempReading as TempReading, @Level as Level, @TaxRate AS TaxRate, @TaxType AS TaxType
END
GO
