-- =============================================
-- Author:		Ascendis Business Solutions
-- Create date: 2/15/2017
-- Description:	Update temporary reading in unit
-- =============================================
ALTER PROCEDURE [QLD].[UpdateUnitTempReading](@unitID int, @newReadingDate datetime2, @newReadingValue float, @isFinal bit)
AS
BEGIN
    
  If (@isFinal = 1)
  Begin
  
      UPDATE
        QLD.tblUnit
      SET        
        TempReadingDate = @newReadingDate, 
        TempReading = @newReadingValue, 
        -- FinalReadingDate = @newReadingDate, 
        FinalReading = 1
      WHERE
        UnitCode = @unitID;   

  End  
  Else          
    Begin
    
      UPDATE
        QLD.tblUnit
      SET
        --LastReadingDate = CurReadingDate, 
        --LastReading = CurReading,  
      
        --CurReadingDate = TempReadingDate, 
        --CurReading = TempReading, 
        
        TempReadingDate = @newReadingDate, 
        TempReading = @newReadingValue, 
        FinalReading = 0

      WHERE
        UnitCode = @unitID;    

    End
  
  
END
