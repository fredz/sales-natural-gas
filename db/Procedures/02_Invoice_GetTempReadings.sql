-- =============================================
-- Author:		AscendisBusinessSolutions
-- Create date: 02/15/2017
-- Description:	Fetch temp reading units for processing
-- =============================================
CREATE PROCEDURE GetTempReadingsFromDate 
	@FromDate DATETIME2
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	FROM [QLD].[tblUnit]
	WHERE TempReading IS NOT NULL
	AND TempReadingDate >= @FromDate
END
GO


-- =============================================
-- Author:		AscendisBusinessSolutions
-- Create date: 02/15/2017
-- Description:	Fetch temp reading units for processing
-- =============================================
CREATE PROCEDURE GetTempReadingsForComplex 
	@ComplexNo INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	FROM [QLD].[tblUnit]
	WHERE TempReading IS NOT NULL
	AND ComplexNo = @ComplexNo
END
GO

-- =============================================
-- Author:		AscendisBusinessSolutions
-- Create date: 02/15/2017
-- Description:	Fetch temp reading units for processing
-- =============================================
CREATE PROCEDURE GetTempReadingsForCustomer 
	@ComplexNo INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	FROM [QLD].[tblUnit]
	WHERE TempReading IS NOT NULL
	AND ComplexNo = @ComplexNo
END
GO

