USE [QLDGas]
GO

INSERT INTO [QLD].[tblGLChart]
     VALUES('40101','Metered Sales',21,0)
GO

INSERT INTO [QLD].[tblGLChart]
     VALUES('40111','Residential Gas Refill Sales',4,0)
GO

INSERT INTO [QLD].[tblGLChart]
     VALUES('40121','Commercial Gas Refill Sales',4,0)
GO

INSERT INTO [QLD].[tblGLChart]
     VALUES('40131','Bottle Exchange Sales',4,0)
GO

INSERT INTO [QLD].[tblGLChart]
     VALUES('44400',' Late Payment Fees',4,0)
GO


