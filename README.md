# Ascendis QLD System

## Server

_Webconfig_

include under static content on system.webserve
```xml
<mimeMap fileExtension=".woff" mimeType="application/font-woff" />
<mimeMap fileExtension=".woff2" mimeType="font/woff2" />
```

## Setup

> it is recommended to setup a base `_git/` folder locally on your computer as all project would be under a _git folder, for convenience and path standards in future

### Console

use `git clone https://ascendis-dev.visualstudio.com/_git/QLD`


> if needed, add remote referende by `git remote add origin https://ascendis-dev.visualstudio.com/_git/QLD`

Commit and push code

`git push -u origin master`

> When needed we would work on feature branches but regurally as of size we will commit directly to master

### IDE Support

As per team services, you are able to clone on Visual Studio among others

## Structure

### qld-gas

React application for QLD intranet system, this is meant to deploy the front-end part of QLD Intranet System

> Please keep in mind that up to this point this application relies on [Open Web Studio (OWS)](www.openwebstudio.com) for DNN page and a seperate OWS configuration under the `ows/` folder

>> Consider that up to this point, the application contains an `ascendis-ui/` folder that is meant to become a separate application later 


> Scripts

>```
`test:unit` - Runs unit tests for application, needs to enhance coverage
```

>```
`dev:watch` - generate file in dev mode and watch for automatic update of bundle, *For production please use prod:build
```

>```
`prod:watch` - generate file in dev mode and watch for automatic update of bundle, *For production please use `prod:build`
```

>```
`dev:build` - Once compile in dev mode
```

>```
`prod:build` - Once compile in prod mode
```

>```
`lint` - review linting errors, useful befor commiting
```

### ows-intranet

Open Web Studio's configuration needed for QLD Intranet System

### qld-skin

QLD Gas websute Bootstrap 3 skin for DNN, used for the front side 

### _etc

QLD Gas files like Logo design and others